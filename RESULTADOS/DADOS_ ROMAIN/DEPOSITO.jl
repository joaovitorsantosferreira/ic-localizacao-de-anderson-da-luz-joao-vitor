
# Dados_C1 = load("RESULTADOS/DADOS ANTIGOS/C1/DATA={2021-July-31}_Dados_Antigos_RELATORIO_ESCALAR_C1.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C1/DATA={2021-July-31}_Dados_Antigos_RELATORIO_ESCALAR_C1.jld2", "Parametros")

# Γ₀= Parametros_Antigos.Γ₀
# ωₐ= Parametros_Antigos.ωₐ
# Ω= Parametros_Antigos.Ω
# k= Parametros_Antigos.k
# N= Parametros_Antigos.N
# Radius= Parametros_Antigos.Radius
# ρ= Parametros_Antigos.ρ
# rₘᵢₙ= Parametros_Antigos.rₘᵢₙ
# Angulo_da_luz_incidente= Parametros_Antigos.Angulo_da_luz_incidente
# vetor_de_onda= Parametros_Antigos.vetor_de_onda
# ω₀= Parametros_Antigos.ω₀
# λ= Parametros_Antigos.λ
# ωₗ= Parametros_Antigos.ωₗ
# Δ= Parametros_Antigos.Δ
# Tipo_de_kernel= Parametros_Antigos.Tipo_de_kernel
# Tipo_de_Onda= Parametros_Antigos.Tipo_de_Onda
# N_Sensores= Parametros_Antigos.N_Sensores
# Distancia_dos_Sensores= Parametros_Antigos.Distancia_dos_Sensores
# Angulo_de_variação_da_tela_circular_1= Parametros_Antigos.Angulo_de_variação_da_tela_circular_1
# Angulo_de_variação_da_tela_circular_2= Parametros_Antigos.Angulo_de_variação_da_tela_circular_2
# Tipo_de_beta= Parametros_Antigos.Tipo_de_beta
# Desordem_Diagonal= Parametros_Antigos.Desordem_Diagonal
# W= Parametros_Antigos.W
# GEOMETRIA_DA_NUVEM= Parametros_Antigos.GEOMETRIA_DA_NUVEM
# L= Parametros_Antigos.L
# Lₐ= Parametros_Antigos.Lₐ
# PERFIL_DA_INTENSIDADE= Parametros_Antigos.PERFIL_DA_INTENSIDADE
# N_Telas = Parametros_Antigos.N_Telas
# g = get_number_thouless(γₙ ,ωₙ ,Δ, Γ₀)                                                                        # Número de Thouless definido para a banda de valores selecionada pelo Deturn: [Δ - Γ₀/2,Δ + Γ₀/2]


# save("ESPECTRO_VETORIAL.jld2", Dict("Dados" => [ωₙ,γₙ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,b₀] ))


# include("PLOTS/Plots.jl")
# Dados_ESCALAR = load("ESPECTRO_ESCALAR.jld2", "Dados")

# ωₙ= Dados_ESCALAR[1]
# γₙ=Dados_ESCALAR[2]
# IPRs=Dados_ESCALAR[3]
# Δ=Dados_ESCALAR[4]
# Γ₀=Dados_ESCALAR[5]
# g=Dados_ESCALAR[6]
# Tipo_de_kernel=Dados_ESCALAR[7]
# Desordem_Diagonal=Dados_ESCALAR[8]
# W=Dados_ESCALAR[9]
# b₀=Dados_ESCALAR[10]

# p1 = vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,b₀,800)


# Dados_VETORIAL = load("ESPECTRO_VETORIAL.jld2", "Dados")

# ωₙ= Dados_VETORIAL[1]
# γₙ=Dados_VETORIAL[2]
# IPRs=Dados_VETORIAL[3]
# Δ=Dados_VETORIAL[4]
# Γ₀=Dados_VETORIAL[5]
# g=Dados_VETORIAL[6]
# Tipo_de_kernel=Dados_VETORIAL[7]
# Desordem_Diagonal=Dados_VETORIAL[8]
# W=Dados_VETORIAL[9]
# b₀=Dados_VETORIAL[10]

# p2 = vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,b₀,800)


# plot(p1, p2, layout = @layout([a ; c]),size = (1000,1250))
# savefig("relatorio_cientifico_ESPETRO_DE_AUTOVALORES_1.png")

# include("PLOTS/Plots.jl")
# vizualizar_relação_entre_gamma_e_comprimento_de_localização_R_cm(ξₙ,γₙ,R_cm_n,Radius,Lₐ,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel,700)
# savefig("relatorio_cientifico_CORRELACAO_XI_E_GAMMA_1.png")



# tamanho = 1000
# Campo_Real = zeros(N)

# for i in 1:N

#     Campo_Real[i] = get_eletric_field_LG_modulo_quadrado(r[i,:],Ω,ω₀/2,k,2,Geometria)

# end

# r_cartesian = r

# x_a = r_cartesian[:,1]
# y_b = r_cartesian[:,2]
# z_c = r_cartesian[:,3]
# t_d = log10.(Campo_Real)

# Limite = L

# scatter(x_a,y_b,z_c,
# size = (tamanho+150, tamanho),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 5Plots.mm,
# gridalpha = 0.3,
# ylims = (-(1.2)*Limite,(1.2)*Limite),
# xlims = (-(1.2)*Limite,(1.2)*Limite),
# zlims = (-(1.2)*Limite,(1.2)*Limite),
# ms = 6,
# zcolor = t_d,
# framestyle = :box,
# label = "Laser Gaussiano",
# color = cgrad(categorical = true),
# aspect_ratio =:equal,
# camera=(45,45),
# # colorbar_title = L"$log_{10}(p_i)$",
# # title = L"\textrm{Modo com maior IPR}",
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 30,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black
# )

# xlabel!(L"\textrm{Posição X}")
# ylabel!(L"\textrm{Posição Y}")


# Variaveis_de_entrada = Entrada 
# r = cloud.r

# function get_dados_histograma_LINEAR(Intensidade_Normalizada,N_div)

#     sep = maximum(Intensidade_Normalizada)/N_div
#     bins = 0:sep:(maximum(Intensidade_Normalizada))
#     h = fit(Histogram, Intensidade_Normalizada,bins) 

#     return h
# end

# function get_dados_histograma_LOG(Intensidade_Normalizada,N_div)
#     a = minimum(Intensidade_Normalizada)
#     b = maximum(Intensidade_Normalizada)

#     bins = get_points_in_log_scale(a,b,N_div)
#     h = fit(Histogram, Intensidade_Normalizada[ : ],bins)

#     return h
# end

# function densidade_efetiva(r,Radius,Geometria::ThreeD)

#     N = size(r,1)
#     d_radial = zeros(N)

#     for i in 1:N
#         d_radial[i] = norm(r[i, : ])
#     end

#     index_modes_inside_range = findall( (d_radial .≤ Radius))
#     N_efetivo = size(index_modes_inside_range,1)

#     densidade_efetiva = N_efetivo/((4/3)*π*(Radius^3))

#     return N_efetivo
# end

# function densidade_efetiva(r,Radius,Geometria::TwoD)

#     N = size(r,1)
#     d_radial = zeros(N)

#     for i in 1:N
#         d_radial[i] = norm(r[i, : ])
#     end

#     index_modes_inside_range = findall( (d_radial .≤ Radius))
#     N_efetivo = size(index_modes_inside_range,1)

#     densidade_efetiva = N_efetivo/(π*(Radius^2))

#     return N_efetivo
# end

# N_pontos = 100
# Δr =  range(2,Radius,length = N_pontos)
# ρs = zeros(N_pontos)

# for i in 1:N_pontos

#     ρs[i] = densidade_efetiva(r,Δr[i],Geometria)

# end


# tamanho = 1000


# raios = zeros(N)

# for i in 1:N

#     raios[i] = norm(r[i, : ])

# end 

# N_div = 40
# # h = get_dados_histograma_LOG(raios,N_div)
# h = get_dados_histograma_LINEAR(raios,N_div)


# x_a = collect(h.edges[1])
# y_b = h.weights




# # plot(x_a[2:end],y_b,
# plot(x_a[2:end],y_b,
# size = (tamanho, (tamanho)/2),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# # xscale = :log10,
# # yscale = :log10,
# # ylims = (10^0,10^4),
# # xlims = (10^-0.1,10^2),
# minorgridstyle = :dashdot,
# lw = 4,
# c = :green,
# lab = L"\textrm{Disco}" ,
# title = "ρ = $ρ , Histograma 2D",
# framestyle = :box,
# legendfontsize = 16,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black

# )


# xlabel!(L"$ Radius $")
# ylabel!(L"$ N $")


# law_n_DISCO(x) = x
# y = law_n_DISCO.(Δr)
# plot!(Δr,y,
# lw = 4,
# linestyle=:dashdot,
# c = :black,
# # lab = "$a_sol_plot x + $b_sol_plot"
# lab = L"$x$"
# )
# law_n_DISCO_2(x) = x^2/10
# y = law_n_DISCO_2.(Δr)
# plot!(Δr,y,
# lw = 4,
# linestyle=:dashdot,
# c = :blue,
# # lab = "$a_sol_plot x + $b_sol_plot"
# lab = L"$x^2/10$"
# )

# savefig("fig20__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_29.png")


# 558
# 4030
# 1018
# 1136 - MUITO BONITO
# 2959
# 4056
# 9799 - Comportamento na borda é curioso
# 917 - Lindo 
# 1147 - Bem centrado e decaimento exponecial bem caracteristico 
# 9891 - Novamente comportamento de borda bem estranho
# 1662 - Interressante o fitting produzido 
# 1225 - Decaimento expoencial e posição do modo sãok bem caracterizados, interressane
# 9852 - Novamente comportamento bizzaro nas bordas, conversar com o Romain sobre essas "franjas" nas bordas
# 972 - Decaimento expoencial bem definido, o modo claramente apresenta propriedades de localização de anderson
# 1193
# 739 - Belo decaimento expoencial 
# 9804
# 1261 - Decaimento hiperuniforme, o modo é bem distribuido em toda nuvem, MUITO INTERRESSANTE
# 2700
# 1305
# 769
# 1580
# 2174
# 1066
# 3171 - O modo meio que chega na superficie da nuvem e produz uma distorção Bizzara 
# 799 - Hiperlocalizado, perfil 3D ficou muito bonito
