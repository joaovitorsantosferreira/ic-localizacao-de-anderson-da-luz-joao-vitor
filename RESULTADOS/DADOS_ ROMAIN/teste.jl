using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("PLOTS/Plots.jl")
include("PLOTS/Animation.jl") 
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[1]---Estatistiscas__Da__Intensidade/R1.jl") 
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-October-20}_Dados_Antigos_C2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-October-20}_Dados_Antigos_C2.jld2", "Parametros")
g_c = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-October-20}_Dados_Antigos_C2.jld2", "g_c")
modelo_fit = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-October-20}_Dados_Antigos_C2.jld2", "fitting")

Intensidade_Resultante_PURA = Dados_Antigos.Intensidade_Resultante_PURA                                     # Intensidade pura, ou seja, aquela que é detectada nos sensores sem nenhuma normalização, valores pequenos 
Intensidade_Resultante_NORMALIZADA = Dados_Antigos.Intensidade_Resultante_NORMALIZADA                       # Intensidade Normalizada, ou seja, são os dados da Intensidade Pura Normalizados
Histograma_LINEAR = Dados_Antigos.Histograma_LINEAR                                                         # Histograma com separação Linear entre as faixas de analise(Colunas)
Histograma_LOG = Dados_Antigos.Histograma_LOG                                                               # Histograma com separação Logaritmica entre as faixas de analise(Colunas)
variancia = Dados_Antigos.variancia                                                                         # Variancia da Intensidade Normalizada


Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],100)
Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],100)





include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
@time A = model_von_Rossun(1,5)

# A = model_von_Rossun(xdata,0.27)



# h = Histograma_LINEAR
h = Histograma_LINEAR
h_normalizado = normalize(h; mode=:pdf)

x = collect(h_normalizado.edges[1])
y = h_normalizado.weights

xdata = x[2:end]
ydata = y
p0 = [0.27]

@time teste_gc = curve_fit(model_von_Rossun, xdata, ydata, p0)

g_c = teste_gc.param[1]                                                                                                                                                     # Coefiente linear da melhor reta

vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,800)
vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)

x = xdata
y = modelo_fit
scatter(xdata,modelo_fit)

gr()





@time fitResult = curve_fit(model_von_Rossum, xdata[findall(xdata .> 15)], ydata[findall(xdata .> 15)], [0.30]; lower = [0.001],upper = [20.0]);
g_c = fitResult.param[1]                                                                                                                                                     # Coefiente linear da melhor reta


# x = get_points_in_log_scale(0.01,100,50)
y = model_von_Rossum(x,g_c)
g_c_op = round(g_c,digits = 2)

plot!(x,y,
lw = 5,
color = :green,
linestyle = :dash,
yscale=:log10,
# xlims = (0,20),
lab = L"\textrm{Lei de Von Rossum}",
title = L"$g_c = %$g_c_op, \rho/k^3=33, \Delta=0.9$",
titlefontsize = 25,
legend =:bottomleft
)

























#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


using SpecialFunctions
using DoubleExponentialFormulas
using QuadGK

include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
@time model_von_Rossum(1,0.27)

Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],10)
Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],10)

h = Histograma_LINEAR
h_normalizado = normalize(h; mode=:pdf)

x = collect(h_normalizado.edges[1])
y = h_normalizado.weights

xdata = x[2:end]
ydata = y
p0 = [0.27]

xdata_coerente = xdata[findall(xdata .> 0 )]
ydata_coerente = ydata[findall(xdata .> 0 )]

teste = curve_fit(model_von_Rossum, xdata_coerente, ydata_coerente, p0; lower = [0.2],upper = [20.0])
g_c = teste.param[1] 

# @time A = model_von_Rossum(xdata_coerente,g_c)
@time B = model_von_Rossum(xdata,g_c)

vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)

x = xdata
y = B
plot!(xdata,B,
lw = 5,
color = :green,
linestyle = :dash,
# xlims = (0.0001,10),
lab = L"\textrm{Lei de Von Rossum}",
# title = L"$g_c = 15, \rho/k^3=33, \Delta=0.9$",
titlefontsize = 25,
legend =:bottomleft
)

plot!(x,y,lab = L"\textrm{Lei de Von Rossum}",lw=5,color = :green,
linestyle = :dash,legend =:bottomleft,title = L"$g_c = %$g_c, \rho/k^3=33, \Delta=0.9$")



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



A = isnan.(All_Intensitys[1,:])
Intensidade = Any[]
for i in 1:size(All_Intensitys[1,:],1)

    if A[i] == false
       append!(Intensidade,All_Intensitys[1,i]) 
    end
end

Intensidade_Resultante_NORMALIZADA = Intensidade/mean(Intensidade)


Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],30)
Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],30)

vizualizar_Histograma_das_Intensidade(Histograma_LINEAR,1000)

vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)

vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)
