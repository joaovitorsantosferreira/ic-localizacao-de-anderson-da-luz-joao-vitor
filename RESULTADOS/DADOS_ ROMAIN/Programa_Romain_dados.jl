###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------- Instalação dos Pacotes ------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Pkg

Pkg.add("Distances")    
Pkg.add("LinearAlgebra")
Pkg.add("LaTeXStrings")  
Pkg.add("Random")       
Pkg.add("Statistics")   
Pkg.add("SpecialFunctions")
Pkg.add("QuadGK")       
Pkg.add("PlotThemes")   
Pkg.add("StatsBase") 
Pkg.add("LsqFit")
Pkg.add("Optim")
Pkg.add("ProgressMeter")
Pkg.add("FileIO")
Pkg.add("JLD2")
Pkg.add("Dates")
Pkg.add("Roots")

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------- Compilação dos Pacotes ------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots

###############################################################################################################################################################################################################
#--------------------------------------------------------------------------------- Estrutura dos Parametros de Entrada da Simulação --------------------------------------------------------------------------#
###############################################################################################################################################################################################################

mutable struct R11_ENTRADA
    Γ₀::Number  
    ωₐ::Number
    Ω ::Number
    rₘᵢₙs::Any
    ρs::Any
    Ls::Any
    b₀s::Any
    k::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    λ::Number
    ωₗ::Number
    Δ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
    angulo_coerente::Number
    Tipo_de_beta::String
    N_pontos::Integer
    N_Realizações::Integer
    Lₐ::Number
    RANGE_INICIAL::Number
    RANGE_FINAL::Number
    Variavel_Constante::String
    Escala_do_range::String
    Desordem_Diagonal::String
    W::Number
end

###############################################################################################################################################################################################################
#--------------------------------------------------------------------------------- Estrutura dos Parametros de Entrada da Simulação --------------------------------------------------------------------------#
###############################################################################################################################################################################################################

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Caso Escalar --------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos_caso_escalar = load("DATA={2021-August-2}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
Parametros_Antigos_caso_escalar = load("DATA={2021-August-2}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

σ²,Range_L = Dados_Antigos_caso_escalar
ρs = Parametros_Antigos_caso_escalar.ρs


Escala_do_range = Parametros_Antigos_caso_escalar.Escala_do_range
rₘᵢₙs = Parametros_Antigos_caso_escalar.rₘᵢₙs
ρs = Parametros_Antigos_caso_escalar.ρs
Ls = Parametros_Antigos_caso_escalar.Ls
b₀s = Parametros_Antigos_caso_escalar.b₀s
Δ = Parametros_Antigos_caso_escalar.Δ
Variavel_Constante = Parametros_Antigos_caso_escalar.Variavel_Constante
Lₐ = Parametros_Antigos_caso_escalar.Lₐ
N_Sensores = Parametros_Antigos_caso_escalar.N_Sensores
Tipo_de_kernel = Parametros_Antigos_caso_escalar.Tipo_de_kernel
N_Realizações = Parametros_Antigos_caso_escalar.N_Realizações
W = Parametros_Antigos_caso_escalar.W


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Caso Vetorial -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos_caso_vetorial = load("DATA={2021-July-18}_Dados_Antigos_C13_VETORIAL.jld2", "SAIDA")
Parametros_Antigos_caso_vetorial = load("DATA={2021-July-18}_Dados_Antigos_C13_VETORIAL.jld2", "Parametros")

σ², Range_L = Dados_Antigos_caso_vetorial
ρs = Parametros_Antigos_caso_vetorial.ρs


Escala_do_range = Parametros_Antigos_caso_vetorial.Escala_do_range
rₘᵢₙs = Parametros_Antigos_caso_vetorial.rₘᵢₙs
ρs = Parametros_Antigos_caso_vetorial.ρs
Ls = Parametros_Antigos_caso_vetorial.Ls
b₀s = Parametros_Antigos_caso_vetorial.b₀s
Δ = Parametros_Antigos_caso_vetorial.Δ
Variavel_Constante = Parametros_Antigos_caso_vetorial.Variavel_Constante
Lₐ = Parametros_Antigos_caso_vetorial.Lₐ
N_Sensores = Parametros_Antigos_caso_vetorial.N_Sensores
Tipo_de_kernel = Parametros_Antigos_caso_vetorial.Tipo_de_kernel
N_Realizações = Parametros_Antigos_caso_vetorial.N_Realizações
W = Parametros_Antigos_caso_vetorial.W





