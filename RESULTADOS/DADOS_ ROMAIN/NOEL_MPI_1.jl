using Distributed, MPIClusterManagers
manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
println("fazer algo")

println("Number of cores: ", nprocs())
println("Number of workers: ", nworkers())

@everywhere function get_something()
    return rand()
end
x = pmap(x->get_something(), 1:nworkers())
println(x)


hosts = []
pids = []
println("We are all connected and ready.")

for i in workers()
	host, pid = fetch(@spawnat i (gethostname(), getpid()))
	println(host, " --- " , pid)
	push!(hosts, host)
	push!(pids, pid)
end



MPIClusterManagers.stop_main_loop(manager)
println("saiu")
