#!/bin/bash
#SBATCH -J TestMPI            # Identificação do job
#SBATCH -o mpi.out         # Nome do arquivo de saída padrão (%j = ID do JOB)
#SBATCH --nodes=2                # node coun
#SBATCH --ntasks-per-node=10
#SBATCH --cpus-per-task=2
#SBATCH --partition=normal
#SBATCH -t 0:05:00                         # Tempo de execução (hh:mm:ss) - 1h30
#SBATCH --mem=1G


export JULIA_MPI_CLUSTER_WARN=n

remote_in=hpc/input                            # Pasta no serviço remoto para input
remote_out=hpc/output                          # Pasta no serviço remoto para output
remote_sing=hpc/containers/ufscar/IC/template  # Pasta no serviço remoto para os containers
container_in=/opt/input                        # Pasta no cluster para input
container_out=/opt/output                      # Pasta no cluster para output
local_sing=.                               # Pasta local para o container singularity
local_job="/scratch/job.${SLURM_JOB_ID}"       # Pasta temporária local
local_in="${local_job}/input/"                 # Pasta local para arquivos de entrada
local_out="${local_job}/output/"               # Pasta local para arquivos de saída

function clean_job() {
  echo "Limpando ambiente..."
  rm -rf "${local_job}"
}
trap clean_job EXIT HUP INT TERM ERR

set -eE
umask 077


echo "Criando pastas temporárias..."
mkdir -p "${local_in}"
mkdir -p "${local_out}"

# echo "Copiando input..."
# rclone copy "${service}:${remote_in}/" "${local_in}/"



# srun hostname -s > HOSTFILE.txt
# sleep 5

MPI_DIR=/opt/ohpc/pub/mpi/openmpi4-gnu9/4.0.5
export MPI_DIR
export SINGULARITY_MPI_DIR=$MPI_DIR
export SINGULARITYENV_APPEND_PATH=$MPI_DIR/bin
export SINGULARITYENV_APPEND_LD_LIBRARY_PATH=$MPI_DIR/lib

export PATH="$MPI_DIR/bin:$PATH"
export LD_LIBRARY_PATH="$MPI_DIR/lib:$LD_LIBRARY_PATH"


echo "Executando..."

srun --mpi=pmi2 singularity run --bind=/scratch:/scratch \
    --bind=/var/spool/slurm:/var/spool/slurm \
    /home/u770885/work.simg /opt/julia/bin/julia ~/Noel/test.jl
