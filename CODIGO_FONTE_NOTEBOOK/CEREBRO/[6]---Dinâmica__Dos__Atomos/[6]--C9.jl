###############################################################################################################################################################################################################
##################################################################### Programa de Controle 1 - Contempla todas as funcionalidades do codigo ###################################################################
###############################################################################################################################################################################################################




###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations
using Loess

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("PLOTS/Plots.jl")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

k = 1                                                                                                                                                   # Número de Onda
ρ_normalizado = 0.125                                                                                                                                      # Densidade normalidade, ou ρ/k²
ρ = ρ_normalizado*(k^2)                                                                                                                                 # Densidade de atomos no sistema


#----------------------------------------------------------------------------------------- Variaveis complementares ------------------------------------------------------------------------------------------#
Radius = 1 
L = 1
Lₐ = 280
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




if GEOMETRIA_DA_NUVEM == "DISCO"
############################################################################################# Geometria de Disco #############################################################################################
#----------------------------------------------------------------------------------------- Numero de atomos fixado ------------------------------------------------------------------------------------------#
N = 2000                                                                                                                                                # Número de Atomos 
kR = sqrt(N/(π*ρ))                                                                                                                                      # Raio Normalidado
Radius = kR/k                                                                                                                                           # Raio do Sistema
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
ω₀ = Radius/2                                                                                                                                           # Cintura do Laser Gaussiano 
#----------------------------------------------------------------------------------------- Raio do sistema fixado -------------------------------------------------------------------------------------------#
# kR = 50                                                                                                                                                # Raio Normalidado
# Radius = kR/k                                                                                                                                          # Raio do Sistema
# N = round(Int64,ρ_normalizado*π*(kR^2))                                                                                                                # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
# ω₀ = Radius/2                                                                                                                                          # Cintura do Laser Gaussiano 


elseif GEOMETRIA_DA_NUVEM == "SLAB"
############################################################################################### Geometria de SLAB #############################################################################################
#------------------------------------------------------------------------------------------- Numero de atomos fixado -----------------------------------------------------------------------------------------#
N = 2000                                                                                                                                                # Número de Atomos 
L = (N/(ρ*Lₐ))                                                                                                                                         # Espessura da Nuvem 
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
ω₀ = Lₐ/20                                                                                                                                                # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano
#----------------------------------------------------------------------------------------- Expessura da Nuvem fixada -----------------------------------------------------------------------------------------#
# L = 10                                                                                                                                                   # Comprimento de lado da nuvem 
# N = round(Int64,ρ_normalizado*(L*k)^2)                                                                                                                   # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     # Distância minima entre os atomos  
# ω₀ = L/4                                                                                                                                                 # Cintura do Laser Gaussiano


end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 1 - Tipo de Kernel ###################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial

######################################################################################## DEFINIÇÂO 2 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"
################################################################################### DEFINIÇÂO 3 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ = 0                                                                                                                        # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

################################################################################### DEFINIÇÂO 5 - Regime das Fases da dinâmica atomica ########################################################################
# PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"                               
# PERFIL_DA_INTENSIDADE = "SIM,para obter a cintura do feixe"                               
PERFIL_DA_INTENSIDADE = "NAO"                               
###############################################################################################################################################################################################################


N_Sensores = 360         
N_Telas = 100                                                                                                    # Quantidade de Sensores ao redor da amostra
comprimento_de_ray = (k*(ω₀^2))/2                                                                                           # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
Distancia_dos_Sensores = 0                                                                                                 # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                                                   # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 360                                                                                 # Define o angulo final da tela circular de Sensores





#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 5                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Gerando e Extraindo Dados da Nuvem -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Entrada = E1_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    PERFIL_DA_INTENSIDADE,
    N_Telas
)

# @time Dados_C9 = extração_de_dados_Geral(Entrada)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:

# cloud = Dados_C9.cloud
# r = cloud.r                                                                                                                                # Posições de todos os atomos
# λ = cloud.λ                                                                                                                                # Autovalores da Matriz de Green
# ψ = cloud.ψ                                                                                                                                # Autovetores da Matriz de Green
# R_jk = cloud.R_jk                                                                                                                          # Matriz que guarda a distância entre os atomos
# G = cloud.G                                                                                                                                # Matriz de Green do sistema 
# b₀ = cloud.b₀                                                                                                                              # Densidade Otica 
# β = Dados_C9.βₙ                                                                                                                             # Matriz que guarda a dinâmica dos atomos

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Graficos e Vizualização do Sistema -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# vizualizar_distribuição_dos_atomos(r,1000)
# savefig("fig1__N={$N}__Densidade={$ρ}__K={$k}.png")


# vizualizar_Dinamica_dos_atomos(r,β,Tipo_de_kernel,Radius,Δ,L,GEOMETRIA_DA_NUVEM,1000)
# savefig("fig_Romain__N={$N}__Densidade={$ρ}__K={$k}__3.png")


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------------------------- Gif"s -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------- Influência do Deturn na Dinâmica dos Atomos  -----------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Range_1 = 0
espaçamento = 0.1
Range_2 = 10
frames = animar_a_evolução_da_dinamica_atomica(Entrada,Range_1,espaçamento,Range_2)
# gif(frames,"animação_da_dinamica_dos_atoms_43.gif",fps=10)


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C9.jld2", Dict("SAIDA" => Dados_C9,"Parametros" => Entrada))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-4}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-4}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-5}_Dados_Antigos_CLUSTER_C9_ESPECIAL.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-5}_Dados_Antigos_CLUSTER_C9_ESPECIAL.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-5}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-5}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-6}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-6}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-9}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-9}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_wo.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_wo.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_wo2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_wo2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_2wo.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_2wo.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_150.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C9_150.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-16}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-16}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-19}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-19}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-22}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-22}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/TEMPORARIO_C9.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/TEMPORARIO_C9.jld2", "Parametros")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

σ²,Perfis_da_Intensidade,Posição_Sensores,βs,range_Δ = Dados_Antigos

Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
k = Parametros_Antigos.k
Ω = Parametros_Antigos.Ω
Ns = Parametros_Antigos.Ns
Radius = Parametros_Antigos.kR
L = Parametros_Antigos.L
Lₐ = Parametros_Antigos.Lₐ
Lₐ_titulo = round(Lₐ,digits = 2)
ρ = Ns./(Lₐ*L)
Angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
λ = Parametros_Antigos.λ
ωₗ = Parametros_Antigos.ωₗ
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
Tipo_de_campo = Parametros_Antigos.Tipo_de_campo
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
N_Realizações = Parametros_Antigos.N_Realizações
Δ_INICIAL = Parametros_Antigos.Δ_INICIAL
Δ_FINAL = Parametros_Antigos.Δ_FINAL
INTERVALO_Δ = Parametros_Antigos.INTERVALO_Δ
DIVISAO_β_X = Parametros_Antigos.DIVISAO_β_X
DIVISAO_β_Y = Parametros_Antigos.DIVISAO_β_Y
Variavel_Constante = Parametros_Antigos.Variavel_Constante
Geometria = Parametros_Antigos.Geometria
ωᵦ = Parametros_Antigos.ωᵦ
ωᵦ_plot = ωᵦ/λ

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Inter = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-August-27}_Dados_Antigos_CLUSTER_C16.jld2", "SAIDA")

Range_ρ = Dados_Inter[1][1:end-1]
Δs = Dados_Inter[2][1:end-1]

Range_ρ = Range_ρ[findall(iszero.(Δs) .== false)]

itp = LinearInterpolation(Range_ρ,Δs)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

anim_Betas = Animation()
index = 1
N_plot = Ns[index]
Δ_loc = round(itp(ρ[index]),digits=2)


for indice_Δ in 1:size(range_Δ,1)
    indice_Δ =1
    gr()
    theme(:vibrant)

    # βs[index,findall(βs[index,:,4] .== 0),4] .= minimum(βs[index,findall(βs[index,:,4] ≠ 0),4])  
    index_beta = findall((βs[index,:,1] .== range_Δ[indice_Δ]).*(βs[index,:,4] .≠ 0))

    x_a = βs[index,index_beta,2]
    y_b = βs[index,index_beta,3]
    z_c = log10.(βs[index,index_beta,4])

    Δplot = round(range_Δ[indice_Δ],digits=2) 

    scatter(x_a,y_b,
        size = (960, 1200),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha=0,
        xlims = (-0.6*L , 1.6*L),
        # ylims = (-(0.7)*Lₐ ,(0.7)*Lₐ),
        ms=11,
        # clims = (-23,-5),
        m =:square,
        zcolor=z_c,
        label=L"$ \Delta = %$Δplot, \Delta_{LOC} = %$Δ_loc $",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 12, 
        xticks = collect(-10:10:10),
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out, 
        # xlabel = L"$      kX  $", 
        ylabel = L"$kY$",
        title = L"$ N = %$N_plot,kL = %$L, kL_a = %$Lₐ_titulo,%$N_Realizações REP,w_0 = %$ωᵦ_plot \lambda, |\beta|^2$",
        legend =:bottomleft,
        xguidefonthalign = :left
        )
    plot!(Perfis_da_Intensidade[indice_Δ, : ,index],Posição_Sensores,
    inset = (1, bbox(0.51, 0, 0.38, 1, :left)), 
    subplot = 2,
    ms = 0,
    # size = (300, 300),
    left_margin = 10Plots.mm,
    right_margin = 14Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    # linestyle=:dashdot,
    xscale = :log10,
    # ylims = (10^-10,10^-5),
    c= :green,
    lw = 8,
    xlims = (10^-20,10^-11),
    # ylims = (10^-13,10^-8),
    # lab = L"$ N = 0 , \sigma^2 = %$σ² $" ,
    lab = "" ,
    framestyle = :box,
    legendfontsize = 17,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 12, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    xlabel = L"$I$", 
    )


    frame(anim_Betas)
end

gif(anim_Betas,"2w0.gif",fps=1)
# mp4(anim_Betas,"betas_1500.mp4",fps=2)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

include("PLOTS/Plots.jl")

ρ = Ns./(L*Lₐ)
anim_Intensidade = Animation()

Perfis_da_Intensidade[ findall(Perfis_da_Intensidade .== 0) ] .= 0.000001

for indice_Δ in 1:size(range_Δ,1)
    Δplot = range_Δ[indice_Δ] 

    vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores,Perfis_da_Intensidade[indice_Δ, : ,:],Ns,0,σ²[indice_Δ,:],Δplot,"N",L,Lₐ,ρ,L"$kL = %$L , \Delta = %$Δplot,kL_a = %$Lₐ_titulo,w_0 = %$ωᵦ_plot \lambda, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)

    frame(anim_Intensidade)
end

gif(anim_Intensidade,"perfil_CAMPO_LONGE.gif",fps=1)
# mp4(anim_Intensidade,"perfil_CAMPO_LONGE.mp4",fps=1)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-6}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-6}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

σ²_1,Perfis_da_Intensidade_1,Posição_Sensores,βs_1,range_Δ_1 = Dados_Antigos_1

Dados_Antigos_2 = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-9}_Dados_Antigos_CLUSTER_C9.jld2", "SAIDA")
Parametros_Antigos_2 = load("RESULTADOS/DADOS ANTIGOS/C9/DATA={2022-September-9}_Dados_Antigos_CLUSTER_C9.jld2", "Parametros")

σ²_2,Perfis_da_Intensidade_2,Posição_Sensores,βs_2,range_Δ_2 = Dados_Antigos_2

ρ = Ns./(L*Lₐ)
anim_Intensidade_Comp = Animation()
index = 1
N_plot = Ns[index]
Δ_loc = round(itp(ρ[index]),digits=2)
Perfis_da_Intensidade_1[ findall(Perfis_da_Intensidade_1 .== 0) ] .= 0.000001
Perfis_da_Intensidade_2[ findall(Perfis_da_Intensidade_2 .== 0) ] .= 0.000001

for indice_Δ in 1:size(range_Δ,1)

    Δplot = range_Δ[indice_Δ] 

    plot(Perfis_da_Intensidade_1[indice_Δ, : ,index],Posição_Sensores[:,2],
    ms = 0,
    size = (1000, 600),
    left_margin = 10Plots.mm,
    right_margin = 14Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    # linestyle=:dashdot,
    xscale = :log10,
    # ylims = (10^-10,10^-5),
    c= :green,
    lw = 8,
    xlims = (10^-20,10^20),
    xticks = [10^-20,10^-15,10^-10,10^-5],
    # lab = L"$ N = 0 , \sigma^2 = %$σ² $" ,
    framestyle = :box,
    label=L"$ \Delta = %$Δplot, \Delta_{LOC} = %$Δ_loc $",
    title = L"$ N = %$N_plot,kL = %$L, kL_a = %$Lₐ_titulo, %$Tipo_de_kernel,%$N_Realizações REP,w_0 = %$ωᵦ_plot \lambda,|\beta|^2$",
    legendfontsize = 17,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 12, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    legend =:bottomleft,
    xlabel = "", 
    )


    plot!(Perfis_da_Intensidade_2[indice_Δ, : ,index],Posição_Sensores[:,2],
    inset = (1, bbox(0.5, 0, 0.5, 1, :left)), 
    subplot = 2,
    ms = 0,
    # size = (300, 300),
    left_margin = 10Plots.mm,
    right_margin = 14Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    # linestyle=:dashdot,
    xscale = :log10,
    # ylims = (10^-10,10^-5),
    c= :red,
    lw = 8,
    xlims = (10^-20,10^4),
    # lab = L"$ N = 0 , \sigma^2 = %$σ² $" ,
    lab = "" ,
    framestyle = :box,
    legendfontsize = 17,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 12, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    xlabel = "", 
    )

    frame(anim_Intensidade_Comp)
end

gif(anim_Intensidade_Comp,"perfil_COMPO.gif",fps=10)



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C1.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C1.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C3.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/C3.jld2", "Parametros")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D1.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D1.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D3.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D3.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D4.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C9/D4.jld2", "Parametros")

indice_Δ = 1
index = 1
σ²,Perfis_da_Intensidade,Posição_Sensores,βs,range_Δ = Dados_Antigos

Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
k = Parametros_Antigos.k
Ω = Parametros_Antigos.Ω
Ns = Parametros_Antigos.Ns
Radius = Parametros_Antigos.kR
L = Parametros_Antigos.L
Lₐ = Parametros_Antigos.Lₐ
Lₐ_titulo = round(Lₐ,digits = 2)
ρ = Ns./(Lₐ*L)
Angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
λ = Parametros_Antigos.λ
ωₗ = Parametros_Antigos.ωₗ
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
Tipo_de_campo = Parametros_Antigos.Tipo_de_campo
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
N_Realizações = Parametros_Antigos.N_Realizações
Δ_INICIAL = Parametros_Antigos.Δ_INICIAL
Δ_FINAL = Parametros_Antigos.Δ_FINAL
INTERVALO_Δ = Parametros_Antigos.INTERVALO_Δ
DIVISAO_β_X = Parametros_Antigos.DIVISAO_β_X
DIVISAO_β_Y = Parametros_Antigos.DIVISAO_β_Y
Variavel_Constante = Parametros_Antigos.Variavel_Constante
Geometria = Parametros_Antigos.Geometria
ωᵦ = Parametros_Antigos.ωᵦ
ωᵦ_plot = round(ωᵦ/λ,digits=2)

L_plot = round(L,digits=2)
Δplot = range_Δ[indice_Δ] 
ρ_plot = round(ρ,digits=2)
plot!(Posição_Sensores,Perfis_da_Intensidade[indice_Δ, : ,index],
ms = 0,
size = (1000, 500),
left_margin = 10Plots.mm,
right_margin = 14Plots.mm,
top_margin = 5Plots.mm,
bottom_margin = 5Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
minorgridstyle = :dashdot,
# linestyle=:dashdot,
yscale = :log10,
# ylims = (10^-10,10^-5),
lw = 8,
ylims = (10^-11,10^-6),
# ylims = (10^-13,10^-8),
title = L"$ \rho = %$ρ_plot,\Delta = \Delta_{LOC} ,kL_a = %$Lₐ_titulo, %$Tipo_de_kernel,%$N_Realizações REP,L = %$L_plot$",
lab = L"$ w_0 = %$ωᵦ_plot \lambda $" ,
framestyle = :box,
legendfontsize = 17,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 12, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
ylabel = L"$I$", 
xlabel = L"$\theta$", 
)

# savefig("perfils_intensidade_far_field.png")

# anim = Animation()
# for i in -4:0.1:3 
#     Δ = i
#     g = get_Thouless_Number(γₙ,ωₙ,Δ,Γ₀)
#     vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,1000)
#     frame(anim)
# end
# gif(anim,"animação_Deturn_3.gif",fps=10)


# anim = Animation()
# for i in 1:1:2*N 
#     RG_Modos = i

#     propriedades_fisicas_modo_especifico = get_propriedade_to_specific_mode(cloud,propriedades_fisicas_global,Tipo_de_kernel,RG_Modo)
    
#     IPRₑ = propriedades_fisicas_modo_especifico.IPRᵦ                                                                                                                             
#     ξₑ = propriedades_fisicas_modo_especifico.ξᵦ                                                                                                                                   
#     Eₑ = propriedades_fisicas_modo_especifico.Eᵦ                                                                                                                                 
#     γₑ = propriedades_fisicas_modo_especifico.γᵦ                                                                                                                                 
#     Γₑ = propriedades_fisicas_modo_especifico.Γᵦ                                                                                                                                 
#     ωₑ = propriedades_fisicas_modo_especifico.ωᵦ 
#     Tₑ = propriedades_fisicas_modo_especifico.Tᵦ
#     Dcmₑ = propriedades_fisicas_modo_especifico.Dcmᵦ
#     Rcmₑ = propriedades_fisicas_modo_especifico.Rcmᵦ                                                                                                                             
#     ψₑ² = propriedades_fisicas_modo_especifico.ψᵦ²
    
    
#     vizualizar_o_modo_especifico_no_disco(r,ψₑ²,Radius,IPRₑ,RG_Modo,1000)    
#     frame(anim)
# end

# gif(anim,"animação_Deturn_4.gif",fps=2)


# progresso_global = 0
# progresso_global = Progress(8, 1,"Geração da Nuvem")
# etapa = Progress(8, 2,"Etapas") 



