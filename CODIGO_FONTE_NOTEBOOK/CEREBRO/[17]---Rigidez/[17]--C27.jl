###############################################################################################################################################################################################################
######################################################## Programa de Controle 23 - Contempla simulações Voltadas a Variância para diferentes Densidades e Δ ####################################################
##############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations
using Loess
using Colors 


###############################################################################################################################################################################################################


#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("PLOTS/Plots.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R11.jl") 

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C27/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C27_{DISCO}_{Escalar}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C27/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C27_{DISCO}_{Escalar}.jld2", "Parametros")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C27/TEMPORARIO_C27.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C27/TEMPORARIO_C27.jld2", "Parametros")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Σ²,Range_L,Espectro_TOTAL = Dados_Antigos

plot(Range_L,Σ²[:,1],xscale=:log10)
plot!(Range_L,Σ²[:,2],xscale=:log10)
plot!(Range_L,Σ²[:,3],xscale=:log10)
plot!(Range_L,Σ²[:,4],xscale=:log10)
plot!(Range_L,Σ²[:,5],xscale=:log10)
plot!(Range_L,Σ²[:,6],xscale=:log10)



for i in 1:(size(range_ρs,1)-1)
    Λₙ = imag.(Espectro_TOTAL[i])
    γₙ = real.(Espectro_TOTAL[i])
    densidade = ρs[i]
    Radius_plot = round(Radius,digits=2)
    n_div = 50
    

    histogram2d(
        Λₙ,
        γₙ, 
        nbins=n_div, 
        normalize=true,
        color = cgrad(:jet1,scale=:exp10),
        # color=Colors.diverging_palette(20,300,20, logscale=true,  wcolor=colorant"red",dcolor1=colorant"green", dcolor2=colorant"red", b=0.1,d1=1.0,d2=1.0),
        size = (1150, 1000),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        colorbar_title = L"\textrm{density}",
        title = L"$\rho/k^2 = %$densidade,kR = %$Radius_plot, %$Tipo_de_kernel $",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
    )
    
    savefig("densidade={$densidade}.png")
end
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# d = Normal()
# fwhm(d)
# using DataFrames


# df = DataFrame(Densidade = Histograma_Diagrama_de_Fase[:,1], Delta = Histograma_Diagrama_de_Fase[:,2] , Comprimento = Histograma_Diagrama_de_Fase[:,3])
# show(df, allcols=true,allrows=true)