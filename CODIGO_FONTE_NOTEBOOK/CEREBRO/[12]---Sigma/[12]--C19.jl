###############################################################################################################################################################################################################
######################################################## Programa de Controle 19 - Contempla simulações Voltadas ao Calculo da Quantidade Sigma ###############################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances
using LinearAlgebra
using LaTeXStrings
using Random
using Statistics
using Plots
using SpecialFunctions
using QuadGK
using PlotThemes
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R18.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[10]---Sigma/R18.jl") 
include("PLOTS/Plots.jl")



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                 # Define a geometria da nuvem como Cubo
# GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                               # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###############################################################################################################################################################################################################


N = 500                                                                                                                        # Número de atomos caso N seja fixado
kR = 1                                                                                                                        # Raio normalizado do sistema caso kR seja fixado
L = 20
Lₐ = 20

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
k = 1                                                                                                                        # Número de Onda
angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
λ = (2*π)/k                                                                                                                  # Comprimento de onda da luz incidente 
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de Onda da frente de onda                                            


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

########################################################################### DEFINIÇÂO 6 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_de_Densidade = "LOG"                                                                                  # Define que a escala no eixo Y será logaritimica
Escala_de_Densidade = "LINEAR"                                                                                 # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################

N_Realizações = 3                                                                                             # Define a quantidade de realizações por locus no diagrama de Fase
N_div_Densidade = 10                                                                                            # Define a quatidade de divisões no eixo y do Diagrama, que é a densidade normalizada do sistema
N_div_Deturn = 40                                                                                               # Define a quatidade de divisões no eixo x do Diagrama, que é o Deturn do sistema

Δ_Inicial = -1                                                                                                 # Define o Valor Inicial do Deturn
Δ_Final = 2                                                                                                    # Define o Valor Final do Deturn
Densidade_Inicial = 1/(2*π)^3                                                                                  # Define o Valor Inicial da Densidade
Densidade_Final = 10/(2*π)^3                                                                                    # Define o Valor Final da Densidade


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 7 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


entrada_rotina = R18_ENTRADA(
    Γ₀,
    k,
    N,
    kR,
    L,
    Lₐ,
    Tipo_de_kernel,
    N_Realizações,
    N_div_Densidade,
    N_div_Deturn,
    Δ_Inicial,
    Δ_Final,
    Densidade_Inicial,
    Densidade_Final,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    Geometria
)


Dados_C19 = ROTINA_18__Diagrama_de_Fase_sigma2(entrada_rotina)

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Histograma_Diagrama_de_Fase = Dados_C19                                                                                      # Define o histograma que contem a variancia calculada para diferentes 
Histograma_Diagrama_de_Fase[:,2] = Histograma_Diagrama_de_Fase[:,2].*(2*π)^3 

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Histograma_Diagrama_de_Fase,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$ kL = %$L , kR = %$Lₐ , %$N_Realizações REP,%$Tipo_de_kernel,%$Geometria,\Sigma^2$",1000)
# savefig("Diagrama_SIGMA_2.png")


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-16}_Dados_Antigos_C19.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-16}_Dados_Antigos_C19.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-17}_Dados_Antigos_C19.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-17}_Dados_Antigos_C19.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-22}_Dados_Antigos_C19.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-22}_Dados_Antigos_C19.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-30}_Dados_Antigos_C19.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2021-December-30}_Dados_Antigos_C19.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2022-January-5}_Dados_Antigos_VET_C19.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2022-January-5}_Dados_Antigos_VET_C19.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2022-January-6}_Dados_Antigos_ESC_C19.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C19/DATA={2022-January-6}_Dados_Antigos_ESC_C19.jld2", "Parametros")

Histograma_Diagrama_de_Fase = Dados_Antigos    

Densidade_Inicial = Parametros_Antigos.Densidade_Inicial 
Densidade_Final = Parametros_Antigos.Densidade_Final 
Escala_de_Densidade = Parametros_Antigos.Escala_de_Densidade 
N_div_Densidade = Parametros_Antigos.N_div_Densidade 
N_div_Deturn = Parametros_Antigos.N_div_Deturn 
Radius = Parametros_Antigos.kR
L = Parametros_Antigos.L  
Lₐ = Parametros_Antigos.Lₐ 
Δ_Inicial = Parametros_Antigos.Δ_Inicial
Δ_Final = Parametros_Antigos.Δ_Final
Geometria = Parametros_Antigos.Geometria
N_Realizações = Parametros_Antigos.N_Realizações 
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel


include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Histograma_Diagrama_de_Fase,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$ kL = %$L , kR = %$Lₐ , %$N_Realizações REP,%$Tipo_de_kernel,%$Geometria,\Sigma^2$",1000)
savefig("Diagrama de fase SIGMA2 , Kernel $Tipo_de_kernel ,$N_Realizações REP.png")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C4.jld2", Dict("SAIDA" => Dados_C4,"Parametros" => entrada_rotina))

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#