###############################################################################################################################################################################################################
################################################### Programa de Controle 18 - Contempla Simulações Voltadas a Estatiscas de gc para geração de um diagrama de fase ############################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Distributions

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R16.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R17.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[9]---Estatistiscas__de__gc/R16.jl")  
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[9]---Estatistiscas__de__gc/R17.jl") 
include("PLOTS/Plots.jl")



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                 # Define a geometria da nuvem como Cubo
GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                               # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
# GEOMETRIA_DA_NUVEM = "TUBO"  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###############################################################################################################################################################################################################


N = 500                                                                                                                        # Número de atomos caso N seja fixado
kR = 1                                                                                                                        # Raio normalizado do sistema caso kR seja fixado
L = 3
Lₐ = 50

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
k = 1                                                                                                                        # Número de Onda
angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
λ = (2*π)/k                                                                                                                  # Comprimento de onda da luz incidente 
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de Onda da frente de onda                                            


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

N_Sensores = 500                                                                                                              # Quantidade de Sensores ao redor da amostra
Angulo_de_variação_da_tela_circular_1 = 50                                                                                   # Define o angulo inicial da Tela Circular
Angulo_de_variação_da_tela_circular_2 = 70                                                                                   # Define o angulo final da Tela Circular


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################ DEFINIÇÂO 5 - Variavel constante no Diagrama de Fase #############################################################################
Variavel_Constante = "N"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "kR"                                                                     # Define que o raio do sistma será a quantidade fixada nas realizações, presupondo uma geometria de DISCO
# Variavel_Constante = "L"                                                                      # Define que o as dimensões do SLAB serão a quantidade fixada nas realizações, presupondo uma geometria de SLAB 
########################################################################### DEFINIÇÂO 6 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_de_Densidade = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_de_Densidade = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################

N_Realizações_Intensidade = 1
N_Realizações_gc = 10                                                                                             # Define a quantidade de realizações por locus no diagrama de Fase
N_div_Densidade = 5                                                                                            # Define a quatidade de divisões no eixo y do Diagrama, que é a densidade normalizada do sistema
N_div_Deturn = 5                                                                                               # Define a quatidade de divisões no eixo x do Diagrama, que é o Deturn do sistema

Δ_Inicial = -1                                                                                                  # Define o Valor Inicial do Deturn
Δ_Final = 2                                                                                                    # Define o Valor Final do Deturn
Densidade_Inicial = 1/(2*π)^3                                                                                           # Define o Valor Inicial da Densidade
Densidade_Final = 40/(2*π)^3                                                                                             # Define o Valor Final da Densidade


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 7 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


entrada_rotina = R17_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    kR,
    L,
    Lₐ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    N_Realizações_Intensidade,
    N_Realizações_gc,
    N_div_Densidade,
    N_div_Deturn,
    Δ_Inicial,
    Δ_Final,
    Densidade_Inicial,
    Densidade_Final,
    Variavel_Constante,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    Geometria
)


Dados_C18 = ROTINA_17__Diagrama_de_Fase_Estatisticas_gc(entrada_rotina)

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Histograma_Diagrama_de_Fase = Dados_C18                                                                                      # Define o histograma que contem a variancia calculada para diferentes 
Histograma_Diagrama_de_Fase[:,2] = Histograma_Diagrama_de_Fase[:,2].*(2*π)^3 
###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Media = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,3])
Mediana = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,4])
Variancia = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,5])
Desvio_r = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,6])

vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Media,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\phi = [50,70], N = 500, 1000 REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Mediana,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\phi = [50,70], N = 500, 1000 REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Variancia,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\phi = [50,70], N = 500, 1000 REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Desvio_r,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\phi = [50,70], N = 500, 1000 REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)


# savefig("Diagrama_Variancia__NR={$N_Realizações}__N={$N}_norm_24.png")
# savefig("Diagrama_Variancia__NR={$N_Realizações}__kR={$kR}_23.png")
# savefig("Romain_302.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C4.jld2", Dict("SAIDA" => Dados_C4,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-3}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-3}_Dados_Antigos_C18.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-4}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-4}_Dados_Antigos_C18.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-9}_Dados_Antigos_C18_COM_ESTATISTICAS.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-November-9}_Dados_Antigos_C18_COM_ESTATISTICAS.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-8}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-8}_Dados_Antigos_C18.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-10}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-10}_Dados_Antigos_C18.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-15}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2021-December-15}_Dados_Antigos_C18.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-January-6}_Dados_Antigos_C18.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-January-6}_Dados_Antigos_C18.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-January-11}_Dados_Antigos_C18.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-January-11}_Dados_Antigos_C18.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-February-6}_Dados_Antigos_C18.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-February-6}_Dados_Antigos_C18.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-February-13}_Dados_Antigos_C18.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C18/DATA={2022-February-13}_Dados_Antigos_C18.jld2", "Parametros")


Histograma_Diagrama_de_Fase,Estatisticas_gc = Dados_Antigos    

Densidade_Inicial = Parametros_Antigos.Densidade_Inicial 
Densidade_Final = Parametros_Antigos.Densidade_Final 
Escala_de_Densidade = Parametros_Antigos.Escala_de_Densidade 
N_div_Densidade = Parametros_Antigos.N_div_Densidade 
N_div_Deturn = Parametros_Antigos.N_div_Deturn 
Radius = Parametros_Antigos.Radius
L = Parametros_Antigos.L  
Lₐ = Parametros_Antigos.Lₐ 
Δ_Inicial = Parametros_Antigos.Δ_Inicial
Δ_Final = Parametros_Antigos.Δ_Final
Geometria = Parametros_Antigos.Geometria
N_Realizações_gc = Parametros_Antigos.N_Realizações_gc 

Media = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,3])
Mediana = hcat(Histograma_Diagrama_de_Fase[:,1:2],log10.(Histograma_Diagrama_de_Fase[:,4]))
Variancia = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,5])
Desvio_r = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,6])
gc_puro = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,7])

print(sort(gc_puro[:,3]))

# include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Variancia,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{gc}^2  $",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Media,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\langle g_c \rangle $",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Mediana,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,median(g_c)  $",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Desvio_r,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(gc_puro,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,g_C $",1000)

savefig("Diagrama_gc_puro_400_Realizações.png")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(kell,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$kL = %$L,kR = %$Lₐ,ESCALAR,%$Geometria,k \ell $",1000)
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Desvio_r,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L,kR = %$Lₐ, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)

Linha = 16
Coluna = 8
Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade).*(2*π)^3 
Range_Δ = range(Δ_Inicial,Δ_Final,length=N_div_Deturn) 

dados_gc_especifico = Estatisticas_gc[Coluna,Linha,:]

var_gc = std(dados_gc_especifico)
media_gc = mean(dados_gc_especifico)
var_r_gc = std(dados_gc_especifico)/mean(dados_gc_especifico)
ρ_gc = Range_ρ[Linha]
Δ_gc = Range_Δ[Coluna]
kℓ_gc =  (1 + 4*(((Δ_gc))^2)) / (4*pi*ρ_gc/(2*pi)^3)  
b0 = 4*((ρ_gc/(2*π)^3)*(L*π*(Lₐ^2)))/(Lₐ^2)

var_gc = round(var_gc,digits = 2)
kℓ_gc = round(kℓ_gc,digits = 3)
ρ_gc = round(ρ_gc,digits = 2)
Δ_gc = round(Δ_gc,digits = 2)
media_gc = round(media_gc,digits = 2)
var_r_gc = round(var_r_gc,digits = 2)
b0 = round(b0,digits = 2)


Histograma_LINEAR = get_dados_histograma_LINEAR(dados_gc_especifico,20)
Histograma_LOG = get_dados_histograma_LOG(dados_gc_especifico,30)
Histograma_LINEAR_lngc = get_dados_histograma_LINEAR_gc(log.(dados_gc_especifico),30)
Histograma_LINEAR_lngc_NORMALIZADO = normalize(Histograma_LINEAR_lngc; mode=:pdf)

include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_de_gc(Histograma_LINEAR,L"$\sigma_r =  %$var_r_gc , \rho \lambda^3 = %$ρ_gc, \delta = %$Δ_gc, \langle g_C \rangle = %$media_gc, \sigma^2_{gc} = %$var_gc , k \ell = %$kℓ_gc $",1000)
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_vom_rossum(Histograma_LINEAR,L"$\sigma_r =  %$var_r_gc , \rho \lambda^3 = %$ρ_gc, \delta = %$Δ_gc, \langle g_C \rangle = %$media_gc, \sigma^2_{gc} = %$var_gc , k \ell = %$kℓ_gc $",1000)
vizualizar_Distribuição_de_Probabilidades_de_lngc(Histograma_LINEAR_lngc,L"$\sigma_r =  %$var_r_gc , \rho \lambda^3 = %$ρ_gc, \delta = %$Δ_gc, \langle g_C \rangle = %$media_gc, b_0 = %$b0 , k \ell = %$kℓ_gc $",1000)


xdata = collect(Histograma_LINEAR_lngc_NORMALIZADO.edges[1])[2:end]
ydata = Histograma_LINEAR_lngc_NORMALIZADO.weights 
fit_normal = fit_mle(Normal, xdata, ydata)

μ = round(params(fit_normal)[1],digits = 2)    
σ = round(params(fit_normal)[2],digits = 2)

norma_ditribution(x, p) = (1/sqrt(2*π*p[2]^2))*exp((-(x-p[1])^2)/2*(p[2]^2))
xfit = xdata 
yfit = zeros(size(xfit,1))
@. yfit = norma_ditribution(xfit,params(fit_normal))

R² = round(compute_R2(ydata, yfit),digits=2)
p1 = μ 
p2 = σ

plot!(xfit,yfit,
lw = 6,
linestyle=:dashdot,
c = :black,
lab = L"$ \sigma = %$p2, \mu = %$p1, R^2 = %$R²$"
# lab = ""
)

savefig("PGC_2.png")


kℓ_gc =  (1 + 4*(((Δ_gc)/(2*π)^3 )^2) ) / (4*pi*ρ_gc/(2*pi)^3)  
(A*B*C)^(1/3)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Animação ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



# anim = Animation()

# for i in 1:N_div_Deturn

#     Linha = 15
#     Coluna = i
#     Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade).*(2*π)^3 
#     Range_Δ = range(Δ_Inicial,Δ_Final,length=N_div_Deturn) 

#     dados_gc_especifico = Estatisticas_gc[Coluna,Linha,:]

#     var_gc = std(dados_gc_especifico)
#     var_gc = round(var_gc,digits = 2)

#     media_gc = mean(dados_gc_especifico)
#     var_r_gc = std(dados_gc_especifico)/mean(dados_gc_especifico)
#     ρ_gc = Range_ρ[Linha]
#     Δ_gc = Range_Δ[Coluna]
#     kℓ_gc =  (1 + 4*(Δ_gc^2) ) / (4*pi*ρ_gc/(2*π)^3 )  
    
#     kℓ_gc = round(kℓ_gc,digits = 2)
#     ρ_gc = round(ρ_gc,digits = 2)
#     Δ_gc = round(Δ_gc,digits = 2)
#     media_gc = round(media_gc,digits = 2)
#     var_r_gc = round(var_r_gc,digits = 2)

#     Histograma_LINEAR = get_dados_histograma_LINEAR(dados_gc_especifico,40)
#     Histograma_LOG = get_dados_histograma_LOG(dados_gc_especifico,40)

#     vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_vom_rossum(Histograma_LOG,L"$\sigma_r =  %$var_r_gc , ,\rho \lambda^3 = %$ρ_gc, \delta = %$Δ_gc, \langle g_C \rangle = %$media_gc, \sigma^2_{gc} = %$var_gc, k \ell = %$kℓ_gc$",1000)
#     frame(anim)
# end

# gif(anim,"Perfil_Pgc.gif",fps=1)
# mp4(anim,"Perfil_Pgc.mp4",fps=1)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Variancia,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L,kR = %$Lₐ, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{gc}^2  $",1000)

kell = zeros(size(Histograma_Diagrama_de_Fase,1),3)
kell[:,1:2] = Histograma_Diagrama_de_Fase[:,1:2]
for i in 1:size(Histograma_Diagrama_de_Fase,1)
    kell[i,3] = (1 + 4*(((Histograma_Diagrama_de_Fase[i,1]) )^2) ) / (4*pi*Histograma_Diagrama_de_Fase[i,2])  
end
include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(log10.(kell),N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$kL = %$L,kR = %$Lₐ,ESCALAR,%$Geometria,log_{10}(k \ell) $",1000)
# savefig("diagrama_kl.png")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# aux = 1
# Media_GEO = zeros(size(Media))
# Media_GEO[:,1:2] = Media[:,1:2] 
# for i in 1:N_div_Densidade
#     for j in 1:N_div_Deturn
#         Media_GEO[aux,3] = produtorio(Estatisticas_gc[j,i,:])
#         aux += 1
#     end
# end

# sort(Media_GEO[:,3])
# vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Media_GEO,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L,kR = %$Lₐ, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\langle g_c \rangle_{geo}$",1000)
# savefig("media_geometrica.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Estatisticas_gc = load("RESULTADOS/DADOS ANTIGOS/C18/Temporario_C18.jld2", "Estatisticas")

# Γ₀ = 1                                                                                                                       
# ωₐ = 1                                                                                                                       
# GEOMETRIA_DA_NUVEM = "TUBO"                                                                                               
# N = 500                                                                                                                     
# Radius = 1                                                                                                                      
# L = 20
# Lₐ = 20
# Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                               
# Tipo_de_kernel = "Escalar"                                                                                                  
# Tipo_de_Onda = "Laser Gaussiano"                                                                                            
# Tipo_de_beta = "Estacionário"                                                                                                
# Ω = Γ₀/1000                                                                                                                  
# k = 1                                                                                                                        
# angulo_da_luz_incidente = 0                                                                                                  
# λ = (2*π)/k                                                                                                                  
# vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            
# N_Sensores = 20*720                                                                                                            
# Angulo_de_variação_da_tela_circular_1 = 50                                                                                   
# Angulo_de_variação_da_tela_circular_2 = 70                                                                                   
# Variavel_Constante = "N"                                                                                                     
# Escala_de_Densidade = "LINEAR"                                                                                               
# N_Realizações_Intensidade = 1
# N_Realizações_gc = 400                                                                                                      
# N_div_Densidade = 20                                                                                                         
# N_div_Deturn = 20                                                                                                            
# Δ_Inicial = -1                                                                                                               
# Δ_Final = 2                                                                                                                  
# Densidade_Inicial = 1/(2*π)^3                                                                                                
# Densidade_Final = 42/(2*π)^3                                                                                                 
# Desordem_Diagonal = "AUSENTE"                                                                                                
# W = 10                     

# media_gc = zeros(N_Realizações_gc)
# median_gc = zeros(N_Realizações_gc)
# desvio_gc = zeros(N_Realizações_gc)
# desvio_r = zeros(N_Realizações_gc)
# Histograma_Diagrama_de_Fase = zeros(N_Realizações_gc,6)

# Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade) 
# range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)

# aux = 1
# for j in 1:N_div_Densidade
#     for i in 1:N_div_Deturn
#         condutancias_Von_Rossum = Estatisticas_gc[i,j,:]
#         Histograma_Diagrama_de_Fase[aux,1:2] = [range_Δ[i],Range_ρ[j]]
        
#         media_gc[aux] = mean(condutancias_Von_Rossum)
#         Histograma_Diagrama_de_Fase[aux,3] = media_gc[aux] 
        
#         median_gc[aux] = median(condutancias_Von_Rossum)
#         Histograma_Diagrama_de_Fase[aux,4] = median_gc[aux] 

#         desvio_gc[aux] = std(condutancias_Von_Rossum)
#         Histograma_Diagrama_de_Fase[aux,5] = desvio_gc[aux] 

#         desvio_r[aux] =  std(condutancias_Von_Rossum)/mean(condutancias_Von_Rossum)
#         Histograma_Diagrama_de_Fase[aux,6] = desvio_r[aux] 

#         aux += 1
#     end
# end

# Media = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,3])
# Mediana = hcat(Histograma_Diagrama_de_Fase[:,1:2],log10.(Histograma_Diagrama_de_Fase[:,4]))
# Variancia = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,5])
# Desvio_r = hcat(Histograma_Diagrama_de_Fase[:,1:2],Histograma_Diagrama_de_Fase[:,6])

# include("PLOTS/Plots.jl")
# vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Variancia,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{gc}^2  $",1000)
# vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Media,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\langle g_c \rangle $",1000)
# vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Mediana,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,median(g_c)  $",1000)
# vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Desvio_r,N_div_Densidade,N_div_Deturn,Densidade_Inicial*(2*π)^3,Densidade_Final*(2*π)^3,Escala_de_Densidade,L"$\theta = [50,70], kL = %$L, %$N_Realizações_gc REP,ESCALAR,%$Geometria,\sigma_{r}^2$",1000)

# savefig("Diagrama_incompleto_variancia_400_Realizações.png")