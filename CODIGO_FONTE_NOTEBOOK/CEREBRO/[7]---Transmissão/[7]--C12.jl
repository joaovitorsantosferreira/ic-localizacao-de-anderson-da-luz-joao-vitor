###############################################################################################################################################################################################################
################################################################ Programa de Controle 12 - Contempla simulações Voltadas a Transmissão da Luz #################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Interpolations

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E9.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R10.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E9.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[5]---Transmissão/R10.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                               # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                # Define a geometria da nuvem como Cubo
# GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                              # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                      # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###############################################################################################################################################################################################################

k = 1                                                                                                                       # Número de Onda
λ = (2*π)/k                                                                                                                 # Comprimento de onda 
Lₐ = 20                                                                                                                     # Dimensão Fixa Complementar 
N = 200
Ls = [5,10,15]

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                           # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                              # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de propagação da onda


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Sensores = 20000                                                                                                            # Quantidade de Sensores ao redor da amostra
Distancia_dos_Sensores = 5*sqrt(Lₐ^2 + L^2)                                                                                   # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                                                     # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 180                                                                                   # Define o angulo final da tela circular de Sensores
angulo_coerente = 30                                                         


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

########################################################################### DEFINIÇÂO 5 - Escala do Eixo Y referente a Densidade ##############################################################################
Escala_de_b = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
# Escala_de_b = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 30                                                                                                         # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
b_INICIAL = 0.01 
b_FINAL = 10
N_pontos = 10
angulo_T_INICIAL = 0
angulo_T_FINAL = 30

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R10_ENTRADA(
    Γ₀,  
    ωₐ,
    Ω,
    N,
    k,
    angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    angulo_coerente,
    Tipo_de_beta,
    N_pontos,
    N_Realizações,
    b_INICIAL,
    b_FINAL,
    angulo_T_INICIAL,
    angulo_T_FINAL,
    Escala_de_b,
    Desordem_Diagonal,
    W,
    Ls,
    Lₐ,
    Geometria
)

# Dados_C12 = ROTINA_10__Estatisticas_da_Transmission(entrada_rotina,Geometria)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Range_b = Dados_C12[1]
T = Dados_C12[2]
Intensidade_por_ponto = Dados_C12[3]


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("PLOTS/Plots.jl")
vizualizar_Transmission_and_b(Range_b,T,Ls,angulo_T_INICIAL,angulo_T_FINAL,Lₐ,Escala_de_b,1000)
# savefig("Transmission_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__5.png")  

angulo_CONTROLE_INICIAL = 80
angulo_CONTROLE_FINAL = 90
include("PLOTS/Plots.jl")
vizualizar_Transmission_and_b_especific_angle(Range_b,Intensidade_por_ponto,Ls,angulo_CONTROLE_INICIAL,angulo_CONTROLE_FINAL,Escala_de_b,Geometria,N_pontos,N_Realizações,1000)
# savefig("Transmission_RELACAO_ROMAIN_{$angulo_CONTROLE_INICIAL}_{$angulo_CONTROLE_FINAL}.png")


# savefig("Transmission_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__10.png")

# esc = 10
# angulo_intervalo = 70
# Δ = round((Γ₀/2)*sqrt((b₀/Range_b[esc]) - 1 ),digits = 2) 
# ρ_titulo = ρ*λ^3
# b₀_titulo = round(b₀,digits = 2)

# include("PLOTS/Plots.jl")
# vizualizar_degrees_of_Beer(Intensidade_por_ponto[esc,:,:],b₀,N_Realizações,angulo_intervalo,L"$ b_0 = %$b₀_titulo , \rho = %$ρ_titulo / \lambda^3 , \Delta = %$Δ, %$Geometria,N = %$N ,REP = %$N_Realizações $",1000)
# savefig("Angulos_de_Beer_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__ρ={$ρ}___b0={$b₀}__7.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C12.jld2", Dict("SAIDA" => Dados_C12,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-January-20}_Dados_Antigos_C12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-January-20}_Dados_Antigos_C12.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-February-8}_Dados_Antigos_C12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-February-8}_Dados_Antigos_C12.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-February-9}_Dados_Antigos_C12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-February-9}_Dados_Antigos_C12.jld2", "Parametros")

# Range_b = Dados_Antigos[1]
# T = Dados_Antigos[2]
# Intensidade_por_ponto = Dados_Antigos[3]

# Γ₀ = Parametros_Antigos.Γ₀  
# ωₐ = Parametros_Antigos.ωₐ
# Ω = Parametros_Antigos.Ω
# N = Parametros_Antigos.N
# k = Parametros_Antigos.k
# angulo_da_luz_incidente = Parametros_Antigos.angulo_da_luz_incidente
# vetor_de_onda = Parametros_Antigos.vetor_de_onda
# λ = Parametros_Antigos.λ
# Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
# Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
# N_Sensores = Parametros_Antigos.N_Sensores
# Angulo_de_variação_da_tela_circular_1 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_1
# Angulo_de_variação_da_tela_circular_2 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_2
# angulo_coerente = Parametros_Antigos.angulo_coerente
# Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
# N_pontos = Parametros_Antigos.N_pontos
# N_Realizações = Parametros_Antigos.N_Realizações
# b_INICIAL = Parametros_Antigos.b_INICIAL
# b_FINAL = Parametros_Antigos.b_FINAL
# angulo_T_INICIAL = Parametros_Antigos.angulo_T_INICIAL
# angulo_T_FINAL = Parametros_Antigos.angulo_T_FINAL
# Escala_de_b = Parametros_Antigos.Escala_de_b
# Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
# W = Parametros_Antigos.W
# Ls = Parametros_Antigos.Ls
# Lₐ = Parametros_Antigos.Lₐ
# Geometria = Parametros_Antigos.Geometria

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-18}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-18}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-19}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Vetorial}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-19}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Vetorial}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-25}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-25}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-27}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/DATA={2022-July-27}_Dados_Antigos_CLUSTER_C12_{SLAB}_{Escalar}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/TEMPORARIO_C12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C12/TEMPORARIO_C12.jld2", "Parametros")
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Range_b,T = Dados_Antigos
T_difuso = T[:,:,1]
T_coerente = T[:,:,2]


Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
Ω = Parametros_Antigos.Ω
k = Parametros_Antigos.k
angulo_da_luz_incidente = Parametros_Antigos.angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
λ = Parametros_Antigos.λ
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
Angulo_de_variação_da_tela_circular_1 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_1
Angulo_de_variação_da_tela_circular_2 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_2
angulo_coerente = Parametros_Antigos.angulo_coerente
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
N_pontos = Parametros_Antigos.N_pontos
N_Realizações = Parametros_Antigos.N_Realizações
b_INICIAL = Parametros_Antigos.b_INICIAL
b_FINAL = Parametros_Antigos.b_FINAL
angulo_T_INICIAL = Parametros_Antigos.angulo_T_INICIAL
angulo_T_FINAL = Parametros_Antigos.angulo_T_FINAL
Escala_de_b = Parametros_Antigos.Escala_de_b
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
L = Parametros_Antigos.L
L_titulo = round(L,digits=2)
Lₐ = Parametros_Antigos.Lₐ
Lₐ_titulo = round(Lₐ,digits=2)
Radius = Parametros_Antigos.Radius
ρs = Parametros_Antigos.ρs
ω₀ = Parametros_Antigos.ω₀
Geometria = Parametros_Antigos.Geometria
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
PERFIL_DA_INTENSIDADE = Parametros_Antigos.PERFIL_DA_INTENSIDADE
Tipo_de_campo = Parametros_Antigos.Tipo_de_campo
angulo_coerente = Parametros_Antigos.angulo_coerente


include("PLOTS/Plots.jl")
vizualizar_transmissão_por_b0(T_coerente,Range_b,"LINEAR",ρs,20,L"$L = %$L_titulo,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP, log_{10}(\rho / k^2) $",1000)
# savefig("Tramissão_por_b0_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_coerente.png")



include("PLOTS/Plots.jl")
vizualizar_transmissão_por_b0(T_difuso,Range_b,"LINEAR",ρs,20,L"$L = %$L_titulo,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP, log_{10}(\rho / k^2) $",1000)
# savefig("Tramissão_por_b0_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_difusa.png")






# xdata = load("RESULTADOS/DADOS ANTIGOS/C16/DADOS_ENERGIA.jld2", "SAIDA")[1]
# ydata = load("RESULTADOS/DADOS ANTIGOS/C16/DADOS_ENERGIA.jld2", "SAIDA")[2]

# i += 1
# ρ = ρs[i]
# itp = LinearInterpolation(xdata,ydata)
# Δ_loc = itp(ρ)

# N_instantaneo = round(Int64,ρ*L*Lₐ)

# if Tipo_de_kernel == "Escalar"            
#     b₀ =  (4*N_instantaneo)/(Lₐ)
# elseif Tipo_de_kernel == "Vetorial"        
#     b₀ =  (8*N_instantaneo)/(Lₐ)
# end

# bδ = (b₀/(4*(Δ_loc^2) +1))/10 

# vline!([bδ])