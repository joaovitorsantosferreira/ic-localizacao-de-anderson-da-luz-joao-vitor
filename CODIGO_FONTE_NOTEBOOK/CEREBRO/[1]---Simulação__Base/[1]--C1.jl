###############################################################################################################################################################################################################
##################################################################### Programa de Controle 1 - Contempla todas as funcionalidades do codigo ###################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------ Adicionando os processadores a serem utilizados ------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances
using LinearAlgebra
using LaTeXStrings
using Random
using Statistics
using Plots
using SpecialFunctions
using QuadGK
using PlotThemes
using StatsBase
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")

include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P7.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E1.jl")
include("PLOTS/Plots.jl")
# include("PLOTS/Animation.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                               # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                # Define a geometria da nuvem como Cubo
# GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                              # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                      # Define a geometria da nuvem como Paralelepipedo
# GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                  # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
 
###############################################################################################################################################################################################################

k = 1                                                                                                                        # Número de Onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
# ρ = 5/λ^3                                                                                                                   # Densidade normalidade, ou ρ/k²
ρ = 1
Lₐ = 100                                                                                                                      # Dimensão Fixa Complementar 

############################################################################################## Parametros da Nuvem #############################################################################################
#-------------------------------------------------------------------------------------------- Número de Atomos Fixo ------------------------------------------------------------------------------------------#
N = 2000                                                                                                                   # Número de Átomos da Nuvem 
Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
#------------------------------------------------------------------------------------------------ Dimensão Fixa ----------------------------------------------------------------------------------------------#
# L = 20                                                                                                                     # Espessura da Nuvem, tanto para o 2D, quanto para o 3D 
# Radius = 20                                                                                                                # Raio da Nuvem, funciona apenas para a geometria de Esfera e Disco
# N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(L,Radius,Lₐ,ρ,GEOMETRIA_DA_NUVEM)
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                           # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                              # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ =0                                                                                                                        # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
# vetor_de_onda = (0,0,1)

ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

################################################################################### DEFINIÇÂO 5 - Regime das Fases da dinâmica atomica ########################################################################
PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"                               
# PERFIL_DA_INTENSIDADE = "SIM,para obter a cintura do feixe"                               
# PERFIL_DA_INTENSIDADE = "NAO"                               
###############################################################################################################################################################################################################


N_Sensores = 1000                                                                                                               # Quantidade de Sensores ao redor da amostra
N_Telas = 1         
# comprimento_de_ray = (k*(ω₀^2))/2                                                                                            # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
# Distancia_dos_Sensores = (5/2)*sqrt((L/2)^2+Lₐ^2)                                                                              # Define a distacia entre os sensores e os sistema
Distancia_dos_Sensores = λ                                                                                                  # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 50                                                                                     # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 70                                                                                     # Define o angulo final da tela circular de Sensores
# N_Sensores = (Angulo_de_variação_da_tela_circular_2 - Angulo_de_variação_da_tela_circular_1)*720                                                       

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 6 - Desordem Diagonal ######################################################################################|
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


if Tipo_de_kernel == "Escalar"
    b₀ = (8*N)/(π*k*Radius)

elseif Tipo_de_kernel == "Vetorial"
    b₀ = (16*N)/(π*k*Radius)
end

# W = 1*b₀                                                                                                                      # Amplitude de Desordem
W = 0
# b₀ = 400
# Radius = 5.641895835477563    
# N = (π*Radius)*b₀/16


# Radius = 5.641895835477563
# ρ = 3
# N = round(Int64,ρ*π*Radius^2)  
# b₀ = (16*N)/(π*Radius)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Gerando e Extraindo Dados da Nuvem -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Entrada = E1_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    PERFIL_DA_INTENSIDADE,
    N_Telas,
    Geometria
)


@time Dados_C1 = E1_extração_de_dados_Geral(Entrada)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Resultados:

cloud = Dados_C1.cloud
r = cloud.r                                                                                                                                # Posições de todos os atomos
λₙ = cloud.λ                                                                                                                                # Autovalores da Matriz de Green
ψ = cloud.ψ                                                                                                                                # Autovetores da Matriz de Green
R_jk = cloud.R_jk                                                                                                                          # Matriz que guarda a distância entre os atomos
G = cloud.G                                                                                                                                # Matriz de Green do sistema 
b₀ = cloud.b₀                                                                                                                              # Densidade Otica 

propriedades_fisicas_global = Dados_C1.propriedades_fisicas_global
IPRs = propriedades_fisicas_global.IPRs                                                                                                    # Indice de parcipação de cada modo
ξₙ = propriedades_fisicas_global.ξₙ                                                                                                         # Comprimento de localização de cada modo
Eₙ = propriedades_fisicas_global.Eₙ                                                                                                         # Erro cometido em cada comprimento de localização
γₙ = propriedades_fisicas_global.γₙ                                                                                                         # Tempo de vida de cada modo
ωₙ = propriedades_fisicas_global.ωₙ                                                                                                         # Frequência de cada modo
qₙ = propriedades_fisicas_global.q                                                                                                          # Tempo de decaimento de cada atomo
Sₑ = propriedades_fisicas_global.Sₑ                                                                                                        # Tempo de vida de cada modo  
R_cm_n = propriedades_fisicas_global.R_cm_n                                                                                                # Centro de Massa de cada modo


propriedades_fisicas_best_mode = Dados_C1.propriedades_fisicas_best_mode
IPRᵦ = propriedades_fisicas_best_mode.IPRᵦ                                                                                                 # Indice de parcipação do melhor modo
ξᵦ = propriedades_fisicas_best_mode.ξᵦ                                                                                                     # Comprimento de localização do melhor modo  
Eᵦ = propriedades_fisicas_best_mode.Eᵦ                                                                                                     # Erro cometido no comprimento de localização do melhor modo
γᵦ = propriedades_fisicas_best_mode.γᵦ                                                                                                     # Tempo de vida do melhor modo
ωᵦ = propriedades_fisicas_best_mode.ωᵦ 
qᵦ = propriedades_fisicas_best_mode.qᵦ
Sₑᵦ = propriedades_fisicas_best_mode.Sₑᵦ                                                                                                   # Tempo de decaimento do melhor modo
RG_best = propriedades_fisicas_best_mode.RG_best                                                                                           # Indexação do modo mais subradiante
Dcmᵦ = propriedades_fisicas_best_mode.Dcmᵦ
Rcmᵦ = propriedades_fisicas_best_mode.Rcmᵦ                                                                                                 # Centro massa do melhor modo
ψᵦ² = propriedades_fisicas_best_mode.ψᵦ²

βₙ = Dados_C1.βₙ                                                                                                                            # Matriz que guarda a dinâmica dos atomos

Posição_Sensores = Dados_C1.Posição_Sensores                                                                                               # Posição dos Sensores

All_Intensitys = Dados_C1.All_Intensitys                                                                                                   # Intensidade da luz expalhada nas direções definidas
Intensidade_Normalizada = All_Intensitys[3,:]/mean(All_Intensitys[3,:])

g = get_number_thouless(γₙ ,ωₙ ,Δ, Γ₀)                                                                        # Número de Thouless definido para a banda de valores selecionada pelo Deturn: [Δ - Γ₀/2,Δ + Γ₀/2]
Σ² = get_Σ2(γₙ ,ωₙ ,Δ, Γ₀,1)
# ρ_titulo = round(ρ*λ^3,digits=2)
ρ_titulo = round(ρ,digits=2)
L_titulo = round(L,digits=2)
R_titulo = round(Radius,digits=2)
W_titulo = round(W/b₀,digits=2)

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Graficos e Vizualização do Sistema -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# vizualizar_distribuição_dos_atomos(r,Radius,L,GEOMETRIA_DA_NUVEM,Geometria,Lₐ,1000)
# savefig("fig1__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_10.png")

# vizualizar_o_modo_mais_subrandiante(r,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,IPRᵦ,Geometria,1000)
# savefig("fig2__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}__7.png")

# vizualizar_fit_modo_mais_subradiante(Dcmᵦ,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,RG_best,ψ,Eᵦ,1000)
# savefig("fig5__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}__9.png")

# vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmᵦ,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,Geometria,1000)
# savefig("distribu_Intensidades__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_12.png")

# vizualizar_o_modo_mais_subrandiante_no_disco_3D(r,ψᵦ²,1000)
# savefig("perfil_3D___2.png")

include("PLOTS/Plots.jl")
# vizualizar_relação_entre_gamma_e_omega_IPR(ωₙ,γₙ,IPRs,Δ,Γ₀,g,Σ²,Tipo_de_kernel,Desordem_Diagonal,W,b₀,L"$ \rho / k^2 =%$ρ_titulo, %$Geometria,W /b_0 = %$W_titulo ,kR = %$R_titulo, %$Tipo_de_kernel  - IPR $",1000)
# savefig("Modos pares 2.png")

# Espectro_plano = zeros(size(λₙ,1),2)
# r_plano = zeros(size(λₙ,1)) 
# Espectro_plano[:,1] .= imag.(λₙ) 
# Espectro_plano[:,2] .= real.(λₙ)

# for i in 1:size(λₙ,1)
#     r_plano[i] = sqrt(Espectro_plano[i,1]^2 + Espectro_plano[i,2]^2)
# end

# Range_L = get_points_in_log_scale(0.1,30,20) 
# N_Realizações = 1
# Σ² = zeros(20)


# for t in 1:20 
#     Σ²[t] = size(findall( r_plano .<= Range_L[t] ),1)/N_Realizações
# end

# plot(Range_L,Σ²,xscale=:log10,yscale=:log10)
# scatter(ωₙ,γₙ)

vizualizar_relação_entre_gamma_e_omega_R2(ωₙ,γₙ,Eₙ,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,b₀,L"$ \rho\lambda^3 =%$ρ_titulo,Geometria = %$Geometria,kL = %$L_titulo,kR = %$Lₐ, %$Tipo_de_kernel -  R^2 $",1000)
savefig("fig8__N={$N}__Densidade={$ρ}__kR={$Radius}_Δ={$Δ}__Kernel={$Tipo_de_kernel}_3.png")
# savefig("Relatorio_Espectro_de_autovalores_figura_4_MODELO_1__N={$N}__Densidade={$ρ}__kR={$Radius}_Δ={$Δ}__Kernel={$Tipo_de_kernel}.png")
# vizualizar_relação_entre_gamma_e_omega_R2(ωₙ,γₙ,log10.(abs.(ξₙ/(sqrt(L^2 + Lₐ^2)))),Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,b₀,1000)
# savefig("Espectro_R2_3.png")
# println(sort(ξₙ))

# include("PLOTS/Plots.jl")
# vizualizar_relação_entre_gamma_e_comprimento_de_localização_R_cm(ξₙ,γₙ,R_cm_n,Radius,Lₐ,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel ,1000)
# savefig("fig4__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_4.png")

# vizualizar_relação_entre_gamma_e_R_cm(γₙ,R_cm_n,Tipo_de_kernel,1000)
# savefig("fig4__8__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_3.png")

# vizualizar_relação_entre_gamma_e_comprimento_de_localização_R²(ξₙ,γₙ,Eₙ,Radius,Lₐ,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel,1000)
# savefig("fig4__7__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_4.png")

# include("PLOTS/Plots.jl")
# vizualizar_Intensidade_nos_sensores(r, All_Intensitys,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,10^6,Geometria,Distancia_dos_Sensores,1000)
# savefig("fig6__N={$N}__Densidade={$ρ}__N={$N}_Δ={$Δ}_Kernel={$Tipo_de_kernel}_34.png")

# vizualizar_Intensidade_ao_longo_dos_angulos(All_Intensitys,PERFIL_DA_INTENSIDADE,1000)
# savefig("distribu_Intensidades__N={$N}__Densidade={$ρ}__Kernel={$Tipo_de_kernel}_6.png")

# vizualizar_relação_entre_q_e_Sₑ(qₙ,Sₑ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,1000)
# savefig("Metodo_de_Pipek__N={$N}__Densidade={$ρ}__GEO={$GEOMETRIA_DA_NUVEM}__Kernel={$Tipo_de_kernel}__5.png")

# vizualizar_Perfil_da_Cintura_nos_sensores(r, Intensidade_Normalizada,Posição_Sensores,Radius,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,4,1000)
# savefig("Perfil_da_Cintura_do_Laser__N={$N}__Densidade={$ρ}__N={$N}_Δ={$Δ}_Kernel={$Tipo_de_kernel}_24.png")

# vizualizar_Dinamica_dos_atomos(r,βₙ ,Tipo_de_kernel,Radius,round(Δ,digits=2),L,Lₐ,GEOMETRIA_DA_NUVEM,0,round(b₀,digits=2),1000)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Vizualizar Modo Especifico ---------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Rcm_norm = zeros(N)
# for i in 1:N 
#     Rcm_norm[i] = norm(R_cm_n[i,:])
# end

# # escala_IPR = sortperm(IPRs)

# escala_IPR = findall((ξₙ.>0).*(ξₙ.< L/2).*(IPRs .>0.4).*(Rcm_norm./L .< 0.3 ))
# ωₑ = zeros(size(escala_IPR,1))

# # Define o modo a ser observado, deve ser selecionado um número inteiro

# i = 1 
# Modo_de_Interresse = escala_IPR[i]

# propriedades_fisicas_modo_especifico = get_propriedade_to_specific_mode(cloud,propriedades_fisicas_global,Tipo_de_kernel,Modo_de_Interresse,Geometria)

# IPRₑ = propriedades_fisicas_modo_especifico.IPRᵦ                                                                                                                             
# Dcmₑ = propriedades_fisicas_modo_especifico.Dcmᵦ
# ψₑ² = propriedades_fisicas_modo_especifico.ψᵦ²
# Eₑ =  propriedades_fisicas_modo_especifico.Eᵦ
# # ωₑ[i] = ωₙ[Modo_de_Interresse]  


# include("PLOTS/Plots.jl")
# vizualizar_o_modo_especifico_no_disco(cloud.r,ψₑ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,IPRₑ,Modo_de_Interresse,700)
# savefig("fig1__N={$N}__Densidade={$ρ}__IPR={$IPRₑ}__Radius={$Radius}.png")

# include("PLOTS/Plots.jl")
# vizualizar_o_modo_mais_subrandiante_no_disco_3D(cloud.r,ψₑ²,1000)


# include("PLOTS/Plots.jl")
# vizualizar_fit_modo_mais_subradiante(Dcmₑ,ψₑ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,Modo_de_Interresse,cloud.ψ,Eₑ,1000)

# include("PLOTS/Plots.jl")
# vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmₑ,ψₑ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,800)
# # savefig("fig2__N={$N}__Densidade={$ρ}__IPR={$IPRₑ}__Radius={$Radius}.png")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------- Estatisticas da Intensidade Espalhada  -----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Intensidade_Resultante_NORMALIZADA = All_Intensitys[1,:]/mean(All_Intensitys[1,:])

# Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],30)
# Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],30)

# # @time gc = get_gc_Von_Rossum(Histograma_LINEAR)

# xdata = collect(Histograma_LINEAR.edges[1])[2:end]
# # xdata = range(0.001,10,length = 30)
# B = exp.(model_von_Rossum(xdata,gc))

# vizualizar_Histograma_das_Intensidade(Histograma_LINEAR,1000)

# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)

# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)

# variancia = round(std(Intensidade_Resultante_NORMALIZADA)^2,digits = 2)
# variancia = get_variancia(Intensidade_Resultante_NORMALIZADA)
# h_normalizado = normalize(Histograma_LINEAR;mode=:pdf)
# R² = compute_R2(log.(h_normalizado.weights), log.(B))


# gc_plot = round(gc,digits=3)
# R²_plot = round(R²,digits=3)
# variancia_plot =  round(variancia,digits= 3)
# densidade_plot = round(Int64, ρ*λ^3)
# DELTA = Δ
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)
# plot!(xdata,B,lab = L"\textrm{Lei de Von Rossum}",lw=8,color = :green, linestyle = :dash,legend =:bottomleft,    titlefontsize = 20,title = L"$g_c = %$gc_plot,R^2 = %$R²_plot,\sigma_I = %$variancia_plot, \rho \lambda^3 = %$densidade_plot , \Delta / \Gamma_0 = 0.9 $")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------- Vizualizar Perfil dos Lasers ---------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# N_Colunas = 1000
# N_pontos = 1000
# Limite = 20
# Ω = 1
# Tipo_de_Onda = "Laser Gaussiano"                                                                                                  
# Tipo_de_Onda = "Onda Plana"                                                                                                     
# Tipo_de_Onda = "FONTE PONTUAL"


# vizualizar_intensidade_e_perfil_do_feixe(N_Colunas,N_pontos,Limite,Ω,Tipo_de_Onda)
# savefig("Perfil_do_Laser_pontutal_1.png")


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------------------------- Gif"s -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Influência da Desordem Diagonal no Espectro de Autovalores -----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Range_1 = 0
# espaçamento = 0.2
# Range_2 = 10
# frames = animar_a_Influencia_da_Desordem_Diagonal_Espectro(Entrada,Range_1,espaçamento,Range_2)
# gif(frames,"animação_do_plano_complexo__1.gif",fps=3)

# Range_1 = -5
# espaçamento = 0.5
# Range_2 = 6
# N_Realizações = 10
# anim,g_tot,Σ²_tot,inter = animar_o_estudo_do_espectro(Entrada,Range_1,espaçamento,Range_2,N_Realizações)
# gif(anim,"animação_do_plano_complexo__8.gif",fps=5)
# mp4(anim,"animação_do_plano_complexo__8.mp4",fps=5)

# ρ_titulo = ρ*λ^3
# L_titulo = round(L,digits=2)
# titulo = L"$ \rho\lambda^3 =%$ρ_titulo,Geometria = %$Geometria,kL = %$L_titulo,kR = %$Lₐ, %$Tipo_de_kernel $"
# plot(inter,Σ²_tot,
# seriestype = :bar,
# color = :orange,
# label = "",
# gridalpha = 0.5,
# size = (1000, 500),
# left_margin = 10Plots.mm,
# right_margin = 10Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 5Plots.mm,
# framestyle = :box,
# title = titulo,
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black
# )
# xlabel!(L"$\Delta$")
# ylabel!(L"$\Sigma^2$")

# savefig("Sigma_3.png")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C1.jld2", Dict("SAIDA" => Dados_C1,"Parametros" => Entrada))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################





#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# #---- Escalar ---#
# f1(x) = 1 + SpecialFunctions.besselh(0,1,x) 
# f2(x) = 1 - SpecialFunctions.besselh(0,1,x)

# x = 0.1:0.1:100
# c1 = f1.(x)
# c2 = f2.(x) 

# r1 = real.(c1)
# i1 = imag.(c1)

# r2 = real.(c2)
# i2 = imag.(c2)

# plot(i1,r1)
# plot!(i2,r2)

#---- Vetorial ---#
# f1(x) = 1 + SpecialFunctions.besselh(0,1,x) + SpecialFunctions.besselh(2,1,x) 
# f2(x) = 1 - SpecialFunctions.besselh(0,1,x) - SpecialFunctions.besselh(2,1,x)
# f3(x) = 1 + SpecialFunctions.besselh(0,1,x) - SpecialFunctions.besselh(2,1,x) 
# f4(x) = 1 - SpecialFunctions.besselh(0,1,x) + SpecialFunctions.besselh(2,1,x)

# x = 0.1:0.01:100
# c1 = f1.(x)
# c2 = f2.(x) 
# c3 = f3.(x)
# c4 = f4.(x) 


# r1 = real.(c1)
# i1 = imag.(c1)

# r2 = real.(c2)
# i2 = imag.(c2)

# r3 = real.(c3)
# i3 = imag.(c3)

# r4 = real.(c4)
# i4 = imag.(c4)

# plot!(i1,r1,yscale = :log10,c=:black,lw=6,label="")
# plot!(i2,r2,yscale = :log10,c=:black,lw=6,label="")
# plot!(i3,r3,yscale = :log10,c=:black,lw=6,label="")
# plot!(i4,r4,yscale = :log10,c=:black,lw=6,label=L"Modos Pares")


#---- Vetorial - Desordem---#


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Conta Romain colaboração UFRJ
# k = 1                                             
# λ = (2*π)/k
# Radius = 25
# ω₀ = Radius/2


# # Angulo de divergencia 
# θ = (λ/(π*ω₀))*(180/pi)

# # Cintura do Laser
# z = 50/k
# zᵣ = (π*(ω₀^2))/λ
# ω = ω₀*sqrt(1 + (z/zᵣ)^2)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




# Intensidades = Any[]
# angulos = Any[]
# N_Realizações = 1000

# ProgressMeter.@showprogress 1 "Extraido Estatisticas da Luz===>" for i in 1:N_Realizações 

#     entrada_Extratora = E2_ENTRADA(
#         Entrada.Γ₀,
#         Entrada.ωₐ,
#         Entrada.Ω,
#         Entrada.k,
#         Entrada.N,
#         Entrada.Radius,
#         Entrada.ρ,
#         Entrada.rₘᵢₙ,
#         Entrada.Angulo_da_luz_incidente,
#         Entrada.vetor_de_onda,
#         Entrada.ω₀,
#         Entrada.λ,
#         Entrada.ωₗ,
#         Entrada.Δ,
#         Entrada.Tipo_de_kernel,
#         Entrada.Tipo_de_Onda,
#         Entrada.N_Sensores,
#         Entrada.Distancia_dos_Sensores,
#         Entrada.Angulo_de_variação_da_tela_circular_1,
#         Entrada.Angulo_de_variação_da_tela_circular_2,
#         Entrada.Tipo_de_beta,
#         Entrada.Desordem_Diagonal,
#         Entrada.W,
#         Entrada.GEOMETRIA_DA_NUVEM,
#         Entrada.L,
#         Entrada.Lₐ,
#         Entrada.PERFIL_DA_INTENSIDADE,
#         N_Telas,
#         Entrada.Geometria
#     )

#     All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)       

#     append!(Intensidades,All_Intensitys[1,:])
#     append!(angulos,All_Intensitys[3,:])
# end

# Var_anel = zeros(size(Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2))
# aux = 1
# for i in Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2
#     index_in_range = findall(angulos .≈ i)
#     Intensidade_Anel = Intensidades[index_in_range]/mean(Intensidades[index_in_range])
#     # Var_anel[aux] = std(Intensidade_Anel)^2
#     Var_anel[aux]  = mean(Intensidade_Anel.^2) - (mean(Intensidade_Anel))^2
#     aux += 1
# end
# gr()
# plot((Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2)*(1/180),Var_anel,
# size = (1000, 500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0.3,
# ylims = (0.1,10),
# xlims = (0,1),
# lw = 5,
# framestyle = :box,
# label = "",
# camera=(45,45),
# c = :green,
# xticks = 0:0.1:1,
# # colorbar_title = L"$log_{10}(p_i)$",
# title = L"$\rho \lambda^3 = 33, \Delta = 0.9, 1000 Rep$",
# yscale = :log10,
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 25,
# tickfontsize = 15, 
# foreground_color_legend = :black)

# xlabel!(L"\theta/\pi")
# ylabel!(L"$\sigma^2_I$")

# vline!([0.25/2]; label=L"$\phi = \pi/8$", linestyle=:dashdot, color=:blue, linewidth=5)
# vline!([0.25]; label=L"$\phi =\pi/4$", linestyle=:dashdot, color=:black, linewidth=5)
# vline!([0.5]; label=L"$\phi =\pi/2$", linestyle=:dashdot, color=:red, linewidth=5)
# hline!([1]; label="", linestyle=:solid, color=:black, linewidth=2)





# A = ((1+0.98+0.65+1+1)/5)*0.1

# B = ((9.1+5.43)/3)*0.9

# C = A + B

# D = 6 - C

# p3 = D*3/0.9

