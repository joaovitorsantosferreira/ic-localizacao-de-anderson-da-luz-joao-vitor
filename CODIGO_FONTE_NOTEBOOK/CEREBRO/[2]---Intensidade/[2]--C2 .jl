###############################################################################################################################################################################################################
################################################################# Programa de Controle 2 - Contempla simulações Voltadas a Estatistica da Luz #################################################################
###############################################################################################################################################################################################################

"""
Neste programa são gerados dados sobre a distribuição de probabilidade da Intensidade que sai do sistema, sendo gerados histogramas que vão representar a quantidade de detecções de uma determinada 
intensidade nos sensores. A partir dos dados fornecidos pelo Histograma teremos como determinando as probabilidades, naturalmeente esperamos que essas estatisticas caiam na Lei de Rayleigh para o
espalhamento, mas em determinados regimes observamos desvios claros na estatisticas.

Para explicar os desvios são colocadas duas Leis que vão explicar como as estatisticas da Luz se comportam em caso de desvio, são as Leis:

Lei de Kogan
Lei de Shnerb

Essas Leis são oriundas de um grupo israelita liderado por Kaveh que nas decadas de 80 e 90 trabalhos com esses desvios, mas não no constexto de localização de Anderson.  
"""

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E6.jl")
include("PLOTS/Plots.jl")
include("PLOTS/Animation.jl") 
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[1]---Estatistiscas__Da__Intensidade/R1.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                 # Define a geometria da nuvem como Cubo
# GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                               # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                 # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###############################################################################################################################################################################################################

k = 1                                                                                                                        # Número de Onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ρ = 33/λ^3                                                                                                                     # Densidade normalidade, ou ρ/k²
Lₐ = 20                                                                                                                      # Dimensão Fixa Complementar 

############################################################################################## Parametros da Nuvem #############################################################################################
#-------------------------------------------------------------------------------------------- Número de Atomos Fixo ------------------------------------------------------------------------------------------#
N = 100                                                                                                                    # Número de Átomos da Nuvem 
Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
#------------------------------------------------------------------------------------------------ Dimensão Fixa ----------------------------------------------------------------------------------------------#
# L = 25                                                                                                                        # Espessura da Nuvem, tanto para o 2D, quanto para o 3D 
# Radius = 10                                                                                                                  # Raio da Nuvem, funciona apenas para a geometria de Esfera e Disco
# N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(L,Radius,Lₐ,ρ,GEOMETRIA_DA_NUVEM)
###############################################################################################################################################################################################################




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ = 0                                                                                                                       # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de propagação da onda
ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


################################################################################### DEFINIÇÂO 5 - Regime das Fases da dinâmica atomica ########################################################################
PERFIL_DA_INTENSIDADE = "NAO"                               
###############################################################################################################################################################################################################


N_Sensores = 360                                                                                                               # Quantidade de Sensores ao redor da amostra
comprimento_de_ray = (k*(ω₀^2))/2                                                                                             # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
# Distancia_dos_Sensores = 10*Radius                                                                                          # Define a distacia entre os sensores e os sistema
Distancia_dos_Sensores = (5/2)*sqrt(L^2+Lₐ^2)
Angulo_de_variação_da_tela_circular_1 = 50                                                                                    # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 70                                                                                    # Define o angulo final da tela circular de Sensores
                                                             

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Realizações = 100                                                # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
N_div = 50                                                         # Define o número de divisões do histograma, esse valor pode ser alterado apos a simulação no modulo "Adaptação dos Histogramas" logo abaixo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 5 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


entrada_rotina = R1_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    N_Realizações,
    N_div,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    PERFIL_DA_INTENSIDADE,
    Geometria
)

# Dados_C2 = ROTINA_1__Estatisticas_da_Intensidade(entrada_rotina)



###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Intensidade_Resultante_PURA = Dados_C2.Intensidade_Resultante_PURA                                     # Intensidade pura, ou seja, aquela que é detectada nos sensores sem nenhuma normalização, valores pequenos 
Intensidade_Resultante_NORMALIZADA = Dados_C2.Intensidade_Resultante_NORMALIZADA                       # Intensidade Normalizada, ou seja, são os dados da Intensidade Pura Normalizados
Histograma_LINEAR = Dados_C2.Histograma_LINEAR                                                         # Histograma com separação Linear entre as faixas de analise(Colunas)
Histograma_LOG = Dados_C2.Histograma_LOG                                                               # Histograma com separação Logaritmica entre as faixas de analise(Colunas)
variancia = Dados_C2.variancia                                                                         # Variancia da Intensidade Normalizada



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Adaptação dos Histogramas ----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],30)
Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],50)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------ Caso Linear ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


vizualizar_Histograma_das_Intensidade(Histograma_LINEAR,1000)

include("PLOTS/Plots.jl")
vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}__σ={$variancia}_LINEAR_9.png")
# savefig("Relario_Cientifico DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__LINEAR.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log(Histograma_LINEAR,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}desvio_8.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------- Caso Log --------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# vizualizar_Histograma_das_Intensidade(Histograma_LOG,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}6.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LOG,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}5.png")


vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)
# savefig("DistriProba__Shnerb_Law__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_desvio_19.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_especial(Histograma_LOG,variancia,Δ,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_especial_desvio_1.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Kogan_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_GEOMETRIA_DA_NUVEM={$GEOMETRIA_DA_NUVEM}__Desvio_Kogan_1.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Intensidade média ao longo dos angulo ----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# include("PLOTS/Plots.jl")
# vizualizar_media_da_Intentensidade_ao_longo_dos_angulos(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,N_Sensores,N_Realizações,Δ,ρ,400)
# savefig("INT_MED__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}__D={$Distancia_dos_Sensores}.png")


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------------------------- Gif"s -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Animar Perfil da Distribuição de Probabilidade da Intensidade -------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Range_1 = 0
Range_2 = 1
espaçamento = 0.5
# frames = animar_perfil_da_probabilidade_para_diferentes_Δs(entrada_rotina,Range_1,espaçamento,Range_2)
# gif(frames,"animação_Perfil_da_Probabilidade_da_Intensidade__2.gif",fps=2)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Animar Perfil da Intensidade Média ao longo dos ângulos -----------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Range_1 = -2
Range_2 = 2
espaçamento = 1
# frames = animar_comportamento_da_Intensidade_Media(entrada_rotina,Range_1,espaçamento,Range_2)
# gif(frames,"animação_Intensidade_media_1.gif",fps=2)


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C2.jld2", Dict("SAIDA" => Dados_C2,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-November-2}_Dados_Antigos_C2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-November-2}_Dados_Antigos_C2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-November-16}_Dados_Antigos_C2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-November-16}_Dados_Antigos_C2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_PURO.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_PURO.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_10.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_10.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_100.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C2_100.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W=1000.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W=1000.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W=10000.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W=10000.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2_W2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C2.jld2", "Parametros")


Intensidade_Resultante_PURA = Dados_Antigos[1,:]                                     
Intensidade_Resultante_NORMALIZADA = Intensidade_Resultante_PURA/mean(Intensidade_Resultante_PURA)                      

Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
Ω = Parametros_Antigos.Ω
k = Parametros_Antigos.k
N = Parametros_Antigos.N
Radius = Parametros_Antigos.Radius
Radius_plot = round(Radius,digits=2)
ρ = Parametros_Antigos.ρ
rₘᵢₙ = Parametros_Antigos.rₘᵢₙ
angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
ω₀ = Parametros_Antigos.ω₀
λ = Parametros_Antigos.λ
ωₗ = Parametros_Antigos.ωₗ
Δ = Parametros_Antigos.Δ
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
Angulo_de_variação_da_tela_circular_1 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_1
Angulo_de_variação_da_tela_circular_2 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_2
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
N_Realizações = Parametros_Antigos.N_Realizações
N_div = Parametros_Antigos.N_div
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
L = Parametros_Antigos.L
Lₐ = Parametros_Antigos.Lₐ
PERFIL_DA_INTENSIDADE = Parametros_Antigos.PERFIL_DA_INTENSIDADE
Geometria = Parametros_Antigos.Geometria
variancia = round(get_dados_to_diagram_fase(Dados_Antigos)[1],digits=3)

if Tipo_de_kernel == "Escalar"
    b₀ = round((8*N)/(π*k*Radius),digits=2)

elseif Tipo_de_kernel == "Vetorial"
    b₀ = round((16*N)/(π*k*Radius),digits=2)
end

if Desordem_Diagonal == "PRESENTE"
    W = round(W/b₀,digits=2)
else
    W = 0
end



Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],30)
Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],100)

# include("PLOTS/Plots.jl")
# vizualizar_Histograma_das_Intensidade(Histograma_LINEAR,1000)

# include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)

# include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log(Histograma_LINEAR,1000)

# include("PLOTS/Plots.jl")
# vizualizar_Histograma_das_Intensidade(Histograma_LOG,1000)

# include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LOG,1000)

include("PLOTS/Plots.jl")
vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,L"%$Tipo_de_kernel,%$N_Realizações REP, \Delta = %$Δ, \rho / k^2 = %$ρ,kR = %$Radius_plot,\sigma^2_I = %$variancia,W/b_0 = %$W ",1000)
# savefig("ESTATISTICAS_VETORIAL_COMPARA.png")

# include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_especial(Histograma_LOG,variancia,Δ,1000)

# include("PLOTS/Plots.jl")
# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Kogan_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Intensidades = Dados_Antigos[1,:]
# angulos = Dados_Antigos[3,:]
# b₀ = round(4*π*N/(L^2),digits = 2)
# beer = round(log10(exp(-4*π*N/(L^2))),digits = 2)
# # b₀ = round(N/(((2*π)^(3/2))*(Radius)^3),digits = 2)
# Trans_anel = zeros(size(0:90))
# aux = 1

# for i in 0:90
#     index_in_range = findall(angulos .≈ i)
#     Intensidade_Anel = sum(Intensidades[index_in_range])
#     Intensidade_Incidente = sum(Intensidades[findall((angulos .< 180).*(angulos .> 90) )])
#     Trans_anel[aux]  = Intensidade_Anel/Intensidade_Incidente
#     aux += 1
# end


# gr()
# plot((0:90)*(1/180),Trans_anel,
# size = (1000, 500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0.3,
# ylims = (0.000001,1),
# xlims = (0,0.5),
# lw = 5,
# framestyle = :box,
# label = "",
# c = :green,
# xticks = 0:0.1:1,
# # colorbar_title = L"$log_{10}(p_i)$",
# title = L"$N=6066,\rho \lambda^3 = 1, \delta = 1,Escalar,100 REP,b_0 = %$b₀$",
# yscale = :log10,
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 20,
# tickfontsize = 20, 
# foreground_color_legend = :black)

# xticks!(0:0.125:0.5,[L"$0$",L"$\pi/8$",L"$\pi/4$",L"$3\pi/8$",L"$\pi/2$"])
# xlabel!(L"\phi")
# ylabel!(L"$T$")

# hline!([exp(-4*π*N/(L^2))]; label= L"$ log_{10}(e^{-b_0}) = %$beer $", linestyle=:solid, color=:black, linewidth=2)



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Dados_Antigos = All_Intensitys
# Intensidades = Dados_Antigos[1,:]
# angulos = Dados_Antigos[3,:]

# Var_anel = zeros(size(Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2))
# aux = 1
# for i in Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2
#     index_in_range = findall(angulos .≈ i)
#     Intensidade_Anel = Intensidades[index_in_range]/mean(Intensidades[index_in_range])
#     # Var_anel[aux] = std(Intensidade_Anel)^2
#     Var_anel[aux]  = mean(Intensidade_Anel.^2) - (mean(Intensidade_Anel))^2
#     aux += 1
# end
# gr()
# plot((Angulo_de_variação_da_tela_circular_1:Angulo_de_variação_da_tela_circular_2)*(1/180),Var_anel,
# size = (1000, 500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0.3,
# ylims = (0.00001,100000),
# xlims = (0,1),
# lw = 5,
# framestyle = :box,
# label = "",
# camera=(45,45),
# c = :green,
# xticks = 0:0.1:1,
# # colorbar_title = L"$log_{10}(p_i)$",
# title = L"$N=6066,\rho \lambda^3 = 44, \delta = 1,Escalar, \theta = 0,100 REP$",
# yscale = :log10,
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 20,
# tickfontsize = 20, 
# foreground_color_legend = :black)

# xticks!(0:0.25:1,[L"$0$",L"$\pi/4$",L"$\pi/2$",L"$3\pi/4$",L"$\pi$"])
# xlabel!(L"\phi")
# ylabel!(L"$\sigma^2_I$")

# vline!([0.25/2]; label=L"$\phi = \pi/8$", linestyle=:dashdot, color=:blue, linewidth=5)
# vline!([0.25]; label=L"$\phi =\pi/4$", linestyle=:dashdot, color=:black, linewidth=5)
# vline!([0.5]; label=L"$\phi =\pi/2$", linestyle=:dashdot, color=:red, linewidth=5)
# hline!([1]; label="", linestyle=:solid, color=:black, linewidth=2)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# @time gc = get_gc_Von_Rossum(Histograma_LINEAR)

# xdata = collect(Histograma_LINEAR.edges[1])[2:end]
# # xdata = range(0.001,10,length = 30)
# B = exp.(model_von_Rossum(xdata,gc))


# variancia = round(std(Intensidade_Resultante_NORMALIZADA)^2,digits = 2)
# variancia = get_variancia(Intensidade_Resultante_NORMALIZADA)
# h_normalizado = normalize(Histograma_LINEAR;mode=:pdf)
# R² = compute_R2(log.(h_normalizado.weights), log.(B))
# kℓ_gc =  (1 + 4*(((Δ)/(2*π)^3 )^2) ) / (4*pi*ρ)  

# kℓ_gc = round(kℓ_gc,digits = 2)
# gc_plot = round(gc,digits=3)
# R²_plot = round(R²,digits=3)
# variancia_plot =  round(variancia,digits= 3)
# densidade_plot = round(Int64, ρ*λ^3)
# DELTA = Δ

# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LINEAR,1000)
# plot!(xdata,B,lab = L"\textrm{Lei de Von Rossum}",lw=8,color = :green, linestyle = :dash,legend =:bottomleft,    titlefontsize = 20,title = L"$g_c = %$gc_plot,R^2 = %$R²_plot,\sigma_I = %$variancia_plot, \rho \lambda^3 = %$densidade_plot , \Delta / \Gamma_0 = 0.9, k \ell = %$kℓ_gc $")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Range_1 = 0
# Range_2 = 2
# espaçamento = 0.5
# frames = animar_a_evolução_do_perfil_da_Intensidade(entrada_rotina,Range_1,espaçamento,Range_2,10)
# gif(frames,"decaimento.gif",fps=2)