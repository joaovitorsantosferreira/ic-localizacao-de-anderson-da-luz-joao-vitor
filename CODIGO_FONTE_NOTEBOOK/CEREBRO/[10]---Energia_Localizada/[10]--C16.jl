###############################################################################################################################################################################################################
################################################# Programa de Controle 16 - Contempla simulações Voltadas a definição da Energia tipicas dos modos localizados ################################################
###############################################################################################################################################################################################################

"""
Neste programa a ideia é fazer algo parecido com o que Cherroret e Skipetrov fizeram no Artigo apresentado no JC
"""

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E10.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R14.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E10.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[8]---Energia/R14.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
###############################################################################################################################################################################################################

k = 1                                                                                                                        # Número de Onda
Lₐ = 280                                                                                                                      # Dimensão Fixa Complementar 
N = 200

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

########################################################################### DEFINIÇÂO 3 - Escala do Eixo X referente a Densidade ##############################################################################
Escala_do_range = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
# Escala_do_range = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################

N_Realizações = 20                                                                                                       # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
RANGE_INICIAL = 0.05
RANGE_FINAL = 10
N_pontos = 10

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R14_ENTRADA(
    Γ₀,  
    k,
    N,
    Tipo_de_kernel,
    N_pontos,
    N_Realizações,
    Lₐ,
    RANGE_INICIAL,
    RANGE_FINAL,
    Escala_do_range,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM
)

Dados_C16 = ROTINA_14__Energia_Localizada(entrada_rotina)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Range_ρ,Δs = Dados_C16


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("PLOTS/Plots.jl")
vizualizar_relação_energia_e_densidade(Δs,Range_ρ,Escala_do_range,1000)
# savefig("Relacao_entre_densidade_faixa_de_energia_N={$N}__Kernel={$Tipo_de_kernel}_LINEAR.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C13.jld2", Dict("SAIDA" => Dados_C13,"Parametros" => entrada_rotina))

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra Simulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-11}_Dados_Antigos_C16.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-11}_Dados_Antigos_C16.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-12}_Dados_Antigos_C16.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-12}_Dados_Antigos_C16.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-25}_Dados_Antigos_C16.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2021-October-25}_Dados_Antigos_C16.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-June-25}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-June-25}_Dados_Antigos_CLUSTER_C6.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/TEMPORARIO_C16.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/TEMPORARIO_C16.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-August-27}_Dados_Antigos_CLUSTER_C16.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-August-27}_Dados_Antigos_CLUSTER_C16.jld2", "Parametros")

Range_ρ = Dados_Antigos[1]
Δs = Dados_Antigos[2]
Escala_do_range = Parametros_Antigos.Escala_do_range
N = Parametros_Antigos.N
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Geometria = Parametros_Antigos.Geometria
N_Realizações = Parametros_Antigos.N_Realizações

include("PLOTS/Plots.jl")
vizualizar_relação_energia_e_densidade(Δs[1:end-1],Range_ρ[1:end-1],Parametros_Antigos,L"$N = %$N,%$Tipo_de_kernel,%$Geometria,%$N_Realizações REP, IPR > 0.4,R^2 > 0.7, \xi < kR  $",1000)
# savefig("Relacao_entre_densidade_faixa_de_energia_N={$N}__Kernel={$Tipo_de_kernel}_LINEAR.png")





#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


      
