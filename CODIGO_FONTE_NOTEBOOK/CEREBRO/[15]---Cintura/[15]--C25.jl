###############################################################################################################################################################################################################
################################################################ Programa de Controle 13 - Contempla simulações Voltadas a Transmissão da Luz #################################################################
###############################################################################################################################################################################################################

"""
Neste programa a ideia é fazer algo parecido com o que Cherroret e Skipetrov fizeram no Artigo apresentado no JC
"""

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations
using Loess

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R11.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R11.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################

# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                 # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                 # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Lₐ = 140
rₘᵢₙs = [0.1,1,2]
ρs = [0.01,0.02]
Ls = [5,8,10]
b₀s = [0.5,1,5]

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                                # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                       # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                    # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################

Δ = 0
k = 1 
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Sensores = 40                                                                                # Quantidade de Sensores ao redor da amostra
Distancia_dos_Sensores = 0                                                                       # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                        # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 360                                                      # Define o angulo final da tela circular de Sensores
angulo_coerente = 30                                                         


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################ DEFINIÇÂO 4 - Variavel constante no Diagrama de Fase #############################################################################
Variavel_Constante = "ρ"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "rmin"                                                                   # Define que o raio de exclusão será a quantidade fixada nas realizações
# Variavel_Constante = "L"                                                                      # Define que a espessura do sistma será a quantidade fixada nas realizações
# Variavel_Constante = "b₀"                                                                     # Define que a espessura otica do sistma será a quantidade fixada nas realizações
########################################################################### DEFINIÇÂO 5 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_do_range = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_do_range = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 10                                                                                                       # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
RANGE_INICIAL = 1
RANGE_FINAL = 5
N_pontos = 5



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R11_ENTRADA(
    Γ₀,  
    ωₐ,
    Ω ,
    rₘᵢₙs,
    ρs,
    Ls,
    b₀s,
    k,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    angulo_coerente,
    Tipo_de_beta,
    N_pontos,
    N_Realizações,
    Lₐ,
    RANGE_INICIAL,
    RANGE_FINAL,
    Variavel_Constante,
    Escala_do_range,
    Desordem_Diagonal,
    W,
    Geometria
)

Dados_C13 = ROTINA_11__Cintura_e_L(entrada_rotina,Geometria)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
σ_L²,Range_W = Dados_C13


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("PLOTS/Plots.jl")
vizualizar_cintura_por_L(σ_L²,Range_W,Escala_do_range,rₘᵢₙs,ρs,Ls,b₀s,Δ,Variavel_Constante,Lₐ,Tipo_de_kernel,1000)
# savefig("Cintura_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}_44.png")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C13.jld2", Dict("SAIDA" => Dados_C13,"Parametros" => entrada_rotina))



###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra Simulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/DATA={2022-September-20}_Dados_Antigos_CLUSTER_C25_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/DATA={2022-September-20}_Dados_Antigos_CLUSTER_C25_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/DATA={2022-September-21}_Dados_Antigos_CLUSTER_C25_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/DATA={2022-September-21}_Dados_Antigos_CLUSTER_C25_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/TEMPORARIO_C25_21_09.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/TEMPORARIO_C25_21_09.jld2", "Parametros")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/TEMPORARIO_C25.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C25/TEMPORARIO_C25.jld2", "Parametros")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

σ = Dados_Antigos[1]
# @. σ = sqrt.(σ)
T = Dados_Antigos[2]
Range_W = Dados_Antigos[3]

ρs = Parametros_Antigos.ρs
Escala_do_range = Parametros_Antigos.Escala_do_range
Δ = Parametros_Antigos.Δ
Lₐ = round(Parametros_Antigos.Lₐ)
L = Parametros_Antigos.L
N_Sensores = Parametros_Antigos.N_Sensores
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
N_Realizações = Parametros_Antigos.N_Realizações
W = Parametros_Antigos.W
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
N_pontos = Parametros_Antigos.N_pontos
Geometria  = Parametros_Antigos.Geometria
angulo_coerente = Parametros_Antigos.angulo_coerente
λ = Parametros_Antigos.λ
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores

RANGE_INICIAL = Parametros_Antigos.RANGE_INICIAL
RANGE_FINAL = Parametros_Antigos.RANGE_FINAL
N_pontos = Parametros_Antigos.N_pontos
println(Range_W)

Range_W = range(RANGE_INICIAL,RANGE_FINAL,length=N_pontos)./λ 
# Range_W = get_points_in_log_scale(RANGE_INICIAL,RANGE_FINAL,N_pontos)./λ 

include("PLOTS/Plots.jl")
vizualizar_cintura_por_L(σ,Range_W,"LINEAR",ρs,Tipo_de_kernel,L"$W/b_0 = %$W, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
xlabel!(L"$w_0/\lambda$")
ylabel!(L"$\sigma$")


# include("PLOTS/Plots.jl")
# vizualizar_transmissão_por_L(T,Range_W,"LINEAR",ρs,Tipo_de_kernel,L"$\Delta = \Delta_{LOC} , %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# # savefig("Tramissão_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}.png")
savefig("Crrescimento_Cintura.png")

# vline!([12]; label=L"$\textrm{Limite fitting} $", linestyle=:dashdot, color= :black, linewidth=5)
# savefig("Tramissão_Vetorial_fitting_2.png")


# include("PLOTS/Plots.jl")
# vizualizar_transmissão_por_b0(T,Range_W,Escala_do_range,ρs,Tipo_de_kernel,Lₐ,L"$W/b_0 = %$W, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("Transmissão_por_b0_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}_3.png")




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




