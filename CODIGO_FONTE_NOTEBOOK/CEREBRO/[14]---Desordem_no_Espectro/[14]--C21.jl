###############################################################################################################################################################################################################
################################################################ Programa de Controle 21 - Contempla simulações Voltadas a Desordem Diagonal ##################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R19.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[11]---Desordem_Espectro/R19.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################

# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                 # Define a geometria da nuvem como Slab

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                 # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Radius = 250
Lₐ = 140
L = 50
b₀s = get_points_in_log_scale(0.1,2,5)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                                # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

Δ = 0
k = 1                                                

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################ DEFINIÇÂO 4 - Variavel constante no Diagrama de Fase #############################################################################
Escala_do_range = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
# Escala_do_range = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 100                                                                                                       # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
RANGE_INICIAL = 0.01
RANGE_FINAL = 20
N_pontos = 20


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R19_ENTRADA(
    ωₐ,
    Γ₀,
    k,
    GEOMETRIA_DA_NUVEM,
    Radius,
    Lₐ,
    L,
    b₀s,
    Tipo_de_kernel,
    Δ, 
    Escala_do_range,
    N_Realizações,
    RANGE_INICIAL,
    RANGE_FINAL,
    N_pontos
)

Dados_C21 = ROTINA_19__PR_Desordem(entrada_rotina,Geometria)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
PR_medio_subradiante,PR_medio_superradiante,Range_W = Dados_C21


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# include("PLOTS/Plots.jl")
vizualizar_PR_por_W(PR_medio_subradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,L"$Subradiancia, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP, Radius = %$Radius, b_0 $",1000)
# savefig("Transição_de_localização_vetorial__SUB_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}.png")

# include("PLOTS/Plots.jl")
vizualizar_PR_por_W(PR_medio_superradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,L"$Superadiancia, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP, Radius = %$Radius, b_0 $",1000)
# savefig("Transição_de_localização_vetorial__SUP_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}.png")

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra Simulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-April-3}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-April-3}_Dados_Antigos_C21.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-April-23}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-April-23}_Dados_Antigos_C21.jld2", "Parametros")

PR_medio_subradiante = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21.jld2", "Dados_SUB")[:,1:3]
PR_medio_superradiante = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21.jld2", "Dados_SUP")[:,1:3]
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21.jld2", "Parametros")
Range_W = get_points_in_log_scale(Parametros_Antigos.RANGE_INICIAL,Parametros_Antigos.RANGE_FINAL,Parametros_Antigos.N_pontos)     

PR_medio_subradiante = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21_2.jld2", "Dados_SUB")[:,1:2]
PR_medio_superradiante = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21_2.jld2", "Dados_SUP")[:,1:2]
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/TEMPORARIO_C21_2.jld2", "Parametros")
Range_W = get_points_in_log_scale(Parametros_Antigos.RANGE_INICIAL,Parametros_Antigos.RANGE_FINAL,Parametros_Antigos.N_pontos)     

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-6}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-6}_Dados_Antigos_C21.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-6}_Dados_Antigos_C21_2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-6}_Dados_Antigos_C21_2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-8}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-8}_Dados_Antigos_C21.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-8}_Dados_Antigos_C21_2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-8}_Dados_Antigos_C21_2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-15}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-15}_Dados_Antigos_C21.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-16}_Dados_Antigos_C21.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C21/DATA={2022-May-16}_Dados_Antigos_C21.jld2", "Parametros")


PR_medio_subradiante,PR_medio_superradiante,Range_W = Dados_Antigos

ωₐ = Parametros_Antigos.ωₐ
Γ₀ = Parametros_Antigos.Γ₀  
k = Parametros_Antigos.k  
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM  
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)
Radius = round(Parametros_Antigos.Radius,digits=2)  
Lₐ = Parametros_Antigos.Lₐ  
L = Parametros_Antigos.L  
b₀s = Parametros_Antigos.b₀s  
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel  
Δ = Parametros_Antigos.Δ  
Escala_do_range = Parametros_Antigos.Escala_do_range  
N_Realizações = Parametros_Antigos.N_Realizações  
RANGE_INICIAL = Parametros_Antigos.RANGE_INICIAL  
RANGE_FINAL = Parametros_Antigos.RANGE_FINAL  
N_pontos = Parametros_Antigos.N_pontos  
percentual_de_modos = Parametros_Antigos.percentual_de_modos

include("PLOTS/Plots.jl")
vizualizar_PR_por_W(PR_medio_subradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,L"$Subradiancia, %$Tipo_de_kernel,%$N_Realizações REP, Radius = %$Radius, \gamma_n \in [1,\infty], \omega_n \in %$percentual_de_modos \%, b_0 $",1000)
# savefig("Transição_de_localização_escalar__SUB_FORMA1_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_percent{$percentual_de_modos}.png")

include("PLOTS/Plots.jl")
vizualizar_PR_por_W(PR_medio_superradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,L"$Superadiancia, %$Tipo_de_kernel, %$N_Realizações REP, Radius = %$Radius, \gamma_n \in [0,1],\omega_n \in %$percentual_de_modos \%, b_0 $",1000)
# savefig("Transição_de_localização_escalar__SUP_FORMA1_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_percent{$percentual_de_modos}.png")

include("PLOTS/Plots.jl")
vizualizar_PR_por_W_transition(PR_medio_subradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,Radius,L"$Subradiancia, %$Tipo_de_kernel,%$N_Realizações REP, Radius = %$Radius, \gamma_n \in [1,\infty],\omega_n \in %$percentual_de_modos \%, b_0 $",1000)
# savefig("Transição_de_localização_escalar__SUB_FORMA2_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_percent{$percentual_de_modos}.png")

include("PLOTS/Plots.jl")
vizualizar_PR_por_W_transition(PR_medio_superradiante,Range_W,Escala_do_range,b₀s,Tipo_de_kernel,Radius,L"$Superadiancia, %$Tipo_de_kernel, %$N_Realizações REP, Radius = %$Radius, \gamma_n \in [0,1],\omega_n \in %$percentual_de_modos \%, b_0 $",1000)
# savefig("Transição_de_localização_escalar__SUP_FORMA2_NR={$N_Realizações}_Kernel={$Tipo_de_kernel}_percent{$percentual_de_modos}.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
