###############################################################################################################################################################################################################
################################################################ Programa de Controle 21 - Contempla simulações Voltadas a Desordem Diagonal ##################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R20.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[11]---Desordem_Espectro/R20.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################

# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                 # Define a geometria da nuvem como Slab

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                 # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

k = 1                                                                                                                        # Número de Onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ρ = 3
Lₐ = 20                                                                                                                      # Dimensão Fixa Complementar 

############################################################################################## Parametros da Nuvem #############################################################################################
#-------------------------------------------------------------------------------------------- Número de Atomos Fixo ------------------------------------------------------------------------------------------#
N = 1000                                                                                                                   # Número de Átomos da Nuvem 
Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
#------------------------------------------------------------------------------------------------ Dimensão Fixa ----------------------------------------------------------------------------------------------#
# L = 20                                                                                                                     # Espessura da Nuvem, tanto para o 2D, quanto para o 3D 
# Radius = 20                                                                                                                # Raio da Nuvem, funciona apenas para a geometria de Esfera e Disco
# N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(L,Radius,Lₐ,ρ,GEOMETRIA_DA_NUVEM)
###############################################################################################################################################################################################################



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                                # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

Δ = 0
k = 1                                                

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Realizações = 10                                                                                                       # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 

if Tipo_de_kernel == "Escalar"
    b₀ = (8*N)/(π*k*Radius)

elseif Tipo_de_kernel == "Vetorial"
    b₀ = (16*N)/(π*k*Radius)
end

W = 1*b₀     



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R20_ENTRADA(
    Γ₀,
    k,
    GEOMETRIA_DA_NUVEM,
    Geometria,
    N,
    ρ,
    Radius,
    Lₐ,
    L,
    rₘᵢₙ,
    Tipo_de_kernel,
    Δ, 
    N_Realizações,
    W
)

Dados_C22 = ROTINA_20__Espectro_Desordem(entrada_rotina,Geometria)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
γ,ω,IPRs_PURO = Dados_C22


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

ρ_titulo = round(ρ,digits=2)
W_titulo = round(W/b₀ ,digits =2)
R_titulo = round(Radius,digits=2)

include("PLOTS/Plots.jl")
vizualizar_relação_entre_gamma_e_omega_IPR(ω,γ,round.(IPRs_PURO,digits=20),Δ,Γ₀,0,0,Tipo_de_kernel,"PRESENTE",W,b₀,L"$ \rho / k^2 =%$ρ_titulo, %$Geometria,W /b_0 = %$W_titulo ,kR = %$R_titulo,%$N_Realizações REP ,%$Tipo_de_kernel  - IPR $",1000)
# savefig("espetro_3.png")


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra Simulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C22/DATA={2022-May-22}_Dados_Antigos_C22.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C22/DATA={2022-May-22}_Dados_Antigos_C22.jld2", "Parametros")


γ,ω,IPRs_PURO = Dados_Antigos

Γ₀ = Parametros_Antigos.Γ₀
k = Parametros_Antigos.k
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
Geometria = Parametros_Antigos.Geometria
N = Parametros_Antigos.N
ρ = Parametros_Antigos.ρ
Radius = Parametros_Antigos.Radius
Lₐ = Parametros_Antigos.Lₐ
L = Parametros_Antigos.L
rₘᵢₙ = Parametros_Antigos.rₘᵢₙ
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Δ = Parametros_Antigos.Δ 
N_Realizações = Parametros_Antigos.N_Realizações
W = Parametros_Antigos.W
if Tipo_de_kernel == "Escalar"
    b₀ = (8*N)/(π*k*Radius)
elseif Tipo_de_kernel == "Vetorial"
    b₀ = (16*N)/(π*k*Radius)
end

ρ_titulo = round(ρ,digits=2)
W_titulo = round(W/b₀ ,digits =2)
R_titulo = round(Radius,digits=2)

include("PLOTS/Plots.jl")
vizualizar_relação_entre_gamma_e_omega_IPR(ω,γ,round.(IPRs_PURO,digits=20),Δ,Γ₀,0,0,Tipo_de_kernel,"PRESENTE",W,b₀,L"$ \rho / k^2 =%$ρ_titulo, %$Geometria,W /b_0 = %$W_titulo ,kR = %$R_titulo,%$N_Realizações REP, %$Tipo_de_kernel  - IPR $",1000)
savefig("Espectro_5.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

