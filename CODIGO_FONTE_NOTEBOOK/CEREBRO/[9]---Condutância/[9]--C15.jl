###############################################################################################################################################################################################################
######################################################## Programa de Controle 15 - Contempla simulações Voltadas a Condutancia para diferentes Densidades e Δ ####################################################
##############################################################################################################################################################################################################

"""

Neste programa são construidos diagramas de Fase da variancia, onde definimos um range de Densidades (ρ/k²) e um range de valores de Deturn (Δ/Γ₀).

A partir destes diagrama somos capazes de observar quando as estatisticas da luz desviam da lei de Rayleigh. Assim a ideia é cruzar os dados do regime localizado e do regime difuso com esses diagramas, assim 
averiguar se a perturbação nas estatisiticas da luz constituem uma assinatura da transição de localização 2D.

"""

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################



using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots 


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E9.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R12.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R13.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E9.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[7]---Condutância/R12.jl") 
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[7]---Condutância/R13.jl") 



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################


N = 100                                                                                                                        # Número de atomos caso N seja fixado
kR = 1                                                                                                                        # Raio normalizado do sistema caso kR seja fixado
L = 3
Lₐ = 50

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
k = 1                                                                                                                        # Número de Onda
angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
λ = (2*π)/k                                                                                                                  # Comprimento de onda da luz incidente 
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de Onda da frente de onda                                            


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

N_Sensores = 45                                                                                                              # Quantidade de Sensores ao redor da amostra
Angulo_de_variação_da_tela_circular_1 = 40                                                                                   # Define o angulo inicial da Tela Circular
Angulo_de_variação_da_tela_circular_2 = 85                                                                                   # Define o angulo final da Tela Circular


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################ DEFINIÇÂO 5 - Variavel constante no Diagrama de Fase #############################################################################
# Variavel_Constante = "N"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
Variavel_Constante = "kR"                                                                     # Define que o raio do sistma será a quantidade fixada nas realizações, presupondo uma geometria de DISCO
# Variavel_Constante = "L"                                                                      # Define que o as dimensões do SLAB serão a quantidade fixada nas realizações, presupondo uma geometria de SLAB 
########################################################################### DEFINIÇÂO 6 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_de_Densidade = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_de_Densidade = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################



N_Realizações = 400                                                                                               # Define a quantidade de realizações por locus no diagrama de Fase
N_div_Densidade = 20                                                                                            # Define a quatidade de divisões no eixo y do Diagrama, que é a densidade normalizada do sistema
N_div_Deturn = 10                                                                                               # Define a quatidade de divisões no eixo x do Diagrama, que é o Deturn do sistema

Δ_Inicial = -5                                                                                                  # Define o Valor Inicial do Deturn
Δ_Final = 2.4                                                                                                     # Define o Valor Final do Deturn
Densidade_Inicial = 1                                                                                           # Define o Valor Inicial da Densidade
Densidade_Final = 10                                                                                             # Define o Valor Final da Densidade


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 7 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


entrada_rotina = R13_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    kR,
    L,
    Lₐ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    N_Realizações,
    N_div_Densidade,
    N_div_Deturn,
    Δ_Inicial,
    Δ_Final,
    Densidade_Inicial,
    Densidade_Final,
    Variavel_Constante,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM
)


Dados_C15 = ROTINA_13__Diagrama_de_Fase_Condutancia(entrada_rotina)

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Histograma_Diagrama_de_Fase_Condutancia_KOGAN = Dados_C15[1]                                                                                      # Define o histograma que contem a variancia calculada para diferentes 
Histograma_Diagrama_de_Fase_Condutancia_SHNERB = Dados_C15[2]

# maximum(Histograma_Diagrama_de_Fase_Condutancia[:,3])
# minimum(Histograma_Diagrama_de_Fase_Condutancia[:,3])

# a = 0

# for i in 1:200

#     if Histograma_Diagrama_de_Fase_Condutancia_SHNERB[i,3] < 0 
#         a +=1
#     end
# end
# a
###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

vizualizar_Diagrama_de_Fase_Condutancia_KOGAN(Histograma_Diagrama_de_Fase_Condutancia_KOGAN,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Escala_de_Densidade,kR,1000)
# savefig("Diagrama_Variancia__NR={$N_Realizações}__N={$N}_norm_21.png")
# savefig("Diagrama_Variancia__NR={$N_Realizações}__kR={$kR}_15.png")
# savefig("Romain_302.png")

vizualizar_Diagrama_de_Fase_Condutancia_SHNERB(Histograma_Diagrama_de_Fase_Condutancia_SHNERB,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Escala_de_Densidade,kR,1000)
# savefig("Diagrama_Variancia__NR={$N_Realizações}__N={$N}_norm_22.png")
# savefig("Diagrama_Variancia__NR={$N_Realizações}__kR={$kR}_18.png")
# savefig("Romain_302.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C4.jld2", Dict("SAIDA" => Dados_C4,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C15/DATA={2021-August-12}_Dados_Antigos_CLUSTER_C15.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C15/DATA={2021-August-12}_Dados_Antigos_CLUSTER_C15.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C15/DATA={2021-August-17}_Dados_Antigos_CLUSTER_C15.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C15/DATA={2021-August-17}_Dados_Antigos_CLUSTER_C15.jld2", "Parametros")

Histograma_Diagrama_de_Fase_Condutancia_SHNERB = Dados_Antigos[2]
N_div_Densidade = Parametros_Antigos.N_div_Densidade 
N_div_Deturn = Parametros_Antigos.N_div_Deturn 
Densidade_Inicial = Parametros_Antigos.Densidade_Inicial 
Densidade_Final = Parametros_Antigos.Densidade_Final 
Escala_de_Densidade = Parametros_Antigos.Escala_de_Densidade 
kR = Parametros_Antigos.kR 
N_Realizações = Parametros_Antigos.N_Realizações
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel 


include("PLOTS/Plots.jl")
p2 = vizualizar_Diagrama_de_Fase_Condutancia_SHNERB(Histograma_Diagrama_de_Fase_Condutancia_SHNERB,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Escala_de_Densidade,kR,1000)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#