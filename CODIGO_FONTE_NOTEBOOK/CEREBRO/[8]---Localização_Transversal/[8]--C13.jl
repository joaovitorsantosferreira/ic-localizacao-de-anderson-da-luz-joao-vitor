###############################################################################################################################################################################################################
################################################################ Programa de Controle 13 - Contempla simulações Voltadas a Transmissão da Luz #################################################################
###############################################################################################################################################################################################################

"""
Neste programa a ideia é fazer algo parecido com o que Cherroret e Skipetrov fizeram no Artigo apresentado no JC
"""

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations
using Loess

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R11.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R11.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################

# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                 # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                 # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Lₐ = 140
rₘᵢₙs = [0.1,1,2]
ρs = [0.01,0.02]
Ls = [5,8,10]
b₀s = [0.5,1,5]

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                                # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                       # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                    # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################

Δ = 0
k = 1 
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Sensores = 40                                                                                # Quantidade de Sensores ao redor da amostra
Distancia_dos_Sensores = 0                                                                       # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                        # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 360                                                      # Define o angulo final da tela circular de Sensores
angulo_coerente = 30                                                         


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################ DEFINIÇÂO 4 - Variavel constante no Diagrama de Fase #############################################################################
Variavel_Constante = "ρ"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "rmin"                                                                   # Define que o raio de exclusão será a quantidade fixada nas realizações
# Variavel_Constante = "L"                                                                      # Define que a espessura do sistma será a quantidade fixada nas realizações
# Variavel_Constante = "b₀"                                                                     # Define que a espessura otica do sistma será a quantidade fixada nas realizações
########################################################################### DEFINIÇÂO 5 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_do_range = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_do_range = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 10                                                                                                       # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
RANGE_INICIAL = 1
RANGE_FINAL = 5
N_pontos = 5



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R11_ENTRADA(
    Γ₀,  
    ωₐ,
    Ω ,
    rₘᵢₙs,
    ρs,
    Ls,
    b₀s,
    k,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    angulo_coerente,
    Tipo_de_beta,
    N_pontos,
    N_Realizações,
    Lₐ,
    RANGE_INICIAL,
    RANGE_FINAL,
    Variavel_Constante,
    Escala_do_range,
    Desordem_Diagonal,
    W,
    Geometria
)

Dados_C13 = ROTINA_11__Cintura_e_L(entrada_rotina,Geometria)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
σ²_L²,Range_L = Dados_C13


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("PLOTS/Plots.jl")
vizualizar_cintura_por_L(σ²_L²,Range_L,Escala_do_range,rₘᵢₙs,ρs,Ls,b₀s,Δ,Variavel_Constante,Lₐ,Tipo_de_kernel,1000)
# savefig("Cintura_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}_44.png")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C13.jld2", Dict("SAIDA" => Dados_C13,"Parametros" => entrada_rotina))



###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra Simulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-17}_Dados_Antigos_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-18}_Dados_Antigos_C13_1.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-18}_Dados_Antigos_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-20}_Dados_Antigos_C15.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={18-May-2021}_Dados_Antigos_C8.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-June-16}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-July-15}_Dados_Antigos_C13.jld2", "SAIDA")
# Parametros_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-July-15}_Dados_Antigos_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-July-18}_Dados_Antigos_C13_VETORIAL.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-July-18}_Dados_Antigos_C13_VETORIAL.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-2}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-2}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-15}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-15}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-15}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-August-15}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-12}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-12}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13_2.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13_3.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-13}_Dados_Antigos_CLUSTER_C13_3.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-15}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-15}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_5.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_5.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_6.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_6.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_3.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-20}_Dados_Antigos_CLUSTER_C13_3.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-23}_Dados_Antigos_CLUSTER_C13_6.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-23}_Dados_Antigos_CLUSTER_C13_6.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-26}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-26}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-26}_Dados_Antigos_CLUSTER_C13_VETORIAL.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-26}_Dados_Antigos_CLUSTER_C13_VETORIAL.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13_VETORIAL.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13_VETORIAL.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13_SLAB.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-27}_Dados_Antigos_CLUSTER_C13_SLAB.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_SLAB_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_SLAB_2.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_VETORIAL.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_VETORIAL.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_ESCALAR.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-September-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_ESCALAR.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_ESCALAR.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_ESCALAR.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_VETORIAL.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_VETORIAL.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-5}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-5}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-6}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-6}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-13}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-13}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-13}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-13}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-23}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-23}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-25}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_W0.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-25}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_W0.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-28}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_la20.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-October-28}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_la20.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_W0.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_W0.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_la20.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_TESTE_la20.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-3}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2021-November-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-February-25}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-February-25}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-March-21}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-March-21}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-April-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-April-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-May-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_2.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_2.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-15}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-15}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-16}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-16}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-19}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-19}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-21}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-21}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-22}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-22}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-26}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-26}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-12}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-12}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-24}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-24}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-30}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-30}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-31}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-July-31}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-10}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-12}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-12}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13_QUADRATICO.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13_QUADRATICO.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-16}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-16}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-17}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-17}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-24}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-24}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-28}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-28}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-30}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-August-30}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-September-14}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-8}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-9}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{PURO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-November-29}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13_01_12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13_01_12.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-1}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-1}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-2}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-7}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = round(Parametros_Antigos.Lₐ)

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-15}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-December-15}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
Lₐ = round(Parametros_Antigos.Lₐ)

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/TEMPORARIO_C13.jld2", "Parametros")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



# ESCALAR IMPORTANTE--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C13/DATA={2022-June-20}_Dados_Antigos_CLUSTER_C13_{SLAB}_{Escalar}.jld2", "Parametros")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

σ² = Dados_Antigos[1]
T = Dados_Antigos[2]
T[findall(T.== 0)] .= 0.001
Range_L = Dados_Antigos[3]

ρs = Parametros_Antigos.ρs
Escala_do_range = Parametros_Antigos.Escala_do_range
Δ = Parametros_Antigos.Δ
Lₐ = round(Parametros_Antigos.Lₐ)
N_Sensores = Parametros_Antigos.N_Sensores
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
N_Realizações = Parametros_Antigos.N_Realizações
W = Parametros_Antigos.W
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
N_pontos = Parametros_Antigos.N_pontos
Geometria  = Parametros_Antigos.Geometria
angulo_coerente = Parametros_Antigos.angulo_coerente

RANGE_INICIAL = Parametros_Antigos.RANGE_INICIAL
RANGE_FINAL = Parametros_Antigos.RANGE_FINAL
N_pontos = Parametros_Antigos.N_pontos
# range_ρ = range(Densidade_Inicial,Densidade_Final,length = N_pontos)
# ρs = zeros(size(range_ρ,1))
# @.ρs = range_ρ


Range_L = range(RANGE_INICIAL,RANGE_FINAL,length=N_pontos) 
# Range_L = get_points_in_log_scale(RANGE_INICIAL,RANGE_FINAL,N_pontos) 

# include("PLOTS/Plots.jl")
vizualizar_cintura_por_L(σ²,Range_L,"LINEAR",ρs,Tipo_de_kernel,L"$\Delta = \Delta_{LOC}, %$Tipo_de_kernel ,%$N_Realizações REP , kL = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("Cintura_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}.png")
# savefig("exemplo_6.png")


# plot(Range_L,T,xscale=:log10,yscale=:log10)

# include("PLOTS/Plots.jl")
vizualizar_transmissão_por_L(T,Range_L,"LINEAR",ρs,Tipo_de_kernel,L"$\Delta = \Delta_{LOC}, %$Tipo_de_kernel ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("Tramissão_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}.png")
# savefig("TESTE_TRAN.png")



# ξ(ρ,Δ) = (4*(norm(2*Δ + 1im)^2)/ρ)*exp( ((π*(norm(2*Δ+1im)^2))/ρ)*real(1+ (1im*ρ)/(4*norm(2*Δ + 1im)^2)  ))
# # ξ(ρ,Δ) = (4/(ρ*(abs(2*Δ + 1im)^2)))*exp( (π/((abs(2*Δ+1im)^2)*ρ))*real(1+ ((1im*ρ*abs2(2*Δ + 1im)^2)/(4))) )

# ρs = exp10.(range(-2,1,length = 100))    

# Δ = 0.5
# # ρ = 0.1
# ξs = ξ.(ρs,Δ)
# plot!(ρs,ξs,
#     left_margin = 10Plots.mm,
#     right_margin = 5Plots.mm,
#     top_margin = 7Plots.mm,
#     bottom_margin = 10Plots.mm,
#     lw = 8,
#     xscale = :log10,
#     yscale =:log10,
#     xlims=(0.01,10),
#     ylims=(0.01,100),
#     lab=L"$ \delta = %$Δ$",
#     size=(1000,500),
#     xlabel=L"$\rho/k^2$",
#     titlelocation = :right,
#     framestyle = :box,
#     legend =:bottomleft,
#     legendfontsize = 15,
#     labelfontsize = 25,
#     titlefontsize = 20,
#     tickfontsize = 15, 
#     legendtitlefontsize = 20,
#     background_color_legend = :white,
#     background_color_subplot = :white,
#     foreground_color_legend = :black,
#     tick_direction = :out 
#     )
# xlabel!(L"$\rho / \lambda^2 $")
# ylabel!(L"$k \xi$")

# savefig("comprimento de localização.png")


# vline!([12]; label=L"$\textrm{Limite fitting} $",linestyle=:dashdot, color= :black, linewidth=5)
# savefig("Tramissão_Vetorial_fitting_2.png")


# include("PLOTS/Plots.jl")
# vizualizar_transmissão_por_b0(T,Range_L,Escala_do_range,ρs,Tipo_de_kernel,Lₐ,L"$W/b_0 = %$W, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("Transmissão_por_b0_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}_3.png")


# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2021-July-9}_Dados_Antigos_C5.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2021-July-9}_Dados_Antigos_C5.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2021-August-1}_Dados_Antigos_C5.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2021-August-1}_Dados_Antigos_C5.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5.jld2", "Parametros")


# ξs = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5.jld2", "SAIDA")
# N_div_Densidade = 20                                                                                             
# ρ_Inicial = 0.01                                                                                            
# ρ_Final = 10     
# ρ_normalizados = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade)     
# Ls = [5,10,15]


ρ_normalizados,ξs = Dados_Antigos
Ls = Parametros_Antigos.Ls[1:2]

ρ_normalizados,ξs = Dados_Antigos

Parametros_Antigos.Desordem_Diagonal

include("PLOTS/Plots.jl")
vizualizar_Relação_entre_a_Densidade_e_ξₘᵢₙ(ρ_normalizados,ξs,0,0,Ls,"L","LOG",L"$Escalar, 2D, \Delta= Inter, L_a = %$Lₐ, SLAB, $",1000)


# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

ξs_fit = zeros(size(ρs,1),3)
model_1(x, p) = p[1].*x .+ p[3]

for i in 1:size(ρs,1)

    index_modes_inside_range = findall(  (Range_L .> 12.6).*(σ²[:,i] .> 0 ) )

    x = Range_L[index_modes_inside_range]
    y = σ²[index_modes_inside_range,i]

    xdata = x
    ydata = y
    p0 = [0.01,2,0]

    fit = curve_fit(model_1, xdata, ydata, p0)
    ξs_fit[i,:] = fit.param
end

scatter!(ρs,ξs_fit[:,1],
m = :star4,
size = (1000,500),
ms = 15,
c = :deepskyblue,
lab = L"$\sigma^2 \propto \xi L - Scalar$",
legend =:topright,
xticks = [10^1,10^0,10^-1],
xlims = (10^-1.1,10^1))

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

ξs_fit = zeros(size(ρs,1))
model_exp(x, p) = exp.(-1 .* (x./p))
model_log(x, p) = (-1) .* (x./p)

for i in 1:size(ρs,1)

    index_modes_inside_range = findall(  (Range_L .> 0).*(T[:,i] .> 10^-6 ) )

    x = Range_L[index_modes_inside_range]
    y = T[index_modes_inside_range,i]

    
    xdata = x
    ydata = similar(xdata)
    ydata_preciso = log.(y)

    for k in 1:size(ydata_preciso,1)
        valor_preciso = ydata_preciso[k]
        string = "$valor_preciso"
        ydata[k] = parse(Float64,string[1:17])
    end

    p0 = [0.01]

    fit = curve_fit(model_log, xdata, ydata, p0)
    ξs_fit[i] = fit.param[1]
end

scatter!(ρs,ξs_fit[:,1],
m = :star4,
size = (1000,500),
ms = 15,
c = :yellow,
lab = L"$ T \propto exp \left( -L / \xi \right) $",
legend =:topright,
xticks = [10^1,10^0,10^-1],
xlims = (10^-1.1,10^1))

# xlabel!(L"$\rho/k^2$")
# ylabel!(L"$ k \xi_{min} $")
# title!("")
# savefig("Comparartivo_comprimentos_de_localizacao_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}.png")
# savefig("Correlação_ROBIN")



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




# B = σ²_L²

# σ²_L² = σ²_L² .* A

# A = collect(Range_L)
# A = A.^2

# vizualizar_cintura_por_L(σ²_L²,A,Escala_do_range,rₘᵢₙs,ρs,Ls,Δ,Variavel_Constante,1000)

# using DelimitedFiles

# linha = size(σ²_L²,1)
# coluna = size(σ²_L²,2)
# A = zeros(coluna+1,linha+1)

# for i in 1:linha    
#     A[2:end,i+1] = σ²_L²[i,:] 
# end

# B = zeros(1,linha)
# for i in 1:linha    
#     B[i] = Range_L[i] 
# end


# C = vcat(B,A[2:end,2:end])

# D[1,2:end] = B 
# D[2:(end-1),1] = ρs
# D = zeros(coluna+1,linha+1)
# D[2:end,2:end] = A[2:end,2:end]
# D = hcat(ρs,A[2:end,2:end])


# writedlm("ESCALAR.txt",D)

# io = open("VETORIAL.txt")
# write(io,A)


# σ² = load("RESULTADOS/DADOS ANTIGOS/C13/Temporario_C13.jld2", "Cinturas")

# ωₐ = 1                                                                                           # Tempo de decaimento atomico
# Γ₀ = 1                                                                                           # Frequencia natural dos atomos 
# Lₐ = 280
# rₘᵢₙs = [0.1,1,2]
# ρs = get_points_in_log_scale(0.01,0.2,5)
# Ls = [5,8,10]
# b₀s = [0.5,1,5]

# GEOMETRIA_DA_NUVEM = "SLAB"                                                                      # Define a geometria da nuvem como Slab
# # GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                          # Define a geometria da nuvem como Paralelepipedo
# # GEOMETRIA_DA_NUVEM = "TUBO"                                                                    # Define a geometria da nuvem como Tubo
# Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                    # Define o struct que representa a dimensão e geometria da nuvem
# Tipo_de_kernel = "Vetorial"                                                                       # Define que a analise é feita com a luz vetorial
# Tipo_de_Onda = "Laser Gaussiano"                                                                 # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_beta = "Estacionário"                                                                    # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Δ = 0
# k = 1 
# Ω = Γ₀/1000                                                                                       # Frequência de Rabi
# Angulo_da_luz_incidente = 0                                                                      # Angulo deIncidencia do Laser
# vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                # Vetor de propagação da onda
# λ = (2*π)/k                                                                                      # Comprimento de onda 
# ωₗ = Δ + ωₐ
# N_Sensores = 2800                                                                                # Quantidade de Sensores ao redor da amostra
# Distancia_dos_Sensores = 0                                                                       # Define a distacia entre os sensores e os sistema
# Angulo_de_variação_da_tela_circular_1 = 0                                                        # Define o angulo inicial da tela circular de Sensores
# Angulo_de_variação_da_tela_circular_2 = 360                                                      # Define o angulo final da tela circular de Sensores
# angulo_coerente = 0                                                         
# Variavel_Constante = "ρ"                                                                         # Define que o número de atomos será a quantidade fixada nas realizações
# Escala_do_range = "LINEAR"                                                                       # Define que a escala no eixo Y será Linear
# N_Realizações = 1000                                                                              # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
# RANGE_INICIAL = 1
# RANGE_FINAL = 30
# N_pontos = 20
# Desordem_Diagonal = "AUSENTE"                                                                    # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
# W = 5                                                                                            # Amplitude de Desordem
# Range_L = range(RANGE_INICIAL,RANGE_FINAL,length=N_pontos) 


# include("PLOTS/Plots.jl")
# vizualizar_cintura_por_L(σ²,Range_L,Escala_do_range,rₘᵢₙs,ρs,Ls,b₀s,Δ,Variavel_Constante,Lₐ,Tipo_de_kernel,L"$ Escalar, %$Geometria, \Delta = %$Δ, L_a = %$Lₐ, %$N_Realizações REP ,\rho / k^2$",1000)
# savefig("Cintura_por_L_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__Δ={$Δ}_3.png")



