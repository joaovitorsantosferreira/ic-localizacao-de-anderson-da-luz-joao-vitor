###############################################################################################################################################################################################################
############################################################## Programa de Controle 11 - Contempla simulações Voltadas a Localização Tranversal ###############################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distances
using LinearAlgebra   
using LaTeXStrings    
using Random   
using Statistics      
using Plots    
using SpecialFunctions
using QuadGK   
using PlotThemes      
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Loess

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R9.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R9.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1       # Tempo de decaimento atomico 
ωₐ = 1       # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Caracteristicas da Nuvem __ Número de átomos Fixado ----------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)        
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Ns = (1,5,10,20,30,40,50,100)                                                                                                                 # Números de atomos caso N seja fixado, aqui você pode entregar um array que contem os números de atomos que você deseja analisar e ele vai entender
b₀s = (0.5,1,5,10)
kR = 10                                                                                                                      # Raios normalizado do sistema caso kR seja fixado

Lₐ = 100
L = 20

ρ = zeros(size(Ns,1))
@.ρ = Ns/(L*Lₐ)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                               # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                       # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                    # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################
Tipo_de_Δ = "PURO"

k = 1
Δ = 0                                                                                                                      # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                          # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                # Comprimento de onda 
ωₗ = Δ + ωₐ                                                                                                                 # Frequencia do laser Incidente 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################ DEFINIÇÂO 4 - Variavel constante no Diagrama de Fase #############################################################################
Variavel_Constante = "N"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "b₀"                                                                   # Define que a densidade ótica será a quantidade fixada nas realizações


N_Realizações = 1000                                                                            # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

######################################################################################## DEFINIÇÂO 5 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 0                                                                              # Amplitude de Desordem


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#                            


N_Sensores = 280                                                                  # Quantidade de Sensores ao redor da amostra
Distancia_dos_Sensores = pi                                                         # Define a distacia entre os sensores e os sistema
                                                             

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


entrada_rotina = R9_ENTRADA(
    Γ₀,
    ωₐ,
    k,
    Δ,
    Ω,
    Ns,
    b₀s,
    kR,
    L,
    Lₐ,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    ωₗ,
    Tipo_de_Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Tipo_de_beta,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    N_Realizações,
    Variavel_Constante,
    Geometria
)



Dados_C11 = ROTINA_9__Perfil_Cintura_Distancia_FIXA(entrada_rotina)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
σ² = Dados_C11[1]
Perfis_da_Intensidade = Dados_C11[2]
Posição_Sensores = Dados_C11[3]
Lₐ_titulo = round(Lₐ,digits=2)
###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


include("PLOTS/Plots.jl")
vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores[:,2],Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,ρ,L"$L = %$L , \Delta = 0,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)

include("PLOTS/Plots.jl")
# vizualizar_Perfil_da_Cintura_e_Transmissao(Posição_Sensores,Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,1000)



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C11.jld2", Dict("SAIDA" => Dados_C11,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-June-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-June-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-June-30}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-June-30}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L=10.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L=10.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-5}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-5}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-6}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{PURO}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{PURO}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{PURO}_L{10}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{PURO}_L{10}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-7}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-10}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-10}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{10}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{10}_CAMPO{FAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{10}_CAMPO{FAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-12}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-July-12}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-3}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-3}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-3}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-3}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{20}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-10}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{25}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-10}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{25}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{9}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{9}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-25}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{18}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-25}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{18}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{18}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{18}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{18}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-26}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{18}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-30}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{18}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-30}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{PURO}_L{18}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{7.818267635707035}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{7.818267635707035}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{13.98058263781148}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{13.98058263781148}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{24.999999999999993}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{24.999999999999993}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{7.818267635707035}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{7.818267635707035}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-5}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-5}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{30}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{30}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/TEMPORARIO_C11_13_12.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/TEMPORARIO_C11_13_12.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-December-14}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-December-14}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-December-14}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{14}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-December-14}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{14}.jld2", "Parametros")
Lₐ = Parametros_Antigos.Lₐ

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2023-January-16}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{30}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2023-January-16}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{30}.jld2", "Parametros")
Lₐ = Parametros_Antigos.Lₐ

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/TEMPORARIO_C11.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/TEMPORARIO_C11.jld2", "Parametros")
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

σ² = Dados_Antigos[1]
Perfis_da_Intensidade = Dados_Antigos[2]
Posição_Sensores = Dados_Antigos[3]
# Perfis_da_Intensidade[ findall(Perfis_da_Intensidade .== 0) ] .= 0.000001

Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
Geometria = Parametros_Antigos.Geometria
Ns = Parametros_Antigos.Ns
b₀s = Parametros_Antigos.b₀s
kR = Parametros_Antigos.kR
Lₐ = Parametros_Antigos.Lₐ
Lₐ_titulo = round(Lₐ,digits=2)
L = Parametros_Antigos.L
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
k = Parametros_Antigos.k
Δ = Parametros_Antigos.Δ


Ω = Parametros_Antigos.Ω
Angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
λ = Parametros_Antigos.λ
ωₗ = Parametros_Antigos.ωₗ

Variavel_Constante = Parametros_Antigos.Variavel_Constante
N_Realizações = Parametros_Antigos.N_Realizações
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
ρ = zeros(size(Ns,1))
@.ρ = Ns/(L*Lₐ)
ρ_plot = round(ρ[1],digits=2)


model_energia(x, p) = log.(x).*p[1] .+ p[2] 
xdata = load("DADOS_ENERGIA.jld2", "SAIDA")[1]
ydata = load("DADOS_ENERGIA.jld2", "SAIDA")[2]
p0 = [0.25, 1.0]

fitting_energia = curve_fit(model_energia, xdata, ydata, p0)

a_energia = fitting_energia.param[1] 
b_energia = fitting_energia.param[2]     


itp = LinearInterpolation(xdata,ydata)
# Δ = round.(itp(ρ),digits=2)
Δ = similar(ρ)
include("PLOTS/Plots.jl")
vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores,Perfis_da_Intensidade[:,:],Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,ρ,L"$ \Delta = \Delta_{LOC},kL_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)
# savefig("exemplo_6.png")


# i = 1
# plot(Posição_Sensores,Perfis_da_Intensidade[:,i],yscale=:log10)

# i += 1
# plot!(Posição_Sensores,Perfis_da_Intensidade[:,i],yscale=:log10)

# savefig("perfil_transversal_kernel={$Tipo_de_kernel}_L={$L}_Δ={$Δ}.png")
# savefig("perfil_VETORIAL_ROBIN.png")

# diferença = zeros((size(Perfis_da_Intensidade,2)-1))  
# for i in 1:(size(Perfis_da_Intensidade,2)-1)
#     A = @time (mean_and_std(Posição_Sensores[:,2], Weights(Perfis_da_Intensidade[:,i] ./ minimum(Perfis_da_Intensidade[:,i])); corrected = false)[2])^2
#     B = @time get_one_σ(Posição_Sensores[:,:],Perfis_da_Intensidade[:,i] ./ minimum(Perfis_da_Intensidade[:,i]),Geometria)[2]
#     diferença[i] = A-B
# end
# plot(ρ,diferença)


# B = Perfis_da_Intensidade[:,1]
# mean_and_std(Posição_Sensores[:,2], Weights(B); corrected = false)[2]
# get_one_σ(Posição_Sensores[:,:],B,Geometria)[2]
# plot(Posição_Sensores[:,2],B)


# C = Perfis_da_Intensidade[:,1] ./ minimum(Perfis_da_Intensidade[:,1])
# mean_and_std(Posição_Sensores[:,2], Weights(C); corrected = false)[2]
# get_one_σ(Posição_Sensores[:,:],C,Geometria)[2]
# plot(Posição_Sensores[:,2],C)


# D = log10.(Perfis_da_Intensidade[:,1])
# mean_and_std(Posição_Sensores[:,2], Weights(D); corrected = false)[2]
# get_one_σ(Posição_Sensores[:,:],D,Geometria)[2]
# plot(Posição_Sensores[:,2],D)



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function get_one_σ(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores[ : ]
    ydata = log.(Intensidade)

    p0 = [maximum(ydata),0,(maximum(xdata)-minimum(xdata))/2]
    teste = curve_fit(model_gaussiana_log, xdata, ydata, p0)
    σ² = teste.param[3]^2    

    return σ²,[exp(teste.param[1]),teste.param[2],teste.param[3]]
end

# μ, σ = mean_and_std(, Weights(); corrected = false)


interresse = 1
σ²_plot,p = get_one_σ(Posição_Sensores,Perfis_da_Intensidade[:,interresse],SLAB())

plot!(Posição_Sensores[:,2],model_gaussiana(Posição_Sensores[:,2],p),lw=6,lab=L"$ \sigma^2 =  %$σ²_plot $")


include("PLOTS/Plots.jl")
vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores[:,2],Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,ρ,L"$L = %$L , \Delta = 0,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)




p = [10,2,1]
xdata = -20:0.1:20
ydata = model_gaussiana(xdata,p)

plot(xdata,ydata)

# using Plots
# using SpecialFunctions

# x = range(pi, 10, length=100)

# A = sqrt.(2 ./ (π.*x)).*cis.((x) .- (π./4))
# B = SpecialFunctions.besselh.(0,x)

# plot(x,real.(A),lw=5)
# plot!(x,real.(B),lw=5)

# plot(x,imag.(A),lw=5)
# plot!(x,imag.(B),lw=5)

# plot(x,(real.(A) .- real.(B))./(real.(A) .+ real.(B)),lw=5)
# plot(x,(imag.(A) .- imag.(B))./(imag.(A) .- imag.(B)),lw=5)




# Lei_de_Ray(x) =  exp(-x)
# x = range(0, 180, length=1000)
# y = Lei_de_Ray.(x)
# plot!(x,y,
# lw = 6,
# color = :green,
# linestyle = :dash,
# lab = L"$e^{-x}$"
# )
# savefig("Perfil_Transmissao_TRANSMISSION_Vetorial_LOG.png")

# using Distributions
# using Pkg 
# Pkg.add("Distributions")
# paleta_de_cores = palette(:rainbow, 4)

# using Distributions
# fit(Normal, Perfis_da_Intensidade[:,])

# using StatsBase

# A = 1
# σ = (mean_and_std(Posição_Sensores[findall(Perfis_da_Intensidade[:,A] .> 10^-7),2], Weights(Perfis_da_Intensidade[findall(Perfis_da_Intensidade[:,A] .> 10^-7),A] ); corrected = false)[2])^2
# A += 1

# k = 4
# model(x, p) = p[1].*x .+ p[2] 
# xdata = log10.(Posição_Sensores[1501:end,2])
# ydata = log10.(Perfis_da_Intensidade[1501:end,k])
# p0 = [1.0,10]



# fit = curve_fit(model, xdata, ydata, p0)

# a_sol_plot = fit.param[1]                                 # Coefiente angular da melhor reta 
# b_sol_plot = fit.param[2]
# # c_sol_plot = fit.param[3]
# # d_sol_plot = fit.param[4]


# law_1(x) = exp10.(a_sol_plot.*(log10.(x)) .+ b_sol_plot)
# y = law_1.(Posição_Sensores[1501:end,2])

# a_sol_plot = round(a_sol_plot,digits=2)
# b_sol_plot = round(b_sol_plot,digits=2)
# # c_sol_plot = round(a_sol_plot,digits=2)
# # d_sol_plot = round(a_sol_plot,digits=2)

# plot!(Posição_Sensores[1501:end,2],y,
# lw = 8,
# linestyle=:dashdot,
# c = paleta_de_cores[k],
# lab = L"$I = exp (%$a_sol_plot y + %$b_sol_plot) $"
# # lab = ""
# )



# Lₐ = 150
# b₀ = 10
# L = 10

# N = round(Int64,(b₀*L)/4)
# ρ = N/(L*Lₐ)
# rₘᵢₙ = 1/(10*sqrt(ρ))


# using Pkg

# Pkg.add("Distances")
                                                                                                   
# Pkg.add("LinearAlgebra")                                                                                                             
# Pkg.add("LaTeXStrings")                                                                                                               
# Pkg.add("Random")   
# Pkg.add("Statistics")                                                                                                                
# Pkg.add("Plots")    
# Pkg.add("SpecialFunctions")                                                                                                          
# Pkg.add("QuadGK")   
# Pkg.add("PlotThemes")                                                                                                                
# Pkg.add("StatsBase") 
# Pkg.add("LsqFit")
# Pkg.add("Optim")
# Pkg.add("ProgressMeter")
# Pkg.add("FileIO")
# Pkg.add("JLD2")
# Pkg.add("Dates")

# scatter(Posição_Sensores[1401:end,2],Perfis_da_Intensidade[1401:end,1],yscale = :log10)
# model(x, p) = p[1].*(exp.(-p[2].*x))
# x_data = Posição_Sensores[1401:end,2]
# y_data = Perfis_da_Intensidade[1401:end,1]
# p0 = [10.0,1.0]

# reta = curve_fit(model, x_data, y_data, p0)

# a = reta.param[1]         
# b = reta.param[2]

# vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores[:,2],Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,L"$L = %$L , \Delta = %$Δ ,L_a = %$Lₐ, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)


# Lei_1(x) = -0.000001*x^(1/20) + 0.000001
# x = range(0, 4000, length=1000)
# y = Lei_1.(x)
# plot!(x,y,
# lw = 6,
# color = :black,
# linestyle = :dash,
# lab = L"$-x$"
# )

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# f_1(x) = abs(real(SpecialFunctions.besselh(0,1,x)))
# f_2(x) = abs(real(SpecialFunctions.besselh(0,2,x)))
# f_3(x) = abs(real(((1-1im)*cis(x))/ sqrt(x)))
# f_4(x) = abs(real(sqrt(2/(π*x))*cis(x-(π/4))))

# # x = range(0, 100, length=50)
# x = get_points_in_log_scale(0.01,1000,50)
# y_1 = f_1.(x)
# y_2 = f_2.(x)
# y_3 = f_3.(x)
# y_4 = f_4.(x)

# gr()

# plot(x,y_1,
# m=:star4,
# ms = 10,
# lw=0,
# color = :black,
# lab = L"$abs(H_0^{(1)})$",
# size = (1000, 500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 5Plots.mm,
# bottom_margin = 5Plots.mm,
# framestyle = :box,
# # gridalpha = 0.3,
# yscale = :log10,
# xscale = :log10,
# legendfontsize = 20,
# labelfontsize = 25,
# titlefontsize = 30,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black,

# )

# plot!(x,y_3,
# lw = 0,
# m = :star3, 
# color = :red,
# linestyle = :dash,
# lab = L"$Maximo$"
# )

# plot!(x,y_2,
# lw = 0,
# m = :square, 
# color = :blue,
# linestyle = :dash,
# lab = L"$abs(H_0^{(2)})$"
# )

# plot!(x,y_2,
# lw = 0,
# m = :circle, 
# color = :orange,
# linestyle = :dash,
# lab = L"$WIKI$"
# )


# savefig("Hankel.png")

# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-21}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-21}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")

# Dados_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{40}.jld2", "SAIDA")
# Parametros_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{40}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# Dados_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos_VETORIAL = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

σ²_VETORIAL = Dados_Antigos_VETORIAL[1]
Perfis_da_Intensidade_VETORIAL = Dados_Antigos_VETORIAL[2][:,1]
Posição_Sensores_VETORIAL = Dados_Antigos_VETORIAL[3]
Lₐ = Parametros_Antigos_VETORIAL.Lₐ

Dados_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")

# Dados_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{40}.jld2", "SAIDA")
# Parametros_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-17}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{40}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ
# Dados_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos_ESCALAR = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ
σ²_ESCALAR = Dados_Antigos_ESCALAR[3]
Perfis_da_Intensidade_ESCALAR = Dados_Antigos_ESCALAR[2][:,1]
Posição_Sensores_ESCALAR = Dados_Antigos_ESCALAR[3]
Lₐ = Parametros_Antigos_ESCALAR.Lₐ

Dados_Antigos_VAZIO = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos_VAZIO = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-20}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")

σ²_VAZIO = Dados_Antigos_VAZIO[3]
Perfis_da_Intensidade_VAZIO = Dados_Antigos_VAZIO[2][:,3]
Posição_Sensores_VAZIO = Dados_Antigos_VAZIO[3]
Lₐ = Parametros_Antigos_VAZIO.Lₐ


L_plot = round(Parametros_Antigos_ESCALAR.L,digits=2)
ρ_plot = round(Parametros_Antigos_ESCALAR.Ns[1]/(Parametros_Antigos_ESCALAR.L*Parametros_Antigos_ESCALAR.Lₐ),digits=2)
Lₐ_titulo = round(Parametros_Antigos_VETORIAL.Lₐ,digits=2)
GEOMETRIA_DA_NUVEM = Parametros_Antigos_VETORIAL.GEOMETRIA_DA_NUVEM
N_Realizações = Parametros_Antigos_VETORIAL.N_Realizações



plot(Posição_Sensores_ESCALAR,Perfis_da_Intensidade_ESCALAR./(maximum(Perfis_da_Intensidade_ESCALAR)),
ms = 0,
size = (1000,500),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
minorgridstyle = :dashdot,
linestyle=:solid,
yscale = :log10,
# xscale = :log10,
ylims = (10^-2.5,10^0),
# xlims = (10^0.5,10^2),
# c= :red,
lw = 8,
# lab = "",
lab = L"$Scalar$",
xlabel=L"$ kY $",
ylabel=L"$ I / I_{max}$",
framestyle = :box,
legendfontsize = 12,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
title=L"$kL_z = %$L_plot ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"
)

plot!(Posição_Sensores_VETORIAL,Perfis_da_Intensidade_VETORIAL./(maximum(Perfis_da_Intensidade_VETORIAL)),
ms = 0,
size = (1000,500),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
minorgridstyle = :dashdot,
linestyle=:solid,
yscale = :log10,
# xscale = :log10,
# ylims = (10^-3,10^1),
# xlims = (10^-1,10^2),
# c= :red,
lw = 8,
# lab = "",
lab = L"$Vectorial$",
xlabel=L"$ kY $",
ylabel=L"$ Intensity $",
framestyle = :box,
legendfontsize = 12,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black
)



plot!(Posição_Sensores_VAZIO,Perfis_da_Intensidade_VAZIO./(maximum(Perfis_da_Intensidade_VAZIO)),
ms = 0,
size = (1000,500),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
minorgridstyle = :dashdot,
linestyle=:dash,
yscale = :log10,
# xscale = :log10,
# ylims = (10^-2.5,10^0),
# xlims = (10^0.5,10^2),
# c= :red,
lw = 5,
c = :black,
# lab = "",
lab = L"$\textrm{Gaussian Beam - Incident}$",
xlabel=L"$ kY $",
ylabel=L"$ I / I_{max}$",
framestyle = :box,
legend=:bottomleft,
legendfontsize = 12,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
title=L"$kL_z = %$L_plot ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"
)

# decay_1(x) = exp(-x)
# x = range(10^0, 100, length=100)
# y = decay_1.(x)
# plot!(x,y,
# lw = 4,
# linestyle=:dash,
# lab = L"$ \exp(-y) $"
# )

# decay_2(x) = 1/x
# x = range(10^0.5, 100, length=100)
# y = decay_2.(x)
# plot!(x,y,
# lw = 4,
# linestyle=:dash,
# lab = L"$ 1/y $"
# )

# decay_3(x) = 1/x^2
# x = range(10^0.5, 100, length=100)
# y = decay_3.(x)
# plot!(x,y,
# lw = 4,
# linestyle=:dash,
# lab = L"$ 1/y^2 $"
# )


index_range = findall(Posição_Sensores_ESCALAR.>20)

x = Posição_Sensores_ESCALAR[index_range]
y = Perfis_da_Intensidade_ESCALAR[index_range]./(maximum(Perfis_da_Intensidade_ESCALAR))

# plot(x,y)
model_1_log(x, p) = (p[1] ./ (x.^p[2]))
xdata = x
ydata = (y)

# plot(xdata,ydata,lw=6)
p0 = [20.0,1.0]

teste_1 = curve_fit(model_1_log, xdata, ydata, p0)
potencia = teste_1.param
a_plot = round(potencia[1],digits=2)
b_plot = round(potencia[2],digits=2)

model_2(x, p) = (-x ./ (p[1])) .+ p[2] 
xdata = x
ydata = log10.(y)

# plot(xdata,ydata,lw=6)
p0 = [5.0,0.0]

teste_2 = curve_fit(model_2, xdata, ydata, p0)
ξ = teste_2.param[1]
α = teste_2.param[2]
ξ_plot = round(ξ,digits=2)
α_plot = round(exp10(α),digits=2)
 


plot(Posição_Sensores_ESCALAR[findall(Posição_Sensores_ESCALAR.>0)],Perfis_da_Intensidade_ESCALAR[findall(Posição_Sensores_ESCALAR.>0)]./(maximum(Perfis_da_Intensidade_ESCALAR)), 
    ms = 0,
    size = (1000,500),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    linestyle=:solid,
    ylims = (10^-4,10^0.01),
    # xlims = (10^1,10^2),
    lab=L"$ Escalar $",
    lw=6,
    styleline=:dash,
    yscale=:log10,
    # xscale=:log10,
    xlabel=L"$ kY $",
    ylabel=L"$ I / I_{max}$",
    framestyle = :box,
    legendfontsize = 12,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    title=L"$kL_z = %$L_plot ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"

)

# plot!(0.1:0.1:100,model_1_log(0.1:0.1:100,potencia),lab=L"$ f(y) = %$a_plot / y^{%$b_plot} $",lw=3,linestyle=:dashdot,size(1000,500),yscale=:log10,xscale=:log10,c=:black)
plot!(0.1:0.1:80,exp10.(model_2(0.1:0.1:80,[ξ,α])),lab=L"$ f(y) = \exp{(-y/ %$ξ_plot)} + %$α_plot $",c=:black)

# ---------------------------------------------------- #
index_range = findall(Posição_Sensores_VETORIAL.>10^(1.6))

x = Posição_Sensores_VETORIAL[index_range]
y = Perfis_da_Intensidade_VETORIAL[index_range]./(maximum(Perfis_da_Intensidade_VETORIAL))

model_1_log(x, p) = (p[1] ./ (x.^p[2]))
xdata = x
ydata = y

# plot(xdata,ydata,lw=6)
p0 = [20.0,1.0]
model_1_log(xdata, p0)


teste = curve_fit(model_1_log, xdata, ydata, p0)
potencia = teste.param
a_plot = round(potencia[1],digits=2)
b_plot = round(potencia[2],digits=2)




plot!(Posição_Sensores_VETORIAL[findall(Posição_Sensores_VETORIAL.>0)],Perfis_da_Intensidade_VETORIAL[findall(Posição_Sensores_VETORIAL.>0)]./(maximum(Perfis_da_Intensidade_VETORIAL)), 
    ms = 0,
    size = (1000,500),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    linestyle=:solid,
    # ylims = (10^-2.5,10^0.01),
    # xlims = (10^1.2,10^1.9),
    lab=L"$ Vetorial $",
    lw=6,
    styleline=:dash,
    yscale=:log10,
    # xscale=:log10,
    c = :blue,
    xlabel=L"$ kY $",
    ylabel=L"$ I / I_{max}$",
    framestyle = :box,
    legendfontsize = 12,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    title=L"$kL_z = %$L_plot ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"

)

plot!(0.1:0.1:100,model_1_log(0.1:0.1:100,potencia),lab=L"$ f(y) = %$a_plot / y^{%$b_plot} $",lw=3,linestyle=:dashdot,size(1000,500),yscale=:log10,xscale=:log10,c=:red)



plot!(Posição_Sensores_VAZIO[findall(Posição_Sensores_VAZIO.>0)],Perfis_da_Intensidade_VAZIO[findall(Posição_Sensores_VAZIO.>0)]./(maximum(Perfis_da_Intensidade_VAZIO)), 
    ms = 0,
    size = (1000,500),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    linestyle=:solid,
    # ylims = (10^-2.5,10^0.01),
    # xlims = (10^1.2,10^1.9),
    lab=L"$ \textrm{ Gaussian Beam} $",
    lw=6,
    styleline=:dash,
    yscale=:log10,
    # xscale=:log10,
    legend=:bottomleft,
    c = :blue,
    xlabel=L"$ kY $",
    ylabel=L"$ I / I_{max}$",
    framestyle = :box,
    legendfontsize = 12,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    title=L"$kL_z = %$L_plot ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"

)
# savefig("PERFIL_TRANSVERSAL_LOG_FIT.png")
# savefig("PERFIL_COMPARA.png")

savefig("PERFIL_TRANSVERSAL.png")

# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Dados_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{7.818267635707035}.jld2", "SAIDA")
# Parametros_Antigos_1 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{7.818267635707035}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ

# σ²_1 = Dados_Antigos_1[1]
# Perfis_da_Intensidade_1 = Dados_Antigos_1[2][:,1]
# Posição_Sensores = Dados_Antigos_1[3]
# L_1 = round(Parametros_Antigos_1.L,digits=2)
# b₀_1 = round((4*Parametros_Antigos_1.Ns)/(Parametros_Antigos_1.Lₐ),digits=2)

# Dados_Antigos_2 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{13.98058263781148}.jld2", "SAIDA")
# Parametros_Antigos_2 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{13.98058263781148}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ
# σ²_2 = Dados_Antigos_2[1]
# Perfis_da_Intensidade_2 = Dados_Antigos_2[2][:,1]
# Posição_Sensores = Dados_Antigos_1[3]
# L_2 = round(Parametros_Antigos_2.L,digits=2)
# b₀_2 = round((4*Parametros_Antigos_2.Ns)/(Parametros_Antigos_2.Lₐ),digits=2)

# Dados_Antigos_3 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{24.999999999999993}.jld2", "SAIDA")
# Parametros_Antigos_3 = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{24.999999999999993}.jld2", "Parametros")
# Lₐ = Parametros_Antigos.Lₐ
# σ²_3 = Dados_Antigos_3[1]
# Perfis_da_Intensidade_3 = Dados_Antigos_3[2][:,1]
# Posição_Sensores = Dados_Antigos_3[3]
# L_3 = round(Parametros_Antigos_3.L,digits=2)
# b₀_3 = round((4*Parametros_Antigos_3.Ns)/(Parametros_Antigos_3.Lₐ),digits=2)

# ρ_plot = round(ρ[1],digits=2)

# plot(Posição_Sensores,Perfis_da_Intensidade_1,
# ms = 0,
# size = (1000,500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# minorgridstyle = :dashdot,
# linestyle=:solid,
# yscale = :log10,
# ylims = (10^-20,10^-5),
# # c= :red,
# lw = 8,
# lab = L"$ kL = %$L_1 , b_0 = %$b₀_1 $",
# xlabel=L"$ kY $",
# ylabel=L"$ I/I_{min} $",
# framestyle = :box,
# legendfontsize = 12,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# legend=:topright,
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black
# )

# plot!(Posição_Sensores,Perfis_da_Intensidade_2,
# ms = 0,
# size = (1000,500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# minorgridstyle = :dashdot,
# linestyle=:solid,
# yscale = :log10,
# ylims = (10^-15,10^-6),
# # c= :red,
# lw = 8,
# lab = L"$kL = %$L_2 , b_0 = %$b₀_2 $",
# xlabel=L"$ kY $",
# ylabel=L"$ Intensity $",
# framestyle = :box,
# legendfontsize = 12,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black,
# title=L"$kL_z = %$L ,kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"
# )

# plot!(Posição_Sensores,Perfis_da_Intensidade_3,
# ms = 0,
# size = (1000,500),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# minorgridstyle = :dashdot,
# linestyle=:solid,
# yscale = :log10,
# ylims = (10^-16,10^-8),
# # c= :red,
# lw = 8,
# lab = L"$kL = %$L_3 , b_0 = %$b₀_3 $",
# xlabel=L"$ kY $",
# ylabel=L"$ I  $",
# framestyle = :box,
# legendfontsize = 12,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black,
# title=L"$ kL_y = %$Lₐ_titulo, \Delta = \Delta_{LOC},\rho = %$ρ_plot, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $"
# )

# savefig("PERFIL_TRANSMISSAO_2.png")

# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{7.818267635707035}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{7.818267635707035}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{FAR FIELD}_L{20}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-18}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-19}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-October-19}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_CAMPO{NEAR FIELD}_L{20}.jld2", "Parametros")

σ² = Dados_Antigos[1]
Perfis_da_Intensidade = Dados_Antigos[2][:,1]
Posição_Sensores= Dados_Antigos[3]

ρ_plot = round(ρ[1],digits=2)

plot(Posição_Sensores,Perfis_da_Intensidade,
ms = 0,
size = (1000,500),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
minorgridstyle = :dashdot,
linestyle=:solid,
yscale = :log10,
ylims = (10^-45,10^-35),
# c= :red,
lw = 4,
lab = "",
xlabel=L"$ \theta $",
ylabel=L"$ Intensity $",
framestyle = :box,
legendfontsize = 17,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black
)


plot(Posição_Sensores.*(pi/180),log10.(Perfis_da_Intensidade[:,1]) .- minimum(log10.(Perfis_da_Intensidade[:,1])),
lw = 4,
proj = :polar,
# lims = (0,1),
c = :blue,
size = (1000,1000),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 5Plots.mm,
bottom_margin = 5Plots.mm,
gridalpha = 1,
minorgrid = true,
minorgridalpha = 1,
minorgridstyle = :dashdot,
lab= "",
title = "",
# legendtitle = "Δ = $Δ_ap
# ρ/k² = $ρ_normalizado  ",
framestyle = :box,
legendtitlefontsize = 20;
legendfontsize = 20,
labelfontsize = 25,
titlefontsize = 30,
tickfontsize = 13, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black
)


# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# F(x) = sqrt(2/(π*k*x))*cis((k*x)-(π/4))
# G(x) =  SpecialFunctions.besselh(0,x) 


# x = range(0, 100, length=100)
# C1 = F.(x)
# C2 = G.(x)

# real_1 = real.(C1)
# real_2 = real.(C2)

# imag_1 = imag(C1)
# imag_2 = imag(C2)

# plot(x,real_1,
#     lw = 4,
#     size = (1000,1000),
#     left_margin = 10Plots.mm,
#     right_margin = 12Plots.mm,
#     top_margin = 5Plots.mm,
#     bottom_margin = 5Plots.mm,
#     framestyle = :box,
#     # ylims = (-0.1,0.1),
#     legendtitlefontsize = 20;
#     legendfontsize = 20,
#     labelfontsize = 25,
#     titlefontsize = 30,
#     tickfontsize = 13, 
#     background_color_legend = :white,
#     background_color_subplot = :white,
#     foreground_color_legend = :black
# )

# plot!(x,real_2,
#     lw = 4,
#     size = (1000,1000),
#     left_margin = 10Plots.mm,
#     right_margin = 12Plots.mm,
#     top_margin = 5Plots.mm,
#     bottom_margin = 5Plots.mm,
#     framestyle = :box,
#     # ylims = (-0.1,0.1),
#     legendtitlefontsize = 20;
#     legendfontsize = 20,
#     labelfontsize = 25,
#     titlefontsize = 30,
#     tickfontsize = 13, 
#     background_color_legend = :white,
#     background_color_subplot = :white,
#     foreground_color_legend = :black
# )
