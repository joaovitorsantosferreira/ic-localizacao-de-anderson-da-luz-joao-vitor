###############################################################################################################################################################################################################
######################################################## Programa de Controle 23 - Contempla simulações Voltadas a Variância para diferentes Densidades e Δ ####################################################
##############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances    
using LinearAlgebra
using LaTeXStrings  
using Random       
using Statistics   
using Plots        
using SpecialFunctions
using QuadGK       
using PlotThemes   
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Roots
using Interpolations
using Loess


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R23.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R11.jl")
include("PLOTS/Plots.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R11.jl") 

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-August-31}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-August-31}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-September-11}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-September-11}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-September-18}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/DATA={2022-September-18}_Dados_Antigos_CLUSTER_C24_{SLAB}_{Escalar}.jld2", "Parametros")

# # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/TEMPORARIO_C24.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C24/TEMPORARIO_C24.jld2", "Parametros")
# # -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Histograma_Diagrama_de_Fase = Dados_Antigos 

ωₐ = Parametros_Antigos.ωₐ 
Γ₀ = Parametros_Antigos.Γ₀ 
Lₐ = Parametros_Antigos.Lₐ 
N_div_Densidade = Parametros_Antigos.N_div_Densidade
N_div_Deturn = Parametros_Antigos.N_div_Deturn 
Δ_Inicial = Parametros_Antigos.Δ_Inicial 
Δ_Final = Parametros_Antigos.Δ_Final                                                                                                     
Densidade_Inicial = Parametros_Antigos.Densidade_Inicial 
Densidade_Final = Parametros_Antigos.Densidade_Final 
Geometria = Parametros_Antigos.Geometria 
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel 
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda 
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta 
k = Parametros_Antigos.k 
Ω = Parametros_Antigos.Ω 
Angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda 
λ = Parametros_Antigos.λ
N_Sensores = Parametros_Antigos.N_Sensores 
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores 
Angulo_de_variação_da_tela_circular_1 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_1 
Angulo_de_variação_da_tela_circular_2 = Parametros_Antigos.Angulo_de_variação_da_tela_circular_2
angulo_coerente = Parametros_Antigos.angulo_coerente 
Escala_do_range = Parametros_Antigos.Escala_do_range 
PERFIL_DA_INTENSIDADE = Parametros_Antigos.PERFIL_DA_INTENSIDADE
N_Realizações = Parametros_Antigos.N_Realizações   
RANGE_INICIAL = Parametros_Antigos.RANGE_INICIAL 
RANGE_FINAL = Parametros_Antigos.RANGE_FINAL 
N_pontos = Parametros_Antigos.N_pontos 
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W              

Histograma = zeros(size(Histograma_Diagrama_de_Fase,1),size(Histograma_Diagrama_de_Fase,2))
Histograma[:,1] = Histograma_Diagrama_de_Fase[:,2]
Histograma[:,2] = Histograma_Diagrama_de_Fase[:,1]
Histograma[:,3] = Histograma_Diagrama_de_Fase[:,3]

Histograma[:,3] = norm.(Histograma[:,3])

RANGE_INICIAL_plot = round(RANGE_INICIAL,digits = 0)
RANGE_FINAL_plot = round(RANGE_FINAL,digits = 0)


# Histograma[findall(Histograma[:,3] .== 0),3] .= 1

include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_ξ(Histograma,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Δ_Inicial,Δ_Final,L"$Fitting - T,  %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP,L_z = [%$RANGE_INICIAL_plot,%$RANGE_FINAL_plot],L_y = %$Lₐ, log_{10}(\xi) $",1000)
# savefig("Diagrama_de_Fase_fitting_Transmissao.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# d = Normal()
# fwhm(d)
# using DataFrames


# df = DataFrame(Densidade = Histograma_Diagrama_de_Fase[:,1], Delta = Histograma_Diagrama_de_Fase[:,2] , Comprimento = Histograma_Diagrama_de_Fase[:,3])
# show(df, allcols=true,allrows=true)