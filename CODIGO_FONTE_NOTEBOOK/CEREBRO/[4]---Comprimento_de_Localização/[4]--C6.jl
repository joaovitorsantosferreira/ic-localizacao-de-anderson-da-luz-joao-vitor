###############################################################################################################################################################################################################
################################################################ Programa de Controle 6 - Contempla simulações Voltadas o comprimento de localização ##########################################################
###############################################################################################################################################################################################################



###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Interpolations


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R5.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E3.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[3]---Comprimento__De__Localização/R4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[3]---Comprimento__De__Localização/R5.jl")



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Caracteristicas da Nuvem __ Raio do Sistema Fixado ----------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

N = 100                                                                                                                         # Número de atomos caso N seja fixado
kR = 1                                                                                                                        # Raio normalizado do sistema caso kR seja fixado
L = 2

Lₐ = 50

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 1 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

k = 1                                                                                                                      # Número de Onda

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################ DEFINIÇÂO 2 - Variavel constante no Diagrama de Fase #############################################################################
Variavel_Constante = "N"                                                                     # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "kR"                                                                    # Define que o raio do sistma será a quantidade fixada nas realizações, presupondo uma geometria de DISCO
# Variavel_Constante = "L"                                                                     # Define que o as dimensões do SLAB serão a quantidade fixada nas realizações, presupondo uma geometria de SLAB 
########################################################################### DEFINIÇÂO 3 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_de_Densidade = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_de_Densidade = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 1                                                                                               # Define a quantidade de realizações por locus no diagrama de Fase
N_div_Densidade = 10                                                                                            # Define a quatidade de divisões no eixo y do Diagrama, que é a densidade normalizada do sistema
N_div_Deturn = 10                                                                                               # Define a quatidade de divisões no eixo x do Diagrama, que é o Deturn do sistema

Δ_Inicial = -10                                                                                                 # Define o Valor Inicial do Deturn
Δ_Final = 10                                                                                                    # Define o Valor Final do Deturn
Densidade_Inicial = 1                                                                                           # Define o Valor Inicial da Densidade
Densidade_Final = 2                                                                                            # Define o Valor Final da Densidade


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R5_ENTRADA(
    Γ₀,
    k,
    N,
    kR,
    L,
    Lₐ,
    Tipo_de_kernel,
    N_div_Densidade,
    N_div_Deturn,
    Densidade_Inicial,
    Densidade_Final,
    Δ_Inicial,
    Δ_Final,
    Variavel_Constante,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM
)

Dados = ROTINA_5__Comprimento_de_localização_Δ_VARIAVEL(entrada_rotina)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Histograma_Resultante = Dados

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


vizualizar_Diagrama_de_Fase_ξ(Histograma_Resultante,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Δ_Inicial,Δ_Final,Escala_de_Densidade,1000)
# savefig("Diagrama__ξ.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C6.jld2", Dict("SAIDA" => Dados_C6,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-3}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-3}_Dados_Antigos_CLUSTER_C6.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-4}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-4}_Dados_Antigos_CLUSTER_C6.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-17}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-June-17}_Dados_Antigos_CLUSTER_C6.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-September-11}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/DATA={2022-September-11}_Dados_Antigos_CLUSTER_C6.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/TEMPORARIO_C6_14_09.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/TEMPORARIO_C6_14_09.jld2", "Parametros")

# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/TEMPORARIO_C6.jld2", "Parametros")
Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C6/TEMPORARIO_C6.jld2", "SAIDA")
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Histograma_Diagrama_de_Fase = Dados_Antigos 

Γ₀ = Parametros_Antigos.Γ₀
k = Parametros_Antigos.k
L = Parametros_Antigos.L
Lₐ = Parametros_Antigos.Lₐ
Radius = Parametros_Antigos.Radius
Radius = 4*pi
N_Realizações = Parametros_Antigos.N_Realizações
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
N_div_Densidade = Parametros_Antigos.N_div_Densidade
N_div_Deturn = Parametros_Antigos.N_div_Deturn
Densidade_Inicial = Parametros_Antigos.Densidade_Inicial
Densidade_Final = Parametros_Antigos.Densidade_Final
Δ_Inicial = Parametros_Antigos.Δ_Inicial
Δ_Final = Parametros_Antigos.Δ_Final
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
Geometria = Parametros_Antigos.Geometria             
Radius_plot = round(Int64,Radius)


# Histograma = zeros(size(Histograma_Diagrama_de_Fase,1),size(Histograma_Diagrama_de_Fase,2))
# Histograma[:,1] = Histograma_Diagrama_de_Fase[:,2]
# Histograma[:,2] = Histograma_Diagrama_de_Fase[:,1]
# Histograma[:,3] = Histograma_Diagrama_de_Fase[:,3]

Histograma_Diagrama_de_Fase[:,3] = norm.(Histograma_Diagrama_de_Fase[:,3])

include("PLOTS/Plots.jl")
vizualizar_Diagrama_de_Fase_ξ(Histograma_Diagrama_de_Fase,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Δ_Inicial,Δ_Final,L"$Green's,  %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP,kR = %$Radius_plot, log_{10}(\xi) $",1000)
# savefig("Diagrama_de_Fase_ξ.png")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
# ωₐ = 1                                                                                                                        # Frequencia natural dos atomos
# k = 1                                                                                                                         # Número de onda
# Δ = 0
# ωₗ = Δ + ωₐ
# kR = 3
# N_div_Densidade = 100
# Densidade_Inicial = 0.1
# Densidade_Final = 30

# Entrada = Γ₀,ωₐ,k,Δ,ωₗ,kR,Tipo_de_kernel,N_div_Densidade,Densidade_Inicial,Densidade_Final
    
# ρ_normalizados,ξs = get_dados_do_comprimento_de_localização_Raio_Fixo(Entrada)

# vizualizar_Relação_entre_a_Densidade_e_ξ(ρ_normalizados,ξs,kR,1000)
