###############################################################################################################################################################################################################
############################################################# Programa de Controle 5 - Contempla simulações Voltadas Comprimento de Localização Mínimo ########################################################
###############################################################################################################################################################################################################

###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R4.jl")
include("CODIGO_FONTE_CLUSTER/CORPO/Estrutura_dos_Dados/Rotinas/ES-R22.jl")

include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E3.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[3]---Comprimento__De__Localização/R4.jl")



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Caracteristicas da Nuvem __ Número de átomos Fixado ----------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################


Ns = (100,200,300)                                        # Números de atomos caso N seja fixado, aqui você pode entregar um array que contem os números de atomos que você deseja analisar e ele vai entender
kRs = (2,3,4)                                       # Raios normalizado do sistema caso kR seja fixado
Ls = (5,10)

Lₐ = 280                                                  # Tamanho vertical da nuvem para o caso SLAB

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                               # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################


Δ = 0
k = 1

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

############################################################################ DEFINIÇÂO 3 - Variavel constante no Diagrama de Fase #############################################################################
# Variavel_Constante = "N"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
# Variavel_Constante = "kR"                                                                     # Define que o raio do sistma será a quantidade fixada nas realizações, presupondo uma geometria de DISCO
Variavel_Constante = "L"                                                                     # Define que o as dimensões do SLAB serão a quantidade fixada nas realizações, presupondo uma geometria de SLAB 
########################################################################### DEFINIÇÂO 4 - Escala do Eixo Y referente a Densidade ##############################################################################
Escala_de_Densidade = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
# Escala_de_Densidade = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_div_Densidade = 20                                                                                                              # Quantidade de pontos a serem gerados no plot
Densidade_Inicial = 0.01                                                                                                         # Denside Inicial no Range
Densidade_Final = 0.5                                                                                                              # Densidade Final no Range 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

######################################################################################## DEFINIÇÂO 5 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R4_ENTRADA(
    Γ₀,
    k,
    Δ,
    Ns,
    kRs,
    Ls,
    Lₐ,
    Tipo_de_kernel,
    N_div_Densidade,
    Densidade_Inicial,
    Densidade_Final,
    Variavel_Constante,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM
)



Dados_C5 = ROTINA_4__Comprimento_de_localização_Δ_FIXO(entrada_rotina)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
ρ_normalizados,ξs = Dados_C5


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


vizualizar_Relação_entre_a_Densidade_e_ξₘᵢₙ(ρ_normalizados,ξs,Ns,kRs,Ls,Variavel_Constante,Escala_de_Densidade,1000)
# savefig("Relação_ξ_ρ_Kernel={$Tipo_de_kernel}_Δ={$Δ}.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C5.jld2", Dict("SAIDA" => Dados_C5,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/DATA={2021-July-9}_Dados_Antigos_C5.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/DATA={2021-July-9}_Dados_Antigos_C5.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/DATA={2022-June-9}_Dados_Antigos_CLUSTER_C5.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5_2.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5_2.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C5/TEMPORARIO_C5.jld2", "Parametros")

ρ_normalizados,ξs = Dados_Antigos
Ls = Parametros_Antigos.Ls
ρ_Inicial = Parametros_Antigos.ρ_Inicial
ρ_Final  = Parametros_Antigos.ρ_Final 
N_div_Densidade = Parametros_Antigos.N_div_Densidade
Lₐ = Parametros_Antigos.Lₐ 
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel                                                                                     
N_Realizações = Parametros_Antigos.N_Realizações                                                                                                
ρ_normalizados = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade)
Geometria = Parametros_Antigos.Geometria


include("PLOTS/Plots.jl")
vizualizar_Relação_entre_a_Densidade_e_ξₘᵢₙ(ρ_normalizados,abs.(ξs),0,0,Ls,"L","LOG",L"$Escalar,%$Geometria, 2D, L_a = %$Lₐ,%$N_Realizações REP $",1000)
savefig("comprimento_de_localizacao.png")



















#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

