#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_9__Perfil_Cintura_Distancia_FIXA(Variaveis_de_Entrada::R9_ENTRADA)
    

    if Variaveis_de_Entrada.Variavel_Constante == "N"

        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        Q_total = N_Sensores*N_Realizações
        N_Telas = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1

        N_Curvas = size(Variaveis_de_Entrada.Ns,1)
        aux_1 = 1

        σ² = zeros(N_Curvas+1)
        Perfis_da_Intensidade = zeros(N_Sensores,N_Curvas+1)

        for j in 1:N_Curvas 
        

            Intensidade_Resultante_PURA = Any[]
            N = Variaveis_de_Entrada.Ns[j]
            aux_2 = 0

            if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                
                ρ_normalizado = N/(π*(Variaveis_de_Entrada.kR*Variaveis_de_Entrada.k)^2)
                Radius = Variaveis_de_Entrada.kR/Variaveis_de_Entrada.k
                ρ = ρ_normalizado*Variaveis_de_Entrada.k^2
                rₘᵢₙ = 1/(10*sqrt(ρ))   
                ω₀ = Radius/2

            elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                Radius = 1 
                ρ_normalizado = N/((Variaveis_de_Entrada.L)^2)
                ρ = ρ_normalizado
                rₘᵢₙ = 1/(10*sqrt(ρ))
                # ωᵦ = Variaveis_de_Entrada.Lₐ/10
                # ωᵦ = 8*pi
                # ωᵦ = 3*(2*π)
                ωᵦ = Lₐ/10
                ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(Variaveis_de_Entrada.L)^2))/2)
                # ω₀ = Variaveis_de_Entrada.Lₐ/10                                                                                                                                     
                                                                                    
            end

            ProgressMeter.@showprogress 2 "Calculo_Cintura_N = {$(Variaveis_de_Entrada.Ns[aux_1])}===>"    for i in 1:N_Realizações

                entrada_extratora = E2_ENTRADA(
                    Variaveis_de_Entrada.Γ₀,
                    Variaveis_de_Entrada.ωₐ,
                    Variaveis_de_Entrada.Ω,
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Variaveis_de_Entrada.vetor_de_onda,
                    ω₀,
                    Variaveis_de_Entrada.λ,
                    Variaveis_de_Entrada.ωₗ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Tipo_de_Onda,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    Variaveis_de_Entrada.Tipo_de_beta,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.L,
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas,
                    Variaveis_de_Entrada.Geometria
                )
                
                All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)
                append!(Intensidade_Resultante_PURA,All_Intensitys[3 , : ])
            end
        
            Intensidade_media = zeros(N_Sensores)

            for i in 1:N_Sensores
        
                fator_soma = 0
                aux_3 = 0
        
                for k in 1:N_Realizações
        
                    fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                    aux_3 += N_Sensores
        
                end
        
                Intensidade_media[i] = fator_soma/N_Realizações
        
            end

            
            entrada_Sensores = get_cloud_sensors_ENTRADA(
                Variaveis_de_Entrada.kR,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Angulo_de_variação_da_tela_circular_1,
                Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

            σ²[j] = get_one_σ_gaussian(Posição_Sensores[ : ,4],Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            # σ²[j] = (mean_and_std(Posição_Sensores[:,4], Weights(Intensidade_media); corrected = false)[2])^2

            Perfis_da_Intensidade[ : ,j] = Intensidade_media

            aux_1 += 1
        end

        if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                
            ω₀ = Variaveis_de_Entrada.kR/2

        elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

            # ω₀ = Variaveis_de_Entrada.Lₐ/10      
            # ωᵦ = Variaveis_de_Entrada.Lₐ/10
            # ωᵦ = 8*pi
            # ωᵦ = 3*(2*π)
            ωᵦ = Lₐ/10
            ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(Variaveis_de_Entrada.L)^2))/2)                                                                                                                               
                                                                                
        end


        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_Entrada.kR,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Angulo_de_variação_da_tela_circular_1,
            Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

        Intensidade_PURA = zeros(N_Sensores)
        for i in 1:N_Sensores
            
            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                Sensores[i,3:4],
                Variaveis_de_Entrada.Ω,
                ω₀,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Δ
            )
            campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
            
            Intensidade_PURA[i] = abs(campo_laser^2) 
        end
        
        Perfis_da_Intensidade[ : ,end] = Intensidade_PURA[ : ]

        cintura = get_one_σ_lorentz(Sensores[ : ,4],Perfis_da_Intensidade[ : ,end],Variaveis_de_Entrada.Geometria)
        
        σ²[end] = cintura[2]
        # σ²[end] = (mean_and_std(Sensores[:,4], Weights(Intensidade_PURA); corrected = false)[2])^2


    elseif Variaveis_de_Entrada.Variavel_Constante == "b₀"

        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        Q_total = N_Sensores*N_Realizações
        N_Telas = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
    
        N_Curvas = size(Variaveis_de_Entrada.b₀s,1)
        aux_1 = 1
    
        σ² = zeros(N_Curvas+1)
        Perfis_da_Intensidade = zeros(N_Sensores,N_Curvas+1)
    
        for j in 1:N_Curvas 
        
    
            Intensidade_Resultante_PURA = zeros(4,Q_total)
            b₀ = Variaveis_de_Entrada.b₀s[j]
            aux_2 = 0
    
            if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                
                ρ_normalizado = N/(π*(Variaveis_de_Entrada.kR*Variaveis_de_Entrada.k)^2)
                Radius = Variaveis_de_Entrada.kR/Variaveis_de_Entrada.k
                ρ = ρ_normalizado*Variaveis_de_Entrada.k^2
                rₘᵢₙ = 1/(10*sqrt(ρ))   
                ω₀ = Radius/2
    
            elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"
    
                N = round(Int64,(b₀*Variaveis_de_Entrada.Lₐ*Variaveis_de_Entrada.k)/4)
                Radius = 1 
                ρ = N/(Variaveis_de_Entrada.L*Variaveis_de_Entrada.Lₐ)
                rₘᵢₙ = 1/(10*sqrt(ρ))
                ω₀ = Variaveis_de_Entrada.Lₐ/10                                                                                                                                     
                                                                                    
            end
    
            ProgressMeter.@showprogress 2 "Calculo_Cintura_b₀ = {$(Variaveis_de_Entrada.b₀s[aux_1])}===>"    for i in 1:N_Realizações
    
                entrada_extratora = E2_ENTRADA(
                    Variaveis_de_Entrada.Γ₀,
                    Variaveis_de_Entrada.ωₐ,
                    Variaveis_de_Entrada.Ω,
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Variaveis_de_Entrada.vetor_de_onda,
                    ω₀,
                    Variaveis_de_Entrada.λ,
                    Variaveis_de_Entrada.ωₗ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Tipo_de_Onda,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    Variaveis_de_Entrada.Tipo_de_beta,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.L,
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas,
                    Variaveis_de_Entrada.Geometria
                )
                
                All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)
    
                Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 , : ]
                aux_2 +=  N_Sensores
    
            end
        
            Intensidade_media = zeros(N_Sensores)
    
            for i in 1:N_Sensores
        
                fator_soma = 0
                aux_3 = 0
        
                for k in 1:N_Realizações
        
                    fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                    aux_3 += N_Sensores
        
                end
        
                Intensidade_media[i] = fator_soma/N_Realizações
        
            end
    
            
            entrada_Sensores = get_cloud_sensors_ENTRADA(
                Variaveis_de_Entrada.kR,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Angulo_de_variação_da_tela_circular_1,
                Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 
    
            σ²[j] = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
           
            Perfis_da_Intensidade[ : ,j] = Intensidade_media
    
            aux_1 += 1
        end
    
        if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                
            ω₀ = Variaveis_de_Entrada.kR/2
    
        elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"
    
            ω₀ = Variaveis_de_Entrada.Lₐ/10                                                                                                                                     
                                                                                
        end
    
    
        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_Entrada.kR,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Angulo_de_variação_da_tela_circular_1,
            Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)
    
        Intensidade_PURA = zeros(N_Sensores)
        for i in 1:N_Sensores
            
            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                Sensores[i,3:4],
                Variaveis_de_Entrada.Ω,
                ω₀,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Δ
            )
            campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
            
            Intensidade_PURA[i] = abs(campo_laser^2) 
        end
        
        Perfis_da_Intensidade[ : ,end] = Intensidade_PURA[ : ]
    
        cintura = get_one_σ(Sensores[ : ,3:4],Perfis_da_Intensidade[ : ,end],Variaveis_de_Entrada.Geometria)
    
        σ²[end] = cintura[2]

    end


    return σ²,Perfis_da_Intensidade,Sensores[ : ,3:4]
end