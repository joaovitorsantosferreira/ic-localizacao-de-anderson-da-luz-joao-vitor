#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_11__Cintura_e_L(Variaveis_de_Entrada::R11_ENTRADA,Geometria::TwoD)
    
    if Variaveis_de_Entrada.Variavel_Constante == "rmin"
        
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.rₘᵢₙs,1)
        aux_1 = 1
        ω₀ = Lₐ/10

        σ²_L² = zeros(N_pontos,N_Curvas)
        
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$(Variaveis_de_Entrada.rₘᵢₙs[aux_1])}===>" for k in 1:N_pontos

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                
                ρ = 1/((rₘᵢₙs[j]*10)^2)
                N = round(Int64,ρ*Lₐ*(Range_L[k]*rₘᵢₙs[j]))
                
                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        ρ,
                        rₘᵢₙs[j],
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        Range_L[k]*rₘᵢₙs[j],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for k in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                
                kR = 1

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    kR,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    GEOMETRIA_DA_NUVEM,
                    Range_L[k]*rₘᵢₙs[j],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]
                # σ²_L²[k,j] = σ²/((Range_L[k]*rₘᵢₙs[j])^2) 
                σ²_L²[k,j] = σ²


            end
            aux_1 += 1
        end

    elseif Variaveis_de_Entrada.Variavel_Constante == "ρ"
    
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        
        ω₀s = find_ws(Range_L,Lₐ,Geometria)
    
        # ω₀ = Lₐ/10

        N_Curvas = size(Variaveis_de_Entrada.ρs,1)
        aux_1 = 1        

        σ²_L² = zeros(N_pontos,N_Curvas + 1)
        σ²_L²[ : ,end] = ω₀s
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$(Variaveis_de_Entrada.ρs[aux_1])}===>" for k in 1:N_pontos

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                
                rₘᵢₙ = 1/(10*sqrt(Variaveis_de_Entrada.ρs[j]))
                N = round(Int64,Variaveis_de_Entrada.ρs[j]*(Range_L[k])*Lₐ)                
                ω₀ = ω₀s[k]
                

                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        Variaveis_de_Entrada.ρs[j],
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        Range_L[k],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas,
                        Variaveis_de_Entrada.Geometria
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for k in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                
                kR = 1

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    kR,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    GEOMETRIA_DA_NUVEM,
                    Range_L[k],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
                # σ²_L²[k,j] = σ²/((Range_L[k])^2) 
                σ²_L²[k,j] = σ²


            end
            aux_1 += 1
        end
        
    elseif Variaveis_de_Entrada.Variavel_Constante == "L"
    
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        b₀_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        b₀_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_b₀ = get_points_in_log_scale(b₀_INICIAL,b₀_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_b₀ = range(b₀_INICIAL,b₀_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.Ls,1)
        aux_1 = 1
        ω₀ = Lₐ/10

        σ²_L² = zeros(N_pontos,N_Curvas)
        
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, L = {$(Variaveis_de_Entrada.Ls[aux_1])}===>" for k in 1:N_pontos

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                

                N = round(Int64,(Range_b₀[k]*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Lₐ)/4)
                ρ = N/(Variaveis_de_Entrada.Ls[j]*Variaveis_de_Entrada.Lₐ)
                rₘᵢₙ = 1/(10*sqrt(ρ))
                

                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        ρ,
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        Variaveis_de_Entrada.Ls[j],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for k in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                
                kR = 1

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    kR,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.Ls[j],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]
                # σ²_L²[k,j] = σ²/((Variaveis_de_Entrada.Ls[j])^2) 
                σ²_L²[k,j] = σ²


            end
            aux_1 += 1
        end

    elseif Variaveis_de_Entrada.Variavel_Constante == "b₀"
    
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.b₀s,1)
        aux_1 = 1
        ω₀ = Lₐ/10

        σ²_L² = zeros(N_pontos,N_Curvas)
        
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, b₀ = {$(Variaveis_de_Entrada.b₀s[aux_1])}===>" for k in 1:N_pontos

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                
                if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"
                    N = round(Int64,(b₀s[j]*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Lₐ)/4)
                    ρ = N/(Range_L[k]*Variaveis_de_Entrada.Lₐ)
                    rₘᵢₙ = 1/(10*sqrt(ρ))
                elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"
                    N = round(Int64,(b₀s[j]*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Lₐ)/8)
                    ρ = N/(Range_L[k]*Variaveis_de_Entrada.Lₐ)
                    rₘᵢₙ = 1/(10*sqrt(ρ))
                end
                
                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        ρ,
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        Range_L[k],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for n in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                
                kR = 1

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    kR,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    GEOMETRIA_DA_NUVEM,
                    Range_L[k],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]
                # σ²_L²[k,j] = σ²/((Range_L[k])^2) 
                σ²_L²[k,j] = σ² 


            end
            aux_1 += 1
        end

        
    end

    if Variaveis_de_Entrada.Variavel_Constante == "rmin"

        Range  = Range_L        
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "ρ"        
    
        Range  = Range_L
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "L"
    
        Range = Range_b₀
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "b₀"
    
        Range = Range_L
    
    end
    
    return σ²_L²,Range
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function ROTINA_11__Cintura_e_L(Variaveis_de_Entrada::R11_ENTRADA,Geometria::ThreeD)
    
    if typeof(Geometria) == PARALELEPIPEDO
        GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"
    elseif typeof(Geometria) == TUBO
        GEOMETRIA_DA_NUVEM = "TUBO"
    end

    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
    L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
    N_pontos = Variaveis_de_Entrada.N_pontos
    N_Realizações = Variaveis_de_Entrada.N_Realizações

    Radius = 1
    PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
    Angulo_de_variação_da_tela_circular_1 = 0 
    Angulo_de_variação_da_tela_circular_2 = 1
    Lₐ = Variaveis_de_Entrada.Lₐ
    Q_total = (N_Sensores^2)*N_Realizações
    N_Telas = 1

    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
    
    end
    
    ω₀s = find_ws(Range_L,Lₐ,Geometria)

    # ω₀ = Lₐ/10
    # ω₀s = fill(Lₐ/2,(1,size(Range_L,1)))

    N_Curvas = size(Variaveis_de_Entrada.ρs,1)
    aux_1 = 1        

    σ²_L² = zeros(N_pontos,N_Curvas + 1)
    σ²_L²[ : ,end] = Lₐ/5

    for j in 1:N_Curvas 
            
        ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$(Variaveis_de_Entrada.ρs[aux_1])}===>" for k in 1:N_pontos

            Intensidade_Resultante_PURA = zeros(Q_total)
            aux_2 = 0
            
            if GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"
                rₘᵢₙ = 1/(10*sqrt(Variaveis_de_Entrada.ρs[j]))
                N = round(Int64,Variaveis_de_Entrada.ρs[j]*(Range_L[k])*(Lₐ^2))                
                ω₀ = ω₀s[k]
            elseif GEOMETRIA_DA_NUVEM == "TUBO"
                rₘᵢₙ = 1/(10*sqrt(Variaveis_de_Entrada.ρs[j]))
                N = round(Int64,Variaveis_de_Entrada.ρs[j]*(Range_L[k])*(π*(Lₐ^2)))                
                ω₀ = ω₀s[k]
            end

            for i in 1:Variaveis_de_Entrada.N_Realizações

                entrada_extratora = E2_ENTRADA(
                    Variaveis_de_Entrada.Γ₀,
                    Variaveis_de_Entrada.ωₐ,
                    Variaveis_de_Entrada.Ω,
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    Variaveis_de_Entrada.ρs[j],
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Variaveis_de_Entrada.vetor_de_onda,
                    ω₀,
                    Variaveis_de_Entrada.λ,
                    Variaveis_de_Entrada.ωₗ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Tipo_de_Onda,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    Variaveis_de_Entrada.Tipo_de_beta,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    GEOMETRIA_DA_NUVEM,
                    Range_L[k],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas,
                    Variaveis_de_Entrada.Geometria
                )

                All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                N_Sensores = Variaveis_de_Entrada.N_Sensores
                Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+(N_Sensores^2))] = All_Intensitys[1, : ]
                aux_2 +=  N_Sensores^2

            end
        
            Intensidade_media = zeros(N_Sensores^2)

            for i in 1:(N_Sensores^2)
        
                fator_soma = 0
                aux_3 = 0
        
                for k in 1:N_Realizações
        
                    fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                    aux_3 += (N_Sensores^2)
        
                end
        
                Intensidade_media[i] = fator_soma/N_Realizações
        
            end

            # Intensidade_media = Intensidade_media/mean(Intensidade_media)

            entrada_Sensores = get_cloud_sensors_ENTRADA(
                Radius,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Angulo_de_variação_da_tela_circular_1,
                Angulo_de_variação_da_tela_circular_2,
                GEOMETRIA_DA_NUVEM,
                Range_L[k],
                Variaveis_de_Entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

            σ² = get_one_σ(Posição_Sensores,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            # σ²_L²[k,j] = σ²/((Range_L[k])^2) 
            σ²_L²[k,j] = σ²


        end
        aux_1 += 1
    end

    return σ²_L²,Range_L
end


