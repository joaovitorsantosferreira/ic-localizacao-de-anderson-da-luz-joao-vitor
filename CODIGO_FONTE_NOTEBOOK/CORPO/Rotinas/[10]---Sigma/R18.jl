#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_18__Diagrama_de_Fase_sigma2(Variaveis_de_Entrada::R18_ENTRADA)

    Q_Dados = Variaveis_de_Entrada.N_div_Deturn*Variaveis_de_Entrada.N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,3) 
    aux_1 = 1

    if Variaveis_de_Entrada.Escala_de_Densidade == "LOG"

        Range_ρ = get_points_in_log_scale(Variaveis_de_Entrada.Densidade_Inicial,Variaveis_de_Entrada.Densidade_Final,Variaveis_de_Entrada.N_div_Densidade) 
    
    elseif Variaveis_de_Entrada.Escala_de_Densidade == "LINEAR"    

        Range_ρ = range(Variaveis_de_Entrada.Densidade_Inicial,Variaveis_de_Entrada.Densidade_Final,length=Variaveis_de_Entrada.N_div_Densidade) 
    
    end
    range_Δ = range(Variaveis_de_Entrada.Δ_Inicial,Variaveis_de_Entrada.Δ_Final,length = Variaveis_de_Entrada.N_div_Deturn)

    ProgressMeter.@showprogress 2 "Sigmas ===>" for i in 1:Variaveis_de_Entrada.N_div_Densidade

        ρ = Range_ρ[i]
        N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.L,Variaveis_de_Entrada.kR,Variaveis_de_Entrada.Lₐ,ρ,Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)
        

        spectro = Any[]
        for k in 1:Variaveis_de_Entrada.N_Realizações
            
            entrada_cloud = Cloud_ENTRADA(
                N,
                ρ,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Γ₀,
                0,
                Variaveis_de_Entrada.kR,
                rₘᵢₙ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                Variaveis_de_Entrada.Geometria
            )  
        
            cloud = Cloud_COMPLETO(entrada_cloud)
            append!(spectro,cloud.λ) 
        end

        for j in 1:N_div_Deturn
            Δ = range_Δ[j]
            Σ² = get_Σ2(real.(spectro),imag.(spectro),Δ,Variaveis_de_Entrada.Γ₀)

            Dados_Resultantes[aux_1,1] = Δ
            Dados_Resultantes[aux_1,2] = ρ
            Dados_Resultantes[aux_1,3] = Σ²
            aux_1 += 1
        end
    end
    return Dados_Resultantes
end




    



































