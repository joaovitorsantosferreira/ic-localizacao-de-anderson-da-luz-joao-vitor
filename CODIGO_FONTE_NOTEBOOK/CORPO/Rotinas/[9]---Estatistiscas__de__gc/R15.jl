###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_15__Estatisticas_de_gc(Variaveis_de_entrada::R15_ENTRADA)
    
    N_Realizações_Intensidade = Variaveis_de_entrada.N_Realizações_Intensidade
    N_Realizações_gc = Variaveis_de_entrada.N_Realizações_gc
    N_div = Variaveis_de_entrada.N_div

    condutancias_Von_Rossum = zeros(N_Realizações_gc)
    

    ProgressMeter.@showprogress 1 "Extraido Estatisticas de gc ===>" for j in 1:N_Realizações_gc

        Intensidade_Resultante_PURA = Any[]

        for i in 1:N_Realizações_Intensidade
            entrada_Extratora = E2_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                Variaveis_de_entrada.N,
                Variaveis_de_entrada.Radius,
                Variaveis_de_entrada.ρ,
                Variaveis_de_entrada.rₘᵢₙ,
                Variaveis_de_entrada.Angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                Variaveis_de_entrada.ω₀,
                Variaveis_de_entrada.λ,
                Variaveis_de_entrada.ωₗ,
                Variaveis_de_entrada.Δ,
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Variaveis_de_entrada.Distancia_dos_Sensores,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_entrada.L,
                Variaveis_de_entrada.Lₐ,
                "NAO",
                1,
                Variaveis_de_entrada.Geometria
            )
    
            All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)     
    
            append!(Intensidade_Resultante_PURA,All_Intensitys[1,:])
        end
                                                                                             
        Intensidade_NORMALIZADA = Intensidade_Resultante_PURA/mean(Intensidade_Resultante_PURA)
        Histograma = get_dados_histograma_LINEAR(Intensidade_NORMALIZADA[ : ],N_div)    
        condutancias_Von_Rossum[j] = get_gc_Von_Rossum(Histograma)
    end

    return condutancias_Von_Rossum
end

