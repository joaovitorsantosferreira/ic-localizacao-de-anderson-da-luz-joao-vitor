#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_16__Estatisticas_gc_Densidade_FIXA(Variaveis_de_entrada::R16_ENTRADA)
    

    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final

    media_gc,median_gc,desvio_gc = zeros(N_div_Deturn),zeros(N_div_Deturn),zeros(N_div_Deturn)

    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)
    media_gc,median_gc,desvio_gc

    for y in 1:N_div_Deturn

        condutancias_Von_Rossum = zeros(Variaveis_de_entrada.N_Realizações_gc)
        
        for j in 1:Variaveis_de_entrada.N_Realizações_gc

            Intensidade_Resultante_PURA = Any[]

            entrada_Extratora = E2_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                Variaveis_de_entrada.N,
                Variaveis_de_entrada.Radius,
                Variaveis_de_entrada.ρ,
                Variaveis_de_entrada.rₘᵢₙ,
                Variaveis_de_entrada.angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                Variaveis_de_entrada.ω₀,
                Variaveis_de_entrada.λ,
                range_Δ[y],
                range_Δ[y],
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Variaveis_de_entrada.Distancia_dos_Sensores,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_entrada.L,
                Variaveis_de_entrada.Lₐ,
                "NAO",
                1,
                Variaveis_de_entrada.Geometria
            )

            for i in 1:Variaveis_de_entrada.N_Realizações_Intensidade

                All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)     
                A = isnan.(All_Intensitys[1,:])
                Intensidade = Any[]
                for i in 1:size(All_Intensitys[1,:],1)
                    if A[i] == false
                    append!(Intensidade,All_Intensitys[1,i]) 
                    end
                end
                append!(Intensidade_Resultante_PURA,Intensidade)
            end
                                                                                                
            Intensidade_NORMALIZADA = Intensidade_Resultante_PURA/mean(Intensidade_Resultante_PURA)
            Histograma = get_dados_histograma_LINEAR(Intensidade_NORMALIZADA[ : ],10)    
            condutancias_Von_Rossum[j] = get_gc_Von_Rossum(Histograma)

        end   
        
        media_gc[y] = mean(condutancias_Von_Rossum)
        median_gc[y] = median(condutancias_Von_Rossum)
        desvio_gc[y] = std(condutancias_Von_Rossum)^2 
    end

    return media_gc,median_gc,desvio_gc
end

