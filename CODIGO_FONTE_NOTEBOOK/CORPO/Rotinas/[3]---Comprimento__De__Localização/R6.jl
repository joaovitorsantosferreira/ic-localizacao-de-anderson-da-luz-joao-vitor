#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_6__Comprimento_de_localização_por_DIM_Δ_FIXO(Variaveis_de_Entrada::R6_ENTRADA)
    
    N_div_Densidade = Variaveis_de_Entrada.N_div_Densidade
    Densidade_Inicial = Variaveis_de_Entrada.Densidade_Inicial
    Densidade_Final = Variaveis_de_Entrada.Densidade_Final
    Variavel_Constante = Variaveis_de_Entrada.Variavel_Constante
    Escala_de_Densidade = Variaveis_de_Entrada.Escala_de_Densidade


    if Escala_de_Densidade == "LOG"

        Range_ρ = get_points_in_log_scale(Densidade_Inicial,Densidade_Final,N_div_Densidade) 
    
    elseif Escala_de_Densidade == "LINEAR"    

        Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade) 
    
    end



    if Variavel_Constante == "N"

        N_Realizações = size(Variaveis_de_Entrada.Ns,1)

        ξs = zeros(N_div_Densidade,N_Realizações)
        
        aux_1 = 1
    
        for j in 1:N_Realizações 
        
            ξₘᵢₙ = zeros(N_div_Densidade)


            ProgressMeter.@showprogress 2 "Calculo_Comprimento__Minimo__N={$(Variaveis_de_Entrada.Ns[aux_1])}===>"    for i in 1:N_div_Densidade
                

                N = Variaveis_de_Entrada.Ns[j]

                if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                    
                    L = 1
                    ρ_normalizado = Range_ρ[i]
                    ρ = ρ_normalizado*(Variaveis_de_Entrada.k^2)
                    kR = sqrt(N/(π*ρ))
                    Radius = kR/Variaveis_de_Entrada.k
                    rₘᵢₙ = 1/(10*sqrt(ρ))   

                elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                    Radius = 1 
                    ρ_normalizado = Range_ρ[i]
                    ρ = ρ_normalizado*(Variaveis_de_Entrada.k^2)
                    L = sqrt(N/(ρ))                                                                                                                                                                                                                                                                                   
                    rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     
                                                                                       
                end


                entrada_entratora = E3_ENTRADA(
                    Variaveis_de_Entrada.Γ₀, 
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                    L
                )
                
                ξₙ_temporario = E3_extração_de_dados_Comprimento_de_Localização(entrada_entratora)

                if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                    
                    ξₘᵢₙ[i] = minimum(abs.(ξₙ_temporario))/kR

                elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                    ξₘᵢₙ[i] = minimum(abs.(ξₙ_temporario))/L
                                                                                       
                end


            end

            ξs[ : ,j] = ξₘᵢₙ
            aux_1 += 1
        end
    

    elseif Variavel_Constante == "kR"

        N_Realizações = size(Variaveis_de_Entrada.kRs,1)

        ξs = zeros(N_div_Densidade,N_Realizações)
        
        aux_1 = 1

        for j in 1:N_Realizações 
        
            ξₘᵢₙ = zeros(N_div_Densidade)

            ProgressMeter.@showprogress 2 "Calculo_Comprimento__Minimo__kR={$(Variaveis_de_Entrada.kRs[aux_1])}===>"   for i in 1:N_div_Densidade
                
                kR = Variaveis_de_Entrada.kRs[j]
                ρ_normalizado = Range_ρ[i]
                ρ = ρ_normalizado*(k^2)
                N = round(Int64,ρ_normalizado*π*(kR^2))
                Radius = kR/Variaveis_de_Entrada.k
                rₘᵢₙ = 1/(10*sqrt(ρ))   
                GEOMETRIA_DA_NUVEM = "DISCO"

                entrada_entratora = E3_ENTRADA(
                    Variaveis_de_Entrada.Γ₀, 
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.Ls[1]            
                )
                
                ξₙ_temporario = E3_extração_de_dados_Comprimento_de_Localização(entrada_entratora)

                ξₘᵢₙ[i] = minimum(abs.(ξₙ_temporario))/kR

            end
        
            ξs[ : ,j] = ξₘᵢₙ
            aux_1 += 1
        end

    elseif Variavel_Constante == "L"

        N_Realizações = size(Variaveis_de_Entrada.kRs,1)

        ξs = zeros(N_div_Densidade,N_Realizações)
        
        aux_1 = 1

        for j in 1:N_Realizações 
        
            ξₘᵢₙ = zeros(N_div_Densidade)

            ProgressMeter.@showprogress 2 "Calculo_Comprimento__Minimo__L={$(Variaveis_de_Entrada.Ls[aux_1])}===>"   for i in 1:N_div_Densidade
                
                Radius = 1
                ρ_normalizado = Range_ρ[i]
                ρ = ρ_normalizado*(k^2)
                N = round(Int64,ρ_normalizado*(Variaveis_de_Entrada.Ls[j]*Variaveis_de_Entrada.k)^2)                                                                                                                
                rₘᵢₙ = 1/(10*sqrt(ρ))  
                GEOMETRIA_DA_NUVEM = "SLAB"

                entrada_entratora = E3_ENTRADA(
                    Variaveis_de_Entrada.Γ₀, 
                    Variaveis_de_Entrada.k,
                    N,
                    Radius,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.Ls[j]            
                )
                
                ξₙ_temporario = E3_extração_de_dados_Comprimento_de_Localização(entrada_entratora)

                ξₘᵢₙ[i] = minimum(abs.(ξₙ_temporario))/Variaveis_de_Entrada.Ls[j] 

            end

            ξs[ : ,j] = ξₘᵢₙ
            aux_1 += 1
        end            
    end 

    
    return Range_ρ,ξs
end



