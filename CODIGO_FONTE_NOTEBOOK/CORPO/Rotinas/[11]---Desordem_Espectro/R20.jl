#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_20__Espectro_Desordem(Variaveis_de_Entrada::R20_ENTRADA,Geometria::TwoD)
    
    IPRs_PURO = Any[]
    Espectros_PURO = Any[]
    

    for i in 1:Variaveis_de_Entrada.N_Realizações
                            
        entrada_cloud = Cloud_ENTRADA(
            Variaveis_de_Entrada.N,
            Variaveis_de_Entrada.ρ,
            Variaveis_de_Entrada.k,
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.Δ,
            Variaveis_de_Entrada.Radius,
            Variaveis_de_Entrada.rₘᵢₙ,
            Variaveis_de_Entrada.Tipo_de_kernel,
            "PRESENTE",
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.Geometria
        )  
    
        cloud = Cloud_COMPLETO(entrada_cloud)
    
        IPRs,q,Sₑ = get_Statistics_profile_of_modes(cloud.ψ,Variaveis_de_Entrada.Tipo_de_kernel,Geometria) 
        
        append!(IPRs_PURO,IPRs) 
        append!(Espectros_PURO,cloud.λ) 
    end

    γ = real.(Espectros_PURO)
    ω = imag.(Espectros_PURO)    
    
    return γ,ω,IPRs_PURO
end

