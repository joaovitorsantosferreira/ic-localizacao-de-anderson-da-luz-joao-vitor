#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_19__PR_Desordem(Variaveis_de_Entrada::R19_ENTRADA,Geometria::TwoD)
    
    N_Curvas = size(Variaveis_de_Entrada.b₀s,1)

    if Variaveis_de_Entrada.Escala_do_range == "LOG"
        Range_W = get_points_in_log_scale(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,Variaveis_de_Entrada.N_pontos)     
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    
        Range_W = range(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,length=Variaveis_de_Entrada.N_pontos)     
    end
    

    PR_medio_subradiante = zeros(Variaveis_de_Entrada.N_pontos,N_Curvas)
    PR_medio_superradiante = zeros(Variaveis_de_Entrada.N_pontos,N_Curvas)

    for j in 1:N_Curvas 
            
        ProgressMeter.@showprogress 2 "Calculo_PR_b₀, b₀ ===>" for k in 1:Variaveis_de_Entrada.N_pontos

            PRs_PURO = Any[]
            Espectros_PURO = Any[]

            if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"
                N_Instantaneo = round(Int64,(π*Variaveis_de_Entrada.Radius*Variaveis_de_Entrada.b₀s[j])/8)     
            elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"    
                N_Instantaneo = round(Int64,(π*Variaveis_de_Entrada.Radius*Variaveis_de_Entrada.b₀s[j])/16)     
            end
            
            ρ_Instantaneo = N_Instantaneo/(π*(Variaveis_de_Entrada.Radius^2))
            rₘᵢₙ = 1/(10*sqrt(ρ_Instantaneo))
            

            for i in 1:Variaveis_de_Entrada.N_Realizações
                                    
                entrada_cloud = Cloud_ENTRADA(
                    N_Instantaneo,
                    ρ_Instantaneo,
                    Variaveis_de_Entrada.k,
                    Variaveis_de_Entrada.Γ₀,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Radius,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    "PRESENTE",
                    Range_W[k]*Variaveis_de_Entrada.b₀s[j],
                    Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.L,
                    Variaveis_de_Entrada.Lₐ,
                    Geometria
                )  
            
                cloud = Cloud_COMPLETO(entrada_cloud)
            
                IPRs,q,Sₑ = get_Statistics_profile_of_modes(cloud.ψ,Variaveis_de_Entrada.Tipo_de_kernel,Geometria) 
                
                append!(PRs_PURO,IPRs.^(-1)) 
                append!(Espectros_PURO,cloud.λ) 
            end

            γ = real.(abs.(Espectros_PURO))
            ω = imag.(Espectros_PURO)
            modos_subradiante = findall((ω .> -1).*(ω .< 1).*(γ .< 1))
            modos_superadiante = findall((ω .> -1).*(ω .< 1).*(γ .> 1))

            PR_medio_subradiante[k,j] = mean(PRs_PURO[modos_subradiante])
            PR_medio_superradiante[k,j] = mean(PRs_PURO[modos_superadiante])
        end
    end
    
    
    return PR_medio_subradiante,PR_medio_superradiante,Range_W
end

