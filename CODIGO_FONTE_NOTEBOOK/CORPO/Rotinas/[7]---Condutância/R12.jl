#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_12__Condutancia_Densidade_FIXA(Variaveis_de_entrada::R12_ENTRADA)
    

    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final
    N_Sensores = 360
    PERFIL_DA_INTENSIDADE = "NAO"
    N_Telas = 1

    ΔD = Δ_Final - Δ_Inicial
    Deturn = zeros(N_div_Deturn)
    Condutancias_KOGAN = zeros(N_div_Deturn)
    Condutancias_SHNERB = zeros(N_div_Deturn)

    N_total_de_Sensores = N_Sensores*N_Realizações
    Intensidade_Resultante = zeros(2,N_total_de_Sensores)
    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)


    for j in 1:N_div_Deturn  
            
        Δ = range_Δ[j]*Γ₀
        ωₗ = Δ + ωₐ
        aux_1 = 0

        entrada_Extratora = E9_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            ωₗ,
            Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            0,
            360,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )

        for i in 1:N_Realizações

            All_Intensitys = E9_extração_de_dados_Transmission(entrada_Extratora)

            Intensidade_Resultante[1,aux_1 + 1:aux_1+N_Sensores] = All_Intensitys[1 , : ]
            Intensidade_Resultante[2,aux_1 + 1:aux_1+N_Sensores] = All_Intensitys[2 , : ]

            aux_1 += N_Sensores

        end

        Condutancias_KOGAN[j] = get_dados_to_diagram_fase_condutancia_KOGAN(Intensidade_Resultante,Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2)
        Condutancias_SHNERB[j] = get_dados_to_diagram_fase_condutancia_Shnerb(Intensidade_Resultante,N_Sensores,Variaveis_de_entrada.N_Realizações,Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2)
        Deturn[j] = Δ/Γ₀ 

    end

      

    return R12_SAIDA(Deturn,Condutancias_KOGAN,Condutancias_SHNERB)
end 


function get_dados_to_diagram_fase_condutancia_KOGAN(All_Intensitys,Angulo_Sensores_1,Angulo_Sensores_2)

    θ = All_Intensitys[2, : ]
    index_sensor = findall(  (θ[ : ] .≥ Angulo_Sensores_1 ).*(θ[ : ] .≤ Angulo_Sensores_2 ))
    

    Intensidade_Resultante_NORMALIZADA = All_Intensitys[1,index_sensor]/mean(All_Intensitys[1,index_sensor])
    I_media = mean(Intensidade_Resultante_NORMALIZADA)
    Ī²c = 0.5*mean(Intensidade_Resultante_NORMALIZADA.^2) - I_media^2
    g₀ = (I_media^2)/Ī²c

    return g₀
end 

using Roots

function get_dados_to_diagram_fase_condutancia_Shnerb(All_Intensitys,N_Sensores,N_Realizações,Angulo_Sensores_1,Angulo_Sensores_2;angulo_coerente = 30)

    θ = All_Intensitys[2, : ]
    index_sensor = findall(  (θ[ : ] .≥ Angulo_Sensores_1 ).*(θ[ : ] .≤ Angulo_Sensores_2 ))    
    Intensiade_Normalizada = All_Intensitys[1,index_sensor]/mean(All_Intensitys[1,index_sensor])
    Histograma = get_dados_histograma_LOG(Intensiade_Normalizada,100)
    Histograma_NORMALIZADO = normalize(Histograma; mode=:pdf)


    model(x, p) = (exp.(-x)).*(0.5*p.*(x.^2 .- 4*x .+ 2) .+ 1)
    x_data = Histograma_NORMALIZADO.edges[1][2:end]
    y_data = Histograma_NORMALIZADO.weights
    p0 = [0.276]

    reta = curve_fit(model, x_data, y_data, p0; lower = [0.001],upper = [1.0])

    Sₜ = reta.param[1]                                                                                                                                                  
    # println("St = $Sₜ")

    T_difuso,T_coerente = get_Tramission(All_Intensitys,N_Sensores,N_Realizações,angulo_coerente)
    # println("T = $T_coerente")


    find_gc(x) = ((1-Sₜ)^(T_coerente^2)) - ((1 - x.^(-2)).^x) 

    g_classico = fzeros(find_gc, 1, 1000000000)	
    # println("g = $g_classico")

    # g_classico = find_zeros(find_gc,1,1000)
    # println("g = $g_classico")

    # g_classico = 1/(Sₜ*T_coerente^2)
    # printlc("g = $g_classico")




    return g_classico[1]
end 


