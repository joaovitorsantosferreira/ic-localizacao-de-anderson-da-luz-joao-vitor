###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_1__Estatisticas_da_Intensidade(Variaveis_de_entrada::R1_ENTRADA)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    N_Realizações = Variaveis_de_entrada.N_Realizações
    N_div = Variaveis_de_entrada.N_div

    Q_total = N_Sensores*N_Realizações
    
    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

        Intensidade_Resultante_PURA = zeros(2,Q_total)
    
    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        Intensidade_Resultante_PURA = zeros(4,Q_total)

    else 
    
        return "NÃO PODE!!!!"
    end

    auxiliar = 0
    N_Telas = 1

    ProgressMeter.@showprogress 1 "Extraido Estatisticas da Luz===>" for i in 1:N_Realizações

        entrada_Extratora = E2_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            Variaveis_de_entrada.ωₗ,
            Variaveis_de_entrada.Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
            N_Telas,
            Variaveis_de_entrada.Geometria
        )

        All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)                                                                                                    

        if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

            Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[1 , : ]
            Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[2 , : ]
            auxiliar +=  N_Sensores
    
        
        elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
    
            Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[1 , : ]
            Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[2 , : ]
            Intensidade_Resultante_PURA[3,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[3 , : ]
            Intensidade_Resultante_PURA[4,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[4 , : ]
            auxiliar +=  N_Sensores
    
        end
    
    

    end


    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

        Intensidade_Resultante_NORMALIZADA = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        variancia = get_variancia(Intensidade_Resultante_NORMALIZADA)
    
        return R1_SAIDA_PROBABILIDADE(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Histograma_LINEAR,Histograma_LOG,variancia)
    
    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_entrada) 
    
        

        Intensidade_Resultante_NORMALIZADA = zeros(2,Q_total)
        Intensidade_Resultante_NORMALIZADA[1, : ] = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        Intensidade_Resultante_NORMALIZADA[2, : ] = Intensidade_Resultante_PURA[3 , : ]/mean(Intensidade_Resultante_PURA[3 , : ])

        r_exemplo = getAtoms_distribution(getAtoms_distribution_ENTRADA(Variaveis_de_entrada.N, Variaveis_de_entrada.Radius, Variaveis_de_entrada.rₘᵢₙ,Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,Variaveis_de_entrada.L,Variaveis_de_entrada.Lₐ),Variaveis_de_entrada.Geometria)                                                                   # Gera o disco e guarda as posições em r

        return R1_SAIDA_PERFIL(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Posição_Sensores,r_exemplo)

    end



end

