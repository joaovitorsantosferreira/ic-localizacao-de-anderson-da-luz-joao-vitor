###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_10__Estatisticas_da_Transmission(Variaveis_de_entrada::R10_ENTRADA,Geometria::TwoD)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    b_INICIAL = Variaveis_de_entrada.b_INICIAL 
    b_FINAL = Variaveis_de_entrada.b_FINAL
    N_pontos = Variaveis_de_entrada.N_pontos
    N_Realizações = Variaveis_de_entrada.N_Realizações
    PERFIL_DA_INTENSIDADE = "NAO"
    GEOMETRIA_DA_NUVEM = "SLAB"
    Radius = 1

    Q_total = N_Sensores*N_Realizações
    T = zeros(N_pontos,2)
    N_Telas = 1
    aux_1 = 1

    if Variaveis_de_entrada.Escala_de_b == "LOG"

        Range_b = get_points_in_log_scale(b_INICIAL,b_FINAL,N_pontos) 
    
    elseif Variaveis_de_entrada.Escala_de_b == "LINEAR"    

        Range_b = range(b_INICIAL,b_FINAL,length=N_pontos) 
    
    end

    for j in 1:N_pontos

        bδ = Range_b[j]
        Δ = (Variaveis_de_entrada.Γ₀/2)*sqrt((Variaveis_de_entrada.b₀/bδ) - 1 ) 
        ωₗ = Δ + Variaveis_de_entrada.ωₐ                                                                                                                  
        Intensidade_Resultante_PURA = zeros(2,Q_total)
        auxiliar = 0

        ProgressMeter.@showprogress 1 "Extraido Transmissão da Luz {$aux_1}===>" for i in 1:N_Realizações

            entrada_Extratora = E9_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                Variaveis_de_entrada.N,
                Radius,
                Variaveis_de_entrada.ρ,
                Variaveis_de_entrada.rₘᵢₙ,
                Variaveis_de_entrada.Angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                Variaveis_de_entrada.ω₀,
                Variaveis_de_entrada.λ,
                ωₗ,
                Δ,
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Variaveis_de_entrada.Distancia_dos_Sensores,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                GEOMETRIA_DA_NUVEM,
                Variaveis_de_entrada.L,
                Variaveis_de_entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas,
                Variaveis_de_entrada.Geometria
            )

            All_Intensitys = E9_extração_de_dados_Transmission(entrada_Extratora)                                                                                                    

            Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[1 , : ]
            Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[2 , : ]
            auxiliar +=  N_Sensores
        
        end

        T_DIFUSO,T_COERENTE = get_Tramission(Intensidade_Resultante_PURA,N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_coerente)

        T[j,1] = T_DIFUSO
        T[j,2] = T_COERENTE 
        aux_1 += 1
    end 

    return Range_b,T

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function ROTINA_10__Estatisticas_da_Transmission(Variaveis_de_entrada::R10_ENTRADA,Geometria::ThreeD)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    b_INICIAL = Variaveis_de_entrada.b_INICIAL 
    b_FINAL = Variaveis_de_entrada.b_FINAL
    N_pontos = Variaveis_de_entrada.N_pontos
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Radius = 1


    Q_total = N_Sensores*N_Realizações
    Intensidade_por_ponto = zeros(size(Variaveis_de_entrada.Ls,1),N_pontos,4,Q_total)
    T = zeros(size(Variaveis_de_entrada.Ls,1),N_pontos)
    aux_2 = 1

    if Variaveis_de_entrada.Escala_de_b == "LOG"

        Range_b = get_points_in_log_scale(b_INICIAL,b_FINAL,N_pontos) 
    
    elseif Variaveis_de_entrada.Escala_de_b == "LINEAR"    

        Range_b = range(b_INICIAL,b_FINAL,length=N_pontos) 
    
    end

    for L_Instantaneo in Variaveis_de_entrada.Ls

        ω₀_INST = Variaveis_de_entrada.Lₐ/2                                                                                                                                             
        Distancia_dos_Sensores_INST = 10*sqrt(Variaveis_de_entrada.Lₐ^2 + (L_Instantaneo/2)^2)
        # b₀_Instantaneo = 4*(Variaveis_de_entrada.N)/(Variaveis_de_entrada.Lₐ^2)
        aux_1 = 1

        for j in 1:N_pontos

            b₀_Instantaneo = Range_b[j]
            N_Instantaneo = round(Int64,b₀_Instantaneo*(Variaveis_de_entrada.Lₐ^2)/4)
            ρ_instantaneo = N_Instantaneo/(L_Instantaneo*π*(Variaveis_de_entrada.Lₐ^2))
            rₘᵢₙ_INST = 1/(10*sqrt(ρ_instantaneo))                                                                                                                                   
            Δ = 0
            # Δ = (Variaveis_de_entrada.Γ₀/2)*sqrt((b₀_Instantaneo/bδ) - 1 ) 
            ωₗ = Δ + Variaveis_de_entrada.ωₐ                                                                                                                  
            Intensidade_Resultante_PURA = zeros(4,Q_total)
            auxiliar = 0

            ProgressMeter.@showprogress 1 "Extraido Transmissão da Luz {$aux_1}===>" for i in 1:N_Realizações

                entrada_Extratora = E9_ENTRADA(
                    Variaveis_de_entrada.Γ₀,
                    Variaveis_de_entrada.ωₐ,
                    Variaveis_de_entrada.Ω,
                    Variaveis_de_entrada.k,
                    N_Instantaneo,
                    Radius,
                    ρ_instantaneo,
                    rₘᵢₙ_INST,
                    Variaveis_de_entrada.Angulo_da_luz_incidente,
                    Variaveis_de_entrada.vetor_de_onda,
                    ω₀_INST,
                    Variaveis_de_entrada.λ,
                    ωₗ,
                    Δ,
                    Variaveis_de_entrada.Tipo_de_kernel,
                    Variaveis_de_entrada.Tipo_de_Onda,
                    Variaveis_de_entrada.N_Sensores,
                    Distancia_dos_Sensores_INST,
                    Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                    Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                    Variaveis_de_entrada.Tipo_de_beta,
                    Variaveis_de_entrada.Desordem_Diagonal,
                    Variaveis_de_entrada.W,
                    GEOMETRIA_DA_NUVEM,
                    L_Instantaneo,
                    Variaveis_de_entrada.Lₐ,
                    "NAO",
                    1,
                    Variaveis_de_entrada.Geometria
                )

                All_Intensitys = E9_extração_de_dados_Transmission(entrada_Extratora)                                                                                                    

                Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[1 , : ]
                Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[2 , : ]
                Intensidade_Resultante_PURA[3,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[3 , : ]
                Intensidade_Resultante_PURA[4,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[4 , : ]

                auxiliar +=  N_Sensores
            
            end

            Intensidade_por_ponto[aux_2,j,:,:] = Intensidade_Resultante_PURA

            T[aux_2,j] = get_Tramission(Intensidade_Resultante_PURA,N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_T_INICIAL,Variaveis_de_entrada.angulo_T_FINAL,Variaveis_de_entrada.Geometria)
            
            aux_1 += 1
        end 

        aux_2 += 1
    end

    return Range_b,T,Intensidade_por_ponto
end


