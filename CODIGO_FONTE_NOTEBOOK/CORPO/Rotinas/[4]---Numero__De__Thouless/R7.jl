#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_7__Number_of_Thouless_Densidade_FIXA(Variaveis_de_entrada::R7_ENTRADA)
    

    Tipo_de_kernel = Variaveis_de_entrada.Tipo_de_kernel    
    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final


    Deturn = zeros(N_div_Deturn)
    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)

    if Tipo_de_kernel == "Escalar"

        γₙ_Resultante = zeros(Variaveis_de_entrada.N*N_Realizações)
        ωₙ_Resultante = zeros(Variaveis_de_entrada.N*N_Realizações)
    
    elseif Tipo_de_kernel == "Vetorial"
    
        γₙ_Resultante = zeros(2*Variaveis_de_entrada.N*N_Realizações)
        ωₙ_Resultante = zeros(2*Variaveis_de_entrada.N*N_Realizações)

    end

    numbers_of_thouless = zeros(N_div_Deturn)

    for j in 1:N_div_Deturn  



        Δ = range_Δ[j]*Variaveis_de_entrada.Γ₀
        ωₗ = Δ + Variaveis_de_entrada.ωₐ
        aux_1 = 0


        entrada_extratora = E4_ENTRADA(
            Variaveis_de_entrada.Γ₀, 
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            ωₗ,
            Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ
        )

        for i in 1:N_Realizações

            Dados = E4_extração_de_dados_Thoouless_Number(entrada_extratora)

            if Tipo_de_kernel == "Escalar"

                γₙ_Resultante[aux_1 + 1:aux_1+Variaveis_de_entrada.N] = Dados[1] 
                ωₙ_Resultante[aux_1 + 1:aux_1+Variaveis_de_entrada.N] = Dados[2] 

                aux_1 += Variaveis_de_entrada.N

            elseif Tipo_de_kernel == "Vetorial"

                γₙ_Resultante[aux_1 + 1:aux_1+2*Variaveis_de_entrada.N] = Dados[1] 
                ωₙ_Resultante[aux_1 + 1:aux_1+2*Variaveis_de_entrada.N] = Dados[2] 

                aux_1 += 2*Variaveis_de_entrada.N
            end
        end

        numbers_of_thouless[j] = get_Thouless_Number(γₙ_Resultante,ωₙ_Resultante,Δ,Variaveis_de_entrada.Γ₀,Tipo_de_kernel)
        Deturn[j] = Δ/Variaveis_de_entrada.Γ₀ 

    end

     

    return Deturn,numbers_of_thouless
end



