mutable struct R20_ENTRADA
    Γ₀::Number
    k::Number
    GEOMETRIA_DA_NUVEM::String
    Geometria::Dimension
    N::Integer
    ρ::Number
    Radius::Number
    Lₐ::Number
    L::Number
    rₘᵢₙ::Number
    Tipo_de_kernel::String
    Δ::Number 
    N_Realizações::Integer
    W::Number
end
