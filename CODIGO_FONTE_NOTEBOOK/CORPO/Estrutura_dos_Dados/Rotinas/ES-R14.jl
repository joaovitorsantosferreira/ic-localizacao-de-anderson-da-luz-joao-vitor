mutable struct R14_ENTRADA
    Γ₀::Number  
    k::Number
    N::Integer
    Tipo_de_kernel::String
    N_pontos::Integer
    N_Realizações::Integer
    Lₐ::Number
    RANGE_INICIAL::Number
    RANGE_FINAL::Number
    Escala_do_range::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    Geometria::Dimension
end
