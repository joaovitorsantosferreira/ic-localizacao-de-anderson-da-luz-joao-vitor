mutable struct R8_ENTRADA
    Γ₀::Number
    ωₐ::Number
    k::Number
    N::Integer
    kR::Number
    Tipo_de_kernel::String
    N_Realizações::Integer
    N_div_Densidade::Integer
    N_div_Deturn::Integer
    Δ_Inicial::Number
    Δ_Final::Number
    Densidade_Inicial::Number
    Densidade_Final::Number
    Variavel_Constante::String
    Escala_de_Densidade::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
end

# entrada_rotina = R9_ENTRADA(
#     Γ₀,
#     ωₐ,
#     k,
#     N,
#     kR,
#     Tipo_de_kernel,
#     N_Realizações,
#     N_div_Densidade,
#     N_div_Deturn,
#     Δ_Inicial,
#     Δ_Final,
#     Densidade_Inicial,
#     Densidade_Final,
#     Variavel_Constante,
#     Escala_de_Densidade,
# )