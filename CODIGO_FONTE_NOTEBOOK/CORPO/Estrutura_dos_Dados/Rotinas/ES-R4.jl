mutable struct R4_ENTRADA
    Γ₀::Number
    k::Number
    ρ::Number
    L::Number
    Lₐ::Number
    N_Realizações::Integer
    Tipo_de_kernel::String
    N_div_Deturn::Integer
    Δ_Inicial::Number
    Δ_Final::Number
    Desordem_Diagonal::String
    W::Number
    Geometria::Dimension
end
