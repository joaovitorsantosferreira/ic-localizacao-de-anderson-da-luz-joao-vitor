mutable struct E10_ENTRADA
    N::Integer
    ρ::Number
    k::Number
    Γ₀::Number
    Δ::Number
    Radius::Number
    rₘᵢₙ::Number
    Tipo_de_kernel::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
    Geometria::Dimension
end
