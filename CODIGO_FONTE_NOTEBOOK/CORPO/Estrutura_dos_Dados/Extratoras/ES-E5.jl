mutable struct E5_ENTRADA
    Γ₀::Number 
    Ω ::Number
    k::Number
    vetor_de_onda::Any
    ω₀::Number
    Δ ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    Tipo_de_beta::String
    Desordem_Diagonal::String
    cloud::Cloud_SAIDA
    L::Number
    Radius::Number
    GEOMETRIA_DA_NUVEM::String
    Geometria::Dimension
end


