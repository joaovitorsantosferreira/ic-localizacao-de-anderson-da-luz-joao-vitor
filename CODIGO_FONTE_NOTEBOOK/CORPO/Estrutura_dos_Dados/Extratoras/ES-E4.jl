mutable struct E4_ENTRADA
    Γ₀::Number
    k::Number
    N::Integer
    Radius::Number
    ρ::Number
    rₘᵢₙ::Number
    ωₗ::Number
    Δ::Number
    Tipo_de_kernel::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
end

# entrada_extratora = E4_ENTRADA(
#     Γ₀, 
#     k,
#     N,
#     Radius,
#     ρ,
#     rₘᵢₙ,
#     ωₗ,
#     Δ,
#     Tipo_de_kernel
# )