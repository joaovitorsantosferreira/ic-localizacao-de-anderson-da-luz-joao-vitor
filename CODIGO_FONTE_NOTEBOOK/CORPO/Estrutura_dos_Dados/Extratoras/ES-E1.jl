mutable struct E1_ENTRADA
    Γ₀::Number 
    ωₐ::Number
    Ω ::Number
    k::Number
    N::Integer
    Radius::Number
    ρ::Number
    rₘᵢₙ::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    ω₀::Number
    λ::Number
    ωₗ::Number
    Δ ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
    Tipo_de_beta::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
    PERFIL_DA_INTENSIDADE::String
    N_Telas::Number
    Geometria::Dimension
end

mutable struct E1_SAIDA
    cloud::Cloud_SAIDA
    propriedades_fisicas_global::Characteristics_of_the_modes_SAIDA
    propriedades_fisicas_best_mode::get_best_mode_SAIDA
    βₙ::Array
    Posição_Sensores::Array
    All_Intensitys::Array
end 
