###############################################################################################################################################################################################################
#------------------------------------------------------------------------- Funções que geram a nuvem com uma distribuição aleatória --------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------ Função Principal responsavel gerenciar e chamar as outras funções que geram o disco ------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function getAtoms_distribution(Variaveis_de_Entrada::getAtoms_distribution_ENTRADA, geometria::TwoD)

    # Chama a função responsavel por gerar duas matrizes "vazias"
    ## r_new_atom é uma matriz que gera pontos aleatorios em coordenadas polares 
    ## r_cartesian é a matriz que vai guardar os pontos validos, ou seja, aqueles no interior do disco
    r_new_atom, r_cartesian = get_empty_arrays(Variaveis_de_Entrada.N,geometria)
    
    nValid = 0                                                                                                           # Variavel auxiliar para o loop
    
    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

        # while true 
        #     get_one_random_atom_inside_disc!(Variaveis_de_Entrada.Radius,r_new_atom)

        #     if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
        #         nValid += 1 
        #         r_cartesian[nValid, : ] = r_new_atom        
        #     end

        #     if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
        #         break
        #     end
        # end
        
        while true 
            get_one_random_atom_inside_square!(Variaveis_de_Entrada.Radius,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ) && norm(r_new_atom) <= Variaveis_de_Entrada.Radius
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom        
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end
    
    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"
        
        while true 
            get_one_random_atom_inside_slab!(Variaveis_de_Entrada.L,Variaveis_de_Entrada.Lₐ,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end

    end

    # Aqui é rotornada uma matriz que contem as coordenadas cartesianas dos N pontos no interior do disco
    ## Primeira coluna coordenadas no eixo x
    ## Segunda coluna coordenadas no eixo y  
    return r_cartesian
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------ Função Principal responsavel gerenciar e chamar as outras funções que geram a nuvem no caso 3D -------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function getAtoms_distribution(Variaveis_de_Entrada::getAtoms_distribution_ENTRADA, geometria::ThreeD)

    # Chama a função responsavel por gerar duas matrizes "vazias"
    ## r_new_atom é uma matriz que gera pontos aleatorios em coordenadas polares 
    ## r_cartesian é a matriz que vai guardar os pontos validos, ou seja, aqueles no interior do disco
    r_new_atom, r_cartesian = get_empty_arrays(Variaveis_de_Entrada.N,geometria)
    
    nValid = 0                                                                                                           # Variavel auxiliar para o loop
    
    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "CUBO"

        while true 
            get_one_random_atom_inside_cube!(Variaveis_de_Entrada.L,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end
    
    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "ESFERA"
        
        # while true 
        #     get_one_random_atom_inside_esfera!(Variaveis_de_Entrada.Radius,r_new_atom)

        #     if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
        #         nValid += 1 
        #         r_cartesian[nValid, : ] = r_new_atom
        #     end

        #     if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
        #         break
        #     end
        # end

        while true 
            get_one_random_atom_inside_cube!(2*Variaveis_de_Entrada.Radius,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ) && norm(r_new_atom) <= Variaveis_de_Entrada.Radius
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end
    
    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"
        
        while true 
            get_one_random_atom_inside_paralelepido!(Variaveis_de_Entrada.L,Variaveis_de_Entrada.Lₐ,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end
    
    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "TUBO"
        
        while true 
            get_one_random_atom_inside_tubo!(Variaveis_de_Entrada.L,Variaveis_de_Entrada.Lₐ,r_new_atom)

            if is_valid_position(r_new_atom, r_cartesian[1:nValid, : ], Variaveis_de_Entrada.rₘᵢₙ)
                nValid += 1 
                r_cartesian[nValid, : ] = r_new_atom
            end

            if nValid == Variaveis_de_Entrada.N                                                                                                   # Aqui existe a contagem de atomos validos, dentro do disco
                break
            end
        end
    
    end
    # Aqui é rotornada uma matriz que contem as coordenadas cartesianas dos N pontos no interior do disco
    ## Primeira coluna coordenadas no eixo x
    ## Segunda coluna coordenadas no eixo y  
    return r_cartesian
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------- Funções que geram os pontos Aleatórios ------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function get_one_random_atom_inside_slab!(L,Lₐ, r_new_atom::Array)
    
    r_new_atom[1] = L*rand() - L/2 
    r_new_atom[2] = Lₐ*rand() - Lₐ/2
    nothing
    
end

function get_one_random_atom_inside_square!(L,r_new_atom::Array)
    
    r_new_atom[1] = 2*L*rand() - L 
    r_new_atom[2] = 2*L*rand() - L
    nothing
    
end

function get_one_random_atom_inside_disc!(Radius::Number, r_new_atom::Array)
    
    r_new_atom[1] = 2*π*rand()
    r_new_atom[2] = rand() + rand()

    if r_new_atom[2] > 1

        r_new_atom[2] = 2 - r_new_atom[2]

    end

    
    r_new_atom[2] = Radius*r_new_atom[2]
    r_new_atom[:] = polar_for_cart(r_new_atom)
    nothing
end


function get_one_random_atom_inside_cube!(L, r_new_atom::Array)
    
    r_new_atom[1] = L*rand() - L/2 
    r_new_atom[2] = L*rand() - L/2
    r_new_atom[3] = L*rand() - L/2
    nothing
    
end


function get_one_random_atom_inside_paralelepido!(L,Lₐ, r_new_atom::Array)
    
    r_new_atom[1] = Lₐ*rand() - Lₐ/2 
    r_new_atom[2] = Lₐ*rand() - Lₐ/2
    r_new_atom[3] = L*rand() - L/2
    nothing
    
end

function get_one_random_atom_inside_tubo!(L::Number,Radius::Number, r_new_atom::Array)
    
    r_new_atom[1] = 2*π*rand()
    r_new_atom[2] = rand() + rand()

    if r_new_atom[2] > 1

        r_new_atom[2] = 2 - r_new_atom[2]

    end

    
    r_new_atom[2] = Radius*r_new_atom[2]
    r_new_atom[1:2] = polar_for_cart(r_new_atom[1:2])
    r_new_atom[3] = L*rand() - L/2
    nothing
end

function get_one_random_atom_inside_esfera!(Radius::Number, r_new_atom::Array)
    
    r_new_atom[1] = 2*π*rand()
    # r_new_atom[2] = π*rand()
    r_new_atom[2] = acos(2*rand()-1)
    r_new_atom[3] = rand() + rand()

    if r_new_atom[3] > 1

        r_new_atom[3] = 2 - r_new_atom[3]

    end

    r_new_atom[3] = Radius*r_new_atom[3]

    r_new_atom[:] = esferic_for_cart(r_new_atom)
    nothing
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_empty_arrays(N::Integer,geometria::TwoD)
    
    r_new_atom = zeros(2)                                                                                                           # Matriz vazia para gerar pontos aleatorios
    r_cartesian = Array{Float64}(undef, N, 2)                                                                                       # Matriz vazia para guardar os pontos validos 
    
    return r_new_atom, r_cartesian
end

function get_empty_arrays(N::Integer,geometria::ThreeD)
    
    r_new_atom = zeros(3)                                                                                                           # Matriz vazia para gerar pontos aleatorios
    r_cartesian = Array{Float64}(undef, N, 3)                                                                                       # Matriz vazia para guardar os pontos validos 
    
    return r_new_atom, r_cartesian
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function polar_for_cart(polar_coordinate::Array)
    
    azimuth = polar_coordinate[1]
    radius = polar_coordinate[2]
    

    x = radius * cos.(azimuth)
    y = radius * sin.(azimuth)


    position_cartesian = [x, y]
    # Esta função retorna as coordenadas dos pontos que antes eram polares para coordenadas cartesianas
    return position_cartesian
end


function esferic_for_cart(esferic_coordinate::Array)
    
    azimuth = esferic_coordinate[1]
    alt = esferic_coordinate[2]
    radius = esferic_coordinate[3]
    

    x = radius * sin.(alt) * cos.(azimuth)
    y = radius * sin.(alt) * sin.(azimuth)
    z = radius * cos.(alt)

    position_cartesian = [x, y, z]
    # Esta função retorna as coordenadas dos pontos que antes eram polares para coordenadas cartesianas
    return position_cartesian
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function is_valid_position(r_new_atom::Array, r_cartesian::Array, rₘᵢₙ::Number)
    
    nAtoms = size(r_cartesian, 1)                                                                                                       # Número de linhas de r que corresponde ao número de atomos do sistema


    # Retorna uma variavel booleana, a ideia é verificar se todos atomos tem distância permitida entre sí   
    return all(get_Distance_A_to_b(r_cartesian, r_new_atom) .≥ rₘᵢₙ)    
end

function get_Distance_A_to_b(A::Array, b::Array)


    n_rows = size(A, 1)                                                                                                                 # Aqui temos a contagem dos N atomos novamente
    distanceAb = zeros(n_rows)                                                                                                          # Define uma matriz que vai guardar a distancia entre os atomos
    @inbounds for i = 1:n_rows                                                                                                          # Fazer o calculo em loop para cada atomo

        # Aqui a matriz distaceAb vai guardar a distancia de todos os atomos com relação ao novo atomo
        ## Neste ponto é usado o pacote Distances 
        distanceAb[i] = Distances.evaluate(Euclidean(), A[i, :], b) 
    

    end
    return distanceAb
end



###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Funções que Geram a Matriz de Green do Sistema --------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


function get_GreensMatrix(Varievaies_de_Entrada::get_GreensMatrix_ENTRADA,geometria::TwoD)

    Desordem_Diagonal = Varievaies_de_Entrada.Desordem_Diagonal

    if Desordem_Diagonal == "AUSENTE"


        if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

            
            G = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                            # Gera uma matriz vazia que será a matriz de Green

            # Preenche a Matriz de Green toda
            @. G = SpecialFunctions.besselh(0,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            
            # Acessa e preenche a diagonal principal da matriz de Green
            G[LinearAlgebra.diagind(G)].= 1


        elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
            
            Γ₁ = Varievaies_de_Entrada.Γ₀/2
            ϕₛ = get_ϕ1(Varievaies_de_Entrada.r)


            G⁽⁰⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            @. G⁽⁰⁾ = SpecialFunctions.hankelh1(0,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            G⁽⁰⁾[LinearAlgebra.diagind(G⁽⁰⁾)].= 1 + 0im 


            H⁽⁺⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            @. H⁽⁺⁾ = -1*SpecialFunctions.hankelh1(2,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk)*cis(-2*ϕₛ) 
            H⁽⁺⁾[LinearAlgebra.diagind(H⁽⁺⁾)].= 0 + 0im

            H⁽⁻⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N) 
            @. H⁽⁻⁾ = -1*SpecialFunctions.hankelh1(2,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk)*cis(2*ϕₛ) 
            H⁽⁻⁾[LinearAlgebra.diagind(H⁽⁻⁾)].= 0 + 0im


            G⁽¹⁾ = Array{Complex{Float64}}(undef, 2*Varievaies_de_Entrada.N, 2*Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            G⁽¹⁾[1:Varievaies_de_Entrada.N,1:Varievaies_de_Entrada.N] = G⁽⁰⁾
            G⁽¹⁾[(1+Varievaies_de_Entrada.N):(2*Varievaies_de_Entrada.N),(1+Varievaies_de_Entrada.N):(2*Varievaies_de_Entrada.N)] = G⁽⁰⁾
            G⁽¹⁾[1:Varievaies_de_Entrada.N,(Varievaies_de_Entrada.N+1):2*Varievaies_de_Entrada.N] = H⁽⁺⁾
            G⁽¹⁾[(Varievaies_de_Entrada.N+1):2*Varievaies_de_Entrada.N,1:Varievaies_de_Entrada.N] = H⁽⁻⁾
        end
    
        
    elseif Desordem_Diagonal == "PRESENTE"

        

        if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

            
            G = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                            # Gera uma matriz vazia que será a matriz de Green

            # Preenche a Matriz de Green toda
            @. G = SpecialFunctions.besselh(0,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            
            # Acessa e preenche a diagonal principal da matriz de Green
            G[LinearAlgebra.diagind(G)] = ones(Varievaies_de_Entrada.N,1) .+ 1im*rand(Varievaies_de_Entrada.N)*Varievaies_de_Entrada.W .- 0.5im*Varievaies_de_Entrada.W 


        elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
            
            Γ₁ = Varievaies_de_Entrada.Γ₀/2
            ϕₛ = get_ϕ1(Varievaies_de_Entrada.r)

            G⁽⁰⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            @. G⁽⁰⁾ = SpecialFunctions.besselh(0,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            G⁽⁰⁾[LinearAlgebra.diagind(G⁽⁰⁾)] = ones(Varievaies_de_Entrada.N,1) .+ 1im*rand(Varievaies_de_Entrada.N)*Varievaies_de_Entrada.W .- 0.5im*Varievaies_de_Entrada.W  

            H⁽⁺⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            @. H⁽⁺⁾ = - SpecialFunctions.besselh(2,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk)*exp(-2im*ϕₛ) 
            H⁽⁺⁾[LinearAlgebra.diagind(H⁽⁺⁾)].= 0 + 0im

            H⁽⁻⁾ = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N) 
            @. H⁽⁻⁾ = - SpecialFunctions.besselh(2,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk)*exp(2im*ϕₛ) 
            H⁽⁻⁾[LinearAlgebra.diagind(H⁽⁻⁾)].= 0 + 0im


            G⁽¹⁾ = Array{Complex{Float64}}(undef, 2*Varievaies_de_Entrada.N, 2*Varievaies_de_Entrada.N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            G⁽¹⁾[1:Varievaies_de_Entrada.N,1:Varievaies_de_Entrada.N] = G⁽⁰⁾
            G⁽¹⁾[(1+Varievaies_de_Entrada.N):(2*Varievaies_de_Entrada.N),(1+Varievaies_de_Entrada.N):(2*Varievaies_de_Entrada.N)] = G⁽⁰⁾
            G⁽¹⁾[1:Varievaies_de_Entrada.N,(Varievaies_de_Entrada.N+1):2*Varievaies_de_Entrada.N] = H⁽⁺⁾
            G⁽¹⁾[(Varievaies_de_Entrada.N+1):2*Varievaies_de_Entrada.N,1:Varievaies_de_Entrada.N] = H⁽⁻⁾
        end
    end

    if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

        return G
    
    elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
        
        return G⁽¹⁾
    
    end 
        
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_GreensMatrix(Varievaies_de_Entrada::get_GreensMatrix_ENTRADA,geometria::ThreeD)

    Desordem_Diagonal = Varievaies_de_Entrada.Desordem_Diagonal

    δ(x,y) = Int(==(x,y))
    P(x) =  1 - 1/x + 1/x^2 
    Q(x) = -1 + 3/x - 3/x^2 

    if Desordem_Diagonal == "AUSENTE"


        if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

            
            G = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                            # Gera uma matriz vazia que será a matriz de Green

            # Preenche a Matriz de Green toda
            @. G = cis(Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk)/(1im*Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            
            # Acessa e preenche a diagonal principal da matriz de Green
            G[LinearAlgebra.diagind(G)].= 1


        elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
            
            Γ₁ = Varievaies_de_Entrada.Γ₀/2
            N = Varievaies_de_Entrada.N
            Δ = Varievaies_de_Entrada.Δ
            r = Varievaies_de_Entrada.r
            k₀ = Varievaies_de_Entrada.k₀



            Xt, Yt, Zt = r[:,1], r[:,2], r[:,3]
        
            Xjn = Xt*ones(1,N) - ones(N,1)*Xt'
            Yjn = Yt*ones(1,N) - ones(N,1)*Yt'
            Zjn = Zt*ones(1,N) - ones(N,1)*Zt'
            
            array_XYZ_jn = [Xjn, Yjn, Zjn]
            Varievaies_de_Entrada.R_jk = sqrt.(Xjn.^2 + Zjn.^2 + Yjn.^2)
            
            α_range = β_range = [-1, +1, 0]
        
            term2 = (3/2)*exp.(im*k₀*Varievaies_de_Entrada.R_jk)./(k₀*Varievaies_de_Entrada.R_jk)
            term2[findall(isnan.(term2))] .= 0
            term2[findall(isinf.(term2))] .= 0
        
            P_Rjn = P.(im*k₀*Varievaies_de_Entrada.R_jk)
            P_Rjn[findall(isnan.(P_Rjn))] .= 0
        
            Q_Rjn_over_Rjn_squared = Q.(im*k₀*Varievaies_de_Entrada.R_jk)./(k₀*Varievaies_de_Entrada.R_jk).^2
            Q_Rjn_over_Rjn_squared[findall(isnan.(Q_Rjn_over_Rjn_squared))] .= 0
        
            A = []
            for (α_idx, α) ∈ enumerate(α_range)
                B = []
                for (β_idx, β) ∈ enumerate(β_range)
                    term1 = im*I(N).*δ(α,β)
                    # term2 = (3/2)*exp.(im*Rjn)./Rjn  ## Defined outside      
                    term3 = P_Rjn.*δ(α,β)
                    term4 = Q_Rjn_over_Rjn_squared.*(array_XYZ_jn[α_idx].*array_XYZ_jn[β_idx])
        
                    K = term1 + term2.*(term3 .+ term4)
                    push!(B, K)
                end
                push!(A, vcat(B[1:length(β_range)]...)  )
            end
        
            G⁽¹⁾ = Array{Complex{Float64}}(undef, 3*N, 3*N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            G⁽¹⁾[:] = (-Γ₁/2)*hcat(A[1:length(α_range)]...) #join all matrices
            G⁽¹⁾ .= -1im*G⁽¹⁾ # For my code to work, I need this imaginary number. Credits to Sheila
            G⁽¹⁾[diagind(G⁽¹⁾)] .= (-Γ₁/2 + 1im*Δ)

        end
    
        
    elseif Desordem_Diagonal == "PRESENTE"

        

        if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

            
            G = Array{Complex{Float64}}(undef, Varievaies_de_Entrada.N, Varievaies_de_Entrada.N)                                                                                            # Gera uma matriz vazia que será a matriz de Green

            # Preenche a Matriz de Green toda
            @. G = SpecialFunctions.besselh(0,1,Varievaies_de_Entrada.k₀*Varievaies_de_Entrada.R_jk) 
            
            # Acessa e preenche a diagonal principal da matriz de Green
            G[LinearAlgebra.diagind(G)] = ones(Varievaies_de_Entrada.N,1) .+ 1im*rand(Varievaies_de_Entrada.N)*Varievaies_de_Entrada.W .- 0.5im*Varievaies_de_Entrada.W 


        elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
            
            Γ₁ = Varievaies_de_Entrada.Γ₀/2
            N = Varievaies_de_Entrada.N
            Δ = Varievaies_de_Entrada.Δ
            r = Varievaies_de_Entrada.r

            Xt, Yt, Zt = Varievaies_de_Entrada[1,:], r[2,:], r[3,:]
        
            Xjn = Xt*ones(1,N) - ones(N,1)*Xt'
            Yjn = Yt*ones(1,N) - ones(N,1)*Yt'
            Zjn = Zt*ones(1,N) - ones(N,1)*Zt'
            
            array_XYZ_jn = [Xjn, Yjn, Zjn]
            Rjn = sqrt.(Xjn.^2 + Zjn.^2 + Yjn.^2)
            
            α_range = β_range = [-1, +1, 0]
        
            term2 = (3/2)*exp.(im*k₀*Rjn)./(k₀*Rjn)
            term2[findall(isnan.(term2))] .= 0
            term2[findall(isinf.(term2))] .= 0
        
            P_Rjn = P.(im*k₀*Rjn)
            P_Rjn[findall(isnan.(P_Rjn))] .= 0
        
            Q_Rjn_over_Rjn_squared = Q.(im*k₀*Rjn)./(k₀*Rjn).^2
            Q_Rjn_over_Rjn_squared[findall(isnan.(Q_Rjn_over_Rjn_squared))] .= 0
        
            A = []
            for (α_idx, α) ∈ enumerate(α_range)
                B = []
                for (β_idx, β) ∈ enumerate(β_range)
                    term1 = im*I(N).*δ(α,β)
                    # term2 = (3/2)*exp.(im*Rjn)./Rjn  ## Defined outside      
                    term3 = P_Rjn.*δ(α,β)
                    term4 = Q_Rjn_over_Rjn_squared.*(array_XYZ_jn[α_idx].*array_XYZ_jn[β_idx])
        
                    K = term1 + term2.*(term3 .+ term4)
                    push!(B, K)
                end
                push!(A, vcat(B[1:length(β_range)]...)  )
            end
        
            G⁽¹⁾ = Array{Complex{Float64}}(undef, 3*N, 3*N)                                                                                      # Gera uma matriz vazia que será a matriz de Green
            G⁽¹⁾[:] = (-Γ₁/2)*hcat(A[1:length(α_range)]...) #join all matrices
            G⁽¹⁾ .= -1im*G⁽¹⁾ # For my code to work, I need this imaginary number. Credits to Sheila
            # G⁽¹⁾[diagind(G⁽¹⁾)] .= (-Γ₁/2 + 1im*Δ)
            G⁽¹⁾[diagind(G⁽¹⁾)] .= 1

        end
    end

    if Varievaies_de_Entrada.Tipo_de_luz == "Escalar"

        return G
    
    elseif Varievaies_de_Entrada.Tipo_de_luz == "Vetorial"
        
        return G⁽¹⁾
    
    end 
        
end


function get_ϕ2(r::Array)
    
    N = size(r,1)
    ϕₛ = zeros(N,N)


    contador_2 = 1
    contador_3 = 1
    contador_4 = 0

    while true

        termo1 = dot(r[contador_2, : ],r[contador_3, : ])/(norm(r[contador_2, : ])*norm(r[contador_3, : ]))
        
        if abs(termo1) > 1
            termo2 = round(termo1) 
            ϕₛ[contador_2,contador_3] = acos(termo2)
        
        else 

            ϕₛ[contador_2,contador_3] = acos(termo1)

        end
        
        contador_4 = contador_4 + 1
    
        if contador_3 == N
            contador_2 = contador_2 + 1
            contador_3 = 1
        else
            contador_3 = contador_3 + 1
        end 
        
        if contador_4 == N^2
            break
        end
    end

    ϕₛ[LinearAlgebra.diagind(ϕₛ)].= 0 
    return ϕₛ
end

function get_ϕ1(r::Array)
    
    N = size(r,1)
    ϕₛ = zeros(N,N)


    contador_2 = 1
    contador_3 = 1
    contador_4 = 0

    while true
        
        ϕₛ[contador_2,contador_3] = atan((r[contador_2,2]-r[contador_3,2]),(r[contador_2,1]-r[contador_3,1]))
        contador_4 += 1
    
        if contador_3 == N
            contador_2 += 1
            contador_3 = 1
        else
            contador_3 += 1
        end 
        
        if contador_4 == N^2
            break
        end
    end

    ϕₛ[LinearAlgebra.diagind(ϕₛ)].= 0 
    return ϕₛ
end



