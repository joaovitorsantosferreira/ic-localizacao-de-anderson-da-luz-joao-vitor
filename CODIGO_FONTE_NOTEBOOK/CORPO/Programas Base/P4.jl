###############################################################################################################################################################################################################
#---------------------------------------------------------------------------------------------- Dinamica dos atomos ------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

function get_all_beta_to_system(Variaveis_de_Entrada::get_all_beta_to_system_ENTRADA, geometria::TwoD)
    
    N = size(Variaveis_de_Entrada.r,1)

    r = Variaveis_de_Entrada.r
    G = Variaveis_de_Entrada.G
    Γ₀ = Variaveis_de_Entrada.Γ₀
    Δ = Variaveis_de_Entrada.Δ
    Ω = Variaveis_de_Entrada.Ω
    k = Variaveis_de_Entrada.k
    ω₀ = Variaveis_de_Entrada.ω₀
    Tipo_de_Kernel = Variaveis_de_Entrada.Tipo_de_Kernel
    Desordem_Diagonal = Variaveis_de_Entrada.Desordem_Diagonal
    Γ₁ = Γ₀/2


    if Desordem_Diagonal == "AUSENTE"

        if Tipo_de_Kernel == "Escalar"

            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                A = Array{Complex{Float64}}(undef, N,N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                Matriz_Resultante = -(Γ₀/2).*G + A

                βₙ = Matriz_Resultante \ b

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        elseif Tipo_de_Kernel == "Vetorial"
            

            k = norm(Variaveis_de_Entrada.vetor_de_onda)
            b = Array{Complex{Float64}}(undef, N) 

            for i ∈ 1:N
                entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                b[i] = -(sqrt(2)/4)*get_eletric_field_LG_real(entrada_campo,geometria)
            end

            b₊₋ = Array{Complex{Float64}}(undef, 2*N) 
            b₊₋[1:N] = b
            b₊₋[(N+1):2*N] = b

            A = Array{Complex{Float64}}(undef, 2*N,2*N)
            @. A = 0 + 0im 
            A[LinearAlgebra.diagind(A)].= 0 +  1im*Δ 

            Matriz_Resultante = -(Γ₁/2)*G .+ A

            βₙ = Matriz_Resultante \ b₊₋

        end

    elseif Desordem_Diagonal == "PRESENTE"
        
        if Tipo_de_Kernel == "Escalar"

            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end
                
                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                A = Array{Complex{Float64}}(undef, N,N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                W_termos_de_Desordem = imag.(G[LinearAlgebra.diagind(G)])

                A[LinearAlgebra.diagind(A)] = A[LinearAlgebra.diagind(A)] + 1im*W_termos_de_Desordem

                G[LinearAlgebra.diagind(G)] = real.(G[LinearAlgebra.diagind(G)])

                Matriz_Resultante = -(Γ₀/2)*G + A

                βₙ = Matriz_Resultante \ b

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        elseif Tipo_de_Kernel == "Vetorial"
            
            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                        
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                b₊₋ = Array{Complex{Float64}}(undef, 2*N) 

                for i ∈ 1:2*N

                    if i <= N
                        b₊₋[i] = -(b[i]*sqrt(2))/2im
                    
                    else
                        b₊₋[i] = -(b[i-N]*sqrt(2))/2im
                    end
                    
                end

                A = Array{Complex{Float64}}(undef, 2*N,2*N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                W_termos_de_Desordem = imag.(G[LinearAlgebra.diagind(G)])

                A[LinearAlgebra.diagind(A)] = A[LinearAlgebra.diagind(A)] + 1im*W_termos_de_Desordem

                G[LinearAlgebra.diagind(G)] = real.(G[LinearAlgebra.diagind(G)])


                A[LinearAlgebra.diagind(A)].= 1im*Δ

                Matriz_Resultante = -(Γ₁/2)*G + A

                βₙ = Matriz_Resultante \ b₊₋
                

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, 2*N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == 2*N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        end
    end

    return βₙ 
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_all_beta_to_system(Variaveis_de_Entrada::get_all_beta_to_system_ENTRADA, geometria::ThreeD)
    


    N = size(Variaveis_de_Entrada.r,1)

    r = Variaveis_de_Entrada.r
    G = Variaveis_de_Entrada.G
    Γ₀ = Variaveis_de_Entrada.Γ₀
    Δ = Variaveis_de_Entrada.Δ
    Ω = Variaveis_de_Entrada.Ω
    k = Variaveis_de_Entrada.k
    ω₀ = Variaveis_de_Entrada.ω₀
    Tipo_de_Kernel = Variaveis_de_Entrada.Tipo_de_Kernel
    Desordem_Diagonal = Variaveis_de_Entrada.Desordem_Diagonal
    Γ₁ = Γ₀/2


    if Desordem_Diagonal == "AUSENTE"

        if Tipo_de_Kernel == "Escalar"

            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 
 
                # for i ∈ 1:N                    
                #     entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                #     b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                # end

                for i ∈ 1:N
                    b[i] = ((1im*Ω)/2)*cis(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ]))
                end



                A = Array{Complex{Float64}}(undef, N,N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                Matriz_Resultante = -(Γ₀/2)*G + A

                βₙ = Matriz_Resultante \ b

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        elseif Tipo_de_Kernel == "Vetorial"
            
            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                        
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end

                   
                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                b₊₋ = Array{Complex{Float64}}(undef, 2*N) 

                for i ∈ 1:2*N

                    if i <= N
                        b₊₋[i] = -(b[i]*sqrt(2))/2im
                    
                    else
                        b₊₋[i] = -(b[i-N]*sqrt(2))/2im
                    end
                    
                end

                A = Array{Complex{Float64}}(undef, 3*N,3*N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                Matriz_Resultante = -(Γ₁/2)*G + A

                βₙ = Matriz_Resultante \ b₊₋

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, 2*N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == 2*N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        end

    elseif Desordem_Diagonal == "PRESENTE"
        
        if Tipo_de_Kernel == "Escalar"

            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end
                
                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                A = Array{Complex{Float64}}(undef, N,N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                W_termos_de_Desordem = imag.(G[LinearAlgebra.diagind(G)])

                A[LinearAlgebra.diagind(A)] = A[LinearAlgebra.diagind(A)] + 1im*W_termos_de_Desordem

                G[LinearAlgebra.diagind(G)] = real.(G[LinearAlgebra.diagind(G)])

                Matriz_Resultante = -(Γ₀/2)*G + A

                βₙ = Matriz_Resultante \ b

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        elseif Tipo_de_Kernel == "Vetorial"
            
            if Variaveis_de_Entrada.tipo_de_beta == "Estacionário"

                k = norm(Variaveis_de_Entrada.vetor_de_onda)
                b = Array{Complex{Float64}}(undef, N) 

                if Variaveis_de_Entrada.Tipo_de_Onda == "Laser Gaussiano"    
                    
                    for i ∈ 1:N
                        
                        entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Variaveis_de_Entrada.Δ)
                        b[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,geometria)
                        
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "Onda Plana"
                    
                    for i ∈ 1:N
                        b[i] = ((1im*Ω)/2)*exp(1im*(dot(Variaveis_de_Entrada.vetor_de_onda,r[i, : ])))
                    end

                elseif Variaveis_de_Entrada.Tipo_de_Onda == "FONTE PONTUAL"

                    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO" 

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.Radius/2) - r[i,1], - r[i,2])))
                        end

                    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                        λ = (2*π)/Variaveis_de_Entrada.k
                        for i ∈ 1:N
                            b[i] = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Variaveis_de_Entrada.L/2) - r[i,1], - r[i,2])))
                        end
                        
                    end
                end

                b₊₋ = Array{Complex{Float64}}(undef, 2*N) 

                for i ∈ 1:2*N

                    if i <= N
                        b₊₋[i] = -(b[i]*sqrt(2))/2im
                    
                    else
                        b₊₋[i] = -(b[i-N]*sqrt(2))/2im
                    end
                    
                end

                A = Array{Complex{Float64}}(undef, 2*N,2*N)
                @. A = 0 + 0im 

                A[LinearAlgebra.diagind(A)].= 1im*Δ

                W_termos_de_Desordem = imag.(G[LinearAlgebra.diagind(G)])

                A[LinearAlgebra.diagind(A)] = A[LinearAlgebra.diagind(A)] + 1im*W_termos_de_Desordem

                G[LinearAlgebra.diagind(G)] = real.(G[LinearAlgebra.diagind(G)])


                A[LinearAlgebra.diagind(A)].= 1im*Δ

                Matriz_Resultante = -(Γ₁/2)*G + A

                βₙ = Matriz_Resultante \ b₊₋

            
            elseif Variaveis_de_Entrada.tipo_de_beta == "Fases Aleatórias"
                
                βₙ =  Array{Complex{Float64}}(undef, 2*N) 

                contador = 1

                while true

                    βₙ[contador] = cis(2*π*rand())

                    if contador == 2*N
                        break
                    else 
                        contador = contador + 1
                    end    
                end
            end

        end
    end

    return βₙ 
end


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Intensidade da luz ---------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------- Gerando os pontos de Analise da intensidade ---------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_cloud_sensors(Variaveis_de_Entrada::get_cloud_sensors_ENTRADA, geometria::TwoD)

    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"

            Radius = Variaveis_de_Entrada.Radius
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
            Angulo_da_luz_incidente = Variaveis_de_Entrada.Angulo_da_luz_incidente                                                                                 
            Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
            Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

            Sensores = zeros(N_Sensores,2)                                                                                            
            Posição_Angular = range(Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2, length=N_Sensores)


            for i in 1:N_Sensores
            
                Sensores[i,1] = (Distancia_do_sistema + Radius)*cosd(Posição_Angular[i]+Angulo_da_luz_incidente)
                Sensores[i,2] = (Distancia_do_sistema + Radius)*sind(Posição_Angular[i]+Angulo_da_luz_incidente)

            end

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
            
            Radius = Variaveis_de_Entrada.Radius
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        

            Sensores_I = zeros(N_Sensores,2)                                                                                             

            Sensores_E = zeros(N_Sensores,2)

            Sensores_I[ : ,1] .= - Distancia_do_sistema - Radius
            Sensores_I[ : ,2] = range(-Radius,Radius,length=N_Sensores)

            Sensores_E[ : ,1] .= Distancia_do_sistema + Radius
            Sensores_E[ : ,2] = range(-Radius,Radius,length=N_Sensores)

            Sensores = zeros(N_Sensores,4)
            Sensores[:,1:2] = Sensores_I
            Sensores[:,3:4] = Sensores_E

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para obter a cintura do feixe"    
            
            Radius = Variaveis_de_Entrada.Radius
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            N_Telas = Variaveis_de_Entrada.N_Telas
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
            aux_1 = 1

            Sensores_I = zeros(N_Telas*N_Sensores,2)                                                                                          
            Sensores_E = zeros(N_Telas*N_Sensores,2)

            for i in 1:N_Telas

                Sensores_I[aux_1:(N_Sensores-1 + aux_1),1] .=  -Distancia_do_sistema -Radius -Radius*(i/N_Telas)
                Sensores_I[aux_1:(N_Sensores-1 + aux_1),2] = range(-Radius,Radius,length=N_Sensores)

                Sensores_E[aux_1:(N_Sensores-1 + aux_1),1] .= Distancia_do_sistema + Radius + Radius*(i/N_Telas)
                Sensores_E[aux_1:(N_Sensores-1 + aux_1),2] = range(-Radius,Radius,length=N_Sensores)

                aux_1 += N_Sensores
            end

            Sensores = zeros(N_Telas*N_Sensores,4)
            Sensores[ : ,1:2] = Sensores_I
            Sensores[ : ,3:4] = Sensores_E

        end

    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"


            L_diagonal = sqrt(Variaveis_de_Entrada.L^2 + Variaveis_de_Entrada.Lₐ^2)
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                   
            Angulo_da_luz_incidente = Variaveis_de_Entrada.Angulo_da_luz_incidente                                                                              
            Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
            Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

            Sensores = zeros(N_Sensores,2)                                                                                            
            Posição_Angular = range(Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2, length=N_Sensores)


            for i in 1:N_Sensores
            
                Sensores[i,1] = (Distancia_do_sistema + (sqrt(2)*L_diagonal)/2)*cosd(Posição_Angular[i]+Angulo_da_luz_incidente)
                Sensores[i,2] = (Distancia_do_sistema + (sqrt(2)*L_diagonal)/2)*sind(Posição_Angular[i]+Angulo_da_luz_incidente)

            end

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

            Lₐ = Variaveis_de_Entrada.Lₐ
            L = Variaveis_de_Entrada.L
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                       

            
            Sensores_I = zeros(N_Sensores,2)                                                                                            
            Sensores_E = zeros(N_Sensores,2)

            Sensores_I[ : ,1] .= - Distancia_do_sistema - L/2
            Sensores_I[ : ,2] = range(-Lₐ/2,Lₐ/2,length=N_Sensores)


            Sensores_E[ : ,1] .= Distancia_do_sistema + L/2
            Sensores_E[ : ,2] = range(-Lₐ/2,Lₐ/2,length=N_Sensores)


            Sensores = zeros(N_Sensores,4)
            Sensores[:,1:2] = Sensores_I
            Sensores[:,3:4] = Sensores_E

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para obter a cintura do feixe"    
            
            Lₐ = Variaveis_de_Entrada.Lₐ
            L = Variaveis_de_Entrada.L
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            N_Telas = Variaveis_de_Entrada.N_Telas
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
            aux_1 = 1

            Sensores_I = zeros(N_Telas*N_Sensores,2)                                                                                           
            Sensores_E = zeros(N_Telas*N_Sensores,2)

            for i in 1:N_Telas

                Sensores_I[aux_1:(N_Sensores-1 + aux_1),1] .=  - Distancia_do_sistema - (L)*(i/N_Telas)
                Sensores_I[aux_1:(N_Sensores-1 + aux_1),2] = range(-(Lₐ/2),(Lₐ/2), length = N_Sensores)

                Sensores_E[aux_1:(N_Sensores-1 + aux_1),1] .=  Distancia_do_sistema + (L)*(i/N_Telas)
                Sensores_E[aux_1:(N_Sensores-1 + aux_1),2] = range(-(Lₐ/2),(Lₐ/2), length = N_Sensores)

                aux_1 += N_Sensores
            end

            Sensores = zeros(N_Telas*N_Sensores,4)
            Sensores[ : ,1:2] = Sensores_I
            Sensores[ : ,3:4] = Sensores_E

        end

    end
    
    return Sensores
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function get_cloud_sensors(Variaveis_de_Entrada::get_cloud_sensors_ENTRADA, geometria::ThreeD)

    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "ESFERA"


        Radius = Variaveis_de_Entrada.Radius
        N_Sensores = Variaveis_de_Entrada.N_Sensores
        Distancia_dos_Sensores = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
        Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1        
        Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

        Sensores = zeros(N_Sensores,3)  
        α_angulo_vertical = range(Angulo_de_variação_da_tela_circular_1+1,Angulo_de_variação_da_tela_circular_2, step=1)
        
        # α_angulo_vertical = (Angulo_de_variação_da_tela_circular_2 - Angulo_de_variação_da_tela_circular_1)
        β_angulo_horizontal = ((size(α_angulo_vertical,1))*360)/(N_Sensores-1)
        contador_sensores = 0
        Posição_Angular = 0:β_angulo_horizontal:((size(α_angulo_vertical,1))*360)


        for i in α_angulo_vertical.-Angulo_de_variação_da_tela_circular_1
            angulos_por_cota = Posição_Angular[findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360))]
            sensores_na_cota = size(findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360)),1)

            for j in 1:sensores_na_cota
                Sensores[j+contador_sensores,1] = (Distancia_dos_Sensores + Radius)*sind(Angulo_de_variação_da_tela_circular_1+i)*cosd(angulos_por_cota[j])
                Sensores[j+contador_sensores,2] = (Distancia_dos_Sensores + Radius)*sind(Angulo_de_variação_da_tela_circular_1+i)*sind(angulos_por_cota[j])
                Sensores[j+contador_sensores,3] = (Distancia_dos_Sensores + Radius)*cosd(Angulo_de_variação_da_tela_circular_1+i)
            end

            contador_sensores += sensores_na_cota
        end

        # for i in 1:N_Sensores
        
        #     Sensores[i,1] = (Distancia_do_sistema + Radius)*sind(40)*cosd(Posição_Angular[i])
        #     Sensores[i,2] = (Distancia_do_sistema + Radius)*sind(40)*sind(Posição_Angular[i])
        #     Sensores[i,3] = (Distancia_do_sistema + Radius)*cosd(40)

        # end

    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "CUBO"

        # N_Sensores = Variaveis_de_Entrada.N_Sensores
        # Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                   
        # Angulo_da_luz_incidente = Variaveis_de_Entrada.Angulo_da_luz_incidente                                                                              
        # Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
        # Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

        # Sensores = zeros(N_Sensores,3)                                                                                            
        # Posição_Angular = range(Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2, length=N_Sensores)


        # for i in 1:N_Sensores
        
        #     Sensores[i,1] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * cosd(Posição_Angular[i])
        #     Sensores[i,2] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * sind(Posição_Angular[i])
        #     Sensores[i,3] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * cosd(40)

        # end

        L = Variaveis_de_Entrada.L
        N_Sensores = Variaveis_de_Entrada.N_Sensores
        Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
        Angulo_da_luz_incidente = Variaveis_de_Entrada.Angulo_da_luz_incidente                                                                                 
        Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
        Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

        Sensores = zeros(N_Sensores,3)                                                                                            
        α_angulo_vertical = range(Angulo_de_variação_da_tela_circular_1+1,Angulo_de_variação_da_tela_circular_2, step=1)
        
        # α_angulo_vertical = (Angulo_de_variação_da_tela_circular_2 - Angulo_de_variação_da_tela_circular_1)
        β_angulo_horizontal = ((size(α_angulo_vertical,1))*360)/(N_Sensores-1)
        contador_sensores = 0
        Posição_Angular = 0:β_angulo_horizontal:((size(α_angulo_vertical,1))*360)


        for i in α_angulo_vertical.-Angulo_de_variação_da_tela_circular_1
            angulos_por_cota = Posição_Angular[findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360))]
            sensores_na_cota = size(findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360)),1)

            for j in 1:sensores_na_cota
                Sensores[j+contador_sensores,1] = (Distancia_do_sistema + L)*sind(Angulo_de_variação_da_tela_circular_1+i)*cosd(angulos_por_cota[j])
                Sensores[j+contador_sensores,2] = (Distancia_do_sistema + L)*sind(Angulo_de_variação_da_tela_circular_1+i)*sind(angulos_por_cota[j])
                Sensores[j+contador_sensores,3] = (Distancia_do_sistema + L)*cosd(Angulo_de_variação_da_tela_circular_1+i)
            end

            contador_sensores += sensores_na_cota
        end

    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            

            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                   
            Angulo_da_luz_incidente = Variaveis_de_Entrada.Angulo_da_luz_incidente                                                                              
            Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
            Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

            Sensores = zeros(N_Sensores,3)                                                                                            
            Posição_Angular = range(Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2, length=N_Sensores)


            for i in 1:N_Sensores
            
                Sensores[i,1] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * cosd(Posição_Angular[i])
                Sensores[i,2] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * sind(Posição_Angular[i])
                Sensores[i,3] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * cosd(40)

            end

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

            Lₐ = Variaveis_de_Entrada.Lₐ
            L = Variaveis_de_Entrada.L
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                       
            
            Sensores = zeros(N_Sensores^2,3)                                                                                            
            contador = 1
            Δs =  range(-Lₐ/2,Lₐ/2,length=N_Sensores) 
            for k in 1:N_Sensores
                for j in 1:N_Sensores
                    Sensores[contador,1] = Δs[k] 
                    Sensores[contador,2] = Δs[j]
                    contador += 1
                end
            end

            Sensores[ : ,3] .= Distancia_do_sistema + L/2
        end
        
    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "TUBO"

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            
            Lₐ = Variaveis_de_Entrada.Lₐ
            L  = Variaveis_de_Entrada.L
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_dos_Sensores = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                        
            Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1        
            Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2
    
            Sensores = zeros(N_Sensores,3)                                                                                            
            α_angulo_vertical = range(Angulo_de_variação_da_tela_circular_1+1,Angulo_de_variação_da_tela_circular_2, step=1)
            
            # α_angulo_vertical = (Angulo_de_variação_da_tela_circular_2 - Angulo_de_variação_da_tela_circular_1)
            β_angulo_horizontal = ((size(α_angulo_vertical,1))*360)/(N_Sensores-1)
            contador_sensores = 0
            Posição_Angular = 0:β_angulo_horizontal:((size(α_angulo_vertical,1))*360)
    
    
            for i in α_angulo_vertical.-Angulo_de_variação_da_tela_circular_1

                angulos_por_cota = Posição_Angular[findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360))]
                sensores_na_cota = size(findall( (Posição_Angular.>=(i-1)*360).*(Posição_Angular.<=i*360)),1)
    
                for j in 1:sensores_na_cota
                    Sensores[j+contador_sensores,1] = (Distancia_dos_Sensores + sqrt(Lₐ^2 + L^2) )*sind(Angulo_de_variação_da_tela_circular_1+i)*cosd(angulos_por_cota[j])
                    Sensores[j+contador_sensores,2] = (Distancia_dos_Sensores + sqrt(Lₐ^2 + L^2) )*sind(Angulo_de_variação_da_tela_circular_1+i)*sind(angulos_por_cota[j])
                    Sensores[j+contador_sensores,3] = (Distancia_dos_Sensores + sqrt(Lₐ^2 + L^2) )*cosd(Angulo_de_variação_da_tela_circular_1+i)
                end
    
                contador_sensores += sensores_na_cota
            end


            # Sensores = zeros(N_Sensores,3)                                                                                            
            # Posição_Angular = range(Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2, length=N_Sensores)


            # for i in 1:N_Sensores
            
            #     Sensores[i,1] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * cosd(Posição_Angular[i])
            #     Sensores[i,2] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * sind(40) * sind(Posição_Angular[i])
            #     Sensores[i,3] = (Distancia_do_sistema + (Variaveis_de_Entrada.L/2)) * cosd(40)

            # end

        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

            Lₐ = Variaveis_de_Entrada.Lₐ
            L = Variaveis_de_Entrada.L
            N_Sensores = Variaveis_de_Entrada.N_Sensores
            Distancia_do_sistema = Variaveis_de_Entrada.Distancia_dos_Sensores                                                                                       
            
            Sensores = zeros(N_Sensores^2,3)                                                                                            
            contador = 1
            Δs =  range(-Lₐ,Lₐ,length=N_Sensores) 
            for k in 1:N_Sensores
                for j in 1:N_Sensores
                    Sensores[contador,1] = Δs[k] 
                    Sensores[contador,2] = Δs[j]
                    contador += 1
                end
            end

            Sensores[ : ,3] .= Distancia_do_sistema + L/2
        end
    end
    
    
    return Sensores
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Analise da Intensidade da luz nos pontos de Interresse -----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Intensity_to_sensors(Entrada::get_Intensity_to_sensors_ENTRADA,geometria::TwoD)
    
    Sensores = Entrada.Sensores

    if Entrada.PERFIL_DA_INTENSIDADE == "NAO"

        N_Sensores = size(Sensores,1)
        Intesity_of_ALL_Sensors = zeros(2,N_Sensores)

        for i in 1:N_Sensores
            θ = get_degree_azimut(Sensores[i, : ])
            Intesity_of_ALL_Sensors[1,i] = get_Intensity_in_point_R(Entrada,Sensores[i, : ],geometria)
            Intesity_of_ALL_Sensors[2,i] = θ
        end

    elseif Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        N_Sensores = size(Sensores,1)
        Intesity_of_ALL_Sensors = zeros(4,N_Sensores)

        for i in 1:N_Sensores

            θ = get_degree_azimut(Sensores[i,1:2])
            
            if Entrada.Tipo_de_Onda == "Laser Gaussiano"

                Intesity_of_ALL_Sensors[1,i] = get_eletric_field_LG_modulo_quadrado(Sensores[i,:],Entrada.Ω,Entrada.ω₀,Entrada.k,geometria)
            
            elseif Entrada.Tipo_de_Onda == "Onda Plana"

                Intesity_of_ALL_Sensors[1,i] = Entrada.Ω*cis(Entrada.k*dot(Sensores[i,1:2],Entrada.vetor_de_onda))

            elseif Entrada.Tipo_de_Onda == "FONTE PONTUAL"


                # λ = (2*π)/Entrada.k

                if Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

                    Intesity_of_ALL_Sensors[1,i] = (abs(Entrada.Ω*SpecialFunctions.besselh(0,1,Entrada.k*norm(((-Entrada.Radius/2) - Sensores[i,1], - Sensores[i,2])))))^2
                    
                elseif Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                    Intesity_of_ALL_Sensors[1,i] = (abs(Entrada.Ω*SpecialFunctions.besselh(0,1,Entrada.k*norm(((-Entrada.L/2) - Sensores[i,1], - Sensores[i,2])))))^2                    
                end

               
            end
            Intesity_of_ALL_Sensors[2,i] = θ
        end

        for i in 1:N_Sensores

            θ = get_degree_azimut(Sensores[i,3:4])
            Intesity_of_ALL_Sensors[3,i] = get_Intensity_in_point_R(Entrada,Sensores[i,3:4],geometria)
            Intesity_of_ALL_Sensors[4,i] = θ
            
        end

    elseif Entrada.PERFIL_DA_INTENSIDADE == "SIM,para obter a cintura do feixe"


        N_Telas = Entrada.N_Telas
        N_Sensores = round(Int64,size(Sensores,1)/N_Telas)
        Q_total = N_Telas*N_Sensores
        Intesity_of_ALL_Sensors = zeros(4,Q_total)

        for i in 1:Q_total

            θ = get_degree_azimut(Sensores[i,1:2])


            if Entrada.Tipo_de_Onda == "Laser Gaussiano"

                Intesity_of_ALL_Sensors[1,i] = get_eletric_field_LG_modulo_quadrado(Sensores[i,1],Sensores[i,2],Entrada.Ω,Entrada.ω₀,Entrada.k)
            
            elseif Entrada.Tipo_de_Onda == "Onda Plana"

                Intesity_of_ALL_Sensors[1,i] = Entrada.Ω*cis(Entrada.k*dot(Sensores[i,1:2],Entrada.vetor_de_onda))

            elseif Entrada.Tipo_de_Onda == "FONTE PONTUAL"


                # λ = (2*π)/Entrada.k

                if Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

                    Intesity_of_ALL_Sensors[1,i] = (abs(Entrada.Ω*SpecialFunctions.besselh(0,1,Entrada.k*norm(((-Entrada.Radius/2) - Sensores[i,1], - Sensores[i,2])))))^2
                    
                elseif Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                    Intesity_of_ALL_Sensors[1,i] = (abs(Entrada.Ω*SpecialFunctions.besselh(0,1,Entrada.k*norm(((-Entrada.L/2) - Sensores[i,1], - Sensores[i,2])))))^2                    
                end

               
            end


            Intesity_of_ALL_Sensors[2,i] = θ

        end

        for i in 1:Q_total

            θ = get_degree_azimut(Sensores[i,3:4])
            Intesity_of_ALL_Sensors[3,i] = get_Intensity_in_point_R(Entrada,Sensores[i,3:4],geometria)
            Intesity_of_ALL_Sensors[4,i] = θ

        end

    end

    return Intesity_of_ALL_Sensors
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Intensity_to_sensors(Entrada::get_Intensity_to_sensors_ENTRADA,geometria::ThreeD)
    
    Sensores = Entrada.Sensores

    if Entrada.PERFIL_DA_INTENSIDADE == "NAO"

        N_Sensores = size(Sensores,1)
        Intesity_of_ALL_Sensors = zeros(3,N_Sensores)

        for i in 1:N_Sensores

            θ = get_degree_azimut(Sensores[i, : ])
            ϕ = get_degree_azimut([Sensores[i,3],norm(Sensores[i,1:2])])
            Intesity_of_ALL_Sensors[1,i] = get_Intensity_in_point_R(Entrada,Sensores[i, : ],geometria)
            Intesity_of_ALL_Sensors[2,i] = θ
            Intesity_of_ALL_Sensors[3,i] = ϕ
        end

    elseif Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        N_Sensores = round(Int64,sqrt(size(Sensores,1)))
        Intesity_of_ALL_Sensors = zeros(3,N_Sensores^2)

        for i in 1:(N_Sensores^2)

            θ = get_degree_azimut(Sensores[i, : ])
            ϕ = get_degree_azimut([Sensores[i,3],norm(Sensores[i,1:2])])

            Intesity_of_ALL_Sensors[1,i] = get_Intensity_in_point_R(Entrada,Sensores[i, : ],geometria)
            Intesity_of_ALL_Sensors[2,i] = θ
            Intesity_of_ALL_Sensors[3,i] = ϕ
            
        end
    end

    return Intesity_of_ALL_Sensors
end




function get_Intensity_in_point_R(Entrada::Any,Position_Sensor::Vector,geometria::TwoD)

    Tipo_de_Kernel = Entrada.Tipo_de_Kernel

    if Tipo_de_Kernel == "Escalar"

        Sensores = Entrada.Sensores
        βₙ = Entrada.βₙ
        k = Entrada.k
        Γ₀ = Entrada.Γ₀
        Ω = Entrada.Ω 
        r = Entrada.r
        vetor_de_onda = Entrada.vetor_de_onda
        ω₀ = Entrada.ω₀
        Tipo_de_Onda = Entrada.Tipo_de_Onda
        Δ = Entrada.Δ
        
        N = size(r,1)    

        if Tipo_de_Onda == "Laser Gaussiano"    

            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                Position_Sensor,
                Ω,
                ω₀,
                k,
                Δ
            )
            campo_laser = get_eletric_field_LG_real(entrada_campo,geometria)
        
        elseif Tipo_de_Onda == "Onda Plana"
            

            campo_laser = Entrada.Ω*cis(k*dot(Entrada.vetor_de_onda,(Position_Sensor[1],Position_Sensor[2])))


        elseif Entrada.Tipo_de_Onda == "FONTE PONTUAL"

            λ = (2*π)/Entrada.k
            if Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

                campo_laser = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Entrada.Radius/2)- Position_Sensor[1], - Position_Sensor[2])))

            elseif Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                campo_laser = Ω*SpecialFunctions.besselh(0,1,k*norm(((-Entrada.L/2)- Position_Sensor[1], - Position_Sensor[2])))
                
            end

        end

        fator_soma = 0
        R_Sensor = norm(Position_Sensor)
        θ_Sensor = get_degree_azimut(Position_Sensor)
        versor_Sensor = (cosd(θ_Sensor),sind(θ_Sensor))

        for i in 1:N
            # fator_soma += (cis(-k*dot(versor_Sensor,r[i, : ])))*βₙ[i]
            # fator_soma += (SpecialFunctions.besselh(0,k*norm(Position_Sensor .- r[i,:]))  + 1/(k*π*k*norm(Position_Sensor .- r[i,:])))*βₙ[i] 
            # fator_soma += (SpecialFunctions.besselh(0,k*norm(Position_Sensor .- r[i,:]))  + 1im/(k*π*k*norm(Position_Sensor .- r[i,:])))*βₙ[i] 
            fator_soma += (SpecialFunctions.besselh(0,k*norm(Position_Sensor .- r[i,:])))*βₙ[i] 
            # fator_soma += (sqrt(2/(π*k*norm(Position_Sensor .- r[i,:])))*cis((k*norm(Position_Sensor .- r[i,:]))-(π/4)))*βₙ[i]
        end

        # Eletric_Field = campo_laser - (((cis(k*R_Sensor))*(1im*(1-1im)*(sqrt(1/π))*Γ₀*fator_soma))/sqrt(k*R_Sensor))
        # Eletric_Field = (campo_laser - (cis(k*R_Sensor))*(1im*(1-1im)*Γ₀*fator_soma)/sqrt(k*R_Sensor))
        Eletric_Field = (campo_laser - 1im*Γ₀*fator_soma)


        Intesity = (abs(Eletric_Field)^2)
    
    elseif Tipo_de_Kernel == "Vetorial"

        Sensores = Entrada.Sensores
        βₙ = Entrada.βₙ
        k = Entrada.k
        Γ₀ = Entrada.Γ₀
        Γ₁ = Γ₀/2
        Ω = Entrada.Ω 
        r = Entrada.r
        vetor_de_onda = Entrada.vetor_de_onda
        ω₀ = Entrada.ω₀
        Tipo_de_Onda = Entrada.Tipo_de_Onda
        Δ = Entrada.Δ
        
        N = size(r,1)    
        βₙ⁽⁺⁾ = βₙ[1:N] 
        βₙ⁽⁻⁾ = βₙ[(N+1):end]

        entrada_campo = get_eletric_field_LG_real_ENTRADA(Position_Sensor,Ω,ω₀,k,Δ)
        campo_laser = get_eletric_field_LG_real(entrada_campo,geometria)
                    
        Eₗ⁽⁺⁾ = -(campo_laser*sqrt(2))/2im
        Eₗ⁽⁻⁾ = -(campo_laser*sqrt(2))/2im

        R_Sensor = norm(Position_Sensor)
        θ_Sensor = get_degree_azimut(Position_Sensor)
        versor_Sensor = (cosd(θ_Sensor),sind(θ_Sensor))
        ϕⱼ = get_ϕj(r,Position_Sensor)

        fator_soma⁽⁺⁾ = 0
        for i in 1:N
            # fator_soma⁽⁺⁾ += (cis(-k*dot(versor_Sensor,r[i, : ])))*(βₙ⁽⁺⁾[i] + cis(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i])
            # fator_soma⁽⁺⁾ += (2im*log(k*norm(Position_Sensor .- r[i,:]))/π)*βₙ⁽⁺⁾[i] + (4im/(π*(k*norm(Position_Sensor .- r[i,:])^2)))*cis(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i]        
            # fator_soma⁽⁺⁾ += SpecialFunctions.hankelh1(0,k*norm(Position_Sensor .- r[i,:]))*βₙ⁽⁺⁾[i] - SpecialFunctions.hankelh1(2,k*norm(Position_Sensor .- r[i,:]))*cis(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i]  
            fator_soma⁽⁺⁾ += sqrt(2/(π*k*norm(Position_Sensor .- r[i,:])))*cis((k*norm(Position_Sensor .- r[i,:]))-(π/4))*βₙ⁽⁺⁾[i] - sqrt(2/(π*k*norm(Position_Sensor .- r[i,:])))*cis((k*norm(Position_Sensor .- r[i,:]))-(5*π/4))*cis(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i]        
        end
        
        fator_soma⁽⁻⁾ = 0
        for i in 1:N
            # fator_soma⁽⁻⁾ += (cis(-k*dot(versor_Sensor,r[i, : ])))*( βₙ⁽⁻⁾[i] + cis(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])        
            # fator_soma⁽⁻⁾ += (2im*log(k*norm(Position_Sensor .- r[i,:]))/π)*βₙ⁽⁻⁾[i] + (4im/(π*(k*norm(Position_Sensor .- r[i,:])^2)))*cis(2*ϕⱼ[i])*βₙ⁽⁺⁾[i]      
            # fator_soma⁽⁻⁾ += SpecialFunctions.hankelh1(0,k*norm(Position_Sensor .- r[i,:]))*βₙ⁽⁻⁾[i] - SpecialFunctions.hankelh1(2,k*norm(Position_Sensor .- r[i,:]))*cis(2*ϕⱼ[i])*βₙ⁽⁺⁾[i]      
            fator_soma⁽⁻⁾ += sqrt(2/(π*k*norm(Position_Sensor .- r[i,:])))*cis((k*norm(Position_Sensor .- r[i,:]))-(π/4))*βₙ⁽⁻⁾[i] - sqrt(2/(π*k*norm(Position_Sensor .- r[i,:])))*cis((k*norm(Position_Sensor .- r[i,:]))-(5*π/4))*cis(2*ϕⱼ[i])*βₙ⁽⁺⁾[i]    
        end


        # E⁽⁺⁾ = Eₗ⁽⁺⁾ - ((1im+1)*(sqrt(2/π))*Γ₁*((cis(k*R_Sensor))/sqrt(k*R_Sensor)))*fator_soma⁽⁺⁾
        E⁽⁺⁾ = Eₗ⁽⁺⁾ - 1im*(Entrada.Γ₀/2)*fator_soma⁽⁺⁾
        # E⁽⁺⁾ = (-1/sqrt(2)).*(E⁽⁺⁾,1im*E⁽⁺⁾)

        # E⁽⁻⁾ = Eₗ⁽⁻⁾ - ((1im+1)*(sqrt(2/π))*Γ₁*((cis(k*R_Sensor))/sqrt(k*R_Sensor)))*fator_soma⁽⁻⁾
        E⁽⁻⁾ = Eₗ⁽⁻⁾ - 1im*(Entrada.Γ₀/2)*fator_soma⁽⁻⁾
        # E⁽⁻⁾ = (1/sqrt(2)).*(E⁽⁻⁾,-1im*E⁽⁻⁾)

        # E⁽⁰⁾ = E⁽⁺⁾ .+ E⁽⁻⁾
        # Intesity = norm(E⁽⁰⁾)^2

        Intesity = ((abs(E⁽⁺⁾))^2 + (abs(E⁽⁻⁾))^2)

        # E⁽⁰⁾ = Eₗ⁽⁰⁾ - (((cis(k*R_Sensor))*(1im*(1-1im)*(sqrt(1/π))*Γ₀*fator_soma))/sqrt(k*R_Sensor))

        # versor_de_poyting = get_vector_of_poyting(Γ₁,Position_Sensor,r,βₙ,Ω,ω₀,k,Δ,geometria)
        # Intesity = norm(versor_de_poyting)
        # Intesity = versor_de_poyting[2]

        # versor_Sensor = (0,0,1) .+ (1/sqrt(2),1im/sqrt(2),0) .+ (1/sqrt(2),-1im/sqrt(2),0) 
        # Intesity = ((abs(E⁽⁺⁾))^2 + (abs(E⁽⁻⁾))^2 + (abs(E⁽⁰⁾))^2)*dot(versor_Sensor,(1,0,0))
    end

    return Intesity
end

function get_Intensity_in_point_R(Entrada::Any,Position_Sensor::Vector,geometria::ThreeD)

    Tipo_de_Kernel = Entrada.Tipo_de_Kernel

    if Tipo_de_Kernel == "Escalar"

        Sensores = Entrada.Sensores
        βₙ = Entrada.βₙ
        k = Entrada.k
        Γ₀ = Entrada.Γ₀
        Ω = Entrada.Ω 
        r = Entrada.r
        vetor_de_onda = Entrada.vetor_de_onda
        ω₀ = Entrada.ω₀
        Tipo_de_Onda = Entrada.Tipo_de_Onda
        Δ = Entrada.Δ

        N = size(r,1)    

 
        # entrada_campo = get_eletric_field_LG_real_ENTRADA(
        #     Position_Sensor,
        #     Ω,
        #     ω₀,
        #     k,
        #     Δ
        # )
        # campo_laser = get_eletric_field_LG_real(entrada_campo,geometria)
        campo_laser = Ω*exp(1im*(dot(Entrada.vetor_de_onda,Position_Sensor)))
      
        fator_soma = 0
        R_Sensor = norm(Position_Sensor)
        θ_Sensor_azimut = get_degree_azimut(Position_Sensor[1:2])
        θ_Sensor_alt = get_degree_azimut([Position_Sensor[3],norm(Position_Sensor[1:2])])
        versor_Sensor = (sind(θ_Sensor_alt)*cosd(θ_Sensor_azimut),sind(θ_Sensor_alt)*sind(θ_Sensor_azimut),cosd(θ_Sensor_alt))

        for i in 1:N
            fator_soma += (cis(-k*dot(versor_Sensor,r[i, : ])))*βₙ[i]        
        end

        # Eletric_Field = campo_laser - (((cis(k*R_Sensor))*(1im*(1-1im)*(sqrt(1/π))*Γ₀*fator_soma))/sqrt(k*R_Sensor))
        Eletric_Field = ((1im/Γ₀)*campo_laser + ((cis(k*R_Sensor))*fator_soma)/(1im*k*R_Sensor))

        Intesity = (abs(Eletric_Field)^2)
    
    elseif Tipo_de_Kernel == "Vetorial"

        Sensores = Entrada.Sensores
        βₙ = Entrada.βₙ
        k = Entrada.k
        Γ₀ = Entrada.Γ₀
        Γ₁ = Entrada.Γ₀/2
        Ω = Entrada.Ω 
        r = Entrada.r
        vetor_de_onda = Entrada.vetor_de_onda
        ω₀ = Entrada.ω₀
        Tipo_de_Onda = Entrada.Tipo_de_Onda
        
        N = size(r,1)    
        βₙ⁽⁺⁾ = βₙ[1:N] 
        βₙ⁽⁻⁾ = βₙ[(N+1):end]

        entrada_campo = get_eletric_field_LG_real_ENTRADA(
            Position_Sensor,
            Ω,
            ω₀,
            k,
            Δ
        )
        campo_laser = get_eletric_field_LG_real(entrada_campo,geometria)
                    

        Eₗ⁽⁺⁾ = -(campo_laser*sqrt(2))/2im
        Eₗ⁽⁻⁾ = -(campo_laser*sqrt(2))/2im

        R_Sensor = norm(Position_Sensor)
        θ_Sensor = get_degree_azimut(Position_Sensor)
        versor_Sensor = (cosd(θ_Sensor),sind(θ_Sensor))
        ϕⱼ = get_ϕj(r,Position_Sensor)

        fator_soma⁽⁺⁾ = 0
        for i in 1:N
            fator_soma⁽⁺⁾ += (cis(-k*dot(r[i, : ],versor_Sensor)))*(βₙ⁽⁺⁾[i] + cis(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i])        
        end
        
        fator_soma⁽⁻⁾ = 0
        for i in 1:N
            fator_soma⁽⁻⁾ += (cis(-k*dot(versor_Sensor,r[i, : ])))*( βₙ⁽⁻⁾[i] + cis(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])        
        end


        E⁽⁺⁾ = Eₗ⁽⁺⁾ - (1im*(1-1im)*(sqrt(1/π))*Γ₁*((cis(k*R_Sensor))/sqrt(k*R_Sensor)))*fator_soma⁽⁺⁾
        E⁽⁻⁾ = Eₗ⁽⁻⁾ - (1im*(1-1im)*(sqrt(1/π))*Γ₁*((cis(k*R_Sensor))/sqrt(k*R_Sensor)))*fator_soma⁽⁻⁾
        
        Intesity = (abs(E⁽⁺⁾))^2 + (abs(E⁽⁻⁾))^2

    end

    return Intesity
end


function get_degree_azimut(Posição_Sensor)
    x = Posição_Sensor[1]
    y = Posição_Sensor[2]

    if x >= 0 && y >= 0
        θ = 90 - atand(x/y)
    elseif x <= 0  && y >= 0
        θ = 90 - atand(x/y)
    elseif x <= 0 && y <= 0
        θ = 270 - atand(x/y)
    elseif x >= 0 && y <= 0
        θ = 270 - atand(x/y)
    end

    return θ
end 


function get_ϕj(r::Array,Position_Sensor::Array)

    N = size(r,1)
    ϕₛ = zeros(N)

    for i in 1:N
        ϕₛ[i] = atan((r[i,2] - Position_Sensor[2]),(r[i,1] - Position_Sensor[1]))
    end
    return ϕₛ
end
    

# function get_all_degree_azimut(Todos_os_Sensores)
    
#     N_sensores = size(Todos_os_Sensores,1)

#     angulos_dos_Sensores = zeros(N_sensores)

#     for i in 1:N_sensores 
    
#         Posição_Sensor = Todos_os_Sensores[i, : ]
#         x = Posição_Sensor[1]
#         y = Posição_Sensor[2]

#         if x >= 0 && y >= 0
#             θ = 90 - atand(x/y)
#         elseif x <= 0  && y >= 0
#             θ = 90 - atand(x/y)
#         elseif x <= 0 && y <= 0
#             θ = 270 - atand(x/y)
#         elseif x >= 0 && y <= 0
#             θ = 270 - atand(x/y)
#         end
        
#         angulos_dos_Sensores[i] = θ

#     end

#     return angulos_dos_Sensores
# end 

# θ







function get_Distance_Sensor_to_atom(A::Array, b::Array)


    n_rows = size(A, 1)                                                                                                       
    distanceAb = zeros(n_rows)                                                                                                 
    @inbounds for i = 1:n_rows                                                                                            

        # Aqui a matriz distaceAb vai guardar a distancia de todos os atomos com relação ao novo atomo
        ## Neste ponto é usado o pacote Distances 
        distanceAb[i] = Distances.evaluate(Euclidean(), A[i, :], b) 
    

    end
    return distanceAb
end


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------- Expressão Campo eletrico Laser Gaussiano --------------------------------------------------------------------------------#  
###############################################################################################################################################################################################################

function get_eletric_field_LG_modulo_quadrado(r,E₀,ω₀,k,geometria::TwoD)

    PX = r[1]
    PY = r[2]
    α = (2*PX)/(k*(ω₀^2)) 
    Campo = (E₀*(exp(1im*k*PX - ((PY^2)/((ω₀^2)*(1 + 1im*α))))))/(sqrt(1 + 1im*α))
    
    Campo_perceptivel = abs(Campo^2)

    return Campo_perceptivel
end

function get_eletric_field_LG_modulo_quadrado(r,E₀,ω₀,k,Δ,geometria::ThreeD)

    PX = r[1]
    PY = r[2]
    PZ = r[3]

    s = 10^-5
    E0 = sqrt( s*(1 + 4*Δ^2)/2 )
    denominator_factor = 1 .+ 2im.*PZ/(k*ω₀^2)
    Eᵢ = E0 .*exp.(+1im*k *PZ)
    Eᵢ = Eᵢ .*exp.( -( PX.^2 + PY.^2 )./( denominator_factor.* ω₀^2 ) )
    Eᵢ = Eᵢ ./denominator_factor
    
    Campo_perceptivel = abs(Eᵢ^2)

    return Campo_perceptivel
end


function get_eletric_field_LG_real(Entrada::get_eletric_field_LG_real_ENTRADA,geometria::TwoD)

    PX = Entrada.r[1]
    PY = Entrada.r[2]
    E₀ = Entrada.E₀
    ω₀ = Entrada.ω₀
    k = Entrada.k

    α = ((2*PX)/(k*(ω₀^2))) 
    Campo = (E₀*(exp(1im*k*PX - ((PY^2)/((ω₀^2)*(1 + 1im*α))))))/(sqrt(1 + 1im*α))
    
    return Campo
end

function get_eletric_field_LG_real(Entrada::get_eletric_field_LG_real_ENTRADA,geometria::ThreeD)

    PX = Entrada.r[1]
    PY = Entrada.r[2]
    PZ = Entrada.r[3]
    ω₀ = Entrada.ω₀
    k = Entrada.k
    Δ = Entrada.Δ

    s = 10^-5
    E0 = sqrt( s*(1 + 4*Δ^2)/2 )
    denominator_factor = 1 .+ 2im.*PZ/(k*ω₀^2)
    Eᵢ = E0 .*exp.(+1im*k *PZ)
    Eᵢ = Eᵢ .*exp.( -( PX.^2 + PY.^2 )./( denominator_factor.* ω₀^2 ) )
    Eᵢ = Eᵢ ./denominator_factor
    
    return Eᵢ
end


function rotacione_Sensores(Sensores,angulo_de_rotação)
    
    N_Sensores = size(Sensores,1)
    angulo = zeros(N_Sensores)
    ρ = zeros(N_Sensores)

    for i in 1:N_Sensores 
        angulo[i] = get_degree_azimut(Sensores[i, : ]) + angulo_de_rotação
    end

    for i in 1:N_Sensores
        ρ[i] = norm(Sensores[i, : ])
    end


    Novas_Posições = zeros(N_Sensores,2)

    for i in 1: N_Sensores
        Novas_Posições[i,1] = ρ[i]*cosd(angulo[i])
        Novas_Posições[i,2] = ρ[i]*sind(angulo[i])
    end

    return Novas_Posições
end 

function get_number_thouless(γₙ,Δₙ,Δ,Γ₀)
    ω_min = Δ - Γ₀/4
    ω_max = Δ + Γ₀/4
    index_modes_inside_range = findall(  (Δₙ .≥ ω_min).*(Δₙ .≤ ω_max).*(γₙ .≥ 0))

    diferença_media_in_range = mean(diff(sort(Δₙ[index_modes_inside_range])))
    γₙ_medio_in_range = sum(γₙ[index_modes_inside_range])/length(index_modes_inside_range)

    number_thouless = γₙ_medio_in_range/diferença_media_in_range
    
    return number_thouless    
end


function get_Σ2(γₙ,Δₙ,Δ,Γ₀,N_Realizações)
    
    ω_min = Δ - Γ₀/4
    ω_max = Δ + Γ₀/4

    N_modos = round(Int64,size(Δₙ,1)/N_Realizações)
    N,N² = zeros(N_Realizações),zeros(N_Realizações)

    for i in 1:N_Realizações
        N[i] = length(findall((Δₙ[((i-1)*N_modos+1):i*N_modos] .≥ ω_min).*(Δₙ[((i-1)*N_modos+1):i*N_modos] .≤ ω_max)))
        N²[i] = N[i]^2 
    end

    Σ² = mean(N²) - mean(N)^2

    return Σ²    
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Analise para obter a cintura ao longo das Distancia_do_sistema --------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function get_waist_in_screens(Entrada::get_waist_in_screens_ENTRADA)

    σ² = zeros(Entrada.N_Telas,4)
    N_Sensores = round(Int64,size(Entrada.Posição_Sensores,1)/Entrada.N_Telas)

    aux_1 = 1
    for i in 1:Entrada.N_Telas 
        σ²[i,1:2] = get_one_σ(Entrada.Posição_Sensores[aux_1:(N_Sensores-1 + aux_1),1:2],Entrada.Intensidade_Normalizada[1,aux_1:(N_Sensores-1 + aux_1)],Entrada.Geometria)
        aux_1 += N_Sensores
    end

    aux_1 = 1
    for i in 1:Entrada.N_Telas 
        σ²[i,3:4] = get_one_σ(Entrada.Posição_Sensores[aux_1:(N_Sensores-1 + aux_1),3:4],Entrada.Intensidade_Normalizada[3,aux_1:(N_Sensores-1 + aux_1)],Entrada.Geometria)
        aux_1 += N_Sensores
    end

    return σ²
end
model_gaussiana_log(x,p) =  -((( x .- p[2]).^2)./(2*(p[3]^2))) .+ p[1]
model_gaussiana(x,p) = p[1].*exp.((-( x .- p[2]).^2)/(2*(p[3]^2)))  

model_lorenziana_log(x,p) =  p[1] .+ p[2].*log.(1 ./ (x.^2 .+ p[3].^2)) 
model_lorenziana(x,p) = p[1].*(1 ./(x.^2 .+ p[3]^2)).^p[2] 

function get_one_σ_lorentz(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)
    
    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))

    percentual = 1
    xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
    ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


    p0 = [maximum(log.(ydata_loess_fit)),1,(maximum(xdata_loess_fit)-minimum(xdata_loess_fit))/10]

    teste = curve_fit(
        model_lorenziana_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end


function get_one_σ_gaussian(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)
    
    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))

    percentual = 0.55
    xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
    ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


    p0 = [maximum(log.(ydata_loess_fit)),1,(maximum(xdata_loess_fit)-minimum(xdata_loess_fit))/10]

    teste = curve_fit(
        model_gaussiana_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end


function get_one_σ_FWHM(Posição_Sensores,Intensidade,Geometria::TwoD)
    
    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)

    model = loess(xs, ys, span=0.5)

    us = range(extrema(xs)...; step = 0.1)[2:end]
    vs = exp10.(Loess.predict(model, us))

    # meia_altura = 10^(middle(log10.(vs)))
    meia_altura = maximum(vs)/2
    B = similar(vs)
    B .= log10(meia_altura)
    A = diff([log10.(vs), B])[1]
    FWHM = abs(diff([us[sortperm(abs.(A))[1]],us[sortperm(abs.(A))[2]]])[1])
    
    return FWHM,FWHM^2
end


# function get_one_σ(Sensores,Intensidade,Geometria::TwoD)
#     σ = zeros(2)

#     σ[1] = Sensores[1,1]
#     σ[2] = sum((Sensores[ : ,2].^2).*Intensidade[ : ])/(sum(Intensidade[ : ]))

#     return σ
# end

# function get_one_σ(Sensores,Intensidade,Geometria::ThreeD)
    
#     σ = zeros(2)

#     σ[1] = Sensores[1,1]
#     σ[2] = sum((norm.(Sensores[ : ,1:2]).^2).*Intensidade[ : ])/(sum(Intensidade[ : ]))

#     return σ
# end
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Analise para obter a Transmissão -------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Intensity_to_Transmission_in_sensors(Entrada::get_Intensity_to_Transmission_in_sensors_ENTRADA,Geometria::TwoD)

    Sensores = Entrada.Sensores

    N_Sensores = size(Sensores,1)
    Intesity_of_ALL_Sensors = zeros(2,N_Sensores)

    for i in 1:N_Sensores

        θ = get_degree_azimut(Sensores[i, : ])

        if θ >= 90 && θ <= 270

            Intesity_of_ALL_Sensors[1,i] = get_eletric_field_LG_modulo_quadrado(Sensores[i,:],Entrada.Ω,Entrada.ω₀,Entrada.k,Entrada.Geometria)
            Intesity_of_ALL_Sensors[2,i] = θ            
        
        else

            Intesity_of_ALL_Sensors[1,i] = get_Intensity_in_point_R(Entrada,Sensores[i, : ],Entrada.Geometria)
            Intesity_of_ALL_Sensors[2,i] = θ

        end
    end


    return Intesity_of_ALL_Sensors    
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Intensity_to_Transmission_in_sensors(Entrada::get_Intensity_to_Transmission_in_sensors_ENTRADA,Geometria::ThreeD)

    Sensores = Entrada.Sensores

    N_Sensores = size(Sensores,1)
    Intesity_of_ALL_Sensors = zeros(4,N_Sensores)

    for i in 1:N_Sensores

        ϕ = get_degree_azimut([Sensores[i,3],norm(Sensores[i,1:2])])
        θ = get_degree_azimut(Sensores[i,1:2])

        I_incidente = get_Intensity_in_point_R(Entrada,Sensores[i, : ],Geometria)

        if I_incidente ≠ NaN
            Intesity_of_ALL_Sensors[1,i] = I_incidente
        end

        Intesity_of_ALL_Sensors[2,i] = get_eletric_field_LG_modulo_quadrado(Sensores[i, : ],Entrada.Ω,Entrada.ω₀,Entrada.k,Entrada.Δ,Geometria)
        Intesity_of_ALL_Sensors[3,i] = ϕ 
        Intesity_of_ALL_Sensors[4,i] = θ           

    end


    return Intesity_of_ALL_Sensors    
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------Complemento--------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function get_points_in_log_scale(Valor_Inicial,Valor_Final,N_div)
    
    a = log10(Valor_Inicial)
    b = log10(Valor_Final)

    sep = abs(b-a) / (N_div-1)

    aux_1 = 1
    pontos = zeros(N_div)

    for i in 0:N_div-1
        pontos[aux_1] = 10.0^(a + sep*i)
        aux_1 += 1
    end

    return pontos
end

function find_ws(Range_L,Lₐ::Number,goemetria::TwoD)

    ωᵦ = Lₐ/10
    N_pontos = size(Range_L,1)
    ω₀s = zeros(N_pontos)

    for i in 1:N_pontos

        find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(Range_L[i]/2)^2

        ω₀s[i] = find_zeros(find_raiz,26,30)[1]

        # ω₀s[i] = sqrt((-(ωᵦ^2) + sqrt((ωᵦ^2)^2 + 4*4*(Range_L[i]/2)^2))/(-2))

        # A = find_zeros(find_raiz,26,30)
        # println(A)
    end

    return ω₀s
end

function find_ws(Range_L,Lₐ::Number,goemetria::ThreeD)

    ωᵦ = Lₐ/2
    N_pontos = size(Range_L,1)
    ω₀s = zeros(N_pontos)

    for i in 1:N_pontos

        # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(Range_L[i]/2)^2

        # ω₀s[i] = find_zeros(find_raiz,3,7)[1]
        ω₀s[i] = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(Range_L[i])^2))/2)

        # A = find_zeros(find_raiz,26,30)
        # println(A)
    end

    return ω₀s
end


function get_fitting_to_cintura(Range_L,σ²_L²,Tipo_de_kernel)

    if Tipo_de_kernel == "Escalar"

        x = Range_L
        y = σ²_L²

        model_1(x, p) = p[1].*x .+ p[2] 
        xdata = x
        ydata = y
        p0 = [0.01,0]

        fit = curve_fit(model_1, xdata, ydata, p0)

        a_sol = fit.param[1]                                                                                                                                                  # Coefiente angular da melhor reta 
        b_sol = fit.param[2]                                                                                                                                                  # Coefiente linear da melhor reta
        # c_sol = fit.param[3]

        a_sol_plot = round(a_sol,digits = 2)
        b_sol_plot = round(Int64,b_sol)
        # b_sol_plot = round(b_sol,digits = 2)

        # c_sol_plot = round(Int64,c_sol)

    elseif Tipo_de_kernel == "Vetorial"

        x = Range_L
        y = σ²_L²

        model_2(x, p) = p[1].*(x.^2) .+ p[2] 
        xdata = x
        ydata = y
        p0 = [0.01,0]

        fit = curve_fit(model_2, xdata, ydata, p0)

        a_sol = fit.param[1]                                                                                                                                                  # Coefiente angular da melhor reta 
        b_sol = fit.param[2]                                                                                                                                                  # Coefiente linear da melhor reta
        # c_sol = fit.param[3]

        a_sol_plot = round(a_sol,digits = 2)
        b_sol_plot = round(Int64,b_sol)
        # b_sol_plot = round(b_sol,digits = 2)

        # c_sol_plot = round(Int64,c_sol)

    end 

    return a_sol_plot,b_sol_plot
end




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------Vetor de Poyting---------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_vector_of_poyting(Γ₁,Position_Sensor,r,βₙ,Ω,ω₀,k,Δ,geometria)
    
    N = size(r,1)
    βₙ⁽⁺⁾ = βₙ[1:N] 
    βₙ⁽⁻⁾ = βₙ[(N+1):end]
    
    R_Sensor = norm(Position_Sensor)
    θ_Sensor = get_degree_azimut(Position_Sensor)
    versor_Sensor = (cosd(θ_Sensor),sind(θ_Sensor))
    ϕⱼ = get_ϕj(r,Position_Sensor)


    ∂Eⱼ = 0
    for i in 1:N
        ∂Eⱼ += derivada_Hankel_0(k*norm(Position_Sensor .- r[i,:]))*(βₙ⁽⁻⁾[i] + βₙ⁽⁺⁾[i])*((Position_Sensor[1] .- r[i,1])/norm(Position_Sensor .- r[i,:]))
        ∂Eⱼ += -derivada_Hankel_2(k*norm(Position_Sensor .- r[i,:]))*(exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i] + exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i])*((Position_Sensor[1] .- r[i,1])/norm(Position_Sensor .- r[i,:]))
        ∂Eⱼ += -SpecialFunctions.besselh(2,k*norm(Position_Sensor .- r[i,:]))*(exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i] - exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])*(2/(1+ (norm(Position_Sensor[2] .- r[i,2]) /norm(Position_Sensor[1] .- r[i,1]))^2))*(norm(Position_Sensor[2] .- r[i,2]) /norm(Position_Sensor[1] .- r[i,1])^2)
    end
    ∂Eⱼ = -(Γ₁/sqrt(2))*∂Eⱼ
    ∂Eⱼ += derivada_laser_gaussiano(Position_Sensor,Ω,ω₀,k)

    ∂Eₓ = 0
    for i in 1:N
        ∂Eₓ += derivada_Hankel_0(k*norm(Position_Sensor .- r[i,:]))*(βₙ⁽⁺⁾[i] - βₙ⁽⁻⁾[i])*((Position_Sensor[2] .- r[i,2])/norm(Position_Sensor .- r[i,:]))
        ∂Eₓ += -derivada_Hankel_2(k*norm(Position_Sensor .- r[i,:]))*(exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i] - exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])*(norm(Position_Sensor[2] .- r[i,2])/norm(Position_Sensor .- r[i,:]))
        ∂Eₓ += -SpecialFunctions.besselh(2,k*norm(Position_Sensor .- r[i,:]))*(exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i] - exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])*(2/(1+ (norm(Position_Sensor[2] .- r[i,2]) /norm(Position_Sensor[1] .- r[i,1]))^2))*( 1 /norm(Position_Sensor[1] .- r[i,1]))
    end
    ∂Eₓ = (1im*Γ₁/sqrt(2))*∂Eₓ

    Eₓ = 0
    for i in 1:N
        Eₓ += SpecialFunctions.besselh(0,k*norm(Position_Sensor .- r[i,:]))*(βₙ⁽⁺⁾[i] - βₙ⁽⁻⁾[i]) - SpecialFunctions.besselh(2,k*norm(Position_Sensor .- r[i,:]))*(exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i] - exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i])
    end
    Eₓ = (1im*Γ₁/sqrt(2))*Eₓ

    Eⱼ = 0
    for i in 1:N
        Eⱼ += SpecialFunctions.besselh(0,k*norm(Position_Sensor .- r[i,:]))*(βₙ⁽⁺⁾[i] + βₙ⁽⁻⁾[i]) - SpecialFunctions.besselh(2,k*norm(Position_Sensor .- r[i,:]))*(exp(2*ϕⱼ[i])*βₙ⁽⁺⁾[i] + exp(-2*ϕⱼ[i])*βₙ⁽⁻⁾[i])
    end
    Eⱼ = -(Γ₁/sqrt(2))*Eⱼ
    entrada_campo = get_eletric_field_LG_real_ENTRADA(Position_Sensor,Ω,ω₀,k,Δ)
    campo_laser = get_eletric_field_LG_real(entrada_campo,geometria)
    Eⱼ = campo_laser + Eⱼ
    
    Sₓ = -1im*Eⱼ*(∂Eⱼ - ∂Eₓ)
    Sⱼ = 1im*Eₓ*(∂Eⱼ - ∂Eₓ)
    vetor_de_poyting = (real(Sₓ),real(Sⱼ))
    
    return vetor_de_poyting
end

derivada_Hankel_0(x) =  -SpecialFunctions.besselj(1,x) - 1im*SpecialFunctions.bessely(1,x) 
derivada_Hankel_2(x) = (SpecialFunctions.besselj(1,x) - SpecialFunctions.besselj(2,x))/2  + 1im*(SpecialFunctions.bessely(1,x) - SpecialFunctions.bessely(2,x))/2

function derivada_laser_gaussiano(Position_Sensor,Ω,ω₀,k)
    PX = Position_Sensor[1]
    PY = Position_Sensor[2]

    α = ((2*PX)/(k*(ω₀^2))) 
    return ((Ω*(exp(1im*k*PX - ((PY^2)/((ω₀^2)*(1 + 1im*α))))))/(sqrt(1 + 1im*α)))*(1im*k - 2*(PY^2)/(ω₀^6 * k*(1+α))) - ((2*Ω*(exp(1im*k*PX - ((PY^2)/((ω₀^2)*(1 + 1im*α))))))/(k * ω₀^2 *(1 + 1im*α)^(3/2)))
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------- Testes ---------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




# Position = zeros(10000,2)

# contador_1 = 1
# contador_2 = 1
# contador_3 = 1

# while true

#     Position[contador_3,1] = -10 + 0.2*contador_1
#     Position[contador_3,2] = -10 + 0.2*contador_2

#     if contador_1 == 100
#         contador_1 = 1
#         contador_2 = contador_2 + 1
#     else
#         contador_1 = contador_1 + 1
#     end  

#     if contador_3 == 10000
#         break
#     else 
#         contador_3 = contador_3 + 1
#     end

# end

# z = zeros(10000)

# contador = 1
# E₀ = 0.001
# ω₀ = 2.5
# k = 1

# while true

#     z[contador] = get_eletric_field_LG(Position[contador,1],Position[contador,2],E₀,ω₀,k)

#     if contador == 10000
#         break
#     else
#         contador = contador + 1
#     end
# end

# x_a = Position[1:end,1]
# y_b = Position[1:end,2]
# z_c = z
# tamanho = 700


# theme(:vibrant)
# scatter(x_a,y_b,size = (tamanho+300, tamanho),gridalpha=0,ms=4,color=palette(:turbo),zcolor=z_c,framestyle=:box,label="",colorbar_title="Eletric Field",title = "Perfil do Campo eletrico")
# xlabel!("Eixo X")
# ylabel!("Eixo Y")
# savefig("Noel_3")


# A = [1/sqrt(2) 1/sqrt(2) ; 2 2; 3 3;4 4]
# b = [0 0] 

# get_Distance_Sensor_to_atom(A,b)