###############################################################################################################################################################################################################
##################################################################################### Extração de dados de uma simulaçao ######################################################################################
###############################################################################################################################################################################################################


function E6_extração_de_dados_Dinamica_dos_Atomos_Intensidade(Variaveis_de_entrada::E6_ENTRADA)


    ###############################################################################################################################################################################################################
    #------------------------------------------------------------------------------------ Ativação dos programas em conjunto -------------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    entrada_beta = get_all_beta_to_system_ENTRADA(
        Variaveis_de_entrada.cloud.G,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.cloud.r,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_beta,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L
    )

    # Guardo a dinâmica dos atomos em uma matriz    
    βₙ = get_all_beta_to_system(entrada_beta,Variaveis_de_entrada.Geometria)                           

    entrada_Intensidade = get_Intensity_to_sensors_ENTRADA(
        Variaveis_de_entrada.Posição_Sensores,
        βₙ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.cloud.r,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Δ  
    )
    # Guardo a intensidade sentida em cada sensor 
    All_Intensitys = get_Intensity_to_sensors(entrada_Intensidade,Variaveis_de_entrada.Geometria)

    ###############################################################################################################################################################################################################
    #--------------------------------------------------------------------------------- Dados Gerados para as condições de entrada --------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    return All_Intensitys,βₙ
end
