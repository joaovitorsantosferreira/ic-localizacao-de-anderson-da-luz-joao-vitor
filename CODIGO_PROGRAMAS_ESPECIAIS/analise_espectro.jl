X = 10
Y = 10
N = 2000

Λₙ = rand(N)*X .- X/2 
γₙ = rand(N)

Λₙ = ωₙ
γₙ = log10.(abs.(γₙ))
n_div = 40
Radius_plot = round(Radius,digits=2)
gr()


histogram2d(
    Λₙ,
    γₙ, 
    nbins=n_div, 
    normalize=true,
    color = cgrad(categorical = true),
    size = (1150, 1000),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    colorbar_title = L"\textrm{density}",
    title = L"$N= %$N, \rho/k^2 = %$ρ,kR = %$Radius_plot, %$Tipo_de_kernel $",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
)

# Λₙ = imag.(λₙ)
# γₙ = real.(λₙ)
# scatter(Λₙ,γₙ)

n_div = 100

range_x = range(minimum(Λₙ),maximum(Λₙ);length = n_div)
range_y = range(minimum(γₙ),maximum(γₙ);length = n_div)
densidade = zeros(n_div^2,3)
cont = 1
N_total = size(Λₙ,1)

for i in 1:(n_div-1)
    for j in 1:(n_div-1)

        densidade[cont,1] = mean([range_x[i],range_x[i+1]])
        densidade[cont,2] = mean([range_y[j],range_y[j+1]])
        densidade[cont,3] = size(findall((Λₙ .> range_x[i]).*(Λₙ .< range_x[i+1]).*(γₙ.>range_y[j]).*(γₙ.<range_y[j+1])),1)/N_total 
        cont += 1
    end
end

valid_index = findall(densidade[:,3] .≠ 0)

scatter(
    densidade[valid_index,1],
    densidade[valid_index,2],
    zcolor=log10.(densidade[valid_index,3]),
    m=:square,
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    ms=5,
    size=(1300,1000),
    colorbar_title = L"\textrm{density}",
    # title = L"$\rho/k^2 = %$densidade,kR = %$Radius_plot, %$Tipo_de_kernel, %$N_Realizações REP $",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
)
xlabel!(L"$\Delta_n $")
ylabel!(L"$\gamma_n$")