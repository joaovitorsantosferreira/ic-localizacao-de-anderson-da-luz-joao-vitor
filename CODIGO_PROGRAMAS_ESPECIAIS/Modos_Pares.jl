using Distances
using LinearAlgebra
using LaTeXStrings
using Random
using Statistics
using Plots
using SpecialFunctions
using QuadGK
using PlotThemes
using StatsBase
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates

f1_vetorial(x,y,z) = (1/2)*(-sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) - 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
f2_vetorial(x,y,z) = (1/2)*(-sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) + 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
f3_vetorial(x,y,z) = (1/2)*(sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) - 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
f4_vetorial(x,y,z) = (1/2)*(sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) + 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)

function modos_pares_desordem(f_entrada,N_div_X,N_div_W,W)
    
    intervalo_W = range(-W/2,W/2,length=N_div_W) 
    intervalo_X = range(0.01,100,length=N_div_X) 
    Dados = Array{Complex{Float64}}(undef,N_div_W,N_div_W,N_div_X)

    for α₁ in 1:N_div_W
        for α₂ in 1:N_div_W
            Dados[α₁,α₂,:] = f_entrada.(intervalo_X,intervalo_W[α₁],intervalo_W[α₂])
        end
    end

    return Dados
end

function orgazine_dados_espectro_desordem(Dados)

    N_div_W = size(Dados,1)

    Dados_ordenado = Any[]
    for α₁ in 1:N_div_W
        for α₂ in 1:N_div_W
            append!(Dados_ordenado,Dados[α₁,α₂,:])
        end
    end

    return Dados_ordenado
end

W = 10

Dados_1 = modos_pares_desordem(f1_vetorial,100000,10,W)
Dados_2 = modos_pares_desordem(f2_vetorial,100000,10,W)
Dados_3 = modos_pares_desordem(f3_vetorial,100000,10,W)
Dados_4 = modos_pares_desordem(f4_vetorial,100000,10,W)

Dados_1 = orgazine_dados_espectro_desordem(Dados_1)
Dados_2 = orgazine_dados_espectro_desordem(Dados_2)
Dados_3 = orgazine_dados_espectro_desordem(Dados_3)
Dados_4 = orgazine_dados_espectro_desordem(Dados_4)

Dados_Totais = Any[]
append!(Dados_Totais,Dados_1)
append!(Dados_Totais,Dados_2)
append!(Dados_Totais,Dados_3)
append!(Dados_Totais,Dados_4)

parte_real = real.(Dados_Totais)
parte_imaginaria = imag.(Dados_Totais)

modos_em_ordem = sortperm(parte_real)

percent = round(Int64,size(Dados_Totais,1)*(0.1/100))

# scatter(parte_imaginaria[modos_em_ordem[(end-percent):end]],parte_real[modos_em_ordem[(end-percent):end]],xlims=(-300,300),yscale=:log10,ylims=(10^-3,10^1))
# scatter!(parte_imaginaria[modos_em_ordem[1:percent]],parte_real[modos_em_ordem[1:percent]],xlims=(-300,300),yscale=:log10,ylims=(10^-3,10^1))


espectro_inferior_imag = parte_imaginaria[modos_em_ordem[1:percent]]
espectro_inferior_real = parte_real[modos_em_ordem[1:percent]]

intervalo_energia = -300:10:300

borda_do_conjunto_x = zeros((size(intervalo_energia,1)-1))
borda_do_conjunto_y = zeros((size(intervalo_energia,1)-1))

for i in 1:(size(intervalo_energia,1)-1)

    pontos_range_x = espectro_inferior_imag[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
    pontos_range_y = espectro_inferior_real[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]

    borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[end]]
    borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[end]]
end


plot(borda_do_conjunto_x,borda_do_conjunto_y, yscale = :log10)

for i in 1:(size(intervalo_energia,1)-1)

    pontos_range_x = espectro_inferior_imag[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
    pontos_range_y = espectro_inferior_real[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]

    borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[1]]
    borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[1]]
end


plot!(borda_do_conjunto_x,borda_do_conjunto_y, yscale = :log10)

espectro_superior_imag = parte_imaginaria[modos_em_ordem[(end-percent):end]]
espectro_superior_real = parte_real[modos_em_ordem[(end-percent):end]]

borda_do_conjunto_x = zeros((size(intervalo_energia,1)-1))
borda_do_conjunto_y = zeros((size(intervalo_energia,1)-1))

for i in 1:(size(intervalo_energia,1)-1)

    pontos_range_x = espectro_superior_imag[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
    pontos_range_y = espectro_superior_real[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]

    borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[end]]
    borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[end]]
end

plot!(borda_do_conjunto_x,borda_do_conjunto_y, yscale = :log10,)

for i in 1:(size(intervalo_energia,1)-1)

    pontos_range_x = espectro_superior_imag[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
    pontos_range_y = espectro_superior_real[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]

    borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[1]]
    borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[1]]
end


plot!(borda_do_conjunto_x,borda_do_conjunto_y, yscale = :log10)