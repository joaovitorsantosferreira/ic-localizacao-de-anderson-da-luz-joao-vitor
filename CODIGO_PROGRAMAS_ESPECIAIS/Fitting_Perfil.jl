using Loess
using Distances
using LinearAlgebra   
using LaTeXStrings    
using Random   
using Statistics      
using Plots    
using SpecialFunctions
using QuadGK   
using PlotThemes      
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Interpolations
using ForwardDiff
using Smoothers


include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R9.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E2.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Rotinas/[6]---Localização Transversal/R9.jl") 

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-1}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{20}_CAMPO{NEAR FIELD}.jld2", "Parametros")

Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{9}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-11}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Escalar}_Delta__{INTERPOLAÇÃO}_L{9}_CAMPO{NEAR FIELD}.jld2", "Parametros")

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C11/DATA={2022-August-4}_Dados_Antigos_CLUSTER_C11_{SLAB}_{Vetorial}_Delta__{INTERPOLAÇÃO}_L{30}_CAMPO{NEAR FIELD}.jld2", "Parametros")


σ² = Dados_Antigos[1]
Perfis_da_Intensidade = Dados_Antigos[2]
Posição_Sensores = Dados_Antigos[3]
# Perfis_da_Intensidade[ findall(Perfis_da_Intensidade .== 0) ] .= 0.000001

Γ₀ = Parametros_Antigos.Γ₀
ωₐ = Parametros_Antigos.ωₐ
GEOMETRIA_DA_NUVEM = Parametros_Antigos.GEOMETRIA_DA_NUVEM
Geometria = Parametros_Antigos.Geometria
Ns = Parametros_Antigos.Ns
b₀s = Parametros_Antigos.b₀s
kR = Parametros_Antigos.kR
Lₐ = Parametros_Antigos.Lₐ
Lₐ_titulo = round(Lₐ,digits=2)
L = Parametros_Antigos.L
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
Tipo_de_Onda = Parametros_Antigos.Tipo_de_Onda
Tipo_de_beta = Parametros_Antigos.Tipo_de_beta
k = Parametros_Antigos.k
Δ = Parametros_Antigos.Δ
Ω = Parametros_Antigos.Ω
Angulo_da_luz_incidente = Parametros_Antigos.Angulo_da_luz_incidente
vetor_de_onda = Parametros_Antigos.vetor_de_onda
λ = Parametros_Antigos.λ
ωₗ = Parametros_Antigos.ωₗ
Variavel_Constante = Parametros_Antigos.Variavel_Constante
N_Realizações = Parametros_Antigos.N_Realizações
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
W = Parametros_Antigos.W
N_Sensores = Parametros_Antigos.N_Sensores
Distancia_dos_Sensores = Parametros_Antigos.Distancia_dos_Sensores
ρ = zeros(size(Ns,1))
@.ρ = Ns/(L*Lₐ)


include("PLOTS/Plots.jl")
vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores[:,2],Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,ρ,L"$L = %$L , \Delta = 0,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)
# savefig("PROFILE_FWHM.png")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# model_gaussiana_log(x,p) =  -((( x .- p[2]).^2)./(2*(p[3]^2))) .+ p[1]
# model_gaussiana(x,p) = p[1].*exp.((-( x .- p[2]).^2)/(2*(p[3]^2)))  


# function get_one_σ(Posição_Sensores,Intensidade,Geometria::TwoD)

#     xdata = Posição_Sensores[ : ]
#     ydata = log10.(Intensidade)

#     model = Loess.loess(xdata, ydata, span=0.5)

#     xdata_loess = range(extrema(xdata)...; step = 0.1)
#     ydata_loess = exp10.(Loess.predict(model, xdata_loess))

#     percentual = 0.55
#     xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
#     ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


#     p0 = [maximum(xdata_loess_fit),0,(maximum(ydata_loess_fit)-minimum(ydata_loess_fit))/2]
#     teste = curve_fit(
#         model_gaussiana_log, 
#         xdata_loess_fit,
#         ydata_loess_fit, 
#         p0
#     )
#     σ² = teste.param[3]^2    

#     return σ²,[exp10(teste.param[1]),teste.param[2],teste.param[3]]
# end

# function get_one_σ(Posição_Sensores,Intensidade,Geometria::TwoD)

#     xdata = Posição_Sensores[ : ]
#     ydata = log10.(Intensidade)

#     model = loess(xdata, ydata, span=0.5)

#     xdata_loess = range(extrema(xdata)...; step = 0.1)
#     ydata_loess = exp10.(predict(model, xdata_loess))

#     percentual = 0.55
#     xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
#     ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


#     p0 = [maximum(xdata_loess_fit),0,(maximum(ydata_loess_fit)-minimum(ydata_loess_fit))/2]
#     teste = curve_fit(
#         model_gaussiana_log, 
#         xdata_loess_fit,
#         ydata_loess_fit, 
#         p0
#     )
#     σ² = teste.param[3]^2    

#     return σ²,[exp10(teste.param[1]),teste.param[2],teste.param[3]]
# end

function get_one_σ(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores[ : ]
    ydata = log.(Intensidade)

    p0 = [maximum(ydata),0,(maximum(xdata)-minimum(xdata))/2]
    teste = curve_fit(model_gaussiana_log, xdata, ydata, p0)
    σ² = teste.param[3]^2    

    return σ²,[exp(teste.param[1]),teste.param[2],teste.param[3]]
end


model_gaussiana_log(x,p) =  -((( x .- p[2]).^2)./(2*(p[3]^2))) .+ p[1]
model_gaussiana(x,p) = p[1].*exp.((-( x .- p[2]).^2)/(2*(p[3]^2)))  

model_lorenziana_log(x,p) =  p[1] .+ p[2].*log.(1 ./ (x.^2 .+ p[3].^2)) 
model_lorenziana(x,p) = p[1].*(1 ./(x.^2 .+ p[3]^2)).^p[2] 

model_exponencial_log(x,p) = p[1] .- p[2].*x.^p[3] 
model_exponencial(x,p) = p[1].*exp.(-p[2].*x.^p[3])


function get_one_σ_lorentz(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)
    
    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range( extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))

    index₀ = round(Int64,size(xdata_loess,1)/2) +1
    xdata_loess⁺ = xdata_loess[index₀:end]
    ydata_loess⁺ = ydata_loess[index₀:end]

    xdata_loess⁻  = xdata_loess[1:index₀]
    ydata_loess⁻  = ydata_loess[1:index₀] 

    raio_decaimento⁻ = xdata_loess⁻[findall(ydata_loess⁻ .== minimum(ydata_loess⁻))]
    raio_decaimento⁺ = xdata_loess⁺[findall(ydata_loess⁺ .== minimum(ydata_loess⁺))]

    index_decaimento = findall((xdata_loess .< raio_decaimento⁺).*(xdata_loess .> raio_decaimento⁻))

    xdata_loess_fit = xdata_loess[index_decaimento] 
    ydata_loess_fit = ydata_loess[index_decaimento]

    p0 = [maximum(log.(ydata_loess_fit)),1,(maximum(xdata_loess_fit)-minimum(xdata_loess_fit))/10]

    teste = curve_fit(
        model_lorenziana_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end


function get_one_σ_gaussian(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)
    
    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))

    percentual = 0.55
    xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
    ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


    p0 = [maximum(log.(ydata_loess_fit)),1,(maximum(xdata_loess_fit)-minimum(xdata_loess_fit))/10]

    teste = curve_fit(
        model_gaussiana_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end


function get_one_σ_exponencial(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)

    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))
    
    percentual = 1
    xdata_loess_fit = xdata_loess[findall((ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess)))))).*(xdata_loess.>0)  )] 
    ydata_loess_fit = ydata_loess[findall((ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess)))))).*(xdata_loess.>0)  )]


    p0 = [maximum(log.(ydata_loess_fit)),1,1/4]

    teste = curve_fit(
        model_exponencial_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end

function get_one_σ_FWHM(xdata,ydata)
    
    paleta_de_cores = palette(:Paired_9, size(Perfis_da_Intensidade,2))

    i += 1
    xdata = Posição_Sensores
    ydata = Perfis_da_Intensidade[:,i]

    densidade = ρ[i]
    plot!(xdata, ydata,
    c=paleta_de_cores[i],
    lab=L"$\rho = %$densidade$",
    lw = 1, 
    legendfontsize = 20,
    size = (1000,500),
    labelfontsize = 10,
    titlefontsize = 10,
    tickfontsize = 10, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    yscale = :log10,
    )

    xs = xdata
    ys = log10.(ydata)

    # plot!(xs, ys)

    model = loess(xs, ys, span=0.5)

    us = range(extrema(xs)...; step = 0.1)
    vs = exp10.(Loess.predict(model, us))

    plot!(us,vs,c=paleta_de_cores[i],linestyle=:dash,lw=4,lab="")

    # meia_altura = 10^(middle(log10.(vs)))
    meia_altura = maximum(vs)/2
    B = similar(vs)
    B .= log10(meia_altura)
    A = diff([log10.(vs), B])[1]
    FWHM = abs(diff([us[sortperm(abs.(A))[1]],us[sortperm(abs.(A))[2]]])[1])
    
    vline!([us[sortperm(abs.(A))[1]],us[sortperm(abs.(A))[2]]],c=paleta_de_cores[i],lab="",lw = 4)


    return FWHM
end


# t = Array(LinRange(-pi,pi,100));
# x = sin.(t) .+ 0.25*rand(length(t));

# # Data
# w = 21; sw = string(w)
# plot(t,x,label="sin(t)+ϵ",linewidth=10,alpha=.3,
#      xlabel = "t", ylabel = "x",
#      title="Smoothers",legend=:bottomright)

# # Henderson Moving Average Filter
# plot!(t,Smoothers.hma(x,w), label ="hma(x,"*sw*")")

# # Locally Estimated Scatterplot Smoothing
# plot!(t,Smoothers.loess(t,x;q=w)(t), label ="loess(t,x;q="*sw*")(t)")

# # Moving Average Filter with Matlab/Octave 'filter'
# b = ones(w)/w; a = [1];
# plot!(t,filter(b,a,x), label ="filter(1,[1/"*sw*",...],x)")

# # Simple Moving Average
# plot!(t, sma(x,w,true), label = "sma(x,"*sw*",true)")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


i = 6
N = Ns[i]
ρ_i = round(ρ[i],digits = 2)
Transmission = round(sum(Perfis_da_Intensidade[ : ,i])/sum(Perfis_da_Intensidade[ : ,end]),digits =2)

ydata_base = Perfis_da_Intensidade[indice_Δ, : ,index]
xdata_base = Posição_Sensores

xdata = xdata_base[findall((xdata_base .> -60).*(xdata_base .< 60))]
ydata = ydata_base[findall((xdata_base .> -60).*(xdata_base .< 60))]

ydata = Perfis_da_Intensidade[:,i]
xdata = Posição_Sensores[:,2]

xs = xdata
ys = log10.(ydata)

@time model_LOESS = Loess.loess(xs, ys, span=0.5)
@time model_SMOOTH = Smoothers.loess(xs,ys;q = round(Int64,(size(xs,1)/2)))


us = range(extrema(xs)...; step = 0.1)
vs_LOESS = exp10.(Loess.predict(model_LOESS, us))
vs_SMOOTH = exp10.(model_SMOOTH.(us))


gr()
theme(:vibrant)

scatter(
        xdata,
        ydata,
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        size=(1000,500),
        ms=4,
        yscale=:log10,
        # xlims = (0,150),
        lab = L"$ \textrm{DADOS PUROS}$",
        legendfontsize = 12,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        minorgridstyle = :dashdot,
        framestyle = :box,
        xlabel = L"$ kY $", 
        ylabel = L"$\textrm{Intensidade}$", 
    )

plot!(us,vs_LOESS,lw=6,linestyle = :dash,c = :red,lab=L"$ \textrm{LOESS - FILTRO} $" )
# plot!(us,vs_SMOOTH,lw=6,linestyle = :dash,c = :green,lab=L"$ \textrm{SMOOTHER - FILTRO} $" )

p,σ²_loess = get_one_σ_lorentz(us,vs_LOESS,SLAB())
σ²_loess = round(σ²_loess,digits=2) 
α_plot = round(p[2],digits = 2)
amplitude,μ,σ = p
plot!(xdata[:],model_lorenziana(xdata,[amplitude,μ,σ]),lw=6,lab=L"$ \textrm{Fitting Lorenziano, } \sigma^2 = %$σ²_loess,\alpha = %$α_plot  $",linestyle = :dash,c = :black,ylims=(10^(log10(minimum(ydata))-0.5),10^(log10(maximum(ydata))+0.5 )))
p,σ²_loess = get_one_σ_gaussian(us,vs_LOESS,SLAB())
σ²_loess = round(σ²_loess,digits=2)
μ_plot = round(p[2],digits=2)
plot!(xdata[:],model_gaussiana(xdata,p),lw=6,lab=L"$ \textrm{Fitting Gaussiano, }\sigma^2 = %$σ²_loess, \mu = %$μ_plot   $",linestyle = :dash,c = :green,ylims=(10^(log10(minimum(ydata))-1),10^(log10(maximum(ydata))+1 )))
# p,σ²_loess = get_one_σ_exponencial(us,vs,SLAB())
# α = round(p[2],digits = 2)
# β = round(p[3],digits = 2)
# plot!(xdata[findall(xdata.>0)],model_exponencial(xdata[findall(xdata.>0)],p),lw=6,lab=L"$ \textrm{FITTING Exponential} , \alpha = %$α,\beta = %$β  $",linestyle = :dash,c = :blue,ylims=(10^(log10(minimum(ydata))-1),10^(log10(maximum(ydata))+1 )))
title!(L"$L_z = %$L , \Delta = Int,L_y = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",titlefontsize = 25)
# savefig("Tail_VETORIAL.png")

σ²_sperling = sum((xdata.^2).*ydata)/(sum(ydata))
# plot!(xdata[:],model_lorenziana(xdata,[amplitude,μ,sqrt(sum((xdata.^2).*ydata)/(sum(ydata)))]),lw=6,lab=L"$ ORIGINAL - \sigma^2 = %$σ²_sperling $",linestyle = :dash,c = :green,ylims=(10^(log10(minimum(ydata))-1),10^(log10(maximum(ydata))+1 )))
# savefig("Filtro.png")

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

i += 1
N = Ns[i]
ρ_i = round(ρ[i],digits = 2)
Transmission = round(sum(Perfis_da_Intensidade[ : ,i])/sum(Perfis_da_Intensidade[ : ,end]),digits =2)

ydata = Perfis_da_Intensidade[:,i]
xdata = Posição_Sensores[:,2]

xs = xdata
ys = log10.(ydata)

model = loess(xs, ys, span=0.5)

us = range(extrema(xs)...; step = 0.1)
vs = exp10.(Loess.predict(model, us))

σ =  get_one_σ_FWHM(vs,us)[1]
σ_plot = round(σ^2,digits=2) 

gr()
theme(:vibrant)

scatter(
        xdata,
        ydata,
        size=(1000,800),
        ms=6,
        yscale=:log10,
        # xlims = (0,150),
        lab = L"$ \textrm{DADOS PUROS},N = %$N,\rho = %$ρ_i,T = %$Transmission  $",
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        minorgridstyle = :dashdot,
        framestyle = :box,
        xlabel = L"$ kY $", 
        ylabel = L"$ Intensidade $", 
    )

plot!(us,vs,lw=6,linestyle = :dash,c = :red,lab=L"$ \textrm{LOESS - FILTRO} $" )
vline!([-σ/2],lab=L"FWHM,\sigma = %$σ_plot ",c=:black,linestyle=:dash,lw=6)
vline!([σ/2],lab="",c=:black,linestyle=:dash,lw=6)















# scatter(xs, ys)
# plot!(us, vs, legend=false,lw=8,size=(1000,1000),c=:black,lab = "Varredura")

# vs = vs .+ 9e-11

# println(vs)

# minimum(vs)



# p0 = [maximum(ydata),0,(maximum(xdata)-minimum(xdata))/2]
# teste = curve_fit(model_gaussiana_log, xdata, ydata, p0)
# plot!(Posição_Sensores[:,2],model_gaussiana_log(Posição_Sensores[:,2],teste.param),lw=6,lab=L"$ \sigma^2 =  %$σ²_plot $")


# σ² = teste.param[3]^2    

# σ²,[exp(teste.param[1]),teste.param[2],teste.param[3]]


# interresse = 1
# σ²_plot,p = get_one_σ(Posição_Sensores,Perfis_da_Intensidade[:,interresse],SLAB())

# plot!(Posição_Sensores[:,2],model_gaussiana(Posição_Sensores[:,2],p),lw=6,lab=L"$ \sigma^2 =  %$σ²_plot $")


# include("PLOTS/Plots.jl")
# vizualizar_Perfil_da_Cintura_e_Intensidade(Posição_Sensores[:,2],Perfis_da_Intensidade,Ns,b₀s,σ²,Δ,Variavel_Constante,L,Lₐ,ρ,L"$L = %$L , \Delta = 0,L_a = %$Lₐ_titulo, %$Tipo_de_kernel, %$GEOMETRIA_DA_NUVEM ,%$N_Realizações REP $",1000)




# p = [10,2,1]
# xdata = -20:0.1:20
# ydata = model_gaussiana(xdata,p)

# plot(xdata,ydata)


