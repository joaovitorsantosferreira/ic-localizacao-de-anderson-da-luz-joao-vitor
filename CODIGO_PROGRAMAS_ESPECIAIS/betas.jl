using Loess
using Distances
using LinearAlgebra   
using LaTeXStrings    
using Random   
using Statistics      
using Plots    
using SpecialFunctions
using QuadGK   
using PlotThemes      
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Interpolations
using ForwardDiff


Dados_Antigos = load("CODIGO_PROGRAMAS_ESPECIAIS/BETAS_Escalar.jld2", "SAIDA")
Parametros_Antigos = load("CODIGO_PROGRAMAS_ESPECIAIS/BETAS_Escalar.jld2", "Parametros")

Dados_Antigos = load("CODIGO_PROGRAMAS_ESPECIAIS/BETAS_Vetorial.jld2", "SAIDA")
Parametros_Antigos = load("CODIGO_PROGRAMAS_ESPECIAIS/BETAS_Vetorial.jld2", "Parametros")


βs_Resultantes = Dados_Antigos
ρs = Parametros_Antigos.ρs
Escala_do_range = Parametros_Antigos.Escala_do_range
Δ = Parametros_Antigos.Δ
Lₐ = round(Parametros_Antigos.Lₐ)
N_Sensores = Parametros_Antigos.N_Sensores
Tipo_de_kernel = Parametros_Antigos.Tipo_de_kernel
N_Realizações = Parametros_Antigos.N_Realizações
W = Parametros_Antigos.W
Desordem_Diagonal = Parametros_Antigos.Desordem_Diagonal
N_pontos = Parametros_Antigos.N_pontos
Geometria  = Parametros_Antigos.Geometria
angulo_coerente = Parametros_Antigos.angulo_coerente

RANGE_INICIAL = Parametros_Antigos.RANGE_INICIAL
RANGE_FINAL = Parametros_Antigos.RANGE_FINAL
N_pontos = Parametros_Antigos.N_pontos
Range_L = range(RANGE_INICIAL,RANGE_FINAL,length=N_pontos) 

include("PLOTS/Plots.jl")
vizualizar_β_por_L(βs_Resultantes,Range_L,ρs,L"$\Delta = \Delta_{LOC}, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("decaimento_betas_vetorial_LINEAR.png")

# i += 1
# xdata = zeros(size(βs_Resultantes[1,i],1))
# xdata .= βs_Resultantes[1,i]

# ydata = zeros(size(βs_Resultantes[2,i],1))
# ydata .= βs_Resultantes[2,i]

# xs = xdata
# ys = log10.(ydata)

# model = Loess.loess(xs, ys, span=0.5)

# xdata_loess = range(extrema(xs)...; step = 0.1)
# ydata_loess = exp10.(Loess.predict(model, xdata_loess))

# gr()
# theme(:vibrant)

# scatter(
#     xdata,
#     ydata,
#     left_margin = 10Plots.mm,
#     right_margin = 12Plots.mm,
#     top_margin = 5Plots.mm,
#     bottom_margin = 5Plots.mm,
#     size=(1000,800),
#     ms=6,
#     yscale=:log10,
#     ylims = (10^-10,10^0),
#     # lab = L"$ \textrm{DADOS PUROS},N = %$N,\rho = %$ρ_i,T = %$Transmission  $",
#     lab = "",
#     legendfontsize = 15,
#     labelfontsize = 20,
#     titlefontsize = 20,
#     tickfontsize = 15,
#     gridalpha = 0,
#     minorgrid = true,
#     minorgridalpha = 0,
#     background_color_legend = :white,
#     background_color_subplot = :white,
#     foreground_color_legend = :black,
#     minorgridstyle = :dashdot,
#     framestyle = :box,
#     c = :blue,
#     xlabel = L"$ kL $", 
#     ylabel = L"$ | \beta_n |^2 $"
# )

# plot!(xdata_loess,ydata_loess,lw=6,linestyle = :dash,c = :black,lab=L"$ \textrm{LOESS - FILTRO} $" )


# vizualizar_β_por_L(βs_Resultantes,Range_L,Escala_do_range,ρs,Tipo_de_kernel,L"$W/b_0 = %$W, %$Tipo_de_kernel , %$Geometria ,%$N_Realizações REP , kL_a = %$Lₐ, log_{10}(\rho / k^2)$",1000)
# savefig("BETAS.png")

DIVISAO_β_X = 20
DIVISAO_β_Y = 100
L = 20
Lₐ = 100
βs = zeros(DIVISAO_β_X*DIVISAO_β_Y,)


X_position = L.*rand(1000) .- L/2
Y_position = Lₐ.*rand(1000) .- Lₐ/2



range_X = -L/2:(L/DIVISAO_β_X):L/2
range_Y = -Lₐ/2:(Lₐ/DIVISAO_β_Y):Lₐ/2
aux_2 = 1

for v in 1:DIVISAO_β_X
    for u in 1:DIVISAO_β_Y                    
        index_beta = findall((X_position .>= range_X[v]).*(X_position .<= range_X[v+1]).*(Y_position .>= range_Y[u]).*(Y_position .>= range_Y[u+1]))
        if  index_beta == Int[]
            βs[aux_2, : ] = [range_Δ[indice_Δ],mean(range_X[v:(v+1)]),mean(range_Y[u:(u+1)]),0]
        else
            βs[aux_2, : ] = [range_Δ[indice_Δ],mean(range_X[v:(v+1)]),mean(range_Y[u:(u+1)]),mean(β_Resultante[index_beta])]
        end
        
        aux_2 += 1
    end
end


index_beta = findall(A .> )