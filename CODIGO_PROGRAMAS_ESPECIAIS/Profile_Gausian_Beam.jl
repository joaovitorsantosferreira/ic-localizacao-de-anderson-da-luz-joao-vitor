using Loess
using Distances
using LinearAlgebra   
using LaTeXStrings    
using Random   
using Statistics      
using Plots    
using SpecialFunctions
using QuadGK   
using PlotThemes      
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates
using Interpolations
using ForwardDiff

include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P5.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P6.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Programas Base/P7.jl")
include("CODIGO_FONTE_NOTEBOOK/CORPO/Extração/E1.jl")
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


k = 1
λ = 2*pi/k
ω₀ = 4*pi
Ω = 1e-11

L = 20
Lₐ = 150

# X = 2000*sqrt(λ^2 + L^2 + Lₐ^2)  
# Y = 2000*sqrt(λ^2 + L^2 + Lₐ^2)   

X = 2*sqrt( L^2 + Lₐ^2)  
Y = 2*sqrt( L^2 + Lₐ^2)   

passo_x = 3
passo_y = 1
x = -X:passo_x:X
y = -Y/3:passo_y:Y/3

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function get_one_σ_gaussian(Posição_Sensores,Intensidade,Geometria::TwoD)

    xdata = Posição_Sensores
    ydata = Intensidade
    
    xs = xdata
    ys = log10.(ydata)
    
    model = Loess.loess(xs, ys, span=0.5)
    
    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))

    percentual = 0.55
    xdata_loess_fit = xdata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))] 
    ydata_loess_fit = ydata_loess[findall(ydata_loess .> 10^(log10(maximum(ydata_loess)) - percentual*(abs(log10(minimum(ydata_loess))) - abs(log10(maximum(ydata_loess))))))]


    p0 = [maximum(log.(ydata_loess_fit)),1,(maximum(xdata_loess_fit)-minimum(xdata_loess_fit))/10]

    teste = curve_fit(
        model_gaussiana_log, 
        xdata_loess_fit,
        log.(ydata_loess_fit), 
        p0
    )
    σ² = teste.param[3]^2    

    return [exp(teste.param[1]),teste.param[2],teste.param[3]],σ²
end


function get_eletric_field_LG_real(Entrada::get_eletric_field_LG_real_ENTRADA,geometria::TwoD)

    PX = Entrada.r[1]
    PY = Entrada.r[2]
    E₀ = Entrada.E₀
    ω₀ = Entrada.ω₀
    k = Entrada.k

    α = ((2*PX)/(k*(ω₀^2))) 
    Campo = (E₀*(exp(1im*k*PX - ((PY^2)/((ω₀^2)*(1 + 1im*α))))))/(sqrt(1 + 1im*α))
    
    return Campo
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



pontos = zeros(size(x,1)*size(y,1),2)
aux = 1

for i in 1:size(x,1)
    for j in 1:size(y,1)
        pontos[aux,:] = [x[i],y[j]]
        aux += 1
    end
end

intensidade = zeros(size(pontos,1))

for i in 1:size(pontos,1)

    entrada_campo = get_eletric_field_LG_real_ENTRADA(
        pontos[i,:],
        Ω,
        ω₀,
        1,
        0
    )
    campo_laser = get_eletric_field_LG_real(entrada_campo,SLAB())
    
    intensidade[i] = abs(campo_laser^2)
end

# σ = sqrt(get_one_σ_gaussian(pontos[findall(pontos[:,1] .== 0),2],intensidade[findall(pontos[:,1] .== 0)],SLAB())[2])
# σ_plot = round(σ/λ) 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# plot(pontos[findall(pontos[:,1] .== 0),2],intensidade[findall(pontos[:,1] .== 0)])
# savefig("PERFIL_X=0.png")

gr()
theme(:vibrant)

scatter(
    size = (1000, 500),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    pontos[:,1],
    pontos[:,2],
    clims=(-50,-60) ,
    zcolor = log10.(intensidade),
    ms=2,
    lab=L"$ w_0 = %$ω₀ $",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
)
# hline!([-σ,σ],c=:black,lw=5,lab=L"$ \sigma = %$σ_plot  - Gaussian fit $")

# savefig("CINTURA.png")
# minimum(intensidade)