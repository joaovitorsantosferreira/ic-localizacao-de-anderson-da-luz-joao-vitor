using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(10, exeflags=`--threads 3`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 

	using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                          
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R2.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R3.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E2.jl")
    include("CORPO/Rotinas/[2]---Variancia/R2.jl")  
    include("CORPO/Rotinas/[2]---Variancia/R3.jl") 
    

    Γ₀ = 1                                                                                                           
    ωₐ = 1                                                                                                           
    GEOMETRIA_DA_NUVEM = "DISCO"
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)  
    N = 100                                                                                                          
    kR = 5.641895835477563                                                                                           
    L = 3
    Lₐ = 50
    Tipo_de_kernel = "Vetorial"                                                                                      
    Tipo_de_Onda = "Laser Gaussiano"                                                                                 
    Tipo_de_beta = "Estacionário"                                                                                    
    Ω = Γ₀/1000                                                                                                      
    k = 1                                                                                                            
    angulo_da_luz_incidente = 0                                                                                      
    λ = (2*π)/k                                                                                                      
    vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                
    N_Sensores = 45                                                                                                  
    Angulo_de_variação_da_tela_circular_1 = 40                                                                       
    Angulo_de_variação_da_tela_circular_2 = 85                                                                       
    Variavel_Constante = "kR"                                                                                        
    Escala_de_Densidade = "LINEAR"                                                                                   
    N_Realizações = 1000                                                                                               
    N_div_Densidade = 40                                                                                             
    N_div_Deturn = 40                                                                                                
    Δ_Inicial = -1000                                                                                                  
    Δ_Final = 1000                                                                                                      
    Densidade_Inicial = 1                                                                                            
    Densidade_Final = 3                                                                                              
    Desordem_Diagonal = "PRESENTE"                                                                                    
    W = 10                                                                                                                 

    Variaveis_de_Entrada = R3_ENTRADA(Γ₀,ωₐ,Ω,k,N,kR,L,Lₐ,angulo_da_luz_incidente,vetor_de_onda,λ,Tipo_de_kernel,Tipo_de_Onda,N_Sensores,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2,Tipo_de_beta,N_Realizações,N_div_Densidade,N_div_Deturn,Δ_Inicial,Δ_Final,Densidade_Inicial,Densidade_Final,Variavel_Constante,Escala_de_Densidade,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,Geometria)

end

darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )


Dados_C4 = ROTINA_3__Diagrama_de_Fase_Variancia(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C4.jld2", Dict("SAIDA" => Dados_C4,"Parametros" => Variaveis_de_Entrada))
println(" - ")


MPIClusterManagers.stop_main_loop(manager)
println("saiu")