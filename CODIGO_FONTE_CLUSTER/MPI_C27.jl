using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
# @time addprocs(16, exeflags=`--threads 5`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin

    using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                        
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots
    using Interpolations
    using Loess

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E8.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R27.jl")

    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E8.jl")
    include("CORPO/Rotinas/[15]---Rigidez/R27.jl") 
   
    
    Γ₀ = 1  
    k = 1
    range_ρs = 1:0.1:2
    ρs = zeros(size(range_ρs,1))
    @.ρs = range_ρs
    Espectro_TOTAL = [Any[],Any[],Any[],Any[],Any[],Any[],Any[],Any[],Any[],Any[],Any[]]
    Δ = 0 
    Tipo_de_kernel = "Escalar"
    GEOMETRIA_DA_NUVEM = "DISCO"                                                                      # Define a geometria da nuvem como Slab
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                    # Define o struct que representa a dimensão e geometria da nuvem
    Radius = 5*pi
    N_Realizações = 10000                                                                             # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
    RANGE_INICIAL = 0.1
    RANGE_FINAL = 80
    N_pontos = 100
    Desordem_Diagonal = "AUSENTE"                                                                    # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
    W = 0                                                                                            # Amplitude de Desordem
    Escala_do_range = "LOG"                                                                       # Define que a escala no eixo Y será Linear


    Variaveis_de_Entrada = R27_ENTRADA(Γ₀,ρs,k,Δ,Tipo_de_kernel,Radius,GEOMETRIA_DA_NUVEM,N_pontos,N_Realizações,RANGE_INICIAL,RANGE_FINAL,Escala_do_range,Desordem_Diagonal,W,Espectro_TOTAL,Geometria)
end

darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )

# Dados_C27 = ROTINA_27(Variaveis_de_Entrada,Geometria)

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C27_{$GEOMETRIA_DA_NUVEM}_{$Tipo_de_kernel}.jld2", Dict("SAIDA" => Dados_C27,"Parametros" => Variaveis_de_Entrada))
# println(" - ")


# Dados_C27 = load("DATA={2022-November-15}_Dados_Antigos_CLUSTER_C27_{DISCO}_{Escalar}.jld2", "SAIDA")

Dados_C27 = load("TEMPORARIO_C27.jld2", "SAIDA")

Σ²,Range_L,Espectro_TOTAL = Dados_C27

# for i in 1:(size(range_ρs,1))
#     Λₙ = imag.(Espectro_TOTAL[i])
#     γₙ = real.(Espectro_TOTAL[i])
#     densidade_plot = ρs[i]
#     Radius_plot = round(Radius,digits=2)
#     n_div = 200
#     valid_index_map = findall(γₙ.>0)

#     gr()
#     histogram2d(
#         Λₙ[valid_index_map],
#         γₙ[valid_index_map], 
#         nbins=n_div, 
#         normalize=true,
#         color = cgrad(:jet1,scale=:exp10),
#         size = (1150, 1000),
#         left_margin = 10Plots.mm,
#         right_margin = 12Plots.mm,
#         top_margin = 5Plots.mm,
#         bottom_margin = 5Plots.mm,
#         gridalpha = 0.3,
#         colorbar_title = L"\textrm{density}",
#         title = L"$\rho/k^2 = %$densidade_plot,kR = %$Radius_plot, %$Tipo_de_kernel, %$N_Realizações REP $",
#         legendfontsize = 20,
#         labelfontsize = 25,
#         titlefontsize = 30,
#         tickfontsize = 15, 
#         background_color_legend = :white,
#         background_color_subplot = :white,
#         foreground_color_legend = :black,
#         # clims = (0,0.5)
#     )
#     xlabel!(L"$\Delta_n $")
#     ylabel!(L"$\gamma_n$")
    
#     # n_div = 100

#     # range_x = range(minimum(Λₙ),maximum(Λₙ);length = n_div)
#     # # range_y = range(minimum(γₙ),maximum(γₙ);length = n_div)
#     # range_y = get_points_in_log_scale(10^(-20),10^(2),n_div)
#     # densidade = zeros(n_div^2,3)
#     # cont = 1
#     # N_total = size(Λₙ,1)

#     # Threads.@threads for i in 1:(n_div-1)
#     #     for j in 1:(n_div-1)

#     #         densidade[cont,1] = mean([range_x[i],range_x[i+1]])
#     #         # densidade[cont,2] = mean([range_y[j],range_y[j+1]])
#     #         densidade[cont,2] = exp10(mean([log10(range_y[j]),log10(range_y[j+1])]))
#     #         densidade[cont,3] = size(findall((Λₙ .> range_x[i]).*(Λₙ .< range_x[i+1]).*(γₙ.>range_y[j]).*(γₙ.<range_y[j+1])),1)/N_total 
#     #         cont += 1
#     #     end
#     # end

#     # valid_index = findall(densidade[:,3] .≠ 0)

#     # gr()
#     # scatter(
#     #     densidade[valid_index,1],
#     #     densidade[valid_index,2],
#     #     zcolor=log10.(densidade[valid_index,3]),
#     #     m=:square,
#     #     left_margin = 10Plots.mm,
#     #     right_margin = 12Plots.mm,
#     #     top_margin = 5Plots.mm,
#     #     bottom_margin = 5Plots.mm,
#     #     gridalpha = 0.3,
#     #     ms=5,
#     #     size=(1300,1000),
#     #     yscale=:log10,
#     #     ylims = (10^-20,10^2), 
#     #     xlims = (-30,30), 
#     #     colorbar_title = "",
#     #     lab="",
#     #     title = L"$\rho/k^2 = %$densidade_plot,kR = %$Radius_plot, %$Tipo_de_kernel, %$N_Realizações REP $",
#     #     legendfontsize = 20,
#     #     labelfontsize = 25,
#     #     titlefontsize = 30,
#     #     tickfontsize = 15, 
#     #     background_color_legend = :white,
#     #     background_color_subplot = :white,
#     #     foreground_color_legend = :black,
#     # )
#     # xlabel!(L"$\Delta_n $")
#     # ylabel!(L"$\gamma_n$")

#     savefig("densidade={$densidade_plot}.png")
# end

N_pontos = 20

contagem = zeros(N_pontos,size(ρs,1)-1)
Range_L = get_points_in_log_scale(0.1,80,N_pontos) 

for i in 1:(size(range_ρs,1)-1)
    Λₙ = imag.(Espectro_TOTAL[i])
    γₙ = real.(Espectro_TOTAL[i])
    densidade_plot = ρs[i]
    Radius_plot = round(Radius,digits=2)
    n_div = 200
    valid_index_map = findall(γₙ.>0)
    N_total = size(Λₙ[valid_index_map],1)

    for j in 1:size(Range_L,1)

        contagem[j,i] =  size(findall( abs.(Λₙ[valid_index_map]) .<= Range_L[j]/2),1)/N_total

    end

end




densidade_plot =  ρs[1]
plot(
    Range_L,
    contagem[:,1],
    size = (1000,1000),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.5,
    label = L"$ \rho / k^2 = %$densidade_plot  $",
    title = L"$ %$Tipo_de_kernel, %$N_Realizações REP $",
    xscale=:log10,
    legendfontsize = 20,
    legendtitlefontsize = 20;
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    legend =:bottomright
)

densidade_plot =  ρs[2]
plot!(Range_L,contagem[:,2],xscale=:log10, label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[3]
plot!(Range_L,contagem[:,3],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[4]
plot!(Range_L,contagem[:,4],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[5]
plot!(Range_L,contagem[:,5],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[6]
plot!(Range_L,contagem[:,6],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[7]
plot!(Range_L,contagem[:,7],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[8]
plot!(Range_L,contagem[:,8],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[9]
plot!(Range_L,contagem[:,9],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

densidade_plot =  ρs[10]
plot!(Range_L,contagem[:,10],xscale=:log10,label = L"$ \rho / k^2 = %$densidade_plot  $")

xlabel!(L"$ L $")
ylabel!(L"$\langle N \rangle $")


savefig("contagem_de_estados.png")



MPIClusterManagers.stop_main_loop(manager)
println("saiu")


