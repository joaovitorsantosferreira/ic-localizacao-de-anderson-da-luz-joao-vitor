using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(10, exeflags=`--threads 10`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])
@time @everywhere begin 

    using Distances    
    using LinearAlgebra
    using LaTeXStrings  
    using Random       
    using Statistics   
    using Plots        
    using SpecialFunctions
    using QuadGK       
    using PlotThemes   
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R20.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Rotinas/[11]---Desordem_Espectro/R20.jl") 
    include("Plots.jl") 

    ωₐ = 1                                                                                                                  
    Γ₀ = 1                                                                                                                  
    GEOMETRIA_DA_NUVEM = "DISCO"                                                                                            
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                           
    k = 1                                                                                                                   
    λ = (2*π)/k                                                                                                             
    ρ = 3
    Lₐ = 20                                                                                                                 
    N = 1000                                                                                                                
    Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
    Tipo_de_kernel = "Vetorial"                                                                                             
    Δ = 0
    k = 1                                                
    N_Realizações = 200                                                                                                      
    if Tipo_de_kernel == "Escalar"
        b₀ = (8*N)/(π*k*Radius)
    elseif Tipo_de_kernel == "Vetorial"
        b₀ = (16*N)/(π*k*Radius)
    end
    W = 1*b₀
    Variaveis_de_Entrada = R20_ENTRADA(Γ₀,k,GEOMETRIA_DA_NUVEM,Geometria,N,ρ,Radius,Lₐ,L,rₘᵢₙ,Tipo_de_kernel,Δ, N_Realizações,W)
end


darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )

Dados_C22 = ROTINA_20__Espectro_Desordem(Variaveis_de_Entrada,Geometria)



DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_C22.jld2", Dict("SAIDA" => Dados_C22,"Parametros" => Variaveis_de_Entrada))

γ,ω,IPRs_PURO = Dados_C22

ρ_titulo = round(ρ,digits=2)
W_titulo = round(W/b₀ ,digits=2)
R_titulo = round(Radius,digits=2)

vizualizar_relação_entre_gamma_e_omega_IPR(ω,γ,round.(IPRs_PURO,digits=20),Δ,Γ₀,0,0,Tipo_de_kernel,"PRESENTE",W,b₀,L"$ \rho / k^2 =%$ρ_titulo, %$Geometria,W /b_0 = %$W_titulo ,kR = %$R_titulo,%$N_Realizações REP, %$Tipo_de_kernel  - IPR $",1000)
savefig("Espectro_vetorial_desordem.png")

println(" - ")
