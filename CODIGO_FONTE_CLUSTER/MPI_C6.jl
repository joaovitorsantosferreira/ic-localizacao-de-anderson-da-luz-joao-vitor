using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(4, exeflags=`--threads 20`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 

	using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                          
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E3.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R4.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R5.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E3.jl")
    include("CORPO/Rotinas/[3]---Comprimento__De__Localização/R4.jl")  
    include("CORPO/Rotinas/[3]---Comprimento__De__Localização/R5.jl") 
    

    k = 1                                                                                                            
    Γ₀ = 1                                                                                                           
    GEOMETRIA_DA_NUVEM = "DISCO"
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)  
    L = 20
    Lₐ = 280
    Radius = 4*pi
    Tipo_de_kernel = "Escalar"                                                                                      
    N_Realizações = 3000                                                                                               
    N_div_Densidade = 50                                                                                             
    N_div_Deturn = 50                                                                                                
    Δ_Inicial = 0                                                                                                  
    Δ_Final = 5                                                                                                      
    Densidade_Inicial = 1                                                                                            
    Densidade_Final = 5                                                                                              
    Desordem_Diagonal = "AUSENTE"                                                                                    
    W = 10                                                                                                                 

    Variaveis_de_Entrada = R5_ENTRADA(Γ₀,k,L,Lₐ,Radius,N_Realizações,Tipo_de_kernel,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Δ_Inicial,Δ_Final,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,Geometria)

end

darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )


Dados_C6 = ROTINA_5(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C6.jld2", Dict("SAIDA" => Dados_C6,"Parametros" => Variaveis_de_Entrada))
println(" - ")
