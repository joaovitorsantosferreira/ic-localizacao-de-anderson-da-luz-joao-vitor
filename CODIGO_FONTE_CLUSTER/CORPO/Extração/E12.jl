###############################################################################################################################################################################################################
##################################################################################### Extração de dados de uma simulaçao ######################################################################################
###############################################################################################################################################################################################################


function E12(Variaveis_de_entrada::E12_ENTRADA)


    ###############################################################################################################################################################################################################
    #------------------------------------------------------------------------------------ Ativação dos programas em conjunto -------------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #---------------------------------------------------------------------------------------------- Gerando o disco ----------------------------------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

    entrada_cloud = Cloud_ENTRADA(
        Variaveis_de_entrada.N,
        Variaveis_de_entrada.ρ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.rₘᵢₙ,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.W,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.Geometria
    )  

    # Chamo a função para construir o sistema aleatório de atomos no disco
    cloud = Cloud_CLEAN(entrada_cloud)

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #------------------------------------------------------------------- Analise da intensidade da luz espalhada do sistem e Dinâmica do Sistem ------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


    entrada_beta = get_all_beta_to_system_ENTRADA(
        cloud.G,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Γ₀,
        cloud.r,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_beta,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L
    )
    # Guardo a dinâmica dos atomos em uma matriz    
    βₙ = get_all_beta_to_system(entrada_beta,Variaveis_de_entrada.Geometria)                           
    

    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.N_Sensores,
        Variaveis_de_entrada.Distancia_dos_Sensores,
        Variaveis_de_entrada.Angulo_da_luz_incidente,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas
    )
    # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
    Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_entrada.Geometria) 

    
    entrada_Intensidade = get_Intensity_to_sensors_ENTRADA(
        Posição_Sensores,
        βₙ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Ω,
        cloud.r,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Tipo_de_campo
    )
    # Guardo a intensidade sentida em cada sensor 
    All_Intensitys = get_Intensity_to_sensors(entrada_Intensidade,Variaveis_de_entrada.Geometria)


    ###############################################################################################################################################################################################################
    #--------------------------------------------------------------------------------- Dados Gerados para as condições de entrada --------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################
    

    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"
        index = 1
    else 
        index = 3
    end

    return All_Intensitys[index, : ],cloud.r,abs.(βₙ).^2
end