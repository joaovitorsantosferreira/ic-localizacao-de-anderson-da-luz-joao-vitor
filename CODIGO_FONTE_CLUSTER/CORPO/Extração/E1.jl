###############################################################################################################################################################################################################
##################################################################################### Extração de dados de uma simulaçao ######################################################################################
###############################################################################################################################################################################################################


function E1_extração_de_dados_Geral(Variaveis_de_entrada::E1_ENTRADA)


    ###############################################################################################################################################################################################################
    #------------------------------------------------------------------------------------ Ativação dos programas em conjunto -------------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #---------------------------------------------------------------------------------------------- Gerando o disco ----------------------------------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

    entrada_cloud = Cloud_ENTRADA(
        Variaveis_de_entrada.N,
        Variaveis_de_entrada.ρ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.rₘᵢₙ,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.W,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.Geometria
    )  

    # Chamo a função para construir o sistema aleatório de atomos no disco
    cloud = Cloud_COMPLETO(entrada_cloud)

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #--------------------------------------------------------------------------------- Extraindo as Propriedades Físicas do Disco --------------------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


    # Guardo as caracteristicas do sistema gerado na variavel prop
    propriedades_fisicas_global = Characteristics_of_the_modes(cloud,Variaveis_de_entrada.Tipo_de_kernel,Variaveis_de_entrada.Geometria)


    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #---------------------------------------------------------------------------- Extraindo as Propriedades do modo mais subrandiante ----------------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


    # Defino dentre todos os modos aquele que seja melhor para analisar as propriedades da localização
    propriedades_fisicas_best_mode = get_best_mode(cloud,propriedades_fisicas_global,Variaveis_de_entrada.Tipo_de_kernel)


    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    #------------------------------------------------------------------- Analise da intensidade da luz espalhada do sistem e Dinâmica do Sistem ------------------------------------------------------------------#  
    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


    entrada_beta = get_all_beta_to_system_ENTRADA(
        cloud.G,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Γ₀,
        cloud.r,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_beta,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L
    )
    # Guardo a dinâmica dos atomos em uma matriz    
    βₙ = get_all_beta_to_system(entrada_beta,Variaveis_de_entrada.Geometria)                           


    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.N_Sensores,
        Variaveis_de_entrada.Distancia_dos_Sensores,
        Variaveis_de_entrada.Angulo_da_luz_incidente,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas
    )
    # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
    Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_entrada.Geometria) 
    
    
    entrada_Intensidade = get_Intensity_to_sensors_ENTRADA(
        Posição_Sensores,
        βₙ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Ω,
        cloud.r,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Δ        
    )
    # Guardo a intensidade sentida em cada sensor 
    All_Intensitys = get_Intensity_to_sensors(entrada_Intensidade,Variaveis_de_entrada.Geometria)

    ###############################################################################################################################################################################################################
    #--------------------------------------------------------------------------------- Dados Gerados para as condições de entrada --------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    return E1_SAIDA(cloud,propriedades_fisicas_global,propriedades_fisicas_best_mode,βₙ,Posição_Sensores,All_Intensitys)
end
