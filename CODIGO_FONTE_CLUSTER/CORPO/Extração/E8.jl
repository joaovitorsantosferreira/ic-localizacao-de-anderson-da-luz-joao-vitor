function E8(Variaveis_de_entrada::E8_ENTRADA)

    entrada_cloud = Cloud_ENTRADA(
        Variaveis_de_entrada.N,
        Variaveis_de_entrada.ρ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.rₘᵢₙ,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.W,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.Geometria
    )  

    cloud = Cloud_COMPLETO(entrada_cloud)
 
    return cloud.λ
end