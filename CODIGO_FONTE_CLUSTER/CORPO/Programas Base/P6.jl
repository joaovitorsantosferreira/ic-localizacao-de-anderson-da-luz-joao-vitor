###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

function get_parametros_da_nuvem(N::Integer,ρ::Number,Lₐ::Number,GEOMETRIA_DA_NUVEM::String)

    Radius = 1
    L = 1
    
    if GEOMETRIA_DA_NUVEM == "DISCO"
        ############################################################################################# Geometria de Disco #############################################################################################
        Radius = sqrt(N/(π*ρ))                                                                                                                                   # Raio Normalidado
        rₘᵢₙ = 1/(3*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
        ω₀ = Radius/2                                                                                                                                           # Cintura do Laser Gaussiano 
        
    elseif GEOMETRIA_DA_NUVEM == "SLAB"
        ############################################################################################### Geometria de SLAB #############################################################################################
        L = N/(Lₐ*ρ)                                                                                                                                                # Espessura da Nuvem 
        rₘᵢₙ = 1/(3*sqrt(ρ))                                                                                                                                        # Distância minima entre os atomos  
        # ω₀ = Lₐ/10                                                                                                                                                  # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano
        ω₀ = 6*π
    elseif GEOMETRIA_DA_NUVEM == "CUBO"
        ############################################################################################### Geometria de Cubo #############################################################################################
        L = (N/ρ)^(1/3)                                                                                                                                                # Espessura da Nuvem 
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                        # Distância minima entre os atomos  
        ω₀ = L/2                                                                                                                                                  # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano
        
    elseif GEOMETRIA_DA_NUVEM == "ESFERA"
        ############################################################################################# Geometria de Esfera #############################################################################################
        Radius = (N/((4/3)π*ρ))^(1/3)                                                                                                                               # Raio Normalidado
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
        ω₀ = Radius/2                                                                                                                                           # Cintura do Laser Gaussiano 
            
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"
        ######################################################################################### Geometria de Paralelepipedo ##########################################################################################
        L = N/(ρ*(Lₐ^2))                                                                                                                                        # Espessura vertical da nuvem  
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
        ω₀ = Lₐ/10                                                                                                                                               # Cintura do Laser Gaussiano 
            
    elseif GEOMETRIA_DA_NUVEM == "TUBO"
        ############################################################################################# Geometria de Tubo ##############################################################################################
        L = N/(ρ*π*(Lₐ^2))                                                                                                                                      # Espessura do tubo 
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
        ω₀ = Lₐ/2                                                                                                                                              # Cintura do Laser Gaussiano 
            
    end
       
    return Radius,L,rₘᵢₙ,ω₀
end

function get_parametros_da_nuvem(L::Number,Radius::Number,Lₐ::Number,ρ::Number,GEOMETRIA_DA_NUVEM::String)
    

    if GEOMETRIA_DA_NUVEM == "DISCO"
        ############################################################################################# Geometria de Disco #############################################################################################
        N = round(Int64,ρ*π*(Radius^2))                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(5*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
        ω₀ = Radius/2                                                                                                                                          # Cintura do Laser Gaussiano 
        
    elseif GEOMETRIA_DA_NUVEM == "SLAB"
        ############################################################################################### Geometria de SLAB #############################################################################################
        #----------------------------------------------------------------------------------------- Expessura da Nuvem fixada -----------------------------------------------------------------------------------------#
        N = round(Int64,ρ*L*Lₐ)                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(5*sqrt(ρ))                                                                                                                                     # Distância minima entre os atomos  
        ω₀ = Lₐ/10                                                                                                                                                # Cintura do Laser Gaussiano
        
    elseif GEOMETRIA_DA_NUVEM == "CUBO"
        ############################################################################################### Geometria de Cubo #############################################################################################
        #----------------------------------------------------------------------------------------- Expessura da Nuvem fixada -----------------------------------------------------------------------------------------#
        N = round(Int64,ρ*(L^3))                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     # Distância minima entre os atomos  
        ω₀ = L/2                                                                                                                                                # Cintura do Laser Gaussiano
        
    elseif GEOMETRIA_DA_NUVEM == "ESFERA"
        ############################################################################################# Geometria de Esfera #############################################################################################
        N = round(Int64,ρ*(4/3)*π*(Radius^3))                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
        ω₀ = Radius/2                                                                                                                                          # Cintura do Laser Gaussiano 
            
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"
        ######################################################################################## Geometria de Paralelepipedo #############################################################################################
        N = round(Int64,ρ*L*(Lₐ^2))                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
        ω₀ = Lₐ/10                                                                                                                                          # Cintura do Laser Gaussiano 
            
    elseif GEOMETRIA_DA_NUVEM == "TUBO"
        ############################################################################################# Geometria de Tubo ##############################################################################################
        N = round(Int64,ρ*L*π*(Lₐ^2))                                                                                                                # Número de Atomos
        rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
        ω₀ = Lₐ/2                                                                                                                                          # Cintura do Laser Gaussiano 
            
    end
        
    return N,rₘᵢₙ,ω₀
end

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
###############################################################################################################################################################################################################


function get_geometria(GEOMETRIA_DA_NUVEM)

    if GEOMETRIA_DA_NUVEM == "DISCO"
            
        geometria = DISCO()

    elseif GEOMETRIA_DA_NUVEM == "SLAB"
        
        geometria = SLAB()

    elseif GEOMETRIA_DA_NUVEM == "CUBO"

        geometria = CUBO()

    elseif GEOMETRIA_DA_NUVEM == "ESFERA"

        geometria = ESFERA()

    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO"

        geometria = PARALELEPIPEDO()

    elseif GEOMETRIA_DA_NUVEM == "TUBO"

        geometria = TUBO()

    end

    return geometria 
end