###############################################################################################################################################################################################################
#--------------------------------------------------------------------------- Caracterização formal das funções que modelam o disco ---------------------------------------------------------------------------#
###############################################################################################################################################################################################################

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Definições e funções que geram efetivamente o disco ----------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



# A densidade optica depende das variaveis globais sistema pela seguinte relação:
## vide o artigo ---> DOI: 10.1103/PhysRevA.92.062702 , na última pág

function get_b₀(Variaveis_de_Entrada::get_b₀_ENTRADA,geometria::TwoD)

    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"

        if Variaveis_de_Entrada.tipo_de_Kernel == "Escalar"
            
            termo =  ((8*Variaveis_de_Entrada.N)/(π*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Radius))

        elseif Variaveis_de_Entrada.tipo_de_Kernel == "Vetorial"

            termo = ((16*Variaveis_de_Entrada.N)/(π*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Radius))
        
        end

    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

        if Variaveis_de_Entrada.tipo_de_Kernel == "Escalar"
            
            termo =  (1/(1 +4*(Variaveis_de_Entrada.Δ/Variaveis_de_Entrada.Γ₀)))*((4*Variaveis_de_Entrada.N)/(Variaveis_de_Entrada.k*Variaveis_de_Entrada.L))

        elseif Variaveis_de_Entrada.tipo_de_Kernel == "Vetorial"

            termo =  (1/(1 +16*(Variaveis_de_Entrada.Δ/Variaveis_de_Entrada.Γ₀)))*((8*Variaveis_de_Entrada.N)/(Variaveis_de_Entrada.k*Variaveis_de_Entrada.L))
        
        end

    end
        
    return termo
end 


function get_b₀(Variaveis_de_Entrada::get_b₀_ENTRADA,geometria::ThreeD)

    termo = 1
        
    return termo
end 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function Cloud_COMPLETO(Entrada::Cloud_ENTRADA)

    r = getAtoms_distribution(getAtoms_distribution_ENTRADA(Entrada.N, Entrada.Radius, Entrada.rₘᵢₙ,Entrada.GEOMETRIA_DA_NUVEM,Entrada.L,Entrada.Lₐ),Entrada.Geometria)                                                                   # Gera o disco e guarda as posições em r
    R_jk = Distances.pairwise(Euclidean(), r, r, dims = 1)                                                      
    G = empty_green_matrix(Entrada.N,Entrada.Tipo_de_kernel,Entrada.Geometria)
    get_GreensMatrix(get_GreensMatrix_ENTRADA(Entrada.N,Entrada.Γ₀,Entrada.k,Entrada.Δ,r,Entrada.Tipo_de_kernel,Entrada.Desordem_Diagonal,Entrada.W),Entrada.Geometria, G, R_jk)
    λ, ψ = LinearAlgebra.eigen(G)                                                                               
    b₀ = get_b₀(get_b₀_ENTRADA(Entrada.N, Entrada.Radius, Entrada.k,Entrada.Tipo_de_kernel,Entrada.GEOMETRIA_DA_NUVEM,Entrada.L,Entrada.Δ,Entrada.Γ₀),Entrada.Geometria)
    
    
    return Cloud_SAIDA(r, λ, ψ,R_jk, G, b₀)
end

function Cloud_CLEAN(Entrada::Cloud_ENTRADA)

    r = getAtoms_distribution(getAtoms_distribution_ENTRADA(Entrada.N, Entrada.Radius, Entrada.rₘᵢₙ,Entrada.GEOMETRIA_DA_NUVEM,Entrada.L,Entrada.Lₐ),Entrada.Geometria)                                                                   # Gera o disco e guarda as posições em r
    R_jk = Distances.pairwise(Euclidean(), r, r, dims = 1)                                                      
    G = empty_green_matrix(Entrada.N,Entrada.Tipo_de_kernel,Entrada.Geometria)
    get_GreensMatrix(get_GreensMatrix_ENTRADA(Entrada.N,Entrada.Γ₀,Entrada.k,Entrada.Δ,r,Entrada.Tipo_de_kernel,Entrada.Desordem_Diagonal,Entrada.W),Entrada.Geometria, G, R_jk)

    return Cloud_SAIDA(r, 0,0,R_jk, G, 0)
end

function empty_green_matrix(N::Integer,Tipo_de_kernel::String,Geometria::TwoD)
    
    if Tipo_de_kernel == "Escalar"
        G = Array{Complex{Float64}}(undef, N, N) 
    elseif Tipo_de_kernel == "Vetorial"
        G = Array{Complex{Float64}}(undef, 2*N, 2*N)     
    end

    return G
end

