#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# function get_force_in_atoms(r,vetor_de_onda,β,Γ₀,Ω,G)
    
#     N = size(r,1)
#     optical_forces = Array{Complex{Float64}}(undef, N,3)

#     β_normalizado = Array{Complex{Float64}}(undef, N)
#     r_dot = zeros(N)

#     for i in 1:N
#         r_dot[i] = - dot(r[i, : ],vetor_de_onda)
#     end

#     @. β_normalizado = β*cis(r_dot)

#     for i in 1:N

#         Fₐⱼ = -Ω*imag(β_normalizado[i]).*vetor_de_onda

#         r_hat = Array{Complex{Float64}}(undef, N,3)
#         for j in 1:N
#             r_hat[j,:] = (r[i,:] - r[j,:])/norm(r[i,:] - r[j,:])  
#         end

#         f_ij = Array{Complex{Float64}}(undef, N,3)
#         for j in 1:N
#             f_ij[j,:] = - sphericalbesselj(1,norm(r[i,:] - r[j,:]))*cis(dot(r[i,:] - r[j,:],vetor_de_onda)).*r_hat[j,:] 
#         end
#         f_ij[i,:] .= 0 + 0im


#         Fₑⱼ = Array{Complex{Float64}}(undef, 3)
#         Fₑⱼ .= 0 + 0im  
#         for n in 1:N
#             Fₑⱼ .+=  -Γ₀.*imag.(f_ij[n,:])*imag(β_normalizado[i]*conj(β_normalizado[n]))
#             # Fₑⱼ .+=  f_ij[n,:]
#         end
    
#         optical_forces[i,:] = Fₐⱼ .+ Fₑⱼ 
#         # optical_forces[i,:] = Fₑⱼ 
#     end

#     return optical_forces
# end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_force_in_atoms(r,vetor_de_onda,β,Γ₀,Ω,G)
    
    N = size(r,1)
    optical_forces =  zeros(N,3)

    for i in 1:N

        fator_soma_x = 0
        for j in 1:N
            if i ≠ j 
                rᵢⱼ = norm((r[i,:] .- r[j,:]))
                # fator_soma_x += imag( (((G[i,j]*(r[i,1] - r[j,1]))/(1im*(rᵢⱼ^3)))  - ((exp(1im*rᵢⱼ)*((r[i,1] - r[j,1])))/rᵢⱼ))*conj(β[i])*β[j])
                # fator_soma_x += imag(((cis(rᵢⱼ)*((r[i,1] - r[j,1])))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1  )*conj(β[i])*β[j])            
                fator_soma_x += imag(((cis(rᵢⱼ)*(r[i,1] - r[j,1]))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1)*conj(β[i])*β[j])
            end
        end

        fator_soma_y = 0
        for j in 1:N
            if i ≠ j 
                rᵢⱼ = norm((r[i,:] .- r[j,:]))
                # fator_soma_y += imag(  (((G[i,j]*(r[i,2] - r[j,2]))/(1im*rᵢⱼ^3))  - ((exp(1im*rᵢⱼ)*((r[i,2] - r[j,2])))/rᵢⱼ))*conj(β[i])*β[j])
                # fator_soma_y += imag(((cis(rᵢⱼ)*((r[i,2] - r[j,2])))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1  )*conj(β[i])*β[j])
                fator_soma_y += imag(((cis(rᵢⱼ)*(r[i,2] - r[j,2]))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1)*conj(β[i])*β[j])

            end
        end

        fator_soma_z = 0
        for j in 1:N
            if i ≠ j 
                rᵢⱼ = norm((r[i,:] .- r[j,:]))
                # fator_soma_z += imag(  (((G[i,j]*(r[i,3] - r[j,3]))/(1im*rᵢⱼ^3))  - ((exp(1im*rᵢⱼ)*((r[i,3] - r[j,3])))/rᵢⱼ))*conj(β[i])*β[j])
                # fator_soma_z += imag(((cis(rᵢⱼ)*((r[i,3] - r[j,3])))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1  )*conj(β[i])*β[j])
                fator_soma_z += imag(((cis(rᵢⱼ)*(r[i,3] - r[j,3]))/rᵢⱼ^2)*((1/(1im*rᵢⱼ)) - 1)*conj(β[i])*β[j])

            end
        end

        optical_forces[i,:] = -Γ₀.*[fator_soma_x,fator_soma_y,fator_soma_z]
    end

    return optical_forces
end
