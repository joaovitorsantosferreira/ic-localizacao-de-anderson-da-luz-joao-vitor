###############################################################################################################################################################################################################
#------------------------------------------------------------------------------ Funções que extraem as caracteristicas da nuvem ------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------ Função Principal responsavel por fornecer as caracteristicas de interresse da nuvem ------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function Characteristics_of_the_modes(cloud::Cloud_SAIDA,Tipo_de_kernel::String,geometria::Dimension)


    # Chama a função responsável por definir os IPRs dos modos encontrador e define uma matriz que vai guardar esta informação
    IPRs,q,Sₑ = get_Statistics_profile_of_modes(cloud.ψ,Tipo_de_kernel,geometria) 



    # Chama a função responsável por definir os comprimentos de localização de cada modo encontrado
    ## Eₙ é a matriz que vai guardar o erro comedido ao definir cada comprimento de localização Erro 
    ## ξₙ é a matriz que vai guardar o comprimento de localização de cada modo encontrado
    ## R_cm_n é a localização do Centro de Massa de cada modo 
    Eₙ , ξₙ, R_cm_n = get_all_ξ_and_R_CM(cloud,Tipo_de_kernel,geometria)

    
    # Chama a função responsável por definir o tempo de vida e a frequência de cada modo encontrado
    ## γₙ é a matriz que vai guardar o inverso do tempo de vida de cada modo encontrado 
    ## ωₙ é a matriz que vai guardar o a frequência de cada modo
    ## Γₙ uma matriz responsavel por definir o tempo de decaimento de cada atomo envolvido
    ## Tₙ é o tempo de vida de cada modo
    γₙ, ωₙ, Γₙ, Tₙ = get_all_gamma_and_frequency_and_eta_and_T_to_modes(cloud.λ,Tipo_de_kernel)


    # É gerada uma tuple como saida, pois achei mais conveniente condensar as viriaveis e depois extrai-las 
    return Characteristics_of_the_modes_SAIDA(IPRs, ξₙ ,Eₙ, γₙ , ωₙ, q, Sₑ, R_cm_n) 
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------ Funções que analisam e guardam o Tempo de Decaimento e Frequencia dos modos/atomos -------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_all_gamma_and_frequency_and_eta_and_T_to_modes(λ::Array,Tipo_de_Kernel)

    if Tipo_de_Kernel == "Escalar"

        n_atomos = size(λ,1)                                                                                                   # Define a quantidade de atomos dispostos no disco


        γₙ = zeros(n_atomos)                                                                                                   # Matriz base que vai ser preenchida pelo inverso dos tempos de de vida dos modos
        ωₙ = zeros(n_atomos)                                                                                                   # Matriz base que vai ser preenchida pela frequencia de cada modo 
        Γₙ = zeros(n_atomos)                                                                                                   # Matriz base que vai ser preenchida pelos tempos de de vida dos atomos
        Tₙ = zeros(n_atomos)                                                                                                   # Matriz base que vai ser preenchida pelos tempos de de vida dos modos


        contador = 1                                                                                                           # Variavel auxiliar do loop 
        
        for i in 1:n_atomos

            real_λₙ = real(λ[i])
            imag_λₙ = imag(λ[i])

            γₙ[i] = real_λₙ
            ωₙ[i] = imag_λₙ
            Γₙ[i] = (1/real_λₙ)
            Tₙ[i] = 1/(γₙ[i])

        end

    elseif Tipo_de_Kernel == "Vetorial"

        # N_atomos = trunc(Int64,size(λ,1)/2)
        # n_modos = N_atomos                                                                                                   # Define a quantidade de atomos dispostos no disco
        n_modos = size(λ,1)

        γₙ = zeros(n_modos)                                                                                                   # Matriz base que vai ser preenchida pelo inverso dos tempos de de vida dos modos
        ωₙ = zeros(n_modos)                                                                                                   # Matriz base que vai ser preenchida pela frequencia de cada modo 
        Γₙ = zeros(n_modos)                                                                                                   # Matriz base que vai ser preenchida pelos tempos de de vida dos atomos
        Tₙ = zeros(n_modos)                                                                                                   # Matriz base que vai ser preenchida pelos tempos de de vida dos modos


        contador = 1                                                                                                           # Variavel auxiliar do loop 
        
        for i in 1:n_modos

            real_λₙ = real(λ[i])
            imag_λₙ = imag(λ[i]) 

            γₙ[i] = real_λₙ
            ωₙ[i] = imag_λₙ
            Γₙ[i] = (1/real_λₙ)
            Tₙ[i] = 1/(γₙ[i])

        end
    end

    #É gerada uma tuple como saida, pois achei mais conveniente condensar as viriaveis e depois extrai-las
    return γₙ, ωₙ, Γₙ, Tₙ
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Funções que definem o IPR de cada modo ----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Statistics_profile_of_modes(ψ,Tipo_de_Kernel,geometria::TwoD)
    
    if Tipo_de_Kernel == "Escalar"
        
        n_modes = size(ψ, 1)
        N_atomos = n_modes
        IPRs = zeros(n_modes)
        q = zeros(n_modes)
        Sₑ = zeros(n_modes)

        for n = 1:n_modes

            ψ²ₙ = abs.((ψ[:, n]) .^ 2)
            ψ⁴ₙ = ψ²ₙ .^ 2

            IPRs[n] = (sum(ψ⁴ₙ)) / (sum(ψ²ₙ)^2)
            q[n] = 1/(N_atomos*IPRs[n])
            Sₑ[n] =  - sum(ψ²ₙ.*log.(ψ²ₙ)) - log(1/IPRs[n])
        end

    elseif Tipo_de_Kernel == "Vetorial"


        N_atomos = trunc(Int64,size(ψ,1)/2)
        n_modes = size(ψ, 1)
        IPRs = zeros(n_modes)
        q = zeros(n_modes)
        Sₑ = zeros(n_modes)

        for n = 1:n_modes

            ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos, n ]).^2
            ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos + 1):end,n]).^2
            ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾
            ψ⁴ₙ = ψ²ₙ .^ 2

            IPRs[n] = (sum(ψ⁴ₙ)) / (sum(ψ²ₙ)^2)
            q[n] = 1/(N_atomos*IPRs[n])
            Sₑ[n] = - sum(ψ²ₙ.*log.(ψ²ₙ)) - log(1/IPRs[n])
        end


    end

    return IPRs,q,Sₑ
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_Statistics_profile_of_modes(ψ,Tipo_de_Kernel,geometria::ThreeD)
    
    if Tipo_de_Kernel == "Escalar"
        
        n_modes = size(ψ, 1)
        N_atomos = n_modes
        IPRs = zeros(n_modes)
        q = zeros(n_modes)
        Sₑ = zeros(n_modes)

        for n = 1:n_modes

            ψ²ₙ = abs.((ψ[:, n]) .^ 2)
            ψ⁴ₙ = ψ²ₙ .^ 2

            IPRs[n] = (sum(ψ⁴ₙ)) / (sum(ψ²ₙ)^2)
            q[n] = 1/(N_atomos*IPRs[n])
            Sₑ[n] =  - sum(ψ²ₙ.*log.(ψ²ₙ)) - log(1/IPRs[n])
        end

    elseif Tipo_de_Kernel == "Vetorial"

        N_atomos = trunc(Int64,size(ψ,1)/3)
        n_modes = size(ψ, 1)
        IPRs = zeros(n_modes)
        q = zeros(n_modes)
        Sₑ = zeros(n_modes)

        for n = 1:n_modes

            ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos, n ]).^2
            ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos + 1):(2*N_atomos),n]).^2
            ψ²ₙ⁽⁰⁾ = abs.(ψ[(2*N_atomos + 1):end,n]).^2
            ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾ + ψ²ₙ⁽⁰⁾
            ψ⁴ₙ = ψ²ₙ .^ 2

            IPRs[n] = (sum(ψ⁴ₙ)) / (sum(ψ²ₙ)^2)
            q[n] = 1/(N_atomos*IPRs[n])
            Sₑ[n] = - sum(ψ²ₙ.*log.(ψ²ₙ)) - log(1/IPRs[n])
        end


    end

    return IPRs,q,Sₑ
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Funções que analisam o comprimento de localização -----------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###############################################################################################################################################################################################################
########################################################################################## Primeira parte das funções #########################################################################################
###############################################################################################################################################################################################################


function get_all_ξ_and_R_CM(cloud::Cloud_SAIDA,Tipo_de_kernel::String,geometria::Dimension)
    
    if Tipo_de_kernel == "Escalar"

        # Aqui é definida uma variavel que representa o número de modos da nuvem
        n_modes = size(cloud.ψ,1)

    elseif Tipo_de_kernel == "Vetorial"

        # Aqui é definida uma variavel que representa o número de modos da nuvem
        # n_modes = trunc(Int64,size(cloud.ψ,1)/2)
        n_modes = size(cloud.ψ,1)
    end

    # Aqui é criada a matriz que vai guardar todos os comprimentos de localização
    ξₙ = zeros(n_modes) 


    # Aqui é definida a matriz que vai guardar o erro cometido ao aproximar o comprimento de localização que se comete em cada um dos modos
    Eₙ = zeros(n_modes)  

    if typeof(geometria) <: TwoD
        r_cm = zeros(n_modes,2)
    elseif typeof(geometria) <: ThreeD
        r_cm = zeros(n_modes,3)
    end

    # Loop responsavel por preencher as matrizes ξₙ Eₙ
    for i in 1:n_modes
        Dcm, ψₙ², r_cm[i, : ] = get_spatial_profile_one_mode(Tipo_de_kernel,cloud.r,cloud.ψ,i,geometria)

        Eₙ[i], ξₙ[i] = get_ξ_of_mode_n(Dcm, ψₙ²)
    end    
    

    # Retorna uma tuple, achei que assim fica mais condesado e melhor para me organizar
    return Eₙ ,ξₙ, r_cm
end


function get_spatial_profile_one_mode(Tipo_de_kernel::String,r::Array, ψ::Array,n_mode::Integer,geometria::TwoD)
    
    if Tipo_de_kernel == "Escalar"

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ = abs.(ψ[ : , n_mode]).^2


        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)


        #Aqui é definida uma matriz que vai guardar a distancia de todos os atomos com relação ao centro de massa
        Dcm = get_Distance_A_to_b(r, r_cm) 
    
    elseif Tipo_de_kernel == "Vetorial"

        N_atomos = size(r,1)

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos, n_mode ]).^2
        ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos + 1):end,n_mode]).^2

        ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾
        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)

        #Aqui é definida uma matriz que vai guardar a distancia de todos os atomos com relação ao centro de massa
        Dcm = get_Distance_A_to_b(r, r_cm) 

    end
    
    
    # Retorna a distancia dos atomos até o centro de massa da localização e o quadrado do autovetores associados ao modo 
    return Dcm, ψ²ₙ, r_cm
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function get_spatial_profile_one_mode(Tipo_de_kernel::String,r::Array, ψ::Array,n_mode::Integer,geometria::ThreeD)
    
    if Tipo_de_kernel == "Escalar"

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ = abs.(ψ[ : , n_mode]).^2


        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)


        #Aqui é definida uma matriz que vai guardar a distancia de todos os atomos com relação ao centro de massa
        Dcm = get_Distance_A_to_b(r, r_cm) 
    
    elseif Tipo_de_kernel == "Vetorial"

        N_atomos = size(r,1)

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        
        ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos, n_mode ]).^2
        ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos + 1):(2*N_atomos),n_mode]).^2
        ψ²ₙ⁽⁰⁾ = abs.(ψ[(2*N_atomos + 1):end,n_mode]).^2

        ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾ + ψ²ₙ⁽⁰⁾
        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)

        #Aqui é definida uma matriz que vai guardar a distancia de todos os atomos com relação ao centro de massa
        Dcm = get_Distance_A_to_b(r, r_cm) 
    end
    
    
    # Retorna a distancia dos atomos até o centro de massa da localização e o quadrado do autovetores associados ao modo 
    return Dcm, ψ²ₙ, r_cm
end



function get_CM_mode(r, Ψ²ₙ)
    
    return vec(sum(r.* Ψ²ₙ, dims = 1))

end




function get_Distance_A_to_b(A, b)
    
    n_rows = size(A, 1)                                                                                                         # Número de atomos 
    distanceAb = zeros(n_rows)
    @inbounds for n = 1:n_rows 
        distanceAb[n] = Distances.evaluate(Euclidean(), A[n, : ], b)                                                             # Uso do pacote Distantes
    end
    
    return distanceAb
end


###############################################################################################################################################################################################################
########################################################################### Segunda parte das funções de comprimento de localização ###########################################################################
###############################################################################################################################################################################################################




# Resolvendo o problema de otimização 
function get_ξ_of_mode_n(Dcm, ψ²ₙ)


    Dcm_ordenado,ψ²ₙ_ordenado = Organize_Matriz_in_Ordem_UP(Dcm, ψ²ₙ)

    atomos_uteis = select_atoms_goods(exp.(ψ²ₙ_ordenado))

    x = Dcm_ordenado[1:atomos_uteis]
    y = ψ²ₙ_ordenado[1:atomos_uteis]

    model(x, p) = x.*p[1] .+ p[2] 
    xdata = x
    ydata = y
    p0 = [-1.0, 1.0]
    
    fit = curve_fit(model, xdata, ydata, p0)

    a_sol = fit.param[1]                                                                                                                                                  # Coefiente angular da melhor reta 
    b_sol = fit.param[2]                                                                                                                                                  # Coefiente linear da melhor reta

    ξₙ = -1/a_sol
    
    # Eₙ =  stderror(fit)[1,1]
    # y_fit = model(xdata,fit.param) 
    y_fit_L₂ = xdata .* a_sol .+ b_sol
    R¹ = compute_R1(ydata,y_fit_L₂)
 

    if R¹ > 0.5

       Eₙ = R¹
    
    else

        # A_L₂, ξ_L₂ = get_exp_fit_coeffs_with_L2(xdata, ydata)
        y_fit_L₂ = xdata[:] .* a_sol .+ b_sol
        R² = compute_R2(ydata, y_fit_L₂)

        Eₙ = R²
    end

    # A função retorna o comprimento de localização e erro associado ao calculo dos coeficientes da reta
    return Eₙ, ξₙ
end


compute_R2(y, y_predict) = generic_R((abs.(y)), abs.(y_predict), SSE_R2, SST_R2)
compute_R1(y, y_predict) = generic_R(y, y_predict, SSE_R1, SST_R1)

SSE_R1(y, y_predict) = sum(abs.(y .- y_predict)) # the sum of squared error
SST_R1(y, average_y) = sum(abs.(y .- average_y)) # sum of squared total

SSE_R2(y, y_predict) = sum((y .- y_predict).^2) # the sum of squared error
SST_R2(y, average_y) = sum((y .- average_y).^2) # sum of squared total

function generic_R(y, y_predict, SSreta_fnc::Function, SSy_fnc::Function)
    if size(y) ≠ size(y_predict)
        @error("Inputs does not have similar dimension")
    end

    # average_y = sum(y) / length(y)
    y_medio = mean(y)

    SSreta = SSreta_fnc(y, y_predict)# the sum of squared error
    SSy = SSy_fnc(y, y_medio) # sum of squared total


    n = length(y)
    p = 2 # number of regression coefficients, for linear, p=2
    R2_adjusted = 1 - (SSreta / SSy)
    # R2_adjusted = 1 - (SSE / SST)

    # if R2_adjusted < 0
    #     R2_adjusted = 1e-16
    # end
    return R2_adjusted
end



function get_erro_ξ(x,y,a_sol,b_sol,atomos_uteis)
    
    erro = 0

    for i in 1:atomos_uteis

        erro = erro + ((y[i] - b_sol - a_sol*x[i])^2)/2
    
    end

    return erro

end
    


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------ Funções que analisam as caracteristicas da Nuvem -----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------- Funções que selecionam e analisam o melhor modo gerado pela nuvem --------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_best_mode(cloud::Cloud_SAIDA,caracteristicas_fisicas_global::Characteristics_of_the_modes_SAIDA,Tipo_de_kernel::String,Geometria::Dimension)

    # Define entre os N modes aquele com o maior Tempo de vida, que é o melhor modo
    RG_best = sortperm(caracteristicas_fisicas_global.IPRs)[end]                  
    
    N = size(cloud.r,1)

    # Propriedades do melhor modo 
    IPRᵦ = caracteristicas_fisicas_global.IPRs[RG_best]                                                                                                          # Indice de parcipação do melhor modo
    ξᵦ = caracteristicas_fisicas_global.ξₙ[RG_best]                                                                                                              # Comprimento de localização do melhor modo  
    Eᵦ = caracteristicas_fisicas_global.Eₙ[RG_best]                                                                                                              # Erro cometido no comprimento de localização do melhor modo
    γᵦ = caracteristicas_fisicas_global.γₙ[RG_best]                                                                                                              # Inverso do Tempo de vida do melhor modo
    ωᵦ = caracteristicas_fisicas_global.ωₙ[RG_best]                                                                                                              # Frequencia do melhor modo
    qᵦ = caracteristicas_fisicas_global.q[RG_best]                                                                                                              # Tempo de vida do melhor modo
    Sₑᵦ = caracteristicas_fisicas_global.Sₑ[RG_best]                                                                                                              # Tempo de vida do melhor modo

    Dcmᵦ,ψᵦ² = get_spatial_profile_one_mode(Tipo_de_kernel,cloud.r,cloud.ψ,RG_best,Geometria)    
    Rcmᵦ = get_Rcm_best_mode(cloud.r,cloud.ψ,RG_best,Tipo_de_kernel,Geometria)                                                                                                      # Centro massa do melhor modo

    # Retorna as propriedades do melhor modo e seu Registro Geral para que seja possivel localiza-lo
    return get_best_mode_SAIDA(IPRᵦ, ξᵦ, Eᵦ, γᵦ, ωᵦ, qᵦ, Sₑᵦ, Dcmᵦ, ψᵦ², Rcmᵦ, RG_best)
end


function get_Rcm_best_mode(r::Array, ψ::Array, best_mode::Integer,Tipo_de_kernel,Geometria::TwoD)
    
    if Tipo_de_kernel == "Escalar"

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ = abs.(ψ[ : , best_mode]).^2


        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)

    elseif Tipo_de_kernel == "Vetorial"
        
        N_atomos = size(r,1)

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos , best_mode]).^2
        ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos+1):end ,best_mode]).^2

        ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾
        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)
    end
    
    return r_cm
end


function get_Rcm_best_mode(r::Array, ψ::Array, best_mode::Integer,Tipo_de_kernel,Geometria::ThreeD)
    
    if Tipo_de_kernel == "Escalar"

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ = abs.(ψ[ : , best_mode]).^2


        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)

    elseif Tipo_de_kernel == "Vetorial"
        
        N_atomos = size(r,1)

        # É isolodado o autovetor corresponde ao modo e todos os seus termos são elevados ao guadrado
        ψ²ₙ⁽⁻⁾ = abs.(ψ[1:N_atomos, best_mode ]).^2
        ψ²ₙ⁽⁺⁾ = abs.(ψ[(N_atomos + 1):(2*N_atomos),best_mode]).^2
        ψ²ₙ⁽⁰⁾ = abs.(ψ[(2*N_atomos + 1):end,best_mode]).^2
        ψ²ₙ = ψ²ₙ⁽⁻⁾ + ψ²ₙ⁽⁺⁾ + ψ²ₙ⁽⁰⁾
        
        #Chama uma função responsavel por encontrar a localização do centro de massa do sistema
        r_cm = get_CM_mode(r, ψ²ₙ)
    end
    
    return r_cm
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------- Funções que fazem a otimização do fit que determina o comprimento de localização -----------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function get_a_and_b_to_best_mode(Dcm, ψ²ₙ)

    Dcm_ordenado,ψ²ₙ_ordenado = Organize_Matriz_in_Ordem_UP(Dcm, ψ²ₙ)

    atomos_uteis = select_atoms_goods(exp.(ψ²ₙ_ordenado))

    x = Dcm_ordenado[1:atomos_uteis]
    y = ψ²ₙ_ordenado[1:atomos_uteis]

    model(x, p) = x.*p[1] .+ p[2] 
    xdata = x
    ydata = y
    p0 = [-1.0,1.0]
    
    fit = curve_fit(model, xdata, ydata, p0)

    a_sol = fit.param[1]                                                                                                                                                  # Coefiente angular da melhor reta 
    b_sol = fit.param[2]                                                                                                                                                  # Coefiente linear da melhor reta


    return a_sol, b_sol,atomos_uteis,Dcm_ordenado,exp.(ψ²ₙ_ordenado)
end


function Organize_Matriz_in_Ordem_UP(Dcm,ψ²ₙ)

    N = size(Dcm,1)
    Dcm_ordenado = sort(Dcm)
    ψ²ₙ_ordenado = zeros(N)
    ordem = sortperm(Dcm)

    for i in 1:N
        ψ²ₙ_ordenado[i] = log(ψ²ₙ[ordem[i]]) 
    end

    return Dcm_ordenado,ψ²ₙ_ordenado
end 



# probabilidade_ideal = 0.999999995
# probabilidade_ideal = 0.8

function select_atoms_goods(Probabilidades;probabilidade_ideal = 0.99999999995)

    N = size(Probabilidades,1)

    total_prob = sum(Probabilidades)
    probabilidades_of_points = Probabilidades*(1/(total_prob))    

    contador = 1
    fator_soma = 0

    while true  
        
        fator_soma = fator_soma + probabilidades_of_points[contador] 

        if fator_soma >= probabilidade_ideal
            break
        else
            contador +=  1        
        end
    end

    return contador
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Funções que classificam os modos ----------------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function get_modulo_R_cm(Positions_RCM)
    
    n_modes = size(Positions_RCM,1)
    M_Rcm = zeros(n_modes)

    for i in 1:n_modes
        
        M_Rcm[i] = sqrt((Positions_RCM[i,1]^2)+(Positions_RCM[i,2]^2))
    end

    return M_Rcm
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Funções que classificam os modos ----------------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function get_propriedade_to_specific_mode(cloud::Cloud_SAIDA,caracteristicas_fisicas_global::Characteristics_of_the_modes_SAIDA,Tipo_de_kernel::String,RG_modo::Int64,Geometria::Dimension)
    
    N = size(cloud.r,1)


    # Propriedades do melhor modo 
    IPRᵦ = caracteristicas_fisicas_global.IPRs[RG_best]                                                                                                          # Indice de parcipação do melhor modo
    ξᵦ = caracteristicas_fisicas_global.ξₙ[RG_best]                                                                                                              # Comprimento de localização do melhor modo  
    Eᵦ = caracteristicas_fisicas_global.Eₙ[RG_best]                                                                                                              # Erro cometido no comprimento de localização do melhor modo
    γᵦ = caracteristicas_fisicas_global.γₙ[RG_best]                                                                                                              # Inverso do Tempo de vida do melhor modo
    ωᵦ = caracteristicas_fisicas_global.ωₙ[RG_best]                                                                                                              # Frequencia do melhor modo
    qᵦ = caracteristicas_fisicas_global.q[RG_best]                                                                                                              # Tempo de vida do melhor modo
    Sₑᵦ = caracteristicas_fisicas_global.Sₑ[RG_best]                                                                                                              # Tempo de vida do melhor modo
    Dcmᵦ,ψᵦ² = get_spatial_profile_one_mode(Tipo_de_kernel,cloud.r,cloud.ψ,RG_modo,Geometria)    
    Rcmᵦ = get_Rcm_best_mode(cloud.r,cloud.ψ,RG_modo,Tipo_de_kernel,Geometria)                                                                                                      # Centro massa do melhor modo

    # Retorna as propriedades do melhor modo e seu Registro Geral para que seja possivel localiza-lo
    return get_best_mode_SAIDA(IPRᵦ, ξᵦ, Eᵦ, γᵦ, ωᵦ, qᵦ,Sₑᵦ,Dcmᵦ,ψᵦ²,Rcmᵦ,RG_modo)
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Testes --------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# A = [1,61,5,8,6,1,71,57,73,45,7,5,2,5] 
# B = [1,4,3,5,3,2,5,6,4,3,2,4,5,3]

# H,T = Organize_Matriz_in_Ordem_UP(A,B,90)

# println(H)
# println(T) 









#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------- Lixeira --------------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
