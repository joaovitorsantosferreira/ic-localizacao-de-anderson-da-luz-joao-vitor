#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_4(Variaveis_de_Entrada::R4_ENTRADA)
    
    N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.L,Variaveis_de_Entrada.Radius,Variaveis_de_Entrada.Lₐ,Variaveis_de_Entrada.ρ,Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)

    entrada_entratora = E3_ENTRADA(
        Variaveis_de_Entrada.Γ₀, 
        Variaveis_de_Entrada.k,
        N,
        Variaveis_de_Entrada.Radius,
        Variaveis_de_Entrada.ρ,
        rₘᵢₙ,
        0,
        Variaveis_de_Entrada.Tipo_de_kernel,
        Variaveis_de_Entrada.Desordem_Diagonal,
        Variaveis_de_Entrada.W,
        Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_Entrada.L,
        Variaveis_de_Entrada.Lₐ,
        Variaveis_de_Entrada.Geometria            
    )
    
    tuplas_resultados = pmap(1:Variaveis_de_Entrada.N_Realizações; retry_delays = zeros(3)) do REALI
        valor_retorno = E3(entrada_entratora)
    end

    ξ_total,ω_total = Any[],Any[]


    for n in 1:Variaveis_de_Entrada.N_Realizações
        append!(ξ_total,tuplas_resultados[n][1]) 
        append!(ω_total,tuplas_resultados[n][2]) 
    end

    Range_Δ = range(Variaveis_de_Entrada.Δ_Inicial,Variaveis_de_Entrada.Δ_Final,length=Variaveis_de_Entrada.N_div_Deturn)     
    
    ξ_medio = zeros(Variaveis_de_Entrada.N_div_Deturn)

    for i in 1:Variaveis_de_Entrada.N_div_Deturn
       
        in_range = findall((ω_total .> (Range_Δ[i]-Variaveis_de_Entrada.Γ₀/4)).*(ω_total .< (Range_Δ[i]+Variaveis_de_Entrada.Γ₀/4)).*(ξ_total .> 0))
        
        ξ_range = ξ_total[in_range]
        ξ_range_ordenado = sort(ξ_range)
        percent = round(Int64,size(ξ_range_ordenado,1)/10)
        ξ_medio[i] = mean(ξ_range_ordenado[1:percent])

    end

    return Range_Δ,ξ_medio
end


