#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_5(Variaveis_de_Entrada::R5_ENTRADA)
    
    Range_ρ = range(Densidade_Inicial,Densidade_Final,length = N_div_Densidade)


    Q_Dados = N_div_Deturn*N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,3) 
    aux_1 = 0

    ProgressMeter.@showprogress 2 "Diagrama - Comprimento de LOC ===>" for i in 1:N_div_Densidade
    
        entrada_rotina = R4_ENTRADA(
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.k,
            Range_ρ[i],
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.Radius,
            Variaveis_de_Entrada.N_Realizações,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.N_div_Densidade,
            Variaveis_de_Entrada.Δ_Inicial,
            Variaveis_de_Entrada.Δ_Final,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.Geometria,
        )
        
        Δ_temporario,ξ_temporario = ROTINA_4(entrada_rotina)
    
        Dados_Resultantes[(1+aux_1):(N_div_Deturn + aux_1),1] = Δ_temporario
        Dados_Resultantes[(1+aux_1):(N_div_Deturn + aux_1),2] .= Range_ρ[i] 
        Dados_Resultantes[(1+aux_1):(N_div_Deturn + aux_1),3] = ξ_temporario
        aux_1 +=  N_div_Deturn
        save("TEMPORARIO_C6.jld2", Dict("SAIDA" => Dados_Resultantes,"Parametros" => Variaveis_de_Entrada))

    end

    return Dados_Resultantes
end
    



































