#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_22(Variaveis_de_Entrada::R22_ENTRADA)
    

    ξ_medio = zeros(Variaveis_de_Entrada.N_div_Densidade,size(Variaveis_de_Entrada.Ls,1))
    Range_ρ = get_points_in_log_scale(Variaveis_de_Entrada.ρ_Inicial,Variaveis_de_Entrada.ρ_Final,Variaveis_de_Entrada.N_div_Densidade)     
    aux_1 = 1
    
    for L_Instantaneo in Variaveis_de_Entrada.Ls

        ProgressMeter.@showprogress 2 "Comprimento de LOC Minimo L={$L_Instantaneo} ===>" for i in 1:Variaveis_de_Entrada.N_div_Densidade

            N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(L_Instantaneo,0,Variaveis_de_Entrada.Lₐ,Range_ρ[i],"SLAB")

            entrada_entratora = E3_ENTRADA(
                Variaveis_de_Entrada.Γ₀, 
                Variaveis_de_Entrada.k,
                N,
                0,
                Range_ρ[i],
                rₘᵢₙ,
                Variaveis_de_Entrada.Δ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                "SLAB",
                L_Instantaneo,
                Variaveis_de_Entrada.Lₐ,
                Variaveis_de_Entrada.Geometria            
            )
            
            tuplas_resultados = pmap(1:Variaveis_de_Entrada.N_Realizações; retry_delays = zeros(3)) do REALI
                valor_retorno = E3(entrada_entratora)
            end

            ξ_total = Any[]

            for n in 1:Variaveis_de_Entrada.N_Realizações
                append!(ξ_total,tuplas_resultados[n][1]) 
            end

            ξ_range_ordenado = sort(ξ_total)
            percent = round(Int64,size(ξ_range_ordenado,1)/10)
            ξ_medio[i,aux_1] = mean(ξ_range_ordenado[1:percent])
            save("TEMPORARIO_C5.jld2", Dict("SAIDA" => (Range_ρ,ξ_medio),"Parametros" => Variaveis_de_Entrada))
        end
        aux_1 += 1
    end

    return Range_ρ,ξ_medio
end

