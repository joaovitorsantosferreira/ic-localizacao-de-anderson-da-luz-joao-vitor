###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_1(Variaveis_de_entrada::R1_ENTRADA)

    
    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"


        if typeof(Variaveis_de_entrada.Geometria) <: TwoD 
            Intensidade_Resultante_PURA = [Any[],Any[]]
        elseif typeof(Variaveis_de_entrada.Geometria) <: ThreeD 
            Intensidade_Resultante_PURA = [Any[],Any[],Any[]]
        end

    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        Intensidade_Resultante_PURA = [Any[],Any[],Any[],Any[]]

    else 
    
        return "NÃO PODE!!!!"
    end
 
    entrada_Extratora = E2_ENTRADA(
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.ωₐ,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.N,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.ρ,
        Variaveis_de_entrada.rₘᵢₙ,
        Variaveis_de_entrada.Angulo_da_luz_incidente,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.λ,
        Variaveis_de_entrada.ωₗ,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_campo,
        Variaveis_de_entrada.N_Sensores,
        Variaveis_de_entrada.Distancia_dos_Sensores,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
        Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
        Variaveis_de_entrada.Tipo_de_beta,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.W,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.L,
        Variaveis_de_entrada.Lₐ,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        1,
        Variaveis_de_entrada.Geometria
    )

    tuplas_resultados = ProgressMeter.@showprogress 1 "Extraido Estatisticas da Luz===>" pmap(1:Variaveis_de_entrada.N_Realizações; retry_delays = zeros(3)) do R_instantaneo
        valor_retorno = E2(entrada_Extratora)                                                                                                     
    end

    for n in 1:N_Realizações

        if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

            if typeof(Variaveis_de_entrada.Geometria) <: TwoD 

                append!(Intensidade_Resultante_PURA[1],tuplas_resultados[n][1, : ]) 
                append!(Intensidade_Resultante_PURA[2],tuplas_resultados[n][2, : ]) 
    
            elseif typeof(Variaveis_de_entrada.Geometria) <: ThreeD 
                append!(Intensidade_Resultante_PURA[1],tuplas_resultados[n][1, : ]) 
                append!(Intensidade_Resultante_PURA[2],tuplas_resultados[n][2, : ]) 
                append!(Intensidade_Resultante_PURA[3],tuplas_resultados[n][3, : ])     
            end
    
        elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
    
            append!(Intensidade_Resultante_PURA[1],tuplas_resultados[n][1, : ]) 
            append!(Intensidade_Resultante_PURA[2],tuplas_resultados[n][2, : ]) 
            append!(Intensidade_Resultante_PURA[3],tuplas_resultados[n][3, : ]) 
            append!(Intensidade_Resultante_PURA[4],tuplas_resultados[n][4, : ]) 

        end
    end


    N_total_de_Sensores = Variaveis_de_entrada.N_Realizações*Variaveis_de_entrada.N_Sensores

    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

        if typeof(Variaveis_de_entrada.Geometria) <: TwoD 

            Intensidade_Resultante_matriz = zeros(2,N_total_de_Sensores)
            Intensidade_Resultante_matriz[1,:] = Intensidade_Resultante_PURA[1]
            Intensidade_Resultante_matriz[2,:] = Intensidade_Resultante_PURA[2]

        elseif typeof(Variaveis_de_entrada.Geometria) <: ThreeD 
            Intensidade_Resultante_matriz = zeros(3,N_total_de_Sensores)
            Intensidade_Resultante_matriz[1,:] = Intensidade_Resultante_PURA[1]
            Intensidade_Resultante_matriz[2,:] = Intensidade_Resultante_PURA[2]
            Intensidade_Resultante_matriz[3,:] = Intensidade_Resultante_PURA[3]

        end

    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
        Intensidade_Resultante_matriz = zeros(4,N_total_de_Sensores)
        Intensidade_Resultante_matriz[1,:] = Intensidade_Resultante_PURA[1]
        Intensidade_Resultante_matriz[2,:] = Intensidade_Resultante_PURA[2]
        Intensidade_Resultante_matriz[3,:] = Intensidade_Resultante_PURA[3]
        Intensidade_Resultante_matriz[4,:] = Intensidade_Resultante_PURA[4]

    end


    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

        # Intensidade_Resultante_NORMALIZADA = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        # Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        # Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        # variancia = get_variancia(Intensidade_Resultante_NORMALIZADA)
    
        # return R1_SAIDA_PROBABILIDADE(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Histograma_LINEAR,Histograma_LOG,variancia)
        return Intensidade_Resultante_matriz
        
    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_entrada.Geometria) 
    
        

        Intensidade_Resultante_NORMALIZADA = zeros(2,Q_total)
        Intensidade_Resultante_NORMALIZADA[1, : ] = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        Intensidade_Resultante_NORMALIZADA[2, : ] = Intensidade_Resultante_PURA[3 , : ]/mean(Intensidade_Resultante_PURA[3 , : ])

        r_exemplo = getAtoms_distribution(getAtoms_distribution_ENTRADA(Variaveis_de_entrada.N, Variaveis_de_entrada.Radius, Variaveis_de_entrada.rₘᵢₙ,Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,Variaveis_de_entrada.L,Variaveis_de_entrada.Lₐ),Variaveis_de_entrada.Geometria)                                                                   # Gera o disco e guarda as posições em r

        return R1_SAIDA_PERFIL(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Posição_Sensores,r_exemplo)

    end
end
