#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_17__Diagrama_de_Fase_Estatisticas_gc(Variaveis_de_Entrada::R17_ENTRADA)
  
    N_div_Densidade = Variaveis_de_Entrada.N_div_Densidade
    N_div_Deturn = Variaveis_de_Entrada.N_div_Deturn
    Densidade_Inicial = Variaveis_de_Entrada.Densidade_Inicial
    Densidade_Final = Variaveis_de_Entrada.Densidade_Final
    Variavel_Constante = Variaveis_de_Entrada.Variavel_Constante
    Escala_de_Densidade = Variaveis_de_Entrada.Escala_de_Densidade
    N_Realizações_gc = Variaveis_de_Entrada.N_Realizações_gc


    Q_Dados = N_div_Deturn*N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,7) 
    Estatisticas_gc = zeros(N_div_Deturn, N_div_Densidade, N_Realizações_gc) 
    aux_1 = 1

    if Escala_de_Densidade == "LOG"

        Range_ρ = get_points_in_log_scale(Densidade_Inicial,Densidade_Final,N_div_Densidade) 
    
    elseif Escala_de_Densidade == "LINEAR"    

        Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade) 
    
    end

    
    if Variavel_Constante == "N"

        ProgressMeter.@showprogress 2 "Estatisticas de gc Global ===>" for i in 1:N_div_Densidade

            # Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.N,Range_ρ[i],Variaveis_de_Entrada.Lₐ,Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)

            N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.L,Variaveis_de_Entrada.Radius,Variaveis_de_Entrada.Lₐ,Range_ρ[i],Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)
            
            if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "ESFERA"
                Distancia_dos_Sensores = 10*Radius                                         
            elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "CUBO"
                Distancia_dos_Sensores = 100*(sqrt(2)/2)*L                                         
            elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "TUBO"
                Distancia_dos_Sensores = 100*sqrt( (L/2)^2 + Variaveis_de_Entrada.Lₐ^2)
            end
            
            entrada_rotina = R16_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                Variaveis_de_Entrada.Radius,
                Range_ρ[i],
                rₘᵢₙ,
                Variaveis_de_Entrada.angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.N_Sensores,
                Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.N_div_Deturn,
                Variaveis_de_Entrada.N_Realizações_gc,
                Variaveis_de_Entrada.N_Realizações_Intensidade,
                Variaveis_de_Entrada.Δ_Inicial,
                Variaveis_de_Entrada.Δ_Final,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.Geometria,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ
            )
        
            media_gc_TEMP,median_gc_TEMP,desvio_gc_TEMP,desvio_r_TEMP,condutancias_Von_Rossum_TEMP,condutancia_florent = ROTINA_16__Estatisticas_gc_Densidade_FIXA(entrada_rotina)
            
            ρ_normalizados = fill(Range_ρ[i],(N_div_Deturn,1))
            Deturn_temporario = range(Variaveis_de_Entrada.Δ_Inicial,Variaveis_de_Entrada.Δ_Final,length = N_div_Deturn)
        
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),1] = Deturn_temporario
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),2] = ρ_normalizados
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),3] = media_gc_TEMP 
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),4] = median_gc_TEMP 
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),5] = desvio_gc_TEMP
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),6] = desvio_r_TEMP
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),7] = condutancia_florent


            for k in 1:N_div_Deturn
                Estatisticas_gc[k,i,:] = condutancias_Von_Rossum_TEMP[:,k]
            end
            save("TEMPORARIO_C18.jld2", Dict("Estatisticas" => Estatisticas_gc))


            aux_1 +=  N_div_Deturn
        end
    end

    return Dados_Resultantes,Estatisticas_gc
end






























