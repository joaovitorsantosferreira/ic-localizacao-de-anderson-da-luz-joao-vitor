#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_16__Estatisticas_gc_Densidade_FIXA(Variaveis_de_entrada::R16_ENTRADA)
    

    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final

    media_gc,median_gc,desvio_gc,desvio_r,condutancia_florent = zeros(N_div_Deturn),zeros(N_div_Deturn),zeros(N_div_Deturn),zeros(N_div_Deturn),zeros(N_div_Deturn)
    condutancias_TOTAIS_Δ = zeros(Variaveis_de_entrada.N_Realizações_gc,N_div_Deturn)

    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)
    media_gc,median_gc,desvio_gc

    for y in 1:N_div_Deturn
        condutancias_Von_Rossum = zeros(Variaveis_de_entrada.N_Realizações_gc)
        intensidades = Any[]
        Δ_INT = range_Δ[y]
        ρ_Instantaneo = Variaveis_de_entrada.ρ
        
        entrada_extratora = E2_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            ρ_Instantaneo,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            Δ_INT,
            Δ_INT,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            "NAO",
            1,
            Variaveis_de_entrada.Geometria
        )

        tuplas_resultados = pmap(1:Variaveis_de_entrada.N_Realizações_gc; retry_delays = zeros(3)) do REALI

            All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora) 
                                                                                                
            Intensidade_NORMALIZADA = All_Intensitys[1,:]/mean(All_Intensitys[1,:])
            Histograma = get_dados_histograma_LINEAR(Intensidade_NORMALIZADA[ : ],30)    
            condutancias_Von_Rossum_TEMP = get_gc_Von_Rossum(Histograma)
            
            valor_retorno = (van_rossum = condutancias_Von_Rossum_TEMP, intensidade = All_Intensitys[1,:])
        end   


        for n in 1:Variaveis_de_entrada.N_Realizações_gc
            condutancias_Von_Rossum[n] = tuplas_resultados[n].van_rossum
            append!(intensidades,tuplas_resultados[n].intensidade) 
        end
        
        Intensidade_NORMALIZADA = intensidades/mean(intensidades)
        Histograma = get_dados_histograma_LINEAR(Intensidade_NORMALIZADA[ : ],30)     


        condutancia_florent[y] = get_gc_Von_Rossum(Histograma)
        media_gc[y] = mean(condutancias_Von_Rossum)
        median_gc[y] = median(condutancias_Von_Rossum)
        desvio_gc[y] = std(condutancias_Von_Rossum)
        desvio_r[y] =  std(condutancias_Von_Rossum)/mean(condutancias_Von_Rossum)
        condutancias_TOTAIS_Δ[:,y] = condutancias_Von_Rossum
    end

    return media_gc,median_gc,desvio_gc,desvio_r,condutancias_TOTAIS_Δ,condutancia_florent
end

