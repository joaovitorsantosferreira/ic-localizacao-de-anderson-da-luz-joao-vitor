###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_15__Estatisticas_de_gc(Variaveis_de_Entrada::R15_ENTRADA)
    
    N_Realizações_Intensidade = Variaveis_de_Entrada.N_Realizações_Intensidade
    N_Realizações_gc = Variaveis_de_Entrada.N_Realizações_gc
    N_div = Variaveis_de_Entrada.N_div

    condutancias_Von_Rossum = zeros(N_Realizações_gc)
    

    ProgressMeter.@showprogress 1 "Extraido Estatisticas de gc ===>" for j in 1:N_Realizações_gc

        tuplasResultados = pmap(1:N_Realizações_Intensidade) do R_instantaneo

            entrada_Extratora = E2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.N,
                Variaveis_de_Entrada.Radius,
                Variaveis_de_Entrada.ρ,
                Variaveis_de_Entrada.rₘᵢₙ,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                Variaveis_de_Entrada.ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.ωₗ,
                Variaveis_de_Entrada.Δ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                "NAO",
                1,
                Variaveis_de_Entrada.Geometria
            )
    
            All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)                                                                                                   
           
            A = isnan.(All_Intensitys[1,:])
            Intensidade = Any[]
            for i in 1:size(All_Intensitys[1,:],1)
                if A[i] == false
                append!(Intensidade,All_Intensitys[1,i]) 
                end
            end

            valor_retorno = Intensidade
        end
        Intensidade_Resultante_PURA = Any[]

        for n in 1:N_Realizações_Intensidade
            append!(Intensidade_Resultante_PURA,tuplasResultados[n])
        end

        Intensidade_NORMALIZADA = Intensidade_Resultante_PURA/mean(Intensidade_Resultante_PURA)
        Histograma = get_dados_histograma_LINEAR(Intensidade_NORMALIZADA[ : ],N_div)    
        condutancias_Von_Rossum[j] = get_gc_Von_Rossum_PARALELO(Histograma)
    end

    return condutancias_Von_Rossum
end

function get_dados_histograma_LINEAR(Intensidade_Normalizada,N_div)

    sep = maximum(Intensidade_Normalizada)/N_div
    bins = 0:sep:(maximum(Intensidade_Normalizada))
    h = fit(Histogram, Intensidade_Normalizada,bins) 

    return h
end

function get_dados_histograma_LOG(Intensidade_Normalizada,N_div)
    a = minimum(Intensidade_Normalizada)
    b = maximum(Intensidade_Normalizada)

    bins = get_points_in_log_scale(a,b,N_div)
    h = fit(Histogram, Intensidade_Normalizada[ : ],bins)

    return h
end


function get_variancia(Intensidade_Normalizada)

    variancia_sim = mean(Intensidade_Normalizada.^2) - mean(Intensidade_Normalizada)^2
    # variancia_sim = Statistics.var(Intensidade_Normalizada,corrected=:false)

    return variancia_sim
end

function get_points_in_log_scale(Valor_Inicial,Valor_Final,N_div)
    
    a = log10(Valor_Inicial)
    b = log10(Valor_Final)

    sep = abs(b-a) / (N_div-1)

    aux_1 = 1
    pontos = zeros(N_div)

    for i in 0:N_div-1
        pontos[aux_1] = 10.0^(a + sep*i)
        aux_1 += 1
    end

    return pontos
end
