#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_25(Variaveis_de_Entrada::R25_ENTRADA,Geometria::TwoD)
    
      
    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    W_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
    W_FINAL = Variaveis_de_Entrada.RANGE_FINAL
    N_pontos = Variaveis_de_Entrada.N_pontos
    N_Realizações = Variaveis_de_Entrada.N_Realizações
    PERFIL_DA_INTENSIDADE = "NAO"
    
    Lₐ = Variaveis_de_Entrada.Lₐ
    L = Variaveis_de_Entrada.L
    ρs = Variaveis_de_Entrada.ρs

    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_w = get_points_in_log_scale(W_INICIAL,W_FINAL,N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Ws = range(W_INICIAL,W_FINAL,length=N_pontos) 
        Range_w = zeros(N_pontos)
        @.Range_w = Ws
    end

    N_Curvas = size(Variaveis_de_Entrada.ρs,1)
    σ²_total = zeros(N_pontos,N_Curvas+1)

    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#

        
    tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_w) do W_instantaneo

        ω₀ = W_instantaneo
        
        entrada_Sensores = get_cloud_sensors_ENTRADA(
            1,
            N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
            "SLAB",
            L,
            Lₐ,
            PERFIL_DA_INTENSIDADE,
            1
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

        Intensidade_PURA = zeros(N_Sensores)
        for i in 1:N_Sensores
            Intensidade_PURA[i] = get_eletric_field_LG_modulo_quadrado(Sensores[i,:],Variaveis_de_Entrada.Ω,W_instantaneo,Variaveis_de_Entrada.k,Geometria)
        end

        referencia = zeros(N_Sensores)
        for i in 1:N_Sensores
            θ =  get_degree_azimut(Sensores[i,1:2])
            if θ > 180
                θ = θ - 360
            end
            referencia[i] = θ
        end
        
        # σ² = get_one_σ_lorentz(referencia,Intensidade_PURA,Variaveis_de_Entrada.Geometria)[2]
        σ² = sum((referencia.^2).*Intensidade_PURA)/(sum(Intensidade_PURA))
        
        valor_retorno = σ²,Intensidade_PURA
    end

    Intensidade_PURA_Transmissão = zeros(N_pontos,N_Sensores)

    for n in 1:N_pontos
        σ²_total[n,end] = tuplasResultados_caso_vazio[n][1]
        Intensidade_PURA_Transmissão[n,:] = tuplasResultados_caso_vazio[n][2]
    end

    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#

    T = Array{BigFloat}(undef, N_pontos, N_Curvas)
    T .= 0

    model_energia(x, p) = log.(x).*p[1] .+ p[2] 
    xdata = load("DADOS_ENERGIA.jld2", "SAIDA")[1]
    ydata = load("DADOS_ENERGIA.jld2", "SAIDA")[2]
    p0 = [0.25, 1.0]

    fitting_energia = curve_fit(model_energia, xdata, ydata, p0)

    a_energia = fitting_energia.param[1] 
    b_energia = fitting_energia.param[2]        


    for j in 1:N_Curvas
        
        ρ = ρs[j]

        if Variaveis_de_Entrada.Tipo_de_Δ == "PURO"
            Δ = Variaveis_de_Entrada.Δ
        elseif Variaveis_de_Entrada.Tipo_de_Δ == "INTERPOLAÇÃO"
            if ρ < 0.1           
                Δ = a_energia*log(ρ) + b_energia
            else
                itp = LinearInterpolation(xdata,ydata)
                Δ = itp(ρ)
            end
        end

        ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$ρ}===>" for W_instantaneo in  Range_w  

            Intensidade_Resultante_PURA = Any[]

            rₘᵢₙ = 1/(3*sqrt(ρ))
            N = round(Int64,ρ*L*Lₐ)

            if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"            
                b₀ =  (4*N)/(L)
            elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"        
                b₀ =  (8*N)/(L)
            end
            
            entrada_extratora = E2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                1,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                W_instantaneo,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.ωₗ,
                Δ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.Tipo_de_campo,
                N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W*b₀,
                "SLAB",
                L,
                Lₐ,
                PERFIL_DA_INTENSIDADE,
                1,
                Variaveis_de_Entrada.Geometria
            )

            tuplas_intensidade = pmap(1:N_Realizações; retry_delays = zeros(3)) do N_Realização
                All_Intensitys = E2(entrada_extratora)
                
                valor_retorno = All_Intensitys[1 , : ]
            end

            for n in 1:N_Realizações
                append!(Intensidade_Resultante_PURA,tuplas_intensidade[n])               
            end
            tuplas_intensidade = 0

            Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
        
            entrada_Sensores = get_cloud_sensors_ENTRADA(
                1,
                N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                "SLAB",
                L,
                Lₐ,
                PERFIL_DA_INTENSIDADE,
                1
            )
            
            Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 
            
            referencia = zeros(N_Sensores)
            for i in 1:N_Sensores
                θ =  get_degree_azimut(Sensores[i,1:2])
                if θ > 180
                    θ = θ - 360
                end
                referencia[i] = θ
            end
                            
            σ²_total[findall(Range_w .== W_instantaneo)[1],j] = get_one_σ_lorentz(referencia,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            T[findall(Range_w .== W_instantaneo)[1],j] = get_Tramission_Cherroret(Intensidade_Resultante_PURA,Intensidade_PURA_Transmissão[findall(Range_w .== W_instantaneo)[1],:],Sensores,Variaveis_de_Entrada.angulo_coerente,N_Realizações,Variaveis_de_Entrada.Geometria)

            save("TEMPORARIO_C25.jld2", Dict("SAIDA" => (σ²,T,Range_w),"Parametros" => Variaveis_de_Entrada))
        end
    end
    
    return σ²_total,T,Range_w
end