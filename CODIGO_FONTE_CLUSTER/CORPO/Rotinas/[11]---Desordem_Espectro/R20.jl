#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_20__Espectro_Desordem(Variaveis_de_Entrada::R20_ENTRADA,Geometria::TwoD)
    
    IPRs_PURO = Any[]
    Espectros_PURO = Any[]
    
    entrada_cloud = Cloud_ENTRADA(
        Variaveis_de_Entrada.N,
        Variaveis_de_Entrada.ρ,
        Variaveis_de_Entrada.k,
        Variaveis_de_Entrada.Γ₀,
        Variaveis_de_Entrada.Δ,
        Variaveis_de_Entrada.Radius,
        Variaveis_de_Entrada.rₘᵢₙ,
        Variaveis_de_Entrada.Tipo_de_kernel,
        "PRESENTE",
        Variaveis_de_Entrada.W,
        Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_Entrada.L,
        Variaveis_de_Entrada.Lₐ,
        Variaveis_de_Entrada.Geometria
    )  

    tuplas_resultados = ProgressMeter.@showprogress 2 "Calculo_PR_b₀, b₀ ===>" pmap(1:Variaveis_de_Entrada.N_Realizações; retry_delays = zeros(3)) do REALI
                        
        cloud = Cloud_COMPLETO(entrada_cloud)
    
        IPRs,q,Sₑ = get_Statistics_profile_of_modes(cloud.ψ,Variaveis_de_Entrada.Tipo_de_kernel,Geometria) 
        
        valor_retorno = (IPR = IPRs,λ = cloud.λ)
    end

    for n in 1:Variaveis_de_Entrada.N_Realizações                                    
        append!(IPRs_PURO,tuplas_resultados[n].IPR) 
        append!(Espectros_PURO,tuplas_resultados[n].λ) 
    end

    γ = real.(Espectros_PURO)
    ω = imag.(Espectros_PURO)    
    
    return γ,ω,IPRs_PURO
end

