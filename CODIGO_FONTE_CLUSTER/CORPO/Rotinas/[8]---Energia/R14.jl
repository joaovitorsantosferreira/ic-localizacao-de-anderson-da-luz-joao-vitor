function ROTINA_14(Variaveis_de_Entrada::R14_ENTRADA)


    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_ρ = get_points_in_log_scale(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,Variaveis_de_Entrada.N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Range_ρ = range(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,length=Variaveis_de_Entrada.N_pontos) 
    
    end

    Δₙ_Localizado = zeros(Variaveis_de_Entrada.N_pontos)
    N_Realizações = Variaveis_de_Entrada.N_Realizações

    ProgressMeter.@showprogress 2 "Energia Modos Localizados ==>" for i in 1:Variaveis_de_Entrada.N_pontos  

        Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.N,Range_ρ[i],Variaveis_de_Entrada.Lₐ,Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)

            
        entrada_extratora = E10_ENTRADA(
            Variaveis_de_Entrada.N,
            Range_ρ[i],
            Variaveis_de_Entrada.k,
            Variaveis_de_Entrada.Γ₀,
            0,
            Radius,
            rₘᵢₙ,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            L,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.Geometria,
        )


        tuplasResultados = pmap(1:N_Realizações; retry_delays = zeros(3)) do N_Realizações

            dados = E10(entrada_extratora)

            IPRs = dados.IPRs
            Δₙ = dados.ωₙ 
            R² = dados.Eₙ
            ξₙ = dados.ξₙ
            Rcm = dados.R_cm_n
            Rcm_norm = zeros(size(Rcm,1))

            for i in 1:size(Rcm,1)
                Rcm_norm[i] = norm(Rcm[i,:]) 
            end

            if GEOMETRIA_DA_NUVEM == "DISCO"
                index_modes_inside_range = findall((IPRs .> 0.4).*(R² .> 0.7).*( (ξₙ./Radius) .< 1).*((Rcm_norm./Radius) .< 0.3))
            else
                L_radius = minimum([L,Variaveis_de_Entrada.Lₐ])/2
                index_modes_inside_range = findall((IPRs .> 0.4).*(R² .> 0.7).*( (ξₙ./L_radius) .< 1).*((Rcm_norm./L_radius) .< 0.7))
            end
            Δₙ_inside = Δₙ[index_modes_inside_range]
            
            valor_retorno = Δₙ_inside
        end

        Δₙ_total = Any[]
        for n in 1:Variaveis_de_Entrada.N_Realizações
            append!(Δₙ_total,tuplasResultados[n])
        end

        Δₙ_Localizado[i] = mean(Δₙ_total)
        save("TEMPORARIO_C16.jld2", Dict("SAIDA" => (Range_ρ,Δₙ_Localizado),"Parametros" => Variaveis_de_Entrada))
    end
        
    return Range_ρ,Δₙ_Localizado
end

