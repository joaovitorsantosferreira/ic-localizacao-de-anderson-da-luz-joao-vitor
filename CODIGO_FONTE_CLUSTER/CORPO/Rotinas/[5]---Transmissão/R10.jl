###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_10(Variaveis_de_entrada::R10_ENTRADA,Geometria::TwoD)
    

    N_Curvas = size(Variaveis_de_entrada.ρs,1)
    T = zeros(N_Curvas,N_pontos,2)
    aux_1 = 1

    if Variaveis_de_entrada.Escala_de_b == "LOG"
        Range_b = get_points_in_log_scale(b_INICIAL,b_FINAL,N_pontos) 
    elseif Variaveis_de_entrada.Escala_de_b == "LINEAR"    
        Range_b = range(b_INICIAL,b_FINAL,length=N_pontos)     
    end

    for i in 1:N_Curvas
        
        ρ = Variaveis_de_entrada.ρs[i]
        N_instantaneo = round(Int64,ρ*Variaveis_de_entrada.L*Variaveis_de_entrada.Lₐ)
        rₘᵢₙ = 1/(10*sqrt(ρ))
        
        if Variaveis_de_entrada.Tipo_de_kernel == "Escalar"            
            b₀ =  (4*N_instantaneo)/(Lₐ)
        elseif Variaveis_de_entrada.Tipo_de_kernel == "Vetorial"        
            b₀ =  (8*N_instantaneo)/(Lₐ)
        end

        ProgressMeter.@showprogress 2 "Transmission, ρ = {$ρ}===>" for j in 1:N_pontos

            bδ = Range_b[j]
            Δ = (Variaveis_de_entrada.Γ₀/2)*sqrt((b₀/bδ) - 1 ) 
            ωₗ = Δ + Variaveis_de_entrada.ωₐ                                                                                                                  

            entrada_Extratora = E9_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                N_instantaneo,
                Variaveis_de_entrada.Radius,
                ρ,
                rₘᵢₙ,
                Variaveis_de_entrada.angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                Variaveis_de_entrada.ω₀,
                Variaveis_de_entrada.λ,
                ωₗ,
                Δ,
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Variaveis_de_entrada.Distancia_dos_Sensores,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_entrada.L,
                Variaveis_de_entrada.Lₐ,
                Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
                1,
                Variaveis_de_entrada.Geometria,
                Variaveis_de_entrada.Tipo_de_campo
            )

            tuplas_resultados_Intensidade = pmap(1:N_Realizações; retry_delays = zeros(3)) do REALI
                valor_retorno = E9(entrada_Extratora)   
            end

            Intensidade_Resultante_PURA = Any[]
            Angulos = Any[]
            for n in 1:Variaveis_de_entrada.N_Realizações
                if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"
                    append!(Intensidade_Resultante_PURA,tuplas_resultados_Intensidade[n][1,:])
                    append!(Angulos,tuplas_resultados_Intensidade[n][2,:])    
                elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
                    append!(Intensidade_Resultante_PURA,tuplas_resultados_Intensidade[n][1,:])
                    append!(Intensidade_Resultante_PURA,tuplas_resultados_Intensidade[n][3,:])
                    append!(Angulos,tuplas_resultados_Intensidade[n][2,:])    
                    append!(Angulos,tuplas_resultados_Intensidade[n][4,:])    
                end

            end

            # dados_transmissao = vcat(Intensidade_Resultante_PURA,Angulos)
            dados_transmissao = zeros(2,size(Intensidade_Resultante_PURA,1))
            dados_transmissao[1,:] = Intensidade_Resultante_PURA
            dados_transmissao[2,:] = Angulos


            if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"
                T_DIFUSO,T_COERENTE = get_Tramission(dados_transmissao,N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_coerente,Variaveis_de_entrada.Geometria)
            elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
                T_DIFUSO,T_COERENTE = get_Tramission(dados_transmissao,2*N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_coerente,Variaveis_de_entrada.Geometria)
            end

            T[i,j,:] = [T_DIFUSO,T_COERENTE]
            save("TEMPORARIO_C12.jld2", Dict("SAIDA" => (Range_b,T),"Parametros" => Variaveis_de_Entrada))
        end 
    end

    return Range_b,T

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function ROTINA_10__Estatisticas_da_Transmission(Variaveis_de_entrada::R10_ENTRADA,Geometria::ThreeD)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    b_INICIAL = Variaveis_de_entrada.b_INICIAL 
    b_FINAL = Variaveis_de_entrada.b_FINAL
    N_pontos = Variaveis_de_entrada.N_pontos
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Radius = 1


    Q_total = N_Sensores*N_Realizações
    Intensidade_por_ponto = zeros(size(Variaveis_de_entrada.Ls,1),N_pontos,4,N_Sensores)
    T = zeros(size(Variaveis_de_entrada.Ls,1),N_pontos)
    aux_2 = 1

    if Variaveis_de_entrada.Escala_de_b == "LOG"

        Range_b = get_points_in_log_scale(b_INICIAL,b_FINAL,N_pontos) 
    
    elseif Variaveis_de_entrada.Escala_de_b == "LINEAR"    

        Range_b = range(b_INICIAL,b_FINAL,length=N_pontos) 
    
    end

    for L_Instantaneo in Variaveis_de_entrada.Ls

        ω₀_INST = Variaveis_de_entrada.Lₐ/2                                                                                                                                             
        Distancia_dos_Sensores_INST = 10*sqrt(Variaveis_de_entrada.Lₐ^2 + (L_Instantaneo/2)^2)
        # b₀_Instantaneo = 4*(Variaveis_de_entrada.N)/(Variaveis_de_entrada.Lₐ^2)
        aux_1 = 1

        for j in 1:N_pontos

            b₀_Instantaneo = Range_b[j]
            N_Instantaneo = round(Int64,b₀_Instantaneo*(Variaveis_de_entrada.Lₐ^2)/4)
            ρ_instantaneo = N_Instantaneo/(L_Instantaneo*π*(Variaveis_de_entrada.Lₐ^2))
            rₘᵢₙ_INST = 1/(10*sqrt(ρ_instantaneo))                                                                                                                                   
            Δ = 0
            # Δ = (Variaveis_de_entrada.Γ₀/2)*sqrt((b₀_Instantaneo/bδ) - 1 ) 
            ωₗ = Δ + Variaveis_de_entrada.ωₐ                                                                                                                  
            
            entrada_Extratora = E9_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                N_Instantaneo,
                Radius,
                ρ_instantaneo,
                rₘᵢₙ_INST,
                Variaveis_de_entrada.angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                ω₀_INST,
                Variaveis_de_entrada.λ,
                ωₗ,
                Δ,
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Distancia_dos_Sensores_INST,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
                L_Instantaneo,
                Variaveis_de_entrada.Lₐ,
                "NAO",
                1,
                Variaveis_de_entrada.Geometria
            )

            tuplas_resultados_Intensidade = ProgressMeter.@showprogress 1 "Extraido Transmissão da Luz {$aux_1}===>" pmap(1:N_Realizações; retry_delays = zeros(3)) do REALI
                valor_retorno = E9_extração_de_dados_Transmission(entrada_Extratora)   
            end
    
            Intensidade_Resultante_AVANÇO = Any[]
            Intensidade_Resultante_RETRO = Any[]
            Angulos_ϕ = Any[]
            Angulos_θ = Any[]

            for n in 1:Variaveis_de_entrada.N_Realizações
                append!(Intensidadse_Resultante_AVANÇO,tuplas_resultados_Intensidade[n][1,:])
                append!(Intensidade_Resultante_RETRO,tuplas_resultados_Intensidade[n][2,:])
                append!(Angulos_ϕ,tuplas_resultados_Intensidade[n][3,:])
                append!(Angulos_θ,tuplas_resultados_Intensidade[n][4,:])
            end

            Intensidade_media_AVANÇO = zeros(N_Sensores)
            Intensidade_media_RETRO = zeros(N_Sensores)

            for i in 1:N_Sensores
                fator_soma_AVANÇO = 0
                fator_soma_RETRO = 0
                aux_mean = 0
                for c in 1:N_Realizações
                    fator_soma_AVANÇO += Intensidade_Resultante_AVANÇO[i + aux_mean]
                    fator_soma_RETRO += Intensidade_Resultante_RETRO[i + aux_mean]
                    aux_mean += N_Sensores
                end
                Intensidade_media_AVANÇO[i] = fator_soma_AVANÇO/N_Realizações
                Intensidade_media_RETRO[i] = fator_soma_RETRO/N_Realizações
            end

            dados_transmissao = zeros(4,N_Sensores)
            dados_transmissao[1,:] = Intensidade_media_AVANÇO
            dados_transmissao[2,:] = Intensidade_media_RETRO
            dados_transmissao[3,:] = Angulos_ϕ[1:N_Sensores]
            dados_transmissao[4,:] = Angulos_θ[1:N_Sensores]
            
            Intensidade_por_ponto[aux_2,j,:,:] = dados_transmissao

            T[aux_2,j] = get_Tramission(dados_transmissao,N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_T_INICIAL,Variaveis_de_entrada.angulo_T_FINAL,Variaveis_de_entrada.Geometria)
            
            aux_1 += 1
            save("TEMPORARIO_C12.jld2", Dict("Intensidades" => Intensidade_por_ponto,"Transmissao" => T))
        end 

        aux_2 += 1
    end

    return Range_b,T,Intensidade_por_ponto
end


