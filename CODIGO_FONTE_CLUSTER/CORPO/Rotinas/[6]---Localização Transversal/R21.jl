#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_21(Entrada::R21_ENTRADA)

    range_Δ = range(Entrada.Δ_Inicial,Entrada.Δ_Final,length = Entrada.N_div_Deturn)
    ρs = range(Entrada.Densidade_Inicial,Entrada.Densidade_Final,length = Entrada.N_div_Densidade)
    range_ρ = zeros(size(ρs,1))
    @.range_ρ = ρs

    Matriz_Resultantes = zeros(Entrada.N_div_Deturn*Entrada.N_div_Densidade,3) 
    aux_1 = 1
    
    ProgressMeter.@showprogress 2 "Gerando MAPA :) ===>" for j in 1:Entrada.N_div_Deturn  
            
        Δ = range_Δ[j]

        Variaveis_de_Entrada = R11_ENTRADA(
            Entrada.Γ₀,
            Entrada.ωₐ,
            Entrada.Ω,
            range_ρ,
            Entrada.k,
            Entrada.Angulo_da_luz_incidente,
            Entrada.vetor_de_onda,
            Entrada.λ,
            Entrada.ωₐ+Δ,
            Δ,
            Entrada.Tipo_de_kernel,
            Entrada.Tipo_de_Onda,
            Entrada.Tipo_de_campo,
            "PURO",
            Entrada.N_Sensores,
            Entrada.PERFIL_DA_INTENSIDADE,
            Entrada.Distancia_dos_Sensores,
            Entrada.Angulo_de_variação_da_tela_circular_1,
            Entrada.Angulo_de_variação_da_tela_circular_2,
            Entrada.angulo_coerente,
            Entrada.Tipo_de_beta,
            Entrada.N_pontos,
            Entrada.N_Realizações,
            Entrada.Lₐ,
            Entrada.RANGE_INICIAL,
            Entrada.RANGE_FINAL,
            Entrada.Escala_do_range,
            Entrada.Desordem_Diagonal,
            Entrada.W,
            Entrada.Geometria
        )


        σ²,T,Range_L = ROTINA_11(Variaveis_de_Entrada,Entrada.Geometria)

        for i in 1:Entrada.N_div_Densidade
            index_modes_inside_range = findall((Range_L .≥ 4*π))
            ξ_instataneo,b_fit = get_fitting_to_cintura(Range_L[index_modes_inside_range],σ²[ index_modes_inside_range,i],Entrada.Tipo_de_kernel)
            Matriz_Resultantes[aux_1, : ] = [range_ρ[i],range_Δ[j],norm(ξ_instataneo)]
            aux_1 += 1
        end
        save("TEMPORARIO_C23.jld2", Dict("SAIDA" => Matriz_Resultantes,"Parametros" => Entrada))
    end

    return Matriz_Resultantes
end