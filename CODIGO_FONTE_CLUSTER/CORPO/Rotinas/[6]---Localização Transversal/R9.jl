#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_9(Variaveis_de_Entrada::R9_ENTRADA)
    
    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    N_Realizações = Variaveis_de_Entrada.N_Realizações
    N_Telas = 1
    PERFIL_DA_INTENSIDADE = Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE
    Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1
    Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2

    N_Curvas = size(Variaveis_de_Entrada.Ns,1)
    aux_1 = 1

    σ² = zeros(N_Curvas+1)
    Perfis_da_Intensidade = zeros(N_Sensores,N_Curvas+1)

    for j in 1:N_Curvas 
    

        Intensidade_Resultante_PURA = Any[]
        N = Variaveis_de_Entrada.Ns[j]
        aux_2 = 0

        if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
            
            ρ_normalizado = N/(π*(Variaveis_de_Entrada.kR*Variaveis_de_Entrada.k)^2)
            Radius = Variaveis_de_Entrada.kR/Variaveis_de_Entrada.k
            ρ = ρ_normalizado*Variaveis_de_Entrada.k^2
            rₘᵢₙ = 1/(5*sqrt(ρ))   
            ω₀ = Radius/2

        elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

            Radius = 1 
            ρ_normalizado = N/(Variaveis_de_Entrada.L*Variaveis_de_Entrada.Lₐ)
            ρ = ρ_normalizado
            rₘᵢₙ = 1/(3*sqrt(ρ))
            # ωᵦ = Variaveis_de_Entrada.Lₐ/10
            # ωᵦ = Variaveis_de_Entrada.ωᵦ
            # ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((Variaveis_de_Entrada.L/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)
            ω₀ = Variaveis_de_Entrada.ω₀
        end

        
        if Variaveis_de_Entrada.Tipo_de_Δ == "PURO"

            Δ_instataneo = Variaveis_de_Entrada.Δ

        elseif Variaveis_de_Entrada.Tipo_de_Δ == "INTERPOLAÇÃO"

            xdata = load("DADOS_ENERGIA.jld2", "SAIDA")[1]
            ydata = load("DADOS_ENERGIA.jld2", "SAIDA")[2]
            
            if ρ < 0.1 
                model_energia(x, p) = log.(x).*p[1] .+ p[2] 
                p0 = [0.25, 1.0]
            
                fitting_energia = curve_fit(model_energia, xdata, ydata, p0)                
                a_energia = fitting_energia.param[1] 
                b_energia = fitting_energia.param[2]                            
                Δ_instataneo = a_energia*log(ρ) + b_energia
            else
                itp = LinearInterpolation(xdata,ydata)
                Δ_instataneo = itp(ρ)
            end
        end
        

        entrada_extratora = E2_ENTRADA(
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.ωₐ,
            Variaveis_de_Entrada.Ω,
            Variaveis_de_Entrada.k,
            N,
            Radius,
            ρ,
            rₘᵢₙ,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Variaveis_de_Entrada.vetor_de_onda,
            ω₀,
            Variaveis_de_Entrada.λ,
            Variaveis_de_Entrada.ωₗ,
            Δ_instataneo,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.Tipo_de_Onda,
            Variaveis_de_Entrada.Tipo_de_campo,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Angulo_de_variação_da_tela_circular_1,
            Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.Tipo_de_beta,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas,
            Variaveis_de_Entrada.Geometria
        )

        tuplas_intensidade = ProgressMeter.@showprogress 2 "Calculo_Cintura_N = {$(Variaveis_de_Entrada.Ns[aux_1])}===>" pmap(1:N_Realizações; retry_delays = zeros(3)) do N_Realização
            All_Intensitys = E2(entrada_extratora)

            if PERFIL_DA_INTENSIDADE == "NAO"
                Intensidade_interesse = All_Intensitys[1 , : ]
            else
                Intensidade_interesse = All_Intensitys[3 , : ]
            end
            
            valor_retorno = Intensidade_interesse
        end

        for n in 1:N_Realizações                
            append!(Intensidade_Resultante_PURA,tuplas_intensidade[n])
        end

        Intensidade_media = zeros(N_Sensores)

        for i in 1:N_Sensores
    
            fator_soma = 0
            aux_3 = 0
    
            for k in 1:N_Realizações
    
                fator_soma += log(Intensidade_Resultante_PURA[i + aux_3])
                aux_3 += N_Sensores
    
            end
    
            Intensidade_media[i] = exp(fator_soma/N_Realizações)
    
        end

        
        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_Entrada.kR,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Angulo_de_variação_da_tela_circular_1,
            Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            referencia = zeros(N_Sensores)
            for i in 1:N_Sensores
                θ =  get_degree_azimut(Sensores[i,1:2])
                if θ > 180
                    θ = θ - 360
                end
                referencia[i] = θ
            end
        else
            referencia = Sensores[ : ,4]    
        end
        
        

        σ²[j] = get_one_σ_lorentz(referencia,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
        # σ²[j] = (mean_and_std(Sensores[:,4], Weights(Intensidade_media); corrected = false)[2])^2

        Perfis_da_Intensidade[ : ,j] = Intensidade_media

        aux_1 += 1
        save("TEMPORARIO_C11.jld2", Dict("SAIDA" => (σ²,Perfis_da_Intensidade,referencia),"Parametros" => Variaveis_de_Entrada))
    end

    if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
            
        ω₀ = Variaveis_de_Entrada.kR/2

    elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

        # ωᵦ = 6*pi
        # ωᵦ = Variaveis_de_Entrada.Lₐ/10
        # ωᵦ = 4*pi
        # ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((Variaveis_de_Entrada.L/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)
        ω₀ = Variaveis_de_Entrada.ω₀                                                                                                                                                                                                            
    end

    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Variaveis_de_Entrada.kR,
        Variaveis_de_Entrada.N_Sensores,
        Variaveis_de_Entrada.Distancia_dos_Sensores,
        Variaveis_de_Entrada.Angulo_da_luz_incidente,
        Angulo_de_variação_da_tela_circular_1,
        Angulo_de_variação_da_tela_circular_2,
        Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_Entrada.L,
        Variaveis_de_Entrada.Lₐ,
        PERFIL_DA_INTENSIDADE,
        N_Telas
    )
    # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
    Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

    if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
        referencia_intensidade = Sensores[ : ,1:2]   
    else
        referencia_intensidade = Sensores[ : ,3:4]    
    end

    Intensidade_PURA = zeros(N_Sensores)
    for i in 1:N_Sensores
        
        entrada_campo = get_eletric_field_LG_real_ENTRADA(
            referencia_intensidade[i,:],
            Variaveis_de_Entrada.Ω,
            ω₀,
            Variaveis_de_Entrada.k,
            Variaveis_de_Entrada.Δ
        )
        campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
        
        Intensidade_PURA[i] = abs(campo_laser)^2
    end

    if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
        referencia = zeros(N_Sensores)
        for i in 1:N_Sensores
            θ =  get_degree_azimut(Sensores[i,1:2])
            if θ > 180
                θ = θ - 360
            end
            referencia[i] = θ
        end
    else
        referencia = Sensores[ : ,4]    
    end

    Perfis_da_Intensidade[ : ,end] = Intensidade_PURA[ : ]

    σ²[end] = get_one_σ_gaussian(referencia,Perfis_da_Intensidade[ : ,end],Variaveis_de_Entrada.Geometria)[2]
    # σ²[end] = (mean_and_std(Sensores[:,4], Weights(Intensidade_PURA); corrected = false)[2])^2

    return σ²,Perfis_da_Intensidade,referencia
end