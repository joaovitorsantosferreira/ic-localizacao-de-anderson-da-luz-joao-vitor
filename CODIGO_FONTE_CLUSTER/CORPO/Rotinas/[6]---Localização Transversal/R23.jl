#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_23(Entrada::R23_ENTRADA)

    range_Δ = range(Entrada.Δ_Inicial,Entrada.Δ_Final,length = Entrada.N_div_Deturn)
    ρs = range(Entrada.Densidade_Inicial,Entrada.Densidade_Final,length = Entrada.N_div_Densidade)
    range_ρ = zeros(size(ρs,1))
    @.range_ρ = ρs

    Matriz_Resultantes = zeros(Entrada.N_div_Deturn*Entrada.N_div_Densidade,3) 
    aux_1 = 1
    
    ProgressMeter.@showprogress 2 "Gerando MAPA :) ===>" for j in 1:Entrada.N_div_Deturn  
            
        Δ = range_Δ[j]

        Variaveis_de_Entrada = R11_ENTRADA(
            Entrada.Γ₀,
            Entrada.ωₐ,
            Entrada.Ω,
            range_ρ,
            Entrada.k,
            Entrada.Angulo_da_luz_incidente,
            Entrada.vetor_de_onda,
            Entrada.λ,
            Entrada.ωₐ+Δ,
            Δ,
            Entrada.Tipo_de_kernel,
            Entrada.Tipo_de_Onda,
            Entrada.Tipo_de_campo,
            "PURO",
            Entrada.N_Sensores,
            Entrada.PERFIL_DA_INTENSIDADE,
            Entrada.Distancia_dos_Sensores,
            Entrada.Angulo_de_variação_da_tela_circular_1,
            Entrada.Angulo_de_variação_da_tela_circular_2,
            Entrada.angulo_coerente,
            Entrada.Tipo_de_beta,
            Entrada.N_pontos,
            Entrada.N_Realizações,
            Entrada.Lₐ,
            Entrada.RANGE_INICIAL,
            Entrada.RANGE_FINAL,
            Entrada.Escala_do_range,
            Entrada.Desordem_Diagonal,
            Entrada.W,
            Entrada.Geometria
        )


        σ²,T,Range_L = ROTINA_11(Variaveis_de_Entrada,Entrada.Geometria)

        model_exp(x, p) = exp.(-1 .* (x./p))
        model_log(x, p) = (-1) .* (x./p)
        

        for i in 1:Entrada.N_div_Densidade

            index_modes_inside_range = findall(  (Range_L .> 0).*(T[:,i] .> 10^-7 ) )
        
            x = Range_L[index_modes_inside_range]
            y = T[index_modes_inside_range,i]
        
            xdata = x
            ydata = similar(xdata)
            ydata_preciso = log.(y)
        
            for k in 1:size(ydata_preciso,1)
                valor_preciso = ydata_preciso[k]
                string = "$valor_preciso"
                ydata[k] = parse(Float64,string[1:20])
            end
        
            p0 = [0.01]
        
            fit_ξ = curve_fit(model_log, xdata, ydata, p0)

            Matriz_Resultantes[aux_1, : ] = [range_ρ[i],range_Δ[j],norm(fit_ξ.param[1])]
            aux_1 += 1

        end
        save("TEMPORARIO_C24.jld2", Dict("SAIDA" => Matriz_Resultantes,"Parametros" => Entrada))
    end

    return Matriz_Resultantes
end