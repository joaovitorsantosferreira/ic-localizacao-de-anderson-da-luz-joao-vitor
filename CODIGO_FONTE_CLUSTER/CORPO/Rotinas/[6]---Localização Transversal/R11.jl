#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_11(Variaveis_de_Entrada::R11_ENTRADA,Geometria::TwoD)
    
      
    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
    L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
    N_pontos = Variaveis_de_Entrada.N_pontos
    N_Realizações = Variaveis_de_Entrada.N_Realizações
        
    Lₐ = Variaveis_de_Entrada.Lₐ
    ρs = Variaveis_de_Entrada.ρs

    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Ls = range(L_INICIAL,L_FINAL,length=N_pontos) 
        Range_L = zeros(N_pontos)
        @.Range_L = Ls
    end

    N_Curvas = size(Variaveis_de_Entrada.ρs,1)
    σ² = zeros(N_pontos,N_Curvas+1)

    # βs_Resultante = [Any[],Any[]]

    # for i in 1:(N_Curvas-1)
    #     βs_Resultante = hcat(βs_Resultante,[Any[],Any[]])
    # end

    

    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#

        
    tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_L) do L_instantaneo

        ωᵦ = 10*pi
        ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((L_instantaneo/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)
        

        entrada_Sensores = get_cloud_sensors_ENTRADA(
            1,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
            "SLAB",
            L_instantaneo,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE,
            1
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)


        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
            referencia =  Sensores[ : ,3:4]
        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            referencia =  Sensores
        end

        Intensidade_PURA = zeros(N_Sensores)
        for i in 1:N_Sensores
            
            if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
                referencia_sensor =  referencia[i ,:]    
            elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
                referencia_sensor = [referencia[i,1]*cosd(referencia[i,2]),referencia[i,1]*sind(referencia[i,2])]                    
            end

            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                referencia_sensor,
                Variaveis_de_Entrada.Ω,
                ω₀,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Δ
            )
            campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
            
            Intensidade_PURA[i] = abs(campo_laser^2) 
        end

        σ² = get_one_σ_gaussian(referencia[:,2],Intensidade_PURA,Variaveis_de_Entrada.Geometria)[2]

        valor_retorno = σ²,Intensidade_PURA
    end

    Intensidade_PURA_Transmissão = zeros(N_pontos,N_Sensores)

    for n in 1:N_pontos
        σ²[n,end] = tuplasResultados_caso_vazio[n][1]
        Intensidade_PURA_Transmissão[n,:] = tuplasResultados_caso_vazio[n][2]
    end

    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#

    T = Array{BigFloat}(undef, N_pontos, N_Curvas)
    T .= 0


    model_energia(x, p) = log.(x).*p[1] .+ p[2] 
    xdata = load("DADOS_ENERGIA.jld2", "SAIDA")[1]
    ydata = load("DADOS_ENERGIA.jld2", "SAIDA")[2]
    p0 = [0.25, 1.0]

    fitting_energia = curve_fit(model_energia, xdata, ydata, p0)

    a_energia = fitting_energia.param[1] 
    b_energia = fitting_energia.param[2]        


    for j in 1:N_Curvas 
        
        ρ = ρs[j]

        if Variaveis_de_Entrada.Tipo_de_Δ == "PURO"
            Δ = Variaveis_de_Entrada.Δ
        elseif Variaveis_de_Entrada.Tipo_de_Δ == "INTERPOLAÇÃO"
            if ρ < 0.1           
                Δ = a_energia*log(ρ) + b_energia
            else
                itp = LinearInterpolation(xdata,ydata)
                Δ = itp(ρ)
            end
        end



        ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$ρ}===>" for L_instantaneo in  Range_L  

            Intensidade_Resultante_PURA = Any[]

            ωᵦ = 10*pi
            ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((L_instantaneo/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)
            rₘᵢₙ = 1/(3*sqrt(ρ))
            N = round(Int64,ρ*(L_instantaneo)*Lₐ)

            if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"            
                b₀ =  (4*N)/(L_instantaneo)
            elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"        
                b₀ =  (8*N)/(L_instantaneo)
            end
            
            entrada_extratora = E2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                1,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.ωₗ,
                Δ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.Tipo_de_campo,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W*b₀,
                "SLAB",
                L_instantaneo,
                Variaveis_de_Entrada.Lₐ,
                Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE,
                1,
                Variaveis_de_Entrada.Geometria
            )

            tuplas_intensidade = pmap(1:N_Realizações; retry_delays = zeros(3)) do N_Realização

                # All_Intensitys,βₙ,r_atoms = E2(entrada_extratora)
                All_Intensitys = E2(entrada_extratora)
                
                if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

                    intensidade_interesse =  All_Intensitys[3 , : ]

                elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"

                    intensidade_interesse = All_Intensitys[1 , : ]
                end
                valor_retorno = intensidade_interesse
            end

            for n in 1:N_Realizações
                append!(Intensidade_Resultante_PURA,tuplas_intensidade[n])
            end
            tuplas_intensidade = 0

            Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
        
            entrada_Sensores = get_cloud_sensors_ENTRADA(
                1,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                "SLAB",
                L_instantaneo,
                Variaveis_de_Entrada.Lₐ,
                Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE,
                1
            )
            
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

            if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

                referencia =  Posição_Sensores[ : ,3:4]

            elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"

                referencia =  Posição_Sensores
            end
                            
            σ²[findall(Range_L .== L_instantaneo)[1],j] = get_one_σ_lorentz(referencia[:,2],Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            T[findall(Range_L .== L_instantaneo)[1],j] = get_Tramission_Cherroret(Intensidade_Resultante_PURA,Intensidade_PURA_Transmissão[findall(Range_L .== L_instantaneo)[1],:],referencia,Variaveis_de_Entrada.angulo_coerente,N_Realizações,Variaveis_de_Entrada.Geometria)


            save("TEMPORARIO_C13.jld2", Dict("SAIDA" => (σ²,T,Range_L),"Parametros" => Variaveis_de_Entrada))
        end

    end
    

    return σ²,T,Range_L
end


# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# function ROTINA_11__Cintura_e_L(Variaveis_de_Entrada::R11_ENTRADA,Geometria::ThreeD)


#     N_Sensores = Variaveis_de_Entrada.N_Sensores    
#     L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
#     L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
#     N_pontos = Variaveis_de_Entrada.N_pontos
#     N_Realizações = Variaveis_de_Entrada.N_Realizações

#     Lₐ = Variaveis_de_Entrada.Lₐ
#     ρs = Variaveis_de_Entrada.ρs


#     if Variaveis_de_Entrada.Escala_do_range == "LOG"

#         Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
    
#     elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

#         Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
    
#     end
    

#     N_Curvas = size(Variaveis_de_Entrada.ρs,1)

#     σ² = zeros(N_pontos,N_Curvas+1)


#     for j in 1:N_Curvas 

#         ρ = ρs[j]

#         tuplasResultados = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$ρ}===>" pmap(Range_L) do L_instantaneo

#             Intensidade_Resultante_PURA = Any[]
            
#             # ωᵦ = Lₐ/10
#             # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
#             # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

#             ωᵦ = Lₐ/5
#             ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo/2)^2))/2)
            
#             rₘᵢₙ = 1/(10*sqrt(ρ))                

#             if typeof(Variaveis_de_Entrada.Geometria) == PARALELEPIPEDO
#                 GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"
#                 N = round(Int64,ρ*(L_instantaneo)*Lₐ^2)
#             elseif typeof(Variaveis_de_Entrada.Geometria) == TUBO
#                 GEOMETRIA_DA_NUVEM = "TUBO"
#                 N = round(Int64,ρ*(L_instantaneo)*(π*Lₐ^2))
#             end

#             for i in 1:N_Realizações

#                 entrada_extratora = E2_ENTRADA(
#                     Variaveis_de_Entrada.Γ₀,
#                     Variaveis_de_Entrada.ωₐ,
#                     Variaveis_de_Entrada.Ω,
#                     Variaveis_de_Entrada.k,
#                     N,
#                     1,
#                     ρ,
#                     rₘᵢₙ,
#                     Variaveis_de_Entrada.Angulo_da_luz_incidente,
#                     Variaveis_de_Entrada.vetor_de_onda,
#                     ω₀,
#                     Variaveis_de_Entrada.λ,
#                     Variaveis_de_Entrada.ωₗ,
#                     Variaveis_de_Entrada.Δ,
#                     Variaveis_de_Entrada.Tipo_de_kernel,
#                     Variaveis_de_Entrada.Tipo_de_Onda,
#                     Variaveis_de_Entrada.N_Sensores,
#                     Variaveis_de_Entrada.Distancia_dos_Sensores,
#                     0,
#                     1,
#                     Variaveis_de_Entrada.Tipo_de_beta,
#                     Variaveis_de_Entrada.Desordem_Diagonal,
#                     Variaveis_de_Entrada.W,
#                     GEOMETRIA_DA_NUVEM,
#                     L_instantaneo,
#                     Variaveis_de_Entrada.Lₐ,
#                     "SIM,para distancia fixada",
#                     1,
#                     Variaveis_de_Entrada.Geometria
#                 )

#                 All_Intensitys = E2(entrada_extratora)

#                 append!(Intensidade_Resultante_PURA,All_Intensitys[1 , : ])

#             end

#             Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
        

#             entrada_Sensores = get_cloud_sensors_ENTRADA(
#                 1,
#                 Variaveis_de_Entrada.N_Sensores,
#                 Variaveis_de_Entrada.Distancia_dos_Sensores,
#                 Variaveis_de_Entrada.Angulo_da_luz_incidente,
#                 0,
#                 1,
#                 GEOMETRIA_DA_NUVEM,
#                 L_instantaneo,
#                 Variaveis_de_Entrada.Lₐ,
#                 "SIM,para distancia fixada",
#                 1
#             )
            
#             Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

#             σ² = get_one_σ(Posição_Sensores,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            
#             valor_retorno = σ²
#         end

#         for n in 1:N_pontos
#             σ²[n,j] = tuplasResultados[n]
#         end
#     end
    
#     tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_L) do L_instantaneo

#         # ωᵦ = Lₐ/10
#         # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
#         # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

#         ωᵦ = Lₐ/5
#         ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo/2)^2))/2)

#         if typeof(Variaveis_de_Entrada.Geometria) == PARALELEPIPEDO
#             GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"
#         elseif typeof(Variaveis_de_Entrada.Geometria) == TUBO
#             GEOMETRIA_DA_NUVEM = "TUBO"
#         end

#         entrada_Sensores = get_cloud_sensors_ENTRADA(
#             1,
#             Variaveis_de_Entrada.N_Sensores,
#             Variaveis_de_Entrada.Distancia_dos_Sensores,
#             Variaveis_de_Entrada.Angulo_da_luz_incidente,
#             0,
#             1,
#             GEOMETRIA_DA_NUVEM,
#             L_instantaneo,
#             Variaveis_de_Entrada.Lₐ,
#             "SIM,para distancia fixada",
#             1
#         )
#         # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
#         Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

#         Intensidade_PURA = zeros(N_Sensores^2)
#         for i in 1:(N_Sensores^2)
            
#             entrada_campo = get_eletric_field_LG_real_ENTRADA(
#                 Sensores[i, : ],
#                 Variaveis_de_Entrada.Ω,
#                 ω₀,
#                 Variaveis_de_Entrada.k,
#                 Variaveis_de_Entrada.Δ
#             )
#             campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
            
#             Intensidade_PURA[i] = abs(campo_laser^2) 
#         end

#         σ² = get_one_σ(Sensores,Intensidade_PURA,Variaveis_de_Entrada.Geometria)[2]

#         valor_retorno = σ²
#     end

#     for n in 1:N_pontos
#         σ²[n,end] = tuplasResultados_caso_vazio[n]
#     end
    
#     return σ²,Range_L
# end





