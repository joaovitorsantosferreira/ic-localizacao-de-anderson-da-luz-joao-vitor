#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_26(Variaveis_de_Entrada::R26_ENTRADA,Geometria::TwoD)
    
      
    
end


M = (-Γ₀/2)*cloud.G .+ 1im*Δ*I(N)
λ,U = eigen(M)

β_novo = M\Ωₙ
β_novo == βₙ

M == inv(U)*D*U

r = cloud.r
Ωₙ = Array{Complex{Float64}}(undef, N)
for i in 1:N
    entrada_campo = get_eletric_field_LG_real_ENTRADA(r[i,:],Ω,ω₀,k,Δ)
    Ωₙ[i] = (1im/2)*get_eletric_field_LG_real(entrada_campo,Geometria)
end

λₙ = cloud.λ                                                                                                                                # Autovalores da Matriz de Green
D = Array{Complex{Float64}}(undef, N,N)
D .= 0+0im
D[LinearAlgebra.diagind(D)] .= λₙ

U = cloud.ψ                                                                                                                                # Autovetores da Matriz de Green
U² = U*U

U²⁽⁻¹⁾ = inv(U²)
D⁽⁻¹⁾ = inv(D) 

αₙ² = abs.(U²⁽⁻¹⁾*D⁽⁻¹⁾*U*Ωₙ).^2
αₙ = U²⁽⁻¹⁾*D⁽⁻¹⁾*U*Ωₙ

modos_loc = findall(IPRs .> 0.3 )

P_loc = 100*(sum(αₙ²[modos_loc])/sum(αₙ²))

gr()
scatter(imag.(λₙ),real.(λₙ), yscale = :log10,ylims=(10^-20,10^2),lab="")
scatter!(imag.(λₙ[modos_loc]),real.(λₙ[modos_loc]), yscale = :log10,ylims=(10^-20,10^2),lab=L"$ P_{LOC}$")


# N_pontos = 100
# P_loc = zeros(N_pontos)
# range_Δ = range(-5,5,length=N_pontos)

# for i in 1:N_pontos

#     M_TEMP = (-Γ₀/2)*G 
#     M_TEMP[LinearAlgebra.diagind(M_TEMP)] .+= 1im*range_Δ[i]
#     λ, U = LinearAlgebra.eigen(M_TEMP)    

#     D = Array{Complex{Float64}}(undef, N,N)
#     D .= 0+0im
#     D[LinearAlgebra.diagind(D)] .= λ

#     U² = U*U

#     U²⁽⁻¹⁾ = inv(U²)
#     D⁽⁻¹⁾ = inv(D)

#     αₙ²_temp = abs.(U²⁽⁻¹⁾*D⁽⁻¹⁾*U*Ωₙ).^2

#     modos_loc = findall((imag.(λ) .> 0).*(imag.(λ) .< 1) )
#     P_loc[i] = 100*sum(αₙ²_temp[modos_loc])/(sum(αₙ²_temp))

# end

# plot(range_Δ,P_loc)


# Radius_plot = round(Radius,digits=2)
# plot(range_Δ_caso1,P_loc_caso1,
# size = (1000, 500 ),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# lw = 4,
# c = :green,
# lab = L"$\textrm{Case 1} $" ,
# title = L"$ \rho/k^2 = %$ρ , Scalar, kR = %$Radius_plot,N = %$N$",
# framestyle = :box,
# legendfontsize = 16,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black,
# xlabel = L"$ \Delta/\Gamma_0 $",
# ylabel = L"$ P_{LOC} $"
# )

# save("POPULAÇÂO_CASO_2.jld2", Dict("SAIDA" => (range_Δ,P_loc)))

# range_Δ_caso1,P_loc_caso1 = load("POPULAÇÂO_CASO_1.jld2", "SAIDA")



# N_pontos = 100
# P_loc = zeros(N_pontos)
# range_Δ = range(-5,5,length=N_pontos)

# for i in 1:N_pontos
#     D_TEMP = Array{Complex{Float64}}(undef, N,N)
#     D_TEMP .= 0+0im
#     D_TEMP[LinearAlgebra.diagind(D_TEMP)] .= λₙ .+ 1im*range_Δ[i]

#     D⁽⁻¹⁾_TEMP = inv(D_TEMP)

#     αₙ²_temp = abs.(U²⁽⁻¹⁾*D⁽⁻¹⁾_TEMP*U*Ωₙ).^2

#     P_loc[i] = 100*sum(αₙ²_temp[modos_loc])/(sum(αₙ²_temp))

# end


# plot!(range_Δ_caso2,P_loc_caso2,
# size = (1000, 500 ),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# gridalpha = 0,
# minorgrid = true,
# minorgridalpha = 0,
# lw = 4,
# c = :red,
# lab = L"$\textrm{Case 2} $" ,
# title = L"$ \rho/k^2 = %$ρ , Scalar, kR = %$Radius_plot,N = %$N$",
# framestyle = :box,
# legendfontsize = 16,
# labelfontsize = 20,
# titlefontsize = 20,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black,
# xlabel = L"$ \Delta/\Gamma_0 $",
# ylabel = L"$ P_{LOC} $"
# )

# savefig("População.png")

βₙ_loc = Array{Complex{Float64}}(undef, N,1)
βₙ_loc .= 0+0im
for j in 1:size(modos_loc,1)
    βₙ_loc .+= αₙ[modos_loc[j]]*U[:,modos_loc[j]] 
end

βₙ_tot = Array{Complex{Float64}}(undef, N,1)
βₙ_tot .= 0+0im
for j in 1:N
    βₙ_tot .+= αₙ[j]*U[:,j] 
end

entrada_Intensidade_LOC = get_Intensity_to_sensors_ENTRADA(
    Posição_Sensores,
    βₙ_loc,
    k,
    Γ₀,
    Ω,
    cloud.r,
    vetor_de_onda,
    ω₀,
    Tipo_de_Onda,
    Tipo_de_kernel,
    PERFIL_DA_INTENSIDADE,
    N_Telas,
    GEOMETRIA_DA_NUVEM,
    Radius,
    L,
    Δ,
)
# Guardo a intensidade sentida em cada sensor 
Intensidade_localizada = get_Intensity_to_sensors(entrada_Intensidade_LOC,Geometria)

entrada_Intensidade_PURA = get_Intensity_to_sensors_ENTRADA(
    Posição_Sensores,
    βₙ_tot,
    k,
    Γ₀,
    Ω,
    cloud.r,
    vetor_de_onda,
    ω₀,
    Tipo_de_Onda,
    Tipo_de_kernel,
    PERFIL_DA_INTENSIDADE,
    N_Telas,
    GEOMETRIA_DA_NUVEM,
    Radius,
    L,
    Δ,
)
# Guardo a intensidade sentida em cada sensor 
Intensidade_pura = get_Intensity_to_sensors(entrada_Intensidade_PURA,Geometria)


plot(Posição_Sensores[:,4],Intensidade_pura[3,:],
size = (1000, 500 ),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
lw = 4,
yscale=:log10,
lab = L"$\textrm{Todos os Modos} $" ,
framestyle = :box,
legendfontsize = 16,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
xlabel = L"$ kY $",
ylabel = L"$ Intensity $"
)
plot!(Posição_Sensores[:,4],Intensidade_localizada[3,:],
size = (1000, 500 ),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
lw = 4,
yscale=:log10,
lab = L"$\textrm{Modos Localizados} $" ,
framestyle = :box,
legendfontsize = 16,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
xlabel = L"$ kY $",
ylabel = L"$ Intensity $"
)
plot!(Posição_Sensores[:,4],All_Intensitys[3,:],
size = (1000, 500 ),
left_margin = 10Plots.mm,
right_margin = 12Plots.mm,
top_margin = 10Plots.mm,
bottom_margin = 10Plots.mm,
gridalpha = 0,
minorgrid = true,
minorgridalpha = 0,
lw = 4,
yscale=:log10,
lab = L"$\textrm{Modos Localizados} $" ,
framestyle = :box,
legendfontsize = 16,
labelfontsize = 20,
titlefontsize = 20,
tickfontsize = 15, 
background_color_legend = :white,
background_color_subplot = :white,
foreground_color_legend = :black,
xlabel = L"$ kY $",
ylabel = L"$ Intensity $"
)


