#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_2__Variancia_Densidade_FIXA(Variaveis_de_entrada::R2_ENTRADA)
    

    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final
    N_Sensores = Variaveis_de_entrada.N_Sensores
    PERFIL_DA_INTENSIDADE = "NAO"
    N_Telas = 1

    Deturn = zeros(N_div_Deturn)
    Variacias_SIMULADAS = zeros(N_div_Deturn)
    Variacias_TEORICAS = zeros(N_div_Deturn)

    N_total_de_Sensores = N_Sensores*N_Realizações
    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)


    for j in 1:N_div_Deturn  
            
        Δ = range_Δ[j]
        ωₗ = Δ + ωₐ
        Intensidade_Resultante = [Any[],Any[]]


        entrada_Extratora = E2_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            ωₗ,
            Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas,
            Variaveis_de_entrada.Geometria
        )

        tuplas_resultados = pmap(1:Variaveis_de_entrada.N_Realizações; retry_delays = zeros(3)) do REALI
            valor_retorno = E2_extração_de_dados_Intensidade(entrada_Extratora)[1:2, : ]
        end

        Threads.@threads for n in 1:Variaveis_de_entrada.N_Realizações
            append!(Intensidade_Resultante[1],tuplas_resultados[n][1, : ]) 
            append!(Intensidade_Resultante[2],tuplas_resultados[n][2, : ]) 
        end

        Intensidade_Resultante_matriz = zeros(2,N_total_de_Sensores)
        Intensidade_Resultante_matriz[1,:] = Intensidade_Resultante[1]
        Intensidade_Resultante_matriz[2,:] = Intensidade_Resultante[2]

        Variacias_SIMULADAS[j],Variacias_TEORICAS[j] = get_dados_to_diagram_fase(Intensidade_Resultante_matriz)
        Deturn[j] = Δ

    end

    return Deturn,Variacias_SIMULADAS,Variacias_TEORICAS
end