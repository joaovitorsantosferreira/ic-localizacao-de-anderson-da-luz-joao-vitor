#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_3__Diagrama_de_Fase_Variancia(Variaveis_de_Entrada::R3_ENTRADA)
  
    N_div_Densidade = Variaveis_de_Entrada.N_div_Densidade
    N_div_Deturn = Variaveis_de_Entrada.N_div_Deturn
    Densidade_Inicial = Variaveis_de_Entrada.Densidade_Inicial
    Densidade_Final = Variaveis_de_Entrada.Densidade_Final
    Variavel_Constante = Variaveis_de_Entrada.Variavel_Constante
    Escala_de_Densidade = Variaveis_de_Entrada.Escala_de_Densidade

    Q_Dados = N_div_Deturn*N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,3) 
    aux_1 = 1

    if Escala_de_Densidade == "LOG"

        Range_ρ = get_points_in_log_scale(Densidade_Inicial,Densidade_Final,N_div_Densidade) 
    
    elseif Escala_de_Densidade == "LINEAR"    

        Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade) 
    
    end

    ProgressMeter.@showprogress 2 "Variancia Global ===>" for ρ_normalizado in Range_ρ 

        N = round(Int64,ρ_normalizado*π*(Variaveis_de_Entrada.kR^2))
        rₘᵢₙ = 1/(5*sqrt(ρ_normalizado))   
        ω₀ = Variaveis_de_Entrada.kR/2                                                                                                                 
        Distancia_dos_Sensores = 10*Variaveis_de_Entrada.kR

        if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"
            b₀ = (8*N)/(π*Variaveis_de_Entrada.kR)
        
        elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"
            b₀ = (16*N)/(π*Variaveis_de_Entrada.kR)
        end

        entrada_rotina = R2_ENTRADA(
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.ωₐ,
            Variaveis_de_Entrada.Ω,
            Variaveis_de_Entrada.k,
            N,
            Variaveis_de_Entrada.kR,
            ρ_normalizado,
            rₘᵢₙ,
            Variaveis_de_Entrada.angulo_da_luz_incidente,
            Variaveis_de_Entrada.vetor_de_onda,
            ω₀,
            Variaveis_de_Entrada.λ,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.Tipo_de_Onda,
            Variaveis_de_Entrada.N_Sensores,
            Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.Tipo_de_beta,
            Variaveis_de_Entrada.N_div_Deturn,
            Variaveis_de_Entrada.N_Realizações,
            Variaveis_de_Entrada.Δ_Inicial,
            Variaveis_de_Entrada.Δ_Final,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W*b₀,
            "DISCO",
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.Geometria
        )
    
        Deturn_temporario,Variancias_temporario,Variacias_TEORICAS = ROTINA_2__Variancia_Densidade_FIXA(entrada_rotina)

        ρ_normalizados = fill(ρ_normalizado,(N_div_Deturn,1))
    
        Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),1] = Deturn_temporario
        Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),2] = ρ_normalizados
        Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),3] = Variancias_temporario 
        aux_1 +=  N_div_Deturn
        save("TEMPORARIO_C4.jld2", Dict("Dados" => Dados_Resultantes))
    end


    return Dados_Resultantes

end


    



































