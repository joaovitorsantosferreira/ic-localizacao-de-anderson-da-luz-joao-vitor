#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_27(Variaveis_de_Entrada::R27_ENTRADA,Geometria::TwoD)
    

    
    L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
    L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
    N_pontos = Variaveis_de_Entrada.N_pontos
    N_Realizações = Variaveis_de_Entrada.N_Realizações
        
    ρs = Variaveis_de_Entrada.ρs

    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Ls = range(L_INICIAL,L_FINAL,length=N_pontos) 
        Range_L = zeros(N_pontos)
        @.Range_L = Ls
    end

    Espectro_TOTAL = Variaveis_de_Entrada.Espectro_TOTAL

    N_Curvas = size(ρs,1)
    Σ² = zeros(N_pontos,N_Curvas)

    for j in 1:N_Curvas 

        ρ = ρs[j]
        rₘᵢₙ = 1/(3*sqrt(ρ))
        N = round(Int64,ρ*π*(Variaveis_de_Entrada.Radius^2))

        Espectro = Any[]

        entrada_extratora = E8_ENTRADA(
            N,
            ρ,
            Variaveis_de_Entrada.k,
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.Δ,
            Variaveis_de_Entrada.Radius,
            rₘᵢₙ,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            1,
            1,
            Variaveis_de_Entrada.Geometria
        )

        tuplas_espectro = ProgressMeter.@showprogress 2 "Rigidez Espectral, ρ = {$ρ}===>" pmap(1:N_Realizações; retry_delays = zeros(3)) do N_Realização
            valor_retorno = E8(entrada_extratora)
        end


        for n in 1:N_Realizações
            append!(Espectro,tuplas_espectro[n])
        end

        tuplas_espectro = 0

        Espectro_TOTAL[j] = append!(Espectro_TOTAL[j],Espectro)


        Espectro_plano = zeros(size(Espectro,1),2)
        r_plano = zeros(size(Espectro,1)) 
        Espectro_plano[:,1] .= imag.(Espectro) 
        Espectro_plano[:,2] .= real.(Espectro) 

        for i in 1:size(Espectro,1)
            r_plano[i] = sqrt(Espectro_plano[i,1]^2 + Espectro_plano[i,2]^2)
        end

        for t in 1:N_pontos 
            Σ²[t,j] = size(findall( r_plano .<= Range_L[t] ),1)/N_Realizações  
        end
        Espectro = 0
        save("TEMPORARIO_C27.jld2", Dict("SAIDA" => (Σ²,Range_L,Espectro_TOTAL),"Parametros" => Variaveis_de_Entrada))

    end
    
    return Σ²,Range_L,Espectro_TOTAL
end



# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
# #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
