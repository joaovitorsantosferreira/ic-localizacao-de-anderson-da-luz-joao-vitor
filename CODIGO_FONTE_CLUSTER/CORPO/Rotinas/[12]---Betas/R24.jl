#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_24(Variaveis_de_Entrada::R24_ENTRADA)

    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    N_Realizações = Variaveis_de_Entrada.N_Realizações
    N_Telas = 1
    PERFIL_DA_INTENSIDADE = Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE   
    Angulo_de_variação_da_tela_circular_1 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1 
    Angulo_de_variação_da_tela_circular_2 = Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2
    L = Variaveis_de_Entrada.L
    Lₐ = Variaveis_de_Entrada.Lₐ

    DIVISAO_β_X = Variaveis_de_Entrada.DIVISAO_β_X
    DIVISAO_β_Y = Variaveis_de_Entrada.DIVISAO_β_Y

    Xs = -L/2:(L/DIVISAO_β_X):L/2
    range_X = zeros(size(Xs,1))
    @.range_X = Xs

    Ys = -Lₐ/2:(Lₐ/DIVISAO_β_Y):Lₐ/2
    range_Y = zeros(size(Ys,1))
    @.range_Y = Ys

    Δs = Variaveis_de_Entrada.Δ_INICIAL:Variaveis_de_Entrada.INTERVALO_Δ:Variaveis_de_Entrada.Δ_FINAL 
    range_Δ = zeros(size(Δs,1))
    @.range_Δ = Δs
    
    N_Curvas = size(Variaveis_de_Entrada.Ns,1)
    σ² = zeros(size(range_Δ,1),N_Curvas+1)
    Perfis_da_Intensidade = zeros(size(range_Δ,1),Variaveis_de_Entrada.N_Sensores,N_Curvas+1)
    βs = zeros(N_Curvas,size(range_Δ,1)*Variaveis_de_Entrada.DIVISAO_β_X*Variaveis_de_Entrada.DIVISAO_β_Y,4)
     


    aux_1 = 1
        
    for j in 1:N_Curvas 

        aux_2 = 1

        ProgressMeter.@showprogress 2 "Calculo_Cintura_N = {$(Variaveis_de_Entrada.Ns[aux_1])}===>" for indice_Δ in 1:size(range_Δ,1)

            Intensidade_Resultante_PURA = Any[]
            β_Resultante = Any[]
            X_position = Any[]
            Y_position = Any[]
            N = Variaveis_de_Entrada.Ns[j]


            Radius = 1 
            ρ_normalizado = N/(Variaveis_de_Entrada.L*Variaveis_de_Entrada.Lₐ)
            ρ = ρ_normalizado
            rₘᵢₙ = 1/(3*sqrt(ρ))
            ωᵦ = Variaveis_de_Entrada.ωᵦ 
            ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((Variaveis_de_Entrada.L/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)


            entrada_extratora = E12_ENTRADA(
                Variaveis_de_Entrada.Γ₀, 
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                Radius,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.ωₗ,
                range_Δ[indice_Δ],
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.Tipo_de_campo,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Angulo_de_variação_da_tela_circular_1,
                Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas,
                Variaveis_de_Entrada.Geometria,
            )

            tuplas_intensidade = pmap(1:N_Realizações; retry_delays = zeros(10)) do N_Realização
                dados = E12(entrada_extratora)

                valor_retorno = (Intensidade = dados[1],r = dados[2],β = dados[3]) 
            end

            for n in 1:N_Realizações                
                append!(Intensidade_Resultante_PURA,tuplas_intensidade[n].Intensidade)
                r_temporario = tuplas_intensidade[n].r
                append!(X_position,r_temporario[:,1])                
                append!(Y_position,r_temporario[:,2])
                append!(β_Resultante,tuplas_intensidade[n].β)                
            end

            Intensidade_media = zeros(N_Sensores)

            for i in 1:N_Sensores
        
                fator_soma = 0
                aux_3 = 0
        
                for k in 1:N_Realizações
        
                    fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                    aux_3 += N_Sensores
        
                end
        
                Intensidade_media[i] = fator_soma/N_Realizações
        
            end

            
            entrada_Sensores = get_cloud_sensors_ENTRADA(
                Variaveis_de_Entrada.kR,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                Angulo_de_variação_da_tela_circular_1,
                Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

            if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
                referencia = Posição_Sensores[ : ,4]
            elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
                referencia = zeros(N_Sensores)
                for i in 1:N_Sensores
                    θ =  get_degree_azimut(Posição_Sensores[i,1:2])
                    if θ > 180
                        θ = θ - 360
                    end
                    referencia[i] = θ
                end
        
            end

            σ²[indice_Δ,j] = get_one_σ_lorentz(referencia,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]

            Perfis_da_Intensidade[indice_Δ, : ,j] = Intensidade_media

            for v in 1:DIVISAO_β_X
                for u in 1:DIVISAO_β_Y                    
                    index_beta = findall((X_position .>= range_X[v]).*(X_position .<= range_X[v+1]).*(Y_position .>= range_Y[u]).*(Y_position .<= range_Y[u+1]))
                    if  index_beta == Int[]
                        βs[j,aux_2, : ] = [range_Δ[indice_Δ],mean([range_X[v],range_X[v+1]]),mean([range_Y[u],range_Y[u+1]]),0]
                    else
                        βs[j,aux_2, : ] = [range_Δ[indice_Δ],mean([range_X[v],range_X[v+1]]),mean([range_Y[u],range_Y[u+1]]),mean(β_Resultante[index_beta])]

                    end
                    
                    aux_2 += 1
                end
            end
            save("TEMPORARIO_C9.jld2", Dict("SAIDA" => (σ²,Perfis_da_Intensidade,referencia,βs,range_Δ),"Parametros" => Variaveis_de_Entrada))
        end
        aux_1 += 1
    end

    for indice_Δ in 1:size(range_Δ,1)

        ωᵦ = Variaveis_de_Entrada.ωᵦ
        ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*((Variaveis_de_Entrada.L/2) + Variaveis_de_Entrada.Distancia_dos_Sensores)^2))/2)


        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_Entrada.kR,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            Angulo_de_variação_da_tela_circular_1,
            Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
            Sensores_INC = Sensores[ : ,3:4]
        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            Sensores_INC = Sensores[ : ,1:2]
        end

        Intensidade_PURA = zeros(N_Sensores)
        for i in 1:N_Sensores
            Intensidade_PURA[i] = get_eletric_field_LG_modulo_quadrado(Sensores_INC[i,:],Variaveis_de_Entrada.Ω,ω₀,Variaveis_de_Entrada.k,Geometria)
        end

        if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
            referencia = Sensores[ : ,4]
        elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
            referencia = zeros(N_Sensores)
            
            for i in 1:N_Sensores
                θ =  get_degree_azimut(Sensores[i,1:2])
                if θ > 180
                    θ = θ - 360
                end
                referencia[i] = θ
            end
        end
        
        Perfis_da_Intensidade[indice_Δ, : ,end] = Intensidade_PURA[ : ]

        σ²[indice_Δ,end]  = get_one_σ_gaussian(referencia,Perfis_da_Intensidade[indice_Δ, : ,end],Variaveis_de_Entrada.Geometria)[2]
    end

    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Variaveis_de_Entrada.kR,
        Variaveis_de_Entrada.N_Sensores,
        Variaveis_de_Entrada.Distancia_dos_Sensores,
        Variaveis_de_Entrada.Angulo_da_luz_incidente,
        Angulo_de_variação_da_tela_circular_1,
        Angulo_de_variação_da_tela_circular_2,
        Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_Entrada.L,
        Variaveis_de_Entrada.Lₐ,
        PERFIL_DA_INTENSIDADE,
        N_Telas
    )
    # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
    Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

    if Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
        referencia = Sensores[ : ,4]
    elseif Variaveis_de_Entrada.PERFIL_DA_INTENSIDADE == "NAO"
        referencia = zeros(N_Sensores)
        for i in 1:N_Sensores
            θ =  get_degree_azimut(Sensores[i,1:2])
            if θ > 180
                θ = θ - 360
            end
            referencia[i] = θ
        end
    end
    

    return σ²,Perfis_da_Intensidade,referencia,βs,range_Δ
end