mutable struct E7_ENTRADA
    Γ₀::Number 
    Ω ::Number
    k::Number
    vetor_de_onda::Any
    ω₀::Number
    Δ ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    Tipo_de_beta::String
    Desordem_Diagonal::String
    PERFIL_DA_INTENSIDADE::String
    cloud::Cloud_SAIDA
    Posição_Sensores::Array
    N_Telas::Integer
    L::Number
    Radius::Number
    GEOMETRIA_DA_NUVEM::String
end


