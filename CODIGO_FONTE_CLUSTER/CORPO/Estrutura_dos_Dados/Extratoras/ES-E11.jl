mutable struct E11_ENTRADA
    Γ₀::Number 
    ωₐ::Number
    Ω ::Number
    k::Number
    N::Integer
    Radius::Number
    ρ::Number
    rₘᵢₙ::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    ω₀::Number
    λ::Number
    ωₗ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    Tipo_de_beta::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
    Geometria::Dimension
    Δ::Number
end

