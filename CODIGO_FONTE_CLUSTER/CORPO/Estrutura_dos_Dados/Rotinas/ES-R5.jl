mutable struct R5_ENTRADA
    Γ₀::Number
    k::Number
    L::Number
    Lₐ::Number
    Radius::Number
    N_Realizações::Integer
    Tipo_de_kernel::String
    N_div_Densidade::Integer
    N_div_Deturn::Integer
    Densidade_Inicial::Number
    Densidade_Final::Number
    Δ_Inicial::Number
    Δ_Final::Number
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    Geometria::Dimension
end



