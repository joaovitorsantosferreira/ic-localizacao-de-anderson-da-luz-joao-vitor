mutable struct R19_ENTRADA
    ωₐ::Number
    Γ₀::Number
    k::Number
    GEOMETRIA_DA_NUVEM::String
    Radius::Number
    Lₐ::Number
    L::Number
    b₀s::Any
    Tipo_de_kernel::String
    Δ::Number
    Escala_do_range::String
    N_Realizações::Integer
    RANGE_INICIAL::Number
    RANGE_FINAL::Number
    N_pontos::Integer
    percentual_de_modos::Number
end
