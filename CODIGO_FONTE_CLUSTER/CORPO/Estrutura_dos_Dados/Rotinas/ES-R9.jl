mutable struct R9_ENTRADA
    Γ₀::Number
    ωₐ::Number
    k::Number
    Δ::Number
    Ω::Number
    Ns::Any
    b₀s::Any
    kR::Number
    L::Number
    Lₐ::Number
    Angulo_da_luz_incidente::Any
    vetor_de_onda::Any
    λ::Number
    ωₗ::Number
    ω₀::Number
    Tipo_de_Δ::String
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    Tipo_de_campo::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Tipo_de_beta::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    N_Realizações::Integer
    Variavel_Constante::String
    Geometria::Dimension
    PERFIL_DA_INTENSIDADE::String
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
end

# entrada_rotina = R9_ENTRADA(
#     Γ₀,
#     ωₐ,
#     k,
#     N,
#     kR,
#     Tipo_de_kernel,
#     N_Realizações,
#     N_div_Densidade,
#     N_div_Deturn,
#     Δ_Inicial,
#     Δ_Final,
#     Densidade_Inicial,
#     Densidade_Final,
#     Variavel_Constante,
#     Escala_de_Densidade,
# )