mutable struct R27_ENTRADA
    Γ₀::Number  
    ρs::Any
    k::Number
    Δ::Number
    Tipo_de_kernel::String
    Radius::Number
    GEOMETRIA_DA_NUVEM::String
    N_pontos::Integer
    N_Realizações::Integer
    RANGE_INICIAL::Number
    RANGE_FINAL::Number
    Escala_do_range::String
    Desordem_Diagonal::String
    W::Number
    Espectro_TOTAL::Any
    Geometria::Dimension    
end
