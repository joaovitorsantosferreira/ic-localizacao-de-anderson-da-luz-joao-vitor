mutable struct R1_ENTRADA
    Γ₀::Any 
    ωₐ::Any
    Ω ::Any
    k::Any
    N::Integer
    Radius::Any
    ρ::Any
    rₘᵢₙ::Any
    Angulo_da_luz_incidente::Integer
    vetor_de_onda::Any
    ω₀::Any
    λ::Any
    ωₗ::Any
    Δ::Any
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    Tipo_de_campo::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Any
    Angulo_de_variação_da_tela_circular_1::Any
    Angulo_de_variação_da_tela_circular_2::Any
    Tipo_de_beta::String
    N_Realizações::Integer
    N_div::Integer
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
    PERFIL_DA_INTENSIDADE::String
    Geometria::Dimension
end

mutable struct R1_SAIDA_PROBABILIDADE
    Intensidade_Resultante_PURA::Array
    Intensidade_Resultante_NORMALIZADA::Array
    Histograma_LINEAR::Any
    Histograma_LOG::Any
    variancia::Number
end

mutable struct R1_SAIDA_PERFIL
    Intensidade_Resultante_PURA::Array
    Intensidade_Resultante_NORMALIZADA::Array
    Posição_Sensores::Array
    r::Array
end

