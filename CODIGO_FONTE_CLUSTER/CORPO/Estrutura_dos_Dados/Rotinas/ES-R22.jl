mutable struct R22_ENTRADA
    Γ₀::Number
    k::Number
    Δ::Number
    Ls::Any
    Lₐ::Number
    N_Realizações::Integer
    Tipo_de_kernel::String
    N_div_Densidade::Integer
    ρ_Inicial::Number
    ρ_Final::Number
    Desordem_Diagonal::String
    W::Number
    Geometria::Dimension
end
