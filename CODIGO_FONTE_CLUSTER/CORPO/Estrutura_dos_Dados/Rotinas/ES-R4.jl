mutable struct R4_ENTRADA
    Γ₀::Number
    k::Number
    ρ::Number
    L::Number
    Lₐ::Number
    Radius::Number
    N_Realizações::Integer
    Tipo_de_kernel::String
    N_div_Deturn::Integer
    Δ_Inicial::Number
    Δ_Final::Number
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    Geometria::Dimension
end
