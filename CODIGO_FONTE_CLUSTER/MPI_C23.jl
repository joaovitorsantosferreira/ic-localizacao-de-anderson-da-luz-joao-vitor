using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(192, exeflags=`--threads 1`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin

    using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                        
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots
    using Loess

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R11.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R21.jl")

    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E2.jl")
    include("CORPO/Rotinas/[6]---Localização Transversal/R11.jl") 
    include("CORPO/Rotinas/[6]---Localização Transversal/R21.jl") 

    ωₐ = 1                                                                                           # Tempo de decaimento atomico
    Γ₀ = 1                                                                                           # Frequencia natural dos atomos 
    Lₐ = 150
    N_div_Densidade = 20                                                                                             
    N_div_Deturn = 40                                                                                                
    Δ_Inicial = 0                                                                                                  
    Δ_Final = 4                                                                                                      
    Densidade_Inicial = 0.1                                                                                            
    Densidade_Final = 1    
    GEOMETRIA_DA_NUVEM = "SLAB"                                                                      # Define a geometria da nuvem como Slab
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                    # Define o struct que representa a dimensão e geometria da nuvem
    Tipo_de_kernel = "Escalar"                                                                       # Define que a analise é feita com a luz vetorial
    Tipo_de_Onda = "Laser Gaussiano"                                                                 # Define que a onda incidente tem um perfil de um Laser Gaussiano
    Tipo_de_beta = "Estacionário"                                                                    # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
    Tipo_de_campo = "NEAR FIELD"
    k = 1 
    Ω = Γ₀/1000                                                                                      # Frequência de Rabi
    Angulo_da_luz_incidente = 0                                                                      # Angulo deIncidencia do Laser
    vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                # Vetor de propagação da onda
    λ = (2*π)/k                                                                                      # Comprimento de onda 
    N_Sensores = 1000                                                                                # Quantidade de Sensores ao redor da amostra
    Distancia_dos_Sensores = λ/8                                                                     # Define a distacia entre os sensores e os sistema
    Angulo_de_variação_da_tela_circular_1 = -90                                                      # Define o angulo inicial da tela circular de Sensores
    Angulo_de_variação_da_tela_circular_2 = 90                                                       # Define o angulo final da tela circular de Sensores
    angulo_coerente = 40                                                         
    Escala_do_range = "LINEAR"                                                                       # Define que a escala no eixo Y será Linear
    PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"         
    N_Realizações = 1000                                                                              # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
    RANGE_INICIAL = 4*pi
    RANGE_FINAL = 25    
    N_pontos = 10
    Desordem_Diagonal = "AUSENTE"                                                                    # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
    W = 0                                                                                           # Amplitude de Desordem

    Entrada = R21_ENTRADA(Γ₀,ωₐ,Ω,k,Angulo_da_luz_incidente,vetor_de_onda,λ,Tipo_de_kernel,Tipo_de_Onda,Tipo_de_campo,N_Sensores,PERFIL_DA_INTENSIDADE,Distancia_dos_Sensores,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2,angulo_coerente,Tipo_de_beta,N_pontos,N_Realizações,Lₐ,RANGE_INICIAL,RANGE_FINAL,Escala_do_range,Desordem_Diagonal,W,Geometria,N_div_Densidade,N_div_Deturn,Δ_Inicial,Δ_Final,Densidade_Inicial,Densidade_Final)
end


darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )

Dados_C23 = ROTINA_21(Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C23_{$GEOMETRIA_DA_NUVEM}_{$Tipo_de_kernel}.jld2", Dict("SAIDA" => Dados_C23,"Parametros" => Entrada))
println(" - ")


MPIClusterManagers.stop_main_loop(manager)
println("saiu")