using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(80, exeflags=`--threads 1`)

@time @everywhere begin

    using Distances
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random   
    using Statistics                                                                                                                
    using SpecialFunctions                                                                                                          
    using QuadGK   
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Interpolations
    using Loess

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R9.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E2.jl")
    include("CORPO/Rotinas/[6]---Localização Transversal/R9.jl") 


    Γ₀ = 1    
    ωₐ = 1   
    k = 1 
    λ = (2*π)/k                                                                                          
    GEOMETRIA_DA_NUVEM = "SLAB"                                                                       
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                       
    b₀s = (0.5,1,5,10)
    kR = 10                                                                                           
    Lₐ = 200
    L = 30
    Δ = 0
    ω₀ = 5*λ
    Ns = ((round(Int64,0.1*L*Lₐ)),(round(Int64,0.2*L*Lₐ)),(round(Int64,0.4*L*Lₐ)),(round(Int64,0.7*L*Lₐ)),(round(Int64,1*L*Lₐ)))
    ρ = zeros(size(Ns,1))       
    @.ρ = Ns/(L*Lₐ)
    Tipo_de_kernel = "Escalar"                            
    Tipo_de_Onda = "Laser Gaussiano"                      
    Tipo_de_beta = "Estacionário"                         
    Tipo_de_Δ = "INTERPOLAÇÃO"
    Tipo_de_campo = "NEAR FIELD"
    PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"
    Angulo_de_variação_da_tela_circular_1 = -60
    Angulo_de_variação_da_tela_circular_2 = 60
    Δ = 0                                                 
    Ω = Γ₀*1e-3                                        
    Angulo_da_luz_incidente = 0                                                                          
    vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                    
    ωₗ = Δ + ωₐ                                                                                          
    Variavel_Constante = "N"                                                       
    N_Realizações = 100                                                             
    Desordem_Diagonal = "AUSENTE"                                                  
    W = 0                                                                          
    N_Sensores = 1000                                                              
    Distancia_dos_Sensores = λ/2                                                  
    # Distancia_dos_Sensores = 2000*sqrt(λ^2 + L^2 + Lₐ^2)                                                  

    Variaveis_de_Entrada = R9_ENTRADA(Γ₀,ωₐ,k,Δ,Ω,Ns,b₀s,kR,L,Lₐ,Angulo_da_luz_incidente,vetor_de_onda,λ,ωₗ,ω₀,Tipo_de_Δ,Tipo_de_kernel,Tipo_de_Onda,Tipo_de_campo,N_Sensores,Distancia_dos_Sensores,Tipo_de_beta,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,N_Realizações,Variavel_Constante,Geometria,PERFIL_DA_INTENSIDADE,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2)
end

darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )

Dados_C11 = ROTINA_9(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)
W
save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C11_{$GEOMETRIA_DA_NUVEM}_{$Tipo_de_kernel}_Delta__{$Tipo_de_Δ}_CAMPO{$Tipo_de_campo}_L{$L}.jld2", Dict("SAIDA" => Dados_C11,"Parametros" => Variaveis_de_Entrada))
# save("BETAS.jld2", Dict("SAIDA" => Dados_C13[4],"Parametros" => Variaveis_de_Entrada))
println(" - ")

MPIClusterManagers.stop_main_loop(manager)
println("saiu")