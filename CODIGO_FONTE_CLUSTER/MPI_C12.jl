using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(20, exeflags=`--threads 4`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin

    using Distances
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random   
    using Statistics                                                                                                                
    using Plots    
    using SpecialFunctions                                                                                                          
    using QuadGK   
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Interpolations

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E9.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R10.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E9.jl")
    include("CORPO/Rotinas/[5]---Transmissão/R10.jl") 


    Γ₀ = 1    
    ωₐ = 1    
    GEOMETRIA_DA_NUVEM = "SLAB"                                                                       
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)        
    Radius = 10                                                                                           
    Lₐ = 100
    L = 30
    ρs = get_points_in_log_scale(0.1,4,7)
    ω₀ = 6*pi
    Tipo_de_kernel = "Escalar"                            
    Tipo_de_Onda = "Laser Gaussiano"                      
    Tipo_de_beta = "Estacionário"                         
    Tipo_de_campo = "NEAR FIELD"
    PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"
    k = 1
    Δ = 0                                                 
    Ω = Γ₀/1000                                           
    angulo_da_luz_incidente = 0                                                                          
    vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                    
    λ = (2*π)/k                                                                                          
    Angulo_de_variação_da_tela_circular_1 = 0
    Angulo_de_variação_da_tela_circular_2 = 360
    angulo_T_INICIAL = 50
    angulo_T_FINAL = 60
    angulo_coerente = 30                                                             
    Desordem_Diagonal = "AUSENTE"                                                  
    W = 0                                                                          
    N_Sensores = 1000                                                              
    Distancia_dos_Sensores = λ                                                  

    b_INICIAL = 0.01
    b_FINAL = 10
    N_pontos = 20
    Escala_de_b = "LOG"
    N_Realizações = 300

    Variaveis_de_Entrada = R10_ENTRADA(Γ₀,ωₐ,Ω, k,angulo_da_luz_incidente,vetor_de_onda,λ,Tipo_de_kernel,Tipo_de_Onda,N_Sensores,Distancia_dos_Sensores,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2,angulo_coerente,Tipo_de_beta,N_pontos,N_Realizações,b_INICIAL,b_FINAL,angulo_T_INICIAL,angulo_T_FINAL,Escala_de_b,Desordem_Diagonal,W,L,Lₐ,Radius,ρs,ω₀,Geometria,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,Tipo_de_campo)
end

darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )

Dados_C12 = ROTINA_10(Variaveis_de_Entrada,Geometria)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C12_{$GEOMETRIA_DA_NUVEM}_{$Tipo_de_kernel}_L{$L}_CAMPO{$Tipo_de_campo}.jld2", Dict("SAIDA" => Dados_C12,"Parametros" => Variaveis_de_Entrada))
println(" - ")


MPIClusterManagers.stop_main_loop(manager)
println("saiu")

