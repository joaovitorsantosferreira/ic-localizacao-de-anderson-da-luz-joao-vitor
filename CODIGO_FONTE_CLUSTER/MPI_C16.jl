using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(40, exeflags=`--threads 2`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin

    using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                        
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots
    using Interpolations

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E10.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R14.jl")
    
    
    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E10.jl")
    include("CORPO/Rotinas/[8]---Energia/R14.jl") 

    Γ₀ = 1                                                                                                                 
    ωₐ = 1                                                                                                                 
    k = 1
    λ = (2*π)/k                                                                                                                   
    GEOMETRIA_DA_NUVEM = "SLAB"                                                                                           
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                            
    Lₐ = 40*λ
    N = 8000
    Tipo_de_kernel = "Escalar"                                                                                             
    Escala_do_range = "LOG"                                                                                                
    N_Realizações = 50                                                                                                     
    RANGE_INICIAL = 0.1
    RANGE_FINAL = 1
    N_pontos = 10
    Desordem_Diagonal = "AUSENTE"                                                     
    W = 0                                                                            
    
    Variaveis_de_Entrada = R14_ENTRADA(Γ₀,k,N,Tipo_de_kernel,N_pontos,N_Realizações,Lₐ,RANGE_INICIAL,RANGE_FINAL,Escala_do_range,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,Geometria)
end

darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )


Dados_C16 = ROTINA_14(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C16.jld2", Dict("SAIDA" => Dados_C16,"Parametros" => Variaveis_de_Entrada))
println(" - ")


MPIClusterManagers.stop_main_loop(manager)
println("saiu")
