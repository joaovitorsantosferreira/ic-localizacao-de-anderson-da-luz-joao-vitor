using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(10, exeflags=`--threads 12`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])
@time @everywhere begin 

    using Distances    
    using LinearAlgebra
    using LaTeXStrings  
    using Random       
    using Statistics   
    using Plots        
    using SpecialFunctions
    using QuadGK       
    using PlotThemes   
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R19.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Rotinas/[11]---Desordem_Espectro/R19.jl") 


    ωₐ = 1                                                                                                                  
    Γ₀ = 1                                                                                                                  
    GEOMETRIA_DA_NUVEM = "DISCO"                                                                                            
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                           
    Radius = 10.3
    Lₐ = 140
    L = 50
    b₀s = get_points_in_log_scale(100,200,3)
    Tipo_de_kernel = "Vetorial"                                                                                             
    Δ = 0
    k = 1                                                
    Escala_do_range = "LOG"                                                                                                 
    N_Realizações = 100                                                                                                      
    RANGE_INICIAL = 0.01
    RANGE_FINAL = 100
    N_pontos = 50
    percentual_de_modos = 20

    Variaveis_de_Entrada = R19_ENTRADA(ωₐ,Γ₀,k,GEOMETRIA_DA_NUVEM,Radius,Lₐ,L,b₀s,Tipo_de_kernel,Δ, Escala_do_range,N_Realizações,RANGE_INICIAL,RANGE_FINAL,N_pontos,percentual_de_modos)
end


darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )

Dados_C21 = ROTINA_19__PR_Desordem(Variaveis_de_Entrada,Geometria)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_C21.jld2", Dict("SAIDA" => Dados_C21,"Parametros" => Variaveis_de_Entrada))
println(" - ")
