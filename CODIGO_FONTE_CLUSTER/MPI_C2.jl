using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(7, exeflags=`--threads 1`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 

    using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                          
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates

    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")

    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E2.jl")
    include("CORPO/Rotinas/[1]---Estatistiscas__Da__Intensidade/R1.jl") 

    GEOMETRIA_DA_NUVEM = "DISCO"                                                                                             
    ωₐ = 1                                                                                                                  
    Γ₀ = 1                                                                                                                 
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                         
    k = 1                                                                                                        
    ρ = 2                                                                                            
    Lₐ = 280                                                                                                
    N = 1000                                                                                                     
    Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
    Tipo_de_kernel = "Escalar"                                                                          
    Tipo_de_Onda = "Laser Gaussiano"                                                                              
    Tipo_de_beta = "Estacionário"       
    Tipo_de_campo = "NEAR FIELD"
    Δ = -1                                                                                          
    Ω = Γ₀/1000                                                                 
    angulo_da_luz_incidente = 0                                                                    
    vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                       
    λ = (2*π)/k                                                                                                  
    ωₗ = Δ + ωₐ                                                                                           
    PERFIL_DA_INTENSIDADE = "NAO"                               
    N_Sensores = 45                                                                     
    comprimento_de_ray = (k*(ω₀^2))/2                                                    
    Distancia_dos_Sensores = 10*Radius                                                                
    Angulo_de_variação_da_tela_circular_1 = 45                                                                    
    Angulo_de_variação_da_tela_circular_2 = 85                                                                                                                              
    N_Realizações = 300                   
    N_div = 30                                     
    Desordem_Diagonal = "AUSENTE"                                           
    W = 0                                                                                                                   

    Variaveis_de_Entrada = R1_ENTRADA(Γ₀,ωₐ,Ω,k,N,Radius,ρ,rₘᵢₙ,angulo_da_luz_incidente,vetor_de_onda,ω₀,λ,ωₗ,Δ,Tipo_de_kernel,Tipo_de_Onda,Tipo_de_campo,N_Sensores,Distancia_dos_Sensores,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2,Tipo_de_beta,N_Realizações,N_div,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,L,Lₐ,PERFIL_DA_INTENSIDADE,Geometria)

end

darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )


Dados_C2 = ROTINA_1(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C2.jld2", Dict("SAIDA" => Dados_C2,"Parametros" => Variaveis_de_Entrada))
println(" - ")


MPIClusterManagers.stop_main_loop(manager)
println("saiu")










#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#





# bins
# aux_1 = 0

# for i in 1:(N_Sensores*N_Realizações)

#     if Intensidade_Normalizada[i] < 0.1
#         aux_1 += 1
#     end

# end
# aux_1


# vizualizar_Histograma_das_Intensidade_log(h,1000)
# savefig("Romain_227.png")






