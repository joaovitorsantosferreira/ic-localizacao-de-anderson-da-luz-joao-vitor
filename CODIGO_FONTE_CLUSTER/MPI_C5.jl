using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(95, exeflags=`--threads 1`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 

	using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                          
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E3.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R22.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E3.jl")
    include("CORPO/Rotinas/[3]---Comprimento__De__Localização/R22.jl")  
    
    k = 1                                                                                                            
    Γ₀ = 1
    Δ = 0                                                                                                           
    GEOMETRIA_DA_NUVEM = "SLAB"
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM)  
    Ls = [10,13,15]
    Lₐ = 80
    Tipo_de_kernel = "Escalar"                                                                                      
    N_Realizações = 200                                                                                               
    N_div_Densidade = 20                                                                                             
    ρ_Inicial = 0.01                                                                                            
    ρ_Final = 10                                                                                              
    Desordem_Diagonal = "AUSENTE"                                                                                    
    W = 10                                                                                                                 

    Variaveis_de_Entrada = R22_ENTRADA(Γ₀,k,Δ,Ls,Lₐ,N_Realizações,Tipo_de_kernel,N_div_Densidade,ρ_Inicial,ρ_Final,Desordem_Diagonal,W,Geometria)
end



darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )


Dados_C5 = ROTINA_22(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C5.jld2", Dict("SAIDA" => Dados_C5,"Parametros" => Variaveis_de_Entrada))
println(" - ")
