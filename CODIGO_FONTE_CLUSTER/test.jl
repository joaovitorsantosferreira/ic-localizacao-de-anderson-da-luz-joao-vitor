# using Pkg
# Pkg.activate(".")

using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(2, exeflags=`--threads 4`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 
	using LinearAlgebra, MKL
    # using Statistics
    # using StatsBase
    # using DifferentialEquations
    # using Random
    # using SpecialFunctions                                                                                                          
    # using QuadGK                                                                                                                                                                                                                
    # using LsqFit
    # using Optim
    # using ProgressMeter
    
	myFunction() = println("rand number of today is $(rand())")

	function myFunction2()
		println("Threads.nthreads(): $(Threads.nthreads())")
		
		N = 10
		A = rand(ComplexF64, N,N); @time eigen(A);

		N = 100
		A = rand(ComplexF64, N,N); @time eigen(A);

		N = 200
		A = rand(ComplexF64, N,N); @time eigen(A);
		
		return nothing
	end
end
darOI() = println("Começou o código")
darOI()


println( "workers(): $(workers())" )
resposta = pmap(1:4; on_error=ex->0) do x
	myFunction2()
	(gethostname(), getpid)
end
display(resposta)
println(" - ")


a = zeros(10)
Threads.@threads for i = 1:10
	println(i)
	a[i] = Threads.threadid()
end
println(" - ")
println(a)



MPIClusterManagers.stop_main_loop(manager)
println("saiu")