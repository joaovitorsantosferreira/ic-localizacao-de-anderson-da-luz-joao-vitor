using Distributed, MPIClusterManagers
@time manager = MPIClusterManagers.start_main_loop(MPI_TRANSPORT_ALL)
@time addprocs(120, exeflags=`--threads 1`)

# @everywhere using StatsBase
# import Pkg; Pkg.add("Distances"); 
# import Pkg; Pkg.add(["Plots", "LaTeXStrings", "PlotThemes", "FileIO", "JLD2", "Dates"]); 

# using Plots
# using LaTeXStrings
# using PlotThemes    
# using FileIO
# using JLD2
# using Dates

# import Pkg; 
# Pkg.add(["Distances", "Statistics", "StatsBase", "DifferentialEquations", 
# 	"Random", "SpecialFunctions", "QuadGK", "LsqFit", "Optim", "ProgressMeter"])

@time @everywhere begin 

    using Distances                                                                                                                 
    using LinearAlgebra                                                                                                             
    using LaTeXStrings                                                                                                               
    using Random                                                                                                                    
    using Statistics                                                                                                                
    using Plots                                                                                                                     
    using SpecialFunctions                                                                                                        
    using QuadGK                                                                                                                    
    using PlotThemes                                                                                                                
    using StatsBase 
    using LsqFit
    using Optim
    using ProgressMeter
    using FileIO
    using JLD2
    using Dates
    using Roots
    using Interpolations
    using Loess


    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
    include("CORPO/Estrutura_dos_Dados/Programas_Base/ES-P5.jl")
    include("CORPO/Estrutura_dos_Dados/Extratoras/ES-E12.jl")
    include("CORPO/Estrutura_dos_Dados/Rotinas/ES-R24.jl")


    include("CORPO/Programas Base/P1.jl")
    include("CORPO/Programas Base/P2.jl")
    include("CORPO/Programas Base/P3.jl")
    include("CORPO/Programas Base/P4.jl")
    include("CORPO/Programas Base/P5.jl")
    include("CORPO/Programas Base/P6.jl")
    include("CORPO/Extração/E12.jl")  
    include("CORPO/Rotinas/[12]---Betas/R24.jl") 
    

    Γ₀ = 1
    ωₐ = 1
    k = 1
    Ω = Γ₀/1000 
    Ns = (1500)
    Radius = 1
    L = 20
    Lₐ = 150
    Lₐ_titulo = round(Lₐ,digits = 2)
    ρ = Ns./(L*Lₐ)   
    Angulo_da_luz_incidente = 0
    vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente)) 
    λ = (2*π)/k                                                                                                                
    ωₗ = 0 + ωₐ    
    Tipo_de_kernel = "Escalar"
    Tipo_de_Onda = "Laser Gaussiano"
    Tipo_de_campo = "NEAR FIELD"
    N_Sensores = 1000
    Distancia_dos_Sensores = λ/4
    # Distancia_dos_Sensores = 10*sqrt((L/2)^2 + (Lₐ/2)^2)
    Tipo_de_beta = "Estacionário"
    Desordem_Diagonal = "AUSENTE" 
    W = 0
    GEOMETRIA_DA_NUVEM = "SLAB" 
    N_Realizações = 240
    Δ_INICIAL = 0.25775853395403314
    Δ_FINAL = 0.4 
    INTERVALO_Δ = 0.1
    DIVISAO_β_X = 20 
    DIVISAO_β_Y = 200
    Variavel_Constante = "N" 
    ωᵦ = 3*λ
    Geometria = get_geometria(GEOMETRIA_DA_NUVEM) 
    PERFIL_DA_INTENSIDADE = "NAO" 
    Angulo_de_variação_da_tela_circular_1 = -90
    Angulo_de_variação_da_tela_circular_2 = 90

    Variaveis_de_Entrada = R24_ENTRADA(Γ₀,ωₐ,k,Ω,Ns,Radius,L,Lₐ,Angulo_da_luz_incidente,vetor_de_onda,λ,ωₗ,Tipo_de_kernel,Tipo_de_Onda,Tipo_de_campo,N_Sensores,Distancia_dos_Sensores,Tipo_de_beta,Desordem_Diagonal,W,GEOMETRIA_DA_NUVEM,N_Realizações,Δ_INICIAL,Δ_FINAL, INTERVALO_Δ,DIVISAO_β_X,DIVISAO_β_Y,Variavel_Constante,Geometria,ωᵦ,PERFIL_DA_INTENSIDADE,Angulo_de_variação_da_tela_circular_1,Angulo_de_variação_da_tela_circular_2)
end

darOI() = println("Começou o código")
darOI()

println( "workers(): $(workers())" )

Dados_C9 = ROTINA_24(Variaveis_de_Entrada)

DATA = Dates.now()
ano = Dates.year(DATA)
mes = Dates.monthname(DATA)
dia = Dates.day(DATA)

save("DATA={$ano-$mes-$dia}_Dados_Antigos_CLUSTER_C9.jld2", Dict("SAIDA" => Dados_C9,"Parametros" => Variaveis_de_Entrada))

println(" - ")






