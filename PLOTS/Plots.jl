
###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------------ Graficos ---------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------- Vizualização da distribuição espacial dos atomos no disco ------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_distribuição_dos_atomos(r,Radius,L,GEOMETRIA_DA_NUVEM,geometria::TwoD,Lₐ,tamanho)
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = sqrt(L^2 + Lₐ^2)/2

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end
    
    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]


    scatter(x_a,y_b,
    size = (tamanho, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    c = :green,
    framestyle = :box,
    gridalpha = 0.3,
    ylims = (-(1.2)*Limite,(1.2)*Limite),
    xlims = (-(1.2)*Limite,(1.2)*Limite),
    ms = 4,
    label = L"\textrm{Atomos}",
    title = L"\textrm{Distribuição espacial dos Atomos}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )
    

    xlabel!(L"\textrm{Posição X}")
    ylabel!(L"\textrm{Posição Y}")
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_distribuição_dos_atomos(r,Radius,L,GEOMETRIA_DA_NUVEM,geometria::ThreeD,Lₐ,tamanho)
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "CUBO"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = Radius
    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        Limite = Lₐ
    
    elseif GEOMETRIA_DA_NUVEM == "TUBO" 
        
        Limite = sqrt(Lₐ^2 + L^2)
    
    end
    
    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = r_cartesian[:,3]

    scatter(x_a,y_b,z_c,
    size = (tamanho, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    c = :green,
    framestyle = :box,
    gridalpha = 0.3,
    ylims = (-(1.2)*Limite,(1.2)*Limite),
    xlims = (-(1.2)*Limite,(1.2)*Limite),
    zlims = (-(1.2)*Limite,(1.2)*Limite),
    ms = 4,
    label = L"\textrm{Atomos}",
    camera=(45,45),
    # title = L"\textrm{Distribuição espacial dos Atomos}",
    aspect_ratio =:equal,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )

    if GEOMETRIA_DA_NUVEM == "CUBO"


    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 

    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)    
    
    elseif GEOMETRIA_DA_NUVEM == "TUBO" 

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
    
    end


    

    xlabel!(L"\textrm{Posição X}")
    ylabel!(L"\textrm{Posição Y}")
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------- Vizualização do melhor modo gerado pelo disco -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_o_modo_mais_subrandiante(r,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,IPR,geometria::TwoD,tamanho)
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = sqrt(L^2 + Lₐ^2)/2

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end
    
    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = log10.(ψᵦ²)

    scatter(x_a,y_b,
    size = (tamanho+150, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    ylims = (-(1.2)*Limite,(1.2)*Limite),
    xlims = (-(1.2)*Limite,(1.2)*Limite),
    ms = 6,
    zcolor = z_c,
    framestyle = :box,
    label = "IPR=$IPR",
    color = cgrad(categorical = true),
    colorbar_title = L"$log_{10}(p_i)$",
    title = L"\textrm{Modo com maior IPR}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )

    xlabel!(L"\textrm{Posição X}")
    ylabel!(L"\textrm{Posição Y}")
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_o_modo_mais_subrandiante(r,ψᵦ²,Radius,L,GEOMETRIA_DA_NUVEM,IPR,geometria::ThreeD,tamanho)
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "CUBO"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = Radius
    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        Limite = Lₐ
    
    elseif GEOMETRIA_DA_NUVEM == "TUBO" 
        
        Limite = Lₐ
    
    end


    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = r_cartesian[:,3]
    t_d = log10.(ψᵦ²)

    scatter(x_a,y_b,z_c,
    size = (tamanho+150, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    ylims = (-(1.2)*Limite,(1.2)*Limite),
    xlims = (-(1.2)*Limite,(1.2)*Limite),
    zlims = (-(1.2)*Limite,(1.2)*Limite),
    ms = 6,
    zcolor = t_d,
    framestyle = :box,
    label = "IPR=$IPR",
    color = cgrad(categorical = true),
    aspect_ratio =:equal,
    camera=(45,45),
    # colorbar_title = L"$log_{10}(p_i)$",
    # title = L"\textrm{Modo com maior IPR}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )

    xlabel!(L"\textrm{Posição X}")
    ylabel!(L"\textrm{Posição Y}")

    if GEOMETRIA_DA_NUVEM == "CUBO"


    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_3(x) = sqrt((Lₐ)^2 - x^2)
        curva_4(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_3.(Lₐs_r)
        c_L_2 =  curva_4.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)

    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)    
    
    elseif GEOMETRIA_DA_NUVEM == "TUBO" 

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
    
    end
end

(1:10) .* (1:10)'

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização do melhor modo gerado pelo disco 3D -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_o_modo_mais_subrandiante_no_disco_3D(r,ψᵦ²,tamanho)
    
    pyplot()
    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = log10.(ψᵦ²)


    plot(x_a, y_b, z_c, 
        st = :surface, 
        xlabel = "", 
        ylabel = "", 
        zlabel = "",
        titlefont = (25, "times"), 
        legendfontsize = 18,
        guidefont = (30, :blue), 
        tickfont = (25, :black), 
        camera=(-35,15),
        guide = "", 
        framestyle = :zerolines,
        size = (tamanho+200, tamanho),
        lw=0
        )

    # begin

    #     default(titlefont = (25, "times"), legendfontsize = 18,guidefont = (30, :blue), tickfont = (25, :black), guide = "", framestyle = :zerolines)
    #     plot(x_a,y_b,z_c,
    #     title = "",
    #     st=:surface,
    #     camera=(45,20),
    #     size = (tamanho+200, tamanho),
    #     lw=0,
    #     left_margin = [5Plots.mm 5Plots.mm], 
    #     bottom_margin = 5Plots.mm,xrotation = 90
    #     )
    # end
    # xaxis!(L"Eixo X")
    # yaxis!(L"Eixo Y")

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------- Vizualização de um modo especifico ------------------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_o_modo_especifico_no_disco(r,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,IPR,RG_Modo,tamanho)
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end
    
    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = log10.(ψᵦ²)

    scatter(x_a,y_b,
    size = (tamanho+150, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0.3,
    ylims = (-(0.6)*Lₐ ,(0.6)*Lₐ),
    xlims = (-2*L,2*L),
    ms = 6,
    zcolor = z_c,
    framestyle = :box,
    label = "",
    # legendtitle = "Modo = $RG_Modo",
    color = cgrad(categorical = true),
    # colorbar_title = L"$log_{10}(p_i)$",
    title = L"$log_{10}(p_i)$",
    titlelocation = :right,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 25,
    tickfontsize = 15, 
    legendtitlefontsize = 20;
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    
    )

    xlabel!(L"$kx$")
    ylabel!(L"$ky$")
end




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------- Vizualização da relação entre o o tempo de vida e a frequencia dos modos gerados pelo sistema -----------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_entre_gamma_e_omega_IPR(ωₙ,γₙ,IPRs,Δ,Γ₀,number_thouless,sigma,Tipo_de_Kernel,Desordem_Diagonal,W,b₀,titulo,tamanho)
    
    gr()
    theme(:vibrant)

    a = ωₙ
    b = abs.(γₙ)
    c = IPRs

    # number_thouless = round(number_thouless,digits = 2)

    W_Normalizado = round(W / b₀ , digits=2)

    f1_vetorial(x) = 1 + SpecialFunctions.besselh(0,1,x) + SpecialFunctions.besselh(2,1,x) 
    f2_vetorial(x) = 1 - SpecialFunctions.besselh(0,1,x) - SpecialFunctions.besselh(2,1,x)
    f3_vetorial(x) = 1 + SpecialFunctions.besselh(0,1,x) - SpecialFunctions.besselh(2,1,x) 
    f4_vetorial(x) = 1 - SpecialFunctions.besselh(0,1,x) + SpecialFunctions.besselh(2,1,x)
    f1_vetorial(x,y,z) = (1/2)*(-sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) - 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
    f2_vetorial(x,y,z) = (1/2)*(-sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) + 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
    f3_vetorial(x,y,z) = (1/2)*(sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) - 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
    f4_vetorial(x,y,z) = (1/2)*(sqrt( 4*(SpecialFunctions.besselh(0,1,x)^2) - (y-z)^2) + 2*SpecialFunctions.besselh(2,1,x) + 1im*y + 1im*z + 2)
        
    f1_escalar(x) = 1 + SpecialFunctions.besselh(0,1,x) 
    f2_escalar(x) = 1 - SpecialFunctions.besselh(0,1,x)

    if Desordem_Diagonal == "PRESENTE"
    
        if Tipo_de_Kernel == "Vetorial"

            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-4,10^2),
            yticks = [10^2,10^0,10^-2,10^-4],
            xlims = (-1+minimum(ωₙ),maximum(ωₙ)+1),
            # xticks = collect(-60:15:60),
            ms = 4,
            # aspect_ratio =:equal,
            zcolor = c,
            framestyle = :box,
            label = "",
            # legendtitle = L"$ W = %$W_Normalizado b_0 $",
            color = cgrad(categorical = true),
            # clims = (0,0.5),
            # colorbar_title = L"\textrm{IPR}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 15,
            legendtitlefontsize = 15;
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            legend =:bottomleft
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")


            Dados_1 = modos_pares_desordem(f1_vetorial,50000,15,W)
            Dados_2 = modos_pares_desordem(f2_vetorial,50000,15,W)
            Dados_3 = modos_pares_desordem(f3_vetorial,50000,15,W)
            Dados_4 = modos_pares_desordem(f4_vetorial,50000,15,W)
            
            Dados_1 = orgazine_dados_espectro_desordem(Dados_1)
            Dados_2 = orgazine_dados_espectro_desordem(Dados_2)
            Dados_3 = orgazine_dados_espectro_desordem(Dados_3)
            Dados_4 = orgazine_dados_espectro_desordem(Dados_4)
            
            Dados_Totais = Any[]
            append!(Dados_Totais,Dados_1)
            append!(Dados_Totais,Dados_2)
            append!(Dados_Totais,Dados_3)
            append!(Dados_Totais,Dados_4)
            
            parte_real = real.(Dados_Totais)
            parte_imaginaria = imag.(Dados_Totais)
            
            modos_em_ordem = sortperm(parte_real)
            
            percent = round(Int64,size(Dados_Totais,1)*(0.1/100))
            
            espectro_inferior_imag = parte_imaginaria[modos_em_ordem[1:percent]]
            espectro_inferior_real = parte_real[modos_em_ordem[1:percent]]
            
            intervalo_energia = -300:20:300
            
            borda_do_conjunto_x = zeros((size(intervalo_energia,1)-1))
            borda_do_conjunto_y = zeros((size(intervalo_energia,1)-1))
            
            for i in 1:(size(intervalo_energia,1)-1)
            
                pontos_range_x = espectro_inferior_imag[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
                pontos_range_y = espectro_inferior_real[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
            
                borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[end]]
                borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[end]]
            end
            
            
            plot!(borda_do_conjunto_x,borda_do_conjunto_y,lw=5,c=:black,label="")
            
            for i in 1:(size(intervalo_energia,1)-1)
            
                pontos_range_x = espectro_inferior_imag[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
                pontos_range_y = espectro_inferior_real[findall( (espectro_inferior_imag .>= intervalo_energia[i]).*(espectro_inferior_imag .<= intervalo_energia[i+1]))]
            
                borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[1]]
                borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[1]]
            end
            
            
            plot!(borda_do_conjunto_x,borda_do_conjunto_y,lw=5,c=:black,label="")
            
            espectro_superior_imag = parte_imaginaria[modos_em_ordem[(end-percent):end]]
            espectro_superior_real = parte_real[modos_em_ordem[(end-percent):end]]
            
            borda_do_conjunto_x = zeros((size(intervalo_energia,1)-1))
            borda_do_conjunto_y = zeros((size(intervalo_energia,1)-1))
            
            for i in 1:(size(intervalo_energia,1)-1)
            
                pontos_range_x = espectro_superior_imag[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
                pontos_range_y = espectro_superior_real[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
            
                borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[end]]
                borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[end]]
            end
            
            plot!(borda_do_conjunto_x,borda_do_conjunto_y,lw=5,c=:black,label="")
            
            for i in 1:(size(intervalo_energia,1)-1)
            
                pontos_range_x = espectro_superior_imag[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
                pontos_range_y = espectro_superior_real[findall( (espectro_superior_imag .>= intervalo_energia[i]).*(espectro_superior_imag .<= intervalo_energia[i+1]))]
            
                borda_do_conjunto_x[i] = pontos_range_x[sortperm(pontos_range_y)[1]]
                borda_do_conjunto_y[i] = pontos_range_y[sortperm(pontos_range_y)[1]]
            end
            
            
            plot!(borda_do_conjunto_x,borda_do_conjunto_y,lw=5,c=:black,label="")            
            

            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label=L"g =%$number_thouless", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label=L"$\Sigma^2 = %$sigma  $", linestyle=:dashdot, color=:red, linewidth=5)

        elseif Tipo_de_Kernel == "Escalar"
            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-4,10^2),
            yticks = [10^2,10^0,10^-2,10^-4],
            xlims = (-1+minimum(ωₙ),maximum(ωₙ)+1),
            # xticks = collect(-60:15:60),
            ms = 4,
            # aspect_ratio =:equal,
            zcolor = c,
            framestyle = :box,
            label = "",
            legendtitle = L"$ W = %$W_Normalizado b_0 $",
            color = cgrad(categorical = true),
            clims = (0,0.5),
            # colorbar_title = L"\textrm{IPR}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            legendtitlefontsize = 20;
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            legend =:bottomleft
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")
            
            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)

            x = get_points_in_log_scale(0.001,100,100)
            c1 = f1_escalar.(x)
            c2 = f2_escalar.(x) 

            r1 = real.(c1)
            i1 = imag.(c1)

            r2 = real.(c2)
            i2 = imag.(c2)

            plot!(i1,r1,yscale = :log10,c=:black,lw=6,label="")
            plot!(i2,r2,yscale = :log10,c=:red,lw=6,label=L"Modos Pares")

        end

    elseif    Desordem_Diagonal == "AUSENTE"

        if Tipo_de_Kernel == "Vetorial"

            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-30,30),
            # xticks = collect(-10:2:10),
            ms = 4,
            # aspect_ratio =:equal,
            zcolor = c,
            framestyle = :box,
            # label = "g = $number_thouless",
            label = "",
            color = cgrad(categorical = true),
            # clims=(0,0.5809667899426832),
            # colorbar_title = L"\textrm{IPR}",
            # title = L"\textrm{Calculo do IPR no plano Complexo}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            legend =:bottomleft
            
            )

            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label=L"$ g_T =%$number_thouless $", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label=L"$\Sigma^2 = %$sigma  $", linestyle=:dashdot, color=:red, linewidth=5)

            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")

            x = get_points_in_log_scale(0.03,100,100)
            c1 = f1_vetorial.(x)
            c2 = f2_vetorial.(x) 
            c3 = f3_vetorial.(x)
            c4 = f4_vetorial.(x) 


            r1 = real.(c1)
            i1 = imag.(c1)

            r2 = real.(c2)
            i2 = imag.(c2)

            r3 = real.(c3)
            i3 = imag.(c3)

            r4 = real.(c4)
            i4 = imag.(c4)

            plot!(i1,r1,c=:black,lw=3,label="",linestyle=:dashdot)
            plot!(i2,r2,c=:black,lw=3,label="")
            plot!(i3,r3,c=:red,lw=3,label="",linestyle=:dashdot)
            plot!(i4,r4,c=:red,lw=3,label=L"Modos Pares")

        elseif Tipo_de_Kernel == "Escalar"
            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-15,15),
            xticks = collect(-15:3:15),
            ms = 4,
            zcolor = c,
            framestyle = :box,
            # label = "g = $number_thouless",
            label = "",
            color = cgrad(categorical = true),
            # colorbar_title = L"\textrm{IPR}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            legend =:bottomleft
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")
            
            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4



            x = get_points_in_log_scale(0.03,100,100)
            c1 = f1_escalar.(x)
            c2 = f2_escalar.(x) 

            r1 = real.(c1)
            i1 = imag.(c1)

            r2 = real.(c2)
            i2 = imag.(c2)

            plot!(i1,r1,yscale = :log10,c=:black,lw=6,label="")
            plot!(i2,r2,yscale = :log10,c=:black,lw=6,label=L"Modos Pares")

            # vline!([ω_min, ω_max]; label=L"$ g_T =%$number_thouless$", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label=L"$\Sigma^2 = %$sigma  $", linestyle=:dashdot, color=:red, linewidth=5)        
        end


    end
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------- Vizualização da relação entre o o tempo de vida e a frequencia dos modos gerados pelo sistema -----------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_entre_gamma_e_omega_R2(ωₙ,γₙ,IPRs,Δ,Γ₀,number_thouless,Tipo_de_Kernel,Desordem_Diagonal,Amplitude_de_Desordem,b₀,titulo,tamanho)
    
    gr()
    theme(:vibrant)

    a = ωₙ
    b = abs.(γₙ)
    c = IPRs

    number_thouless = round(number_thouless,digits = 2)
    
    W_Normalizado = Amplitude_de_Desordem / b₀

    if Desordem_Diagonal == "PRESENTE"
    
        if Tipo_de_Kernel == "Vetorial"

            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-60,60),
            xticks = collect(-60:15:60),
            ms = 4,
            aspect_ratio =:equal,
            zcolor = c,
            framestyle = :box,
            label = "g = $number_thouless",
            legendtitle = "W = $W_Normalizado b₀",
            color = cgrad(categorical = true),
            # colorbar_title = L"\textrm{IPR}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            legendtitlefontsize = 20;
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
            
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")

            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)

        elseif Tipo_de_Kernel == "Escalar"
            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-15,15),
            xticks = collect(-15:3:15),
            ms = 4,
            zcolor = c,
            framestyle = :box,
            # label = "g = $number_thouless",
            label = "",
            legendtitle = "W = $W_Normalizado b₀",
            color = cgrad(categorical = true),
            # colorbar_title = L"\textrm{R2}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            legendtitlefontsize = 20;
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
            
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")
            
            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
        end

    elseif    Desordem_Diagonal == "AUSENTE"

        if Tipo_de_Kernel == "Vetorial"

            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-10,10),
            xticks = collect(-10:2:10),
            ms = 4,
            # aspect_ratio =:equal,
            zcolor = c,
            framestyle = :box,
            # label = "g = $number_thouless",
            label = "",
            color = cgrad(categorical = true),
            clims=(0,0.5809667899426832),
            # colorbar_title = L"\textrm{R2}",
            # title = L"\textrm{Calculo do IPR no plano Complexo}",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
            
            )

            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)

            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")

        elseif Tipo_de_Kernel == "Escalar"
            scatter(a,b,
            size = (tamanho+100, 3*tamanho/4),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0.5,
            yscale = :log10,
            ylims = (10^-12,10^2),
            yticks = [10^2,10^0,10^-4,10^-8,10^-12],
            xlims = (-15,15),
            xticks = collect(-15:3:15),
            ms = 4,
            zcolor = c,
            framestyle = :box,
            # label = "g = $number_thouless",
            label = "",
            color = cgrad(categorical = true),
            # colorbar_title = L"\textrm{R2}",
            # title = L"$log_{10}(\xi_n /R)$",
            title = titulo,
            titlelocation = :right,
            legendfontsize = 20,
            labelfontsize = 25,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
            
            )
            xlabel!(L"$\Delta_n $")
            ylabel!(L"$\gamma_n$")
            
            # ω_min = Δ - Γ₀/4  
            # ω_max = Δ + Γ₀/4


            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
            # vline!([ω_min, ω_max]; label="", linestyle=:dashdot, color=:red, linewidth=5)
        end


    end
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------- Vizualização da relação entre o fator q e a entropia estrutural ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_entre_q_e_Sₑ(qₙ,Sₑ,IPRs,Δ,Γ₀,number_thouless,Tipo_de_Kernel,Desordem_Diagonal,Amplitude_de_Desordem,tamanho)
    
    gr()
    theme(:vibrant)

    a = qₙ
    b = abs.(Sₑ)
    c = IPRs

    if Tipo_de_Kernel == "Vetorial"

        scatter(a,b,
        size = (tamanho+100, 3*tamanho/4),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        ms = 4,
        zcolor = c,
        framestyle = :box,
        label = "",
        color = cgrad(categorical = true),
        # colorbar_title = L"\textrm{IPR}",
        title = L"\textrm{IPR}",
        titlelocation = :right,
        legendfontsize = 20,
        legendtitlefontsize = 20;
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        
        )
        xlabel!(L"$q_n $")
        ylabel!(L"$S_{str}$")

    elseif Tipo_de_Kernel == "Escalar"

        scatter(a,b,
        size = (tamanho+100, 3*tamanho/4),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        ms = 4,
        zcolor = c,
        label = "",
        framestyle = :box,
        color = cgrad(categorical = true),
        # colorbar_title = L"\textrm{IPR}",
        title = L"\textrm{IPR}",
        titlelocation = :right,
        legendfontsize = 20,
        legendtitlefontsize = 25;
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        
        )
        xlabel!(L"$q_n $")
        ylabel!(L"$S_{str}$")

        

    end

    limite(x) = -log(x)   
    x = range(minimum(a),maximum(a), length=1000)
    y = limite.(x)
    plot!(x,y,
    lw = 5,
    lab = L"\textrm{Limite Teorico}",
    c = :black)




end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------- Vizualização da relação entre comprimento de localização e o tempo de vida de cada modo -------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_entre_gamma_e_comprimento_de_localização_R_cm(ξₙ,γₙ,R_cm_n,Radius,L,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel,tamanho)
    gr()
    theme(:mute)    
    
    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    end

    index = findall(ξₙ .> 0)

    x_a = abs.(ξₙ[:])
    y_b = abs.(γₙ[:])
    z_c = get_modulo_R_cm(R_cm_n[:,:])

    if Tipo_de_kernel == "Escalar"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-18,10^2),
        xlims = (10^-2,10^3),
        ms = 8,
        colorbar = :bottom,
        zcolor = z_c,
        framestyle = :box,
        label = "",
        color = cgrad(:jet1),
        # color = cgrad(:tempo),
        # color = cgrad(:tempo),
        # color = reverse(cgrad(:PuOr_11)),
        # color = cgrad(:Paired_6,rev=:true),
        # color = cgrad(:tempo,rev=:true,scale=:exp), 
        # colorbar_title = L"$k|\mathbf{ R_{CM} }|$",
        # title = L"\textrm{Relação entre γ e o Comprimento de Localização}",
        title = L"$k|\mathbf{ R_{CM} }|$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 30,
        titlefontsize = 25,
        tickfontsize = 20, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        legend =:bottomright
        

        )

    elseif Tipo_de_kernel == "Vetorial"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-4,10^0),
        xlims = (10^0,10^2),
        ms = 4,
        colorbar = :bottom,
        zcolor = z_c,
        framestyle = :box,
        label = "",
        color = cgrad(:jet1),
        # colorbar_title = L"$k|\mathbf{ R_{CM} }|$",
        # title = L"\textrm{Relação entre γ e o Comprimento de Localização}",
        title = L"$k|\mathbf{ R_{CM} }|$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        

        )

    end

    xlabel!(L"$ k\xi_n$")
    ylabel!(L"$\mathbf{\gamma_n}$")

    lim_raio(y) = Limite  
    y = range(10^-20,10^2, length=100)
    x = lim_raio.(y)
    plot!(x,y,
    lw = 4,
    lab = L"\textrm{Raio do Sistema}",
    # lab = "",
    c = :black)

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------ Vizualização da relação entre comprimento de localização e o tempo de vida de cada modo e o erro cometido na aproximação do fit ----------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_entre_gamma_e_comprimento_de_localização_R²(ξₙ,γₙ,Eₙ,Radius,L,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel,tamanho)
    A = "SLA"
    gr()
    theme(:mute)    
    
    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite =Radius
    
    end


    x_a = k*abs.(ξₙ)
    y_b = abs.(γₙ)
    
    M = size(ξₙ,1)
    for i in 1:M

        if Eₙ[i] == Inf

            Eₙ[i] = 100

        end

    end

    z_c = log10.(Eₙ)


    if Tipo_de_kernel == "Escalar"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-15,10^0),
        xlims = (10^-1,10^3),
        ms = 4,
        colorbar = :bottom,
        zcolor = z_c,
        framestyle = :box,
        label = "",
        color = cgrad(:oxy),
        # colorbar_title = L"$R^2$",
        # title = L"\textrm{Relação entre γ e o Comprimento de Localização}",
        title = L"$log_{10}(R^2)$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        

        )

    elseif Tipo_de_kernel == "Vetorial"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-4,10^0),
        xlims = (10^0,10^2),
        ms = 4,
        colorbar = :bottom,
        zcolor = z_c,
        framestyle = :box,
        label = "",
        color = cgrad(:oxy),
        # colorbar_title = L"$R^2$",
        # title = L"\textrm{Relação entre γ e o Comprimento de Localização}",
        title = L"$log_{10}(R^2)$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        

        )

    end
        xlabel!(L"$ k\xi_n$")
    ylabel!(L"$\mathbf{\gamma_n}$")

    lim_raio(y) = Limite  
    y = range(10^-15,10^0, length=100)
    x = lim_raio.(y)
    plot!(x,y,
    lw = 2,
    lab = L"\textrm{Raio do Sistema}",
    c = :black)

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------- Vizualização da relação entre comprimento de localização e o tempo de vida de cada modo -------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_relação_entre_gamma_e_R_cm(γₙ,R_cm_n,Tipo_de_kernel,tamanho)
    gr()
    theme(:mute)    
    
    x_a = abs.(γₙ)
    y_b = get_modulo_R_cm(R_cm_n)


    
    if Tipo_de_kernel == "Escalar"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ms = 4,
        framestyle = :box,
        label = "",
        # colorbar_title = L"$k|\mathbf{ R_{CM} }|$",
        title = L"\textrm{Relação entre γ e o Centro de Massa do Modo}",
        # title = L"$k|\mathbf{ R_{CM} }|$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        

        )

    elseif Tipo_de_kernel == "Vetorial"

        scatter(x_a,y_b,
        size = (tamanho+200, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ms = 4,
        colorbar = :bottom,
        framestyle = :box,
        label = "",
        # colorbar_title = L"$k|\mathbf{ R_{CM} }|$",
        title = L"\textrm{Relação entre γ e o Centro de Massa do modo}",
        # title = L"$k|\mathbf{ R_{CM} }|$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        

        )

    end

    xlabel!(L"$\mathbf{\gamma_n}$")
    ylabel!(L"$k|\mathbf{ R_{CM} }|$")


end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------- Vizualização do comportamento do melhor modo para obter o comprimento de localização ------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_fit_modo_mais_subradiante(Dcmᵦ,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,RG_best,ψ,Eᵦ,tamanho)
    
    gr()
    theme(:vibrant)
    
    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = sqrt(L^2+Lₐ^2)/2

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius

    elseif GEOMETRIA_DA_NUVEM == "CUBO" 
        
        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = Radius

    end


    if Tipo_de_kernel == "Escalar"      
        
        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = L"\textrm{Todos os atomos}", 
        size = (tamanho, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k_a |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-30,1),
        xlims = (0,2*Limite),
        framestyle = :box,
        # title = L"\textrm{Fit determinar o comprimento de localização}",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

    elseif Tipo_de_kernel == "Vetorial"                                                                                                   # Define se a analise é feita com a luz escalar ou vetorial

        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = L"\textrm{Todos os atomos}", 
        size = (tamanho, tamanho/1.5),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k_a |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-12,2),
        xlims = (0,2*Limite),
        framestyle = :box,
        title = L"\textrm{Fit determinar o comprimento de localização}",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

    end

    a_best, b_best,atomos_uteis,X_ordenado,Y_ordenado = get_a_and_b_to_best_mode(Dcmᵦ,ψᵦ²)


    scatter!(X_ordenado[1:atomos_uteis],(Y_ordenado[1:atomos_uteis]),
    c = :red,
    lab = L"\textrm{Atomos importantes}")

    # Defino o melhor fit, que se aproxima dos pontos por esta reta 
    fit_best_mode(x) = exp(a_best*x + b_best) 
    x = range(0,2*Limite, length=100)
    y = fit_best_mode.(x)
    plot!(x,y,
    lw = 6,
    lab = L"\textrm{fit otimizado - R2: %$Eᵦ}",
    c=:black)

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Vizualização fiting de um modo e seu perfil espacial --------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmᵦ,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,geometria::TwoD,tamanho)
    
    gr()
    theme(:vibrant)


    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = sqrt(L^2 +Lₐ^2)/2

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end


    X_t = Dcmᵦ
    Y_t = ψᵦ²


    if Tipo_de_kernel == "Escalar"      
        
        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = "", 
        size = (tamanho, tamanho*(3/4)),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-30,1),
        xlims = (0,2*Limite),
        framestyle = :box,
        title = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

    elseif Tipo_de_kernel == "Vetorial"                                                                                          

        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = L"", 
        size = (tamanho, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-12,2),
        xlims = (0,2*Limite),
        framestyle = :box,
        title = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

    end

    a_best, b_best,atomos_uteis,X_ordenado,Y_ordenado = get_a_and_b_to_best_mode(Dcmᵦ,ψᵦ²)


    scatter!(X_ordenado[1:atomos_uteis],(Y_ordenado[1:atomos_uteis]),
    c = :red,
    lab = "")

    # Defino o melhor fit, que se aproxima dos pontos por esta reta 
    fit_best_mode(x) = exp(a_best*x + b_best) 
    x = range(0,2*Limite, length=100)
    y = fit_best_mode.(x)
    plot!(x,y,
    lw = 6,
    lab = "",
    c=:black)

    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = log10.(ψᵦ²)

    scatter!(x_a,y_b,
    inset = (1, bbox(0, 0, 0.4, 0.4, :right)), 
    subplot = 2,
    size = (tamanho+150, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 1,
    ylims = (-1.2*Limite,1.2*Limite),
    xlims = (-1.2*Limite,1.2*Limite),
    xticks=nothing,
    yticks=nothing,
    legend = false,
    ms = 6,
    zcolor = z_c,
    framestyle = :box,
    label = "",
    legendtitle = "",
    color = cgrad(categorical = true),
    colorbar_title = "",
    title = "",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    legendtitlefontsize = 20;
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    xlabel = L"$kx_j$",
    ylabel = L"$ky_j$"

    )

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmᵦ,ψᵦ²,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,geometria::ThreeD,tamanho)
    
    gr()
    theme(:vibrant)


    if GEOMETRIA_DA_NUVEM == "CUBO"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = Radius
    
    end


    X_t = Dcmᵦ
    Y_t = ψᵦ²


    if Tipo_de_kernel == "Escalar"      
        
        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = "", 
        size = (tamanho, tamanho*(3/4)),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-30,1),
        xlims = (0,2*Limite),
        framestyle = :box,
        title = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

    elseif Tipo_de_kernel == "Vetorial"                                                                                          

        X_t = Dcmᵦ
        Y_t = ψᵦ²
        

        scatter(X_t,Y_t,
        lab = L"", 
        size = (tamanho, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        c = :blue,
        gridalpha = 1,
        xlabel = L"$k |\mathbf{r_j} - \mathbf{R_{CM}}|$",
        ylabel = L"$p_{i}$",
        yscale = :log10,
        ylims = (10^-12,2),
        xlims = (0,2*Limite),
        framestyle = :box,
        title = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

    end

    a_best, b_best,atomos_uteis,X_ordenado,Y_ordenado = get_a_and_b_to_best_mode(Dcmᵦ,ψᵦ²)


    scatter!(X_ordenado[1:atomos_uteis],(Y_ordenado[1:atomos_uteis]),
    c = :red,
    lab = "")

    # Defino o melhor fit, que se aproxima dos pontos por esta reta 
    fit_best_mode(x) = exp(a_best*x + b_best) 
    x = range(0,2*Limite, length=100)
    y = fit_best_mode.(x)
    plot!(x,y,
    lw = 6,
    lab = "",
    c=:black)

    r_cartesian = r

    x_a = r_cartesian[:,1]
    y_b = r_cartesian[:,2]
    z_c = r_cartesian[:,3]
    t_d = log10.(ψᵦ²)

    scatter!(x_a,y_b,z_c,
    inset = (1, bbox(0, 0.1, 0.4, 0.4, :bottom)), 
    subplot = 2,
    size = (tamanho+150, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 1,
    ylims = (-Limite,Limite),
    xlims = (-Limite,Limite),
    zlims = (-Limite,Limite),
    aspect_ratio=:equal,
    xticks=nothing,
    yticks=nothing,
    zticks=nothing,
    legend = false,
    ms = 6,
    zcolor = t_d,
    framestyle = :box,
    label = "",
    legendtitle = "",
    color = cgrad(categorical = true),
    colorbar_title = "",
    title = "",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    legendtitlefontsize = 20;
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    xlabel = L"$kx_j$",
    ylabel = L"$ky_j$"


    )

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização da intensidade na direção Radial ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Intensidade_nos_sensores(r, All_Intensitys,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,fator_de_controle,Geometria::TwoD,Distancia_dos_Sensores,tamanho)
    
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "DISCO"


        scatter(r[ : ,1],r[ : ,2],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-4*Radius,4*Radius),
        xlims = (-4*Radius,4*Radius),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        Laser_cima_disc(x) =  (Radius/2)*sqrt((x/(15*Radius/16))^2 + 1)
        x = range(-10*Radius,10*Radius, length=100)
        y = Laser_cima_disc.(x)
        plot!(x,y,lw=3,lab="",color=:black,fill = (0, :black),fillalpha=0.4)

        Laser_baixo_disc(x) = -(Radius/2)*sqrt((x/(15*Radius/16))^2 + 1)
        x = range(-10*Radius,10*Radius, length=100)
        y = Laser_baixo_disc.(x)
        plot!(x,y,lw=3,color=:black,lab=L"\textrm{Laser Gaussiano}",fill = (0, :black),fillalpha=0.4)



        if PERFIL_DA_INTENSIDADE == "NAO"
        
            Intensity = (10^8)*All_Intensitys[1,:]


            scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],
            aspect_ratio=:equal,
            ms = 4,
            zcolor = Intensity,
            framestyle =:box,
            label = "",
            color = cgrad(categorical = true),
            colorbar_title = L"\textrm{Intensidade Normalizada}"
            )


        elseif PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"  

            N_Sensores = size(Posição_Sensores,1)

            Intensity = zeros(2*N_Sensores) 

            Intensity[1:N_Sensores] = All_Intensitys[1, : ]
            Intensity[(1+N_Sensores):end] = All_Intensitys[3, : ]

            Todos_SENSORES = zeros(2*N_Sensores,2) 
            Todos_SENSORES[1:N_Sensores,1] = Posição_Sensores[ : ,1]
            Todos_SENSORES[(1+N_Sensores):end,1] = Posição_Sensores[ : ,3]
            Todos_SENSORES[1:N_Sensores,2] = Posição_Sensores[ : ,2]
            Todos_SENSORES[(1+N_Sensores):end,2] = Posição_Sensores[ : ,4]


            # scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],
            # aspect_ratio=:equal,
            # ms = 4,
            # zcolor = Intensity,
            # framestyle =:box,
            # label = "",
            # color = cgrad(categorical = true),
            # colorbar_title = L"\textrm{Intensidade Normalizada}"
            # )

            u = fator_de_controle*(Intensity)
            v = zeros(2*N_Sensores)

            quiver!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],quiver=(u,v),c=:blue)
            scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],c=:orangered1,lab="Sensores",ms=4)        


        end
    
    elseif GEOMETRIA_DA_NUVEM == "SLAB" 
        
        Limite = sqrt(L^2 + Lₐ^2)/2

        scatter(r[ : ,1],r[ : ,2],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-1.2*Limite,1.2*Limite),
        xlims = (-2*Limite,2*Limite),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        # Laser_cima_slab(x) =  (L/4)*sqrt((x/(15*L/32))^2 + 1)
        # x = range(-10*L,10*L, length=1000)
        # y = Laser_cima_slab.(x)
        # plot!(x,y,lw=3,lab="",color=:black,fill = (0, :black),fillalpha=0.4)

        # Laser_baixo_slab(x) = -(L/4)*sqrt((x/(15*L/32))^2 + 1)
        # x = range(-10*L,10*L, length=1000)
        # y = Laser_baixo_slab.(x)
        # plot!(x,y,lw=3,color=:black,lab=L"\textrm{Laser Gaussiano}",fill = (0, :black),fillalpha=0.4)



        if PERFIL_DA_INTENSIDADE == "NAO"
        
            Intensity = All_Intensitys[1,:]


            scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],
            aspect_ratio=:equal,
            ms = 4,
            zcolor = Intensity,
            framestyle =:box,
            label = "",
            color = cgrad(categorical = true),
            colorbar_title = L"\textrm{Intensidade Normalizada}"
            )


        elseif PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"  

            N_Sensores = size(Posição_Sensores,1)

            # Intensity = zeros(2*N_Sensores) 

            # Intensity[1:N_Sensores] = All_Intensitys[1, : ]
            # Intensity[(1+N_Sensores):end] = All_Intensitys[3, : ]

            # Todos_SENSORES = zeros(2*N_Sensores,2) 
            # Todos_SENSORES[1:N_Sensores,1] = Posição_Sensores[ : ,1]
            # Todos_SENSORES[(1+N_Sensores):end,1] = Posição_Sensores[ : ,3]
            # Todos_SENSORES[1:N_Sensores,2] = Posição_Sensores[ : ,2]
            # Todos_SENSORES[(1+N_Sensores):end,2] = Posição_Sensores[ : ,4]

            Intensity = zeros(N_Sensores) 

            Intensity = All_Intensitys[3, : ]

            Todos_SENSORES = zeros(N_Sensores,2) 
            Todos_SENSORES[:,1] = Posição_Sensores[ : ,3]
            Todos_SENSORES[:,2] = Posição_Sensores[ : ,4]


            Intensidade_dim = fator_de_controle*(Intensity)
            v = zeros(N_Sensores)


            # quiver!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],quiver=(u,v),c=:blue)
            scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],c=:orangered1,lab=L"Sensores",ms=4)        
            plot!(Todos_SENSORES[ : ,1].+Intensidade_dim,Todos_SENSORES[ : ,2],c=:blue,lw = 5,lab=L"Intensidade")

        end
        
    end

    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Intensidade_nos_sensores(r, All_Intensitys,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,fator_de_controle,Geometria::ThreeD,Distancia_dos_Sensores,tamanho)
                                            
    
    gr()
    theme(:vibrant)

    if GEOMETRIA_DA_NUVEM == "ESFERA"


        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-2.5*Radius,2.5*Radius),
        xlims = (-2.5*Radius,2.5*Radius),
        zlims = (-2.5*Radius,2.5*Radius),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.4,
        camera = (45,45),
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )


        Intensity = (fator_de_controle)*All_Intensitys[1,:]


        scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],Posição_Sensores[ : ,3],
        aspect_ratio=:equal,
        ms = 4,
        zcolor = Intensity,
        framestyle =:box,
        label = "",
        color = cgrad(),
        # clims = (-3,0),
        # colorbar_title = L"\textrm{Intensidade Normalizada}"
        )


    
    elseif GEOMETRIA_DA_NUVEM == "CUBO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-2*L,2*L),
        xlims = (-2*L,2*L),
        zlims = (-2*L,2*L),
        camera = (45,45),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        
        Intensity = (10^8)*All_Intensitys[1,:]


        scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],Posição_Sensores[ : ,3],
        aspect_ratio=:equal,
        ms = 4,
        zcolor = Intensity,
        framestyle =:box,
        label = "",
        color = cgrad(categorical = true),
        # colorbar_title = L"\textrm{Intensidade Normalizada}"
        )
        
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        xlims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        zlims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        camera = (45,30),
        aspect_ratio=:equal,
        m = (:green,stroke(1,:green)),
        ms = 3,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        
        Intensity = (10^7)*All_Intensitys[1,:]


        # scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],Posição_Sensores[ : ,3],
        # aspect_ratio=:equal,
        # ms = 4,
        # zcolor = Intensity,
        # framestyle =:box,
        # label = "",
        # color = cgrad(categorical = true),
        # # colorbar_title = L"\textrm{Intensidade Normalizada}"
        # )

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))

        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)

        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        
        plot!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2], Intensity .+ Distancia_dos_Sensores .+ L/2,
        # title = "",
        st=:surface,
        clims = (minimum(Intensity) + Distancia_dos_Sensores + L/2, maximum(Intensity) + Distancia_dos_Sensores + L/2),
        # color = cgrad(:tempo,scale=:log),
        # camera=(-30,20),
        # size = (tamanho+200, tamanho),
        lw=0,
        # left_margin = [5Plots.mm 5Plots.mm], 
        # bottom_margin = 5Plots.mm,xrotation = 90
        )


    elseif GEOMETRIA_DA_NUVEM == "TUBO" 
        
        Limite = sqrt(L^2 + Lₐ^2) + Distancia_dos_Sensores

        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-(0.8)*Limite,(0.8)*Limite),
        xlims = (-(0.8)*Limite,(0.8)*Limite),
        zlims = (-(0.8)*Limite,(0.8)*Limite),
        camera = (45,45),
        aspect_ratio=:equal,
        m = (:green,stroke(1,:green)),
        ms = 3,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        
        Intensity = (fator_de_controle)*(All_Intensitys[1,:])


        scatter!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],Posição_Sensores[ : ,3],
        # aspect_ratio=:equal,
        ms = 4,
        zcolor = log10.(Intensity),
        clims = (-2.5,0),
        # framestyle =:box,
        label = "",
        color = cgrad(),
        # colorbar_title = L"$I 10^9 $"
        )

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        # plot!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2], Intensity .+ Distancia_dos_Sensores .+ L/2,
        # # title = "",
        # st=:surface,
        # clims = (minimum(Intensity) + Distancia_dos_Sensores + L/2, maximum(Intensity) + Distancia_dos_Sensores + L/2),
        # # color = cgrad(:tempo,scale=:log),
        # # camera=(-30,20),
        # # size = (tamanho+200, tamanho),
        # lw=0,
        # # left_margin = [5Plots.mm 5Plots.mm], 
        # # bottom_margin = 5Plots.mm,xrotation = 90
        # )


    end



    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização da intensidade na direção Radial ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Perfil_da_Cintura_nos_sensores(r, Intensidade_Normalizada,Posição_Sensores,Radius,L,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,fator_de_controle,tamanho)
    
    if PERFIL_DA_INTENSIDADE ≠ "SIM,para obter a cintura do feixe"
       return "NÃO PODE!!!" 
    end

    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = L/2

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end

    gr()
    theme(:vibrant)

    p1 = scatter(Posição_Sensores[ : ,1],Posição_Sensores[:,2],
    zcolor = (Intensidade_Normalizada[1, : ]),
    label=L"\textrm{Perfil Luz Incidadente}",
    title = L"\textrm{I}",
    titlelocation = :right,
    size = (tamanho+100, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    ylims = (-Limite,Limite),
    xlims = (-Limite,0),
    ms = 4,
    gridalpha = 0.8,
    legendfontsize = 20,
    labelfontsize = 20,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )

    p2 = scatter(Posição_Sensores[ : ,3],Posição_Sensores[:,4],
    zcolor = log10.(Intensidade_Normalizada[3, : ]),
    label=L"\textrm{Perfil Luz Espahada}",
    title =  L"$log_{10}(I)$",
    titlelocation = :right,
    size = (tamanho+100, tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    ylims = (-Limite,Limite),
    xlims = (0,Limite),
    ms = 4,
    gridalpha = 0.8,
    legendfontsize = 20,
    labelfontsize = 20,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )  

    plot(p1, p2, layout = (2,1))

    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------Vizualização o Comportamento da Intensidade ao longo dos Angulos ----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Intensidade_ao_longo_dos_angulos(All_Intensitys,PERFIL_DA_INTENSIDADE,tamanho)
    
    gr()
    theme(:wong)

    if PERFIL_DA_INTENSIDADE == "NAO"

        x_a = (1/180)*All_Intensitys[2, : ]

        N_Sensores = size(All_Intensitys,2)
        
        for i in 1:N_Sensores

            if x_a[i] > 1

                x_a[i] = x_a[i] - 2
            end
            
        end

        y_b = (10^8)*All_Intensitys[1, : ]
        
        plot(x_a,y_b,
        seriestype = :bar,
        gridalpha = 0.4  ,
        size = (tamanho+100, 3(tamanho+100)/4),
        left_margin = 10Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        framestyle=:box,
        c = :blue,
        title = L"\textrm{Relação entre Intensidade e Angulo}",
        label = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black    

        )

    elseif PERFIL_DA_INTENSIDADE == "SIM"

        x_a_INCIDENTE = (1/180)*All_Intensitys[2, : ]
        x_a_ESPALHADO = (1/180)*All_Intensitys[4, : ]

        N_Sensores = size(All_Intensitys,2)
        
        for i in 1:N_Sensores

            if x_a_INCIDENTE[i] > 1

                x_a_INCIDENTE[i] = x_a_INCIDENTE[i] - 2
            end
            
        end

        for i in 1:N_Sensores

            if x_a_ESPALHADO[i] > 1

                x_a_ESPALHADO[i] = x_a_ESPALHADO[i] - 2
            end
            
        end

        y_b_INCIDENTE = All_Intensitys[1, : ]
        y_b_ESPALHADO = All_Intensitys[3, : ]
        
        plot(x_a_INCIDENTE,y_b_INCIDENTE,
        seriestype = :bar,
        gridalpha = 0.4  ,
        size = (tamanho+100, 3(tamanho+100)/4),
        left_margin = 10Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        framestyle=:box,
        c = :blue,
        title = L"\textrm{Relação entre Intensidade e Angulo}",
        label = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black    

        )

        plot!(x_a_ESPALHADO,y_b_ESPALHADO,
        seriestype = :bar,
        gridalpha = 0.4  ,
        size = (tamanho+100, 3(tamanho+100)/4),
        left_margin = 10Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        framestyle=:box,
        c = :red,
        title = L"\textrm{Relação entre Intensidade e Angulo}",
        label = "",
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black    

        )

    end

    xlabel!(L"$\theta(rad)$")
    ylabel!(L"\textrm{Intensidade Normalizada}")

end 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------- Vizualização Histograma das Intesidades Normalizadas ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Histograma_das_Intensidade(Histograma,tamanho)
    
    gr()
    theme(:wong)

    x_a = collect(Histograma.edges[1])
    y_b = Histograma.weights
    
    plot(x_a[2:end],y_b,
    seriestype = :bar,
    yscale = :log10,
    gridalpha = 0  ,
    c = :blue,
    size = (tamanho+100, 50 + 3(tamanho+100)/4),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    framestyle=:box,
    label = L"\textrm{Quantidades em cada nivel de Intensidade}",
    title = L"\textrm{Histograma das Intensidades}",
    xrotation = 45,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    )
    xlabel!(L"\textrm{Intensidades Normalizadas}")
    ylabel!(L"\textrm{Quantidade de Pixels por Intensidade}")

end 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------- Vizualização Histograma das Intesidades Normalizadas ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Histograma_das_Intensidade_log(h,tamanho)
    
    gr()
    theme(:wong)
    x_a = collect(h.edges[1])
    y_b = h.weights
    
    plot(x_a[2:end],y_b,
    seriestype = :bar,
    yscale = :log10,
    xscale = :log10,
    gridalpha = 1  ,
    c = :blue,
    size = (tamanho+100, 50 + 3(tamanho+100)/4),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    framestyle=:box,
    label = L"\textrm{Quantidades em cada nivel de Intensidade}",
    title = L"\textrm{Histograma das Intensidades}",
    xrotation = 45,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    )
    xlabel!(L"\textrm{Intensidades Normalizadas}")
    ylabel!(L"\textrm{Quantidade de Pixels por Intensidade}")

end 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma,tamanho)
    
    gr()
    theme(:vibrant)
    
    h_normalizado = normalize(Histograma; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights

    Lei_de_Ray(x) =  exp(-x)
    x = range(0, 20, length=100)
    y = Lei_de_Ray.(x)
    plot(x,y,
    lw = 5,
    color = :black,
    linestyle = :dash,
    lab = L"\textrm{Lei de Rayleigh}"
    )

    # Desvio_Lei_de_Ray(x) = exp(-x)*( 1 + 0.2*((x^2) - 4*x + 2))
    # x = range(10^-1, 20, length=1000)
    # y = Desvio_Lei_de_Ray.(x)
    # plot!(x,y,
    # lw = 5,
    # color = :red,
    # linestyle = :dash,
    # lab = L"\textrm{N. Shnerb e Kaveh}"
    # )

    scatter!(x_a[2:end],y_b,
    m = :star4,
    ms = 12,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xlims = (0,10),
    ylims = (10^-6,10^1),
    lw = 5,
    framestyle=:box,
    yticks = [10^1,10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    # title = L"\textrm{Distribuição da Probabilidade da Intensidade}",
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    
    )
    xlabel!(L"I" )
    ylabel!(L" P (I) ")





end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio(Histograma,tamanho)
    
    gr()
    theme(:vibrant)
    x_a = collect(Histograma.edges[1])
    y_b = Histograma.weights/sum(Histograma.weights)


    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 15,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgrid = true,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xscale = :log10,
    xlims = (10^-1,10^2),
    xticks = [10^-1,10^0,10^1,10^2],
    ylims = (10^-6,10^0),
    yticks = [10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    title = L"\textrm{Distribuição da Probabilidade da Intensidade}",
    framestyle = :box,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    
    )
    xlabel!(L"I")
    ylabel!(L"P(I)")


    Lei_de_Ray(x) =  exp(-x)
    x = range(10^-1, 100, length=1000)
    y = Lei_de_Ray.(x)
    plot!(x,y,
    lw = 3,
    color = :black,
    linestyle = :dash,
    lab = L"\textrm{Lei de Rayleigh}"
    )

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(h,titulo,tamanho)
    
    gr()
    theme(:vibrant)

    h_normalizado = normalize(h; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights


    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 15,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgrid = true,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xscale = :log10,
    xlims = (10^-1,10^2),
    xticks = [10^-1,10^0,10^1,10^2],
    ylims = (10^-6,10^1),
    yticks = [10^1,10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    title = titulo,
    framestyle = :box,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 17,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    
    )
    xlabel!(L"I")
    ylabel!(L"P(I)")


    Lei_de_Ray(x) =  exp(-x)
    x = range(10^-1, 100, length=1000)
    y = Lei_de_Ray.(x)
    plot!(x,y,
    lw = 3,
    color = :black,
    linestyle = :dash,
    lab = L"\textrm{Lei de Rayleigh}"
    )


    # Sₜ = get_Shnerb_Law(h_normalizado)
    # Desvio_Lei_de_Ray(x) = exp(-x)*( 1 + 0.5*Sₜ*((x^2) - 4*x + 2))
    # x = range(10^-1, 100, length=1000)
    # y = abs.(Desvio_Lei_de_Ray.(x))
    # plot!(x,y,
    # lw = 5,
    # color = :red,
    # linestyle = :dash,
    # lab = L"\textrm{N. Shnerb}"
    # )
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Kogan_Law(h,Intensidade_Resultante_NORMALIZADA,tamanho)
    
    gr()
    theme(:vibrant)

    h_normalizado = normalize(h; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights


    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 15,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgrid = true,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xscale = :log10,
    xlims = (10^-1,10^2),
    xticks = [10^-1,10^0,10^1,10^2],
    ylims = (10^-6,10^1),
    yticks = [10^1,10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    title = L"\textrm{Distribuição da Probabilidade da Intensidade}",
    framestyle = :box,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 25,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    
    )
    xlabel!(L"I")
    ylabel!(L"P(I)")


    Lei_de_Ray(x) =  exp(-x)
    x = range(10^-1, 100, length=1000)
    y = Lei_de_Ray.(x)
    plot!(x,y,
    lw = 3,
    color = :black,
    linestyle = :dash,
    lab = L"\textrm{Lei de Rayleigh}"
    )

    x_Kogan,y_Kogan = get_Kogan_Law(Intensidade_Resultante_NORMALIZADA)

    plot!(x_Kogan,y_Kogan,
    lw = 5,
    color = :red,
    linestyle = :dash,
    lab = L"\textrm{E. Kogan}"
    )
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_especial(h,variancia,Δ,tamanho)
    
    gr()
    theme(:vibrant)

    h_normalizado = normalize(h; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights


    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 15,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgrid = true,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xscale = :log10,
    xlims = (10^-2,10^2),
    xticks = [10^-2,10^-1,10^0,10^1,10^2],
    ylims = (10^-6,10^1),
    yticks = [10^1,10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    legendtitle = "Δ = $Δ 
    σᵢ² = $variancia ",
    title = L"\textrm{Distribuição da Probabilidade da Intensidade}",
    framestyle = :box,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15,
    legendtitlefontsize = 12; 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    
    )
    xlabel!(L"I")
    ylabel!(L"P(I)")


    Lei_de_Ray(x) =  exp(-x)
    x = range(10^-2, 100, length=1000)
    y = Lei_de_Ray.(x)
    plot!(x,y,
    lw = 3,
    color = :black,
    linestyle = :dash,
    lab = L"\textrm{Lei de Rayleigh}"
    )

    # Desvio_Lei_de_Ray(x) = exp(-x)*( 1 + 0.27*((x^2) - 4*x + 2))
    # x = range(10^-1, 100, length=1000)
    # y = Desvio_Lei_de_Ray.(x)
    # plot!(x,y,
    # lw = 5,
    # color = :red,
    # linestyle = :dash,
    # lab = L"\textrm{N. Shnerb and Kaveh}"
    # )
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------- Vizualização o comportamento da Variancia para uma densidade Fixa ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Relação_entre_deturn_e_a_variancia_da_Intensidade_TEORIA(Deturn,Variacias_exp,Variacias_teo,Variancia_Inicial,Variancia_Final,tamanho)
    
    gr()
    theme(:vibrant)
    x_a = Deturn
    y_b_1 = Variacias_exp
    y_b_2 = Variacias_teo
    
    plot(x_a,y_b_2,
    seriestype = :bar,
    color = :black,
    label = L"\textrm{Valor teorico}",
    gridalpha = 0.6,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    framestyle = :box,
    title = L"\textrm{Relação entre a variância e o Deturn}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 25,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )
    
    xlabel!(L"$\Delta / \Gamma_0$")
    ylabel!(L"$\sigma^2_I$")
    plot!(x_a,y_b_1,
    seriestype = :bar ,
    label = L"\textrm{Gerado}",
    xticks = collect(Variancia_Inicial:1:Variancia_Final),
    c=:blue
    
    )

end 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------- Vizualização o comportamento da Variancia para uma densidade Fixa Comparando com os valores Teoricos ----------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Relação_entre_deturn_e_a_variancia_da_Intensidade_PURO(Deturn,Variacias_exp,Variancia_Inicial,Variancia_Final,tamanho)
    
    gr()
    theme(:vibrant)
    x_a = Deturn
    y_b = Variacias_exp
    
    plot(x_a,y_b,
    seriestype = :bar ,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    label = "",
    xticks = collect(Variancia_Inicial:1:Variancia_Final),
    c=:blue,
    framestyle = :box,
    title = L"\textrm{Relação entre a Variância e o Deturn}",
    gridalpha = 0.6,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 25,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )
    xlabel!(L"$\Delta / \Gamma_0$")
    ylabel!(L"$\sigma^2_I$")

end 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------- Vizualização o comportamento da Condutancia  segundo Koganpara uma densidade Fixa ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Relação_entre_deturn_e_a_condutancia_KOGAN(Deturn,Condutancias,densidade,Δ_Inicial,Δ_Final,tamanho)
    
    gr()
    theme(:vibrant)
    x_a = Deturn
    y_b = Condutancias
    
    plot(x_a,y_b,
    seriestype = :bar,
    color = :blue,
    label = "ρ/k² = $densidade",
    gridalpha = 0.6,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    framestyle = :box,
    xticks = collect(Δ_Inicial:1:Δ_Final),
    title = L"\textrm{Relação entre a Condutancia e o Deturn - Kogan}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )
    
    xlabel!(L"$\Delta / \Gamma_0$")
    ylabel!(L"$g_c$")


end 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------- Vizualização o comportamento da Variancia para uma densidade Fixa Comparando com os valores Teoricos ----------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Relação_entre_deturn_e_a_condutancia_SHNERB(Deturn,Condutancias,densidade,Δ_Inicial,Δ_Final,tamanho)
    
    gr()
    theme(:vibrant)
    x_a = Deturn
    y_b = Condutancias
    
    plot(x_a,y_b,
    seriestype = :bar,
    color = :blue,
    label = "ρ/k² = $densidade",
    gridalpha = 0.6,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    framestyle = :box,
    xticks = collect(Δ_Inicial:1:Δ_Final),
    title = L"\textrm{Relação entre a Condutancia e o Deturn - Shnerb}",
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    )
    
    xlabel!(L"$\Delta / \Gamma_0$")
    ylabel!(L"$g_c$")


end 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------- Vizualização do Diagrama de Fase da Variancia -----------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Diagrama_de_Fase_Variancia(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Escala_de_Densidade,titulo,tamanho)

    if Escala_de_Densidade == "LINEAR"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = range(ρ_Inicial,ρ_Final,length=N_div_Densidade)
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        # color = cgrad(:tempo,rev=:true),
        # color = cgrad(:tempo,[0.3,0.35]),
        # color = cgrad(:tempo,[0.05,0.15,0.25],rev=:true),
        # color = cgrad(:PuOr_11),
        # color = cgrad(:Paired_10),
        # color = cgrad(:rainbow),        
        # color = cgrad(:cividis),
        # log10.(A),
        A,
        # clims = (0,3),
        # colorbar_title = L"$log_{10}(\sigma^2_I)$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        # title = L"$log_{10}(\sigma_I)$",
        title = titulo,
        # legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")

    
        
    elseif Escala_de_Densidade == "LOG"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade) 
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        color = cgrad(:tempo,[0.7,0.75]),
        # color = cgrad(:cividis),
        # color = cgrad([:yellow,:yellow1,:yellow2,:yellow3,:gold,:darkgoldenrod2,:orange,:darkorange1,:green4,:deepskyblue3,:royalblue3,:darkblue], [0.02,0.023,0.03,0.04,0.045,0.1,0.2,0.33,0.41,0.47,0.5,0.51,0.6,0.7]), 
        log10.(A),
        title =  titulo,
        legendtitle = "kR = $kR",
        titlelocation = :right,
        # colorbar_title = L"$\sigma^2_I$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        framestyle = :box,
        yaxis = :log,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15,
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")
    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização do Diagrama de Fase da Condutancia Segundo KOGAN ------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Diagrama_de_Fase_Condutancia_KOGAN(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Variavel_Constante,kR,tamanho)

    if Escala_de_Densidade == "LINEAR"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = range(ρ_Inicial,ρ_Final,length=N_div_Densidade)
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        color = cgrad(:tempo,[0.55,0.6]),
        # color = cgrad(:cividis),
        A,
        # colorbar_title = L"$log_{10}(\sigma^2_I)$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        title = L"$g_C$",
        legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")

    
        
    elseif Escala_de_Densidade == "LOG"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade) 
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        color = cgrad(:tempo,[0.7,0.75]),
        # color = cgrad(:cividis),
        # color = cgrad([:yellow,:yellow1,:yellow2,:yellow3,:gold,:darkgoldenrod2,:orange,:darkorange1,:green4,:deepskyblue3,:royalblue3,:darkblue], [0.02,0.023,0.03,0.04,0.045,0.1,0.2,0.33,0.41,0.47,0.5,0.51,0.6,0.7]), 
        A,
        title = L"$g_C$",
        legendtitle = "kR = $kR",
        titlelocation = :right,
        # colorbar_title = L"$\sigma^2_I$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        framestyle = :box,
        yaxis = :log,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15,
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")
    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização do Diagrama de Fase da Condutancia Segundo KOGAN ------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Diagrama_de_Fase_Condutancia_SHNERB(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Variavel_Constante,kR,tamanho)

    if Escala_de_Densidade == "LINEAR"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = range(ρ_Inicial,ρ_Final,length=N_div_Densidade)
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 0Plots.mm,
        right_margin = 0Plots.mm,
        top_margin = 0Plots.mm,
        bottom_margin = 0Plots.mm,
        # color = cgrad(:tempo),
        # color = cgrad(:tempo,[0.55,0.6]),
        color = cgrad(:Paired_10),
        # color = cgrad(:RdBu_10),
        # clims=(0,5),
        log10.(abs.(A)),
        # A,
        # colorbar_title = L"$log_{10}(\sigma^2_I)$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        title = L"$log_{10}(g_C)$",
        # legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")

    
        
    elseif Escala_de_Densidade == "LOG"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade) 
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        # color = cgrad(:tempo,[0.7,0.75]),
        # color = cgrad(:tempo),
        # color = cgrad(:cividis),
        color = cgrad(:gist_rainbow),
        # color = cgrad([:yellow,:yellow1,:yellow2,:yellow3,:gold,:darkgoldenrod2,:orange,:darkorange1,:green4,:deepskyblue3,:royalblue3,:darkblue], [0.02,0.023,0.03,0.04,0.045,0.1,0.2,0.33,0.41,0.47,0.5,0.51,0.6,0.7]), 
        abs.(A),
        title = L"$log_{10}(g_C)$",
        legendtitle = "kR = $kR",
        titlelocation = :right,
        # colorbar_title = L"$\sigma^2_I$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        framestyle = :box,
        yaxis = :log,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15,
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")
    end

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase do ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Relação_entre_a_Densidade_e_ξₘᵢₙ(ρ_normalizados,ξₘᵢₙ,Ns,kRs,Ls,Variavel_Constante,Escala_de_Densidade,titulo,tamanho)

    Teoria_kξ(x) =  (1/(4*x))*exp(π/(8*x))

    if Variavel_Constante == "N"

        gr()
        theme(:vibrant)

        N_Realizações = size(Ns,1)

        for i in 1:N_Realizações
            
            if i == 1 
                N = Ns[i]
                
                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    xlims = (10^-2,10^1),
                    yticks = [10^-2,10^-1,10^0,10^1,10^2,10^3,10^4],
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                elseif Escala_de_Densidade == "LINEAR"
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    yticks = [10^-2,10^-1,10^0,10^1,10^2,10^3,10^4],
                    ylims = (10^-2,10^4),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                end
            
            else 

                N = Ns[i]

                if Escala_de_Densidade == "LOG"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    xlims = (10^-2,10^1),
                    yticks = [10^-2,10^-1,10^0,10^1,10^2,10^3,10^4],
                    ylims = (10^-2,10^4),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xlims = (10^-1,10^1),
                    yticks = [10^-2,10^-1,10^0,10^1,10^2,10^3,10^4],
                    ylims = (10^-2,10^4),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                end
            end
        end

        ylabel!(L"$ k \xi_{min}$")
        xlabel!(L"$\rho / k^2$")



        x = range(10^-4, 100, length=10000)
        y = Teoria_kξ.(x)
        plot!(x,y,
        lw = 3,
        color = :black,
        lab = L"\textrm{Teoria}"
        )

    elseif Variavel_Constante == "kR"

        gr()
        theme(:vibrant)

        N_Realizações = size(kRs,1)
        paleta_de_cores = palette(:gist_rainbow, N_Realizações)

        for i in 1:N_Realizações
            
            if i == 1 
                kR = kRs[i]
                
                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    xlims = (10^-2,10^1),
                    lab = "kR = $kR",
                    title = "",
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )

                    hline!([kR]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                elseif Escala_de_Densidade == "LINEAR"
                    
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    xlims = (10^-2,10^1),
                    lab = "kR = $kR",
                    title = "",
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                    hline!([kR]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                end
            
            else 

                kR = kRs[i]

                if Escala_de_Densidade == "LOG"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    xlims = (10^-2,10^1),
                    lab = "kR = $kR",
                    title = "",
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                    hline!([kR]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    xlims = (10^-2,10^1),
                    lab = "kR = $kR",
                    title = "",
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                    hline!([kR]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                end
            end
        end

        ylabel!(L"$ k \xi_{min}$")
        xlabel!(L"$\rho / k^2$")



        x = range(10^-4, 100, length=10000)
        y = Teoria_kξ.(x)
        plot!(x,y,
        lw = 3,
        color = :black,
        lab = L"\textrm{Teoria}"
        )


    elseif Variavel_Constante == "L"

        gr()
        theme(:vibrant)

        N_Realizações = size(Ls,1)
        paleta_de_cores = palette(:rainbow, N_Realizações)
        # paleta_de_cores = palette(:gist_rainbow, N_Realizações)

        for i in 1:N_Realizações
            
            if i == 1 
                L = Ls[i]
                
                if Escala_de_Densidade == "LOG"

                    # plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    # ms = 8,
                    # m = :circle,
                    # size = (tamanho, tamanho/2),
                    # left_margin = 10Plots.mm,
                    # right_margin = 12Plots.mm,
                    # top_margin = 5Plots.mm,
                    # bottom_margin = 10Plots.mm,
                    # gridalpha = 0,
                    # minorgrid = true,
                    # minorgridalpha = 0,
                    # minorgridstyle = :dashdot,
                    # yscale = :log10,
                    # xscale = :log10,
                    # ylims = (10^-2,10^2),
                    # xticks = [10^1,10^0,10^-1],
                    # xlims = (10^-1,10^1),
                    # lab = L"$kL = %$L$",
                    # # title = L"$Escalar - 2D - \Delta= 2 - L_a = 280 -- \rho / k^2$",
                    # title = L"",
                    # c = paleta_de_cores[i],
                    # framestyle = :box,
                    # legendfontsize = 25,
                    # labelfontsize = 25,
                    # titlefontsize = 30,
                    # tickfontsize = 15, 
                    # background_color_legend = :white,
                    # background_color_subplot = :white,
                    # foreground_color_legend = :black
                
                    # )
                    # hline!([L]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                elseif Escala_de_Densidade == "LINEAR"
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    # ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1],
                    xlims = (10^-2,10^1),
                    lab = L"$kL = %$L$",
                    title = titulo,
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                    # hline!([L]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                end
            
            else 

                L = Ls[i]

                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados[7:end],ξₘᵢₙ[7:end,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho, tamanho/2 +100),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 10Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    xlims = (10^-2,10^1),
                    ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    lab = L"$\textrm{Microscopic Approach}$",
                    title = titulo,
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legend =:topright,
                    legendfontsize = 15,
                    labelfontsize = 25,
                    titlefontsize = 20,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                    # hline!([L]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    # ylims = (10^-2,10^2),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    xlims = (10^-2,10^1),
                    lab = "kL = $L",
                    title = titulo,
                    c = paleta_de_cores[i],
                    framestyle = :box,
                    legendfontsize = 15,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                    # hline!([L]; label="", linestyle=:dashdot, color= paleta_de_cores[i], linewidth=5)

                end
            end
        end

        ylabel!(L"$ k \xi_{min}$")
        xlabel!(L"$\rho / k^2 $")



        x = range(10^-4, 100, length=10000)
        y = Teoria_kξ.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        lab = L"\textrm{Self-Consistent Theory}"
        )
    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------- Vizualização do Diagrama de Fase da Rrelação entre ξ e o Raio -----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Relação_entre_a_Densidade_e_ξ_por_Raio(ρ_normalizados,ξₘᵢₙ,Ns,kRs,Ls,Variavel_Constante,GEOMETRIA_DA_NUVEM,tamanho)

    if Variavel_Constante == "N"

        gr()
        theme(:vibrant)

        N_Realizações = size(Ns,1)

        for i in 1:N_Realizações
            
            if i == 1 
                N = Ns[i]
                
                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )

                elseif Escala_de_Densidade == "LINEAR"
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                end
            
            else 

                N = Ns[i]

                if Escala_de_Densidade == "LOG"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "N = $N",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                end
            end
        end

        if GEOMETRIA_DA_NUVEM == "DISCO"

            ylabel!(L"$ k \xi_{min}/kR $")
            xlabel!(L"$\rho / k^2$")
    
            
        elseif GEOMETRIA_DA_NUVEM == "SLAB"
    
            ylabel!(L"$ k \xi_{min}/kL $")
            xlabel!(L"$\rho / k^2$")

        end


    elseif Variavel_Constante == "kR"

        gr()
        theme(:vibrant)

        N_Realizações = size(kRs,1)

        for i in 1:N_Realizações
            
            if i == 1 
                kR = kRs[i]
                
                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kR = $kR",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                elseif Escala_de_Densidade == "LINEAR"
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kR = $kR",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                end
            
            else 

                kR = kRs[i]

                if Escala_de_Densidade == "LOG"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kR = $kR",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kR = $kR",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                end
            end
        end

        ylabel!(L"$ k \xi_{min}/kR $")
        xlabel!(L"$\rho / k^2$")






    elseif Variavel_Constante == "L"

        gr()
        theme(:vibrant)

        N_Realizações = size(Ls,1)

        for i in 1:N_Realizações
            
            if i == 1 
                L = Ls[i]
                
                if Escala_de_Densidade == "LOG"

                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kL = $L",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                elseif Escala_de_Densidade == "LINEAR"
                    plot(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "L = $L",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black
                
                    )
                end
            
            else 

                L = Ls[i]

                if Escala_de_Densidade == "LOG"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    xscale = :log10,
                    ylims = (10^-3,10^0),
                    xticks = [10^1,10^0,10^-1,10^-2,10^-3],
                    lab = "kL = $L",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                elseif Escala_de_Densidade == "LINEAR"

                    plot!(ρ_normalizados,ξₘᵢₙ[ : ,i],
                    ms = 8,
                    m = :circle,
                    size = (tamanho+150, (tamanho+100)/2 + 250),
                    left_margin = 10Plots.mm,
                    right_margin = 12Plots.mm,
                    top_margin = 5Plots.mm,
                    bottom_margin = 5Plots.mm,
                    gridalpha = 0,
                    minorgrid = true,
                    minorgridalpha = 0,
                    minorgridstyle = :dashdot,
                    yscale = :log10,
                    ylims = (10^-3,10^0),
                    lab = "kL = $L",
                    title = "",
                    framestyle = :box,
                    legendfontsize = 20,
                    labelfontsize = 25,
                    titlefontsize = 30,
                    tickfontsize = 15, 
                    background_color_legend = :white,
                    background_color_subplot = :white,
                    foreground_color_legend = :black

                    )
                end
            end
        end

        ylabel!(L"$ k \xi_{min}/kL $")
        xlabel!(L"$\rho / k^2$")

    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Vizualização do Diagrama de Fase da Variancia em escala log ----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# function vizualizar_Diagrama_de_Fase_ξ(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Δ_Inicial,Δ_Final,Escala_de_Densidade,tamanho)

#     if Escala_de_Densidade == "LINEAR"

#         gr()
#         theme(:vibrant)

#         # x_a = Dados_Resultantes[ : ,1]
#         # y_b = Dados_Resultantes[ : ,2]
#         # z_c = Dados_Resultantes[ : ,3]

#         # tamanho = 600
#         # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

#         A = zeros(N_div_Densidade,N_div_Deturn)

#         contador_1 = 1
#         contador_2 = 1

#         while true

#             A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
#             contador_2 = contador_2 + N_div_Deturn


#             if contador_1 == N_div_Densidade
#                 break
#             else
#                 contador_1 = contador_1 + 1
#             end
#         end


#         y = range(ρ_Inicial,ρ_Final,length = N_div_Densidade)
#         x = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)


#         heatmap(x, y, 
#         size = (tamanho + 200, tamanho),
#         left_margin = 10Plots.mm,
#         right_margin = 10Plots.mm,
#         top_margin = 5Plots.mm,
#         bottom_margin = 5Plots.mm,
#         color = cgrad(:tempo), 
#         A,
#         colorbar_title = L"$\xi_m$",
#         title = L"\textrm{Diagrama de Fase}",
#         framestyle = :box,
#         legendfontsize = 20,
#         labelfontsize = 25,
#         titlefontsize = 30,
#         tickfontsize = 15, 
#         background_color_legend = :white,
#         background_color_subplot = :white,
#         foreground_color_legend = :black,
#         tick_direction = :out 
#         )
#         xlabel!(L"$\Delta / \Gamma_0$")
#         ylabel!(L"$\rho / k^2$")

#     elseif Escala_de_Densidade ==  "LOG"

#         gr()
#         theme(:vibrant)

#         # Dados_Resultantes = Histograma_Resultante
#         # x_a = Dados_Resultantes[ : ,1]
#         # y_b = Dados_Resultantes[ : ,2]
#         # z_c = Dados_Resultantes[ : ,3]

#         # tamanho = 600
#         # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

#         A = zeros(N_div_Densidade,N_div_Deturn)

#         contador_1 = 1
#         contador_2 = 1

#         while true

#             A[ : ,contador_1] = Dados_Resultantes[contador_2:(contador_2+N_div_Densidade-1),3]
#             contador_2 += N_div_Densidade


#             if contador_1 == N_div_Deturn
#                 break
#             else
#                 contador_1 += 1
#             end
#         end

#         y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade)
#         x = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)
        


#         heatmap(x, y, 
#         size = (tamanho + 200, tamanho),
#         left_margin = 10Plots.mm,
#         right_margin = 10Plots.mm,
#         top_margin = 5Plots.mm,
#         bottom_margin = 5Plots.mm,
#         color = cgrad(:tempo), 
#         A,
#         colorbar_title = L"$\xi_m$",
#         title = L"\textrm{Diagrama de Fase}",
#         framestyle = :box,
#         yaxis = :log,
#         legendfontsize = 20,
#         labelfontsize = 25,
#         titlefontsize = 30,
#         tickfontsize = 15, 
#         background_color_legend = :white,
#         background_color_subplot = :white,
#         foreground_color_legend = :black,
#         tick_direction = :out 
#         )
#         xlabel!(L"$\Delta / \Gamma_0$")
#         ylabel!(L"$\rho / k^2$")
#     end

# end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------- Vizualização do Diagrama de Fase da Variancia -----------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Diagrama_de_Fase_Thouless_Number(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Variavel_Constante,Escala_de_Densidade,kR,tamanho)

    if Escala_de_Densidade == "LINEAR"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = range(ρ_Inicial,ρ_Final,length=N_div_Densidade)
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 4Plots.mm,
        right_margin = 0Plots.mm,
        top_margin = 0Plots.mm,
        bottom_margin = 0Plots.mm,
        # clims=(0,3),
        # color = cgrad(:tempo,rev=:true,[0.4,0.56, 0.6]),
        # color = cgrad(:tempo,[0.2,0.5,0.73,0.8,0.9]),
        # color = cgrad(:tempo,[0.78,0.83,0.86,0.9,0.95]),
        # color = cgrad(:tempo,scale=:log),
        # color = cgrad(:tempo),
        # color = reverse(cgrad(:PuOr_11)),
        # color = cgrad(:Paired_6,rev=:true),
        color = cgrad(:Paired_10),
        # color = cgrad(:tempo,rev=:true,scale=:exp), 
        # color = cgrad(:haline,[0.55, 0.6,0.61,0.62,0.621]), 
        # color = cgrad([:darkblue,:royalblue3,:darkgoldenrod2,:gold,:yellow3,:yellow2,:yellow1,:yellow], [0.02,0.05,0.07,0.08,0.1,0.15,0.91]), 
        log10.(A),
        # A,
        # colorbar_title = L"$log(g_T)$",
        # title = L"\textrm{Diagrama de Fase Thouless Number(g)}",
        title =  L"$log_{10}(g_T)$",
        # legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")
        
    elseif Escala_de_Densidade == "LOG"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade) 
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        color = cgrad(:tempo,rev=:true,[0.35,0.51, 0.55]),
        # color = cgrad([:yellow,:yellow1,:yellow2,:yellow3,:gold,:darkgoldenrod2,:orange,:darkorange1,:green4,:deepskyblue3,:royalblue3,:darkblue], [0.02,0.023,0.03,0.04,0.045,0.1,0.2,0.33,0.41,0.47,0.5,0.51,0.6,0.7]),  
        log10.(A),
        # colorbar_title = L"$\sigma_T$",
        # title = L"\textrm{Diagrama de Fase Thouless Number(g)}",
        title =  L"$log_{10}(g_T)$",
        legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        yaxis = :log,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho / k^2$")
    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Vizualização Media das Intensidades ao longo dos Angulos -------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_media_da_Intentensidade_ao_longo_dos_angulos(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,N_Sensores,N_Realizações,Δ,ρ_normalizado,tamanho)
    

    gr()
    theme(:vibrant)


    Intensidade_media = zeros(N_Sensores)

    for i in 1:N_Sensores

        fator_soma = 0
        aux_1 = 0

        for j in 1:N_Realizações

            fator_soma += Intensidade_Resultante_NORMALIZADA[i + aux_1]
            aux_1 += N_Sensores

        end

        Intensidade_media[i] = fator_soma/N_Realizações

    end


    Angulos = Intensidade_Resultante_PURA[2,1:N_Sensores]*π/180
    Δ_ap = round(Δ,digits=2)

    plot(Angulos,Intensidade_media/minimum(Intensidade_media),
    lw = 4,
    proj = :polar,
    lims = (0,10),
    c = :blue,
    size = (tamanho+150, (tamanho+100)/2 + 250),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 1,
    minorgrid = true,
    minorgridalpha = 1,
    minorgridstyle = :dashdot,
    lab= "",
    title = "",
    # legendtitle = "Δ = $Δ_ap
    # ρ/k² = $ρ_normalizado  ",
    framestyle = :box,
    legendtitlefontsize = 20;
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 13, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Vizualização Media das Intensidades ao longo dos Angulos -------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_media_do_Perfil_da_Intensidade(r,Posição_Sensores,Radius,L,Intensidade_Resultante_NORMALIZADA,N_Sensores,N_Realizações,GEOMETRIA_DA_NUVEM,tamanho)
    

    gr()
    theme(:vibrant)


    Intensidade_INCIDENTE_media = zeros(N_Sensores)

    for i in 1:N_Sensores

        fator_soma = 0
        aux_1 = 0

        for j in 1:N_Realizações

            fator_soma += Intensidade_Resultante_NORMALIZADA[1 , i + aux_1]
            aux_1 += N_Sensores

        end

        Intensidade_INCIDENTE_media[i] = fator_soma/N_Realizações

    end

    Intensidade_ESPALHADA_media = zeros(N_Sensores)

    for i in 1:N_Sensores

        fator_soma = 0
        aux_1 = 0

        for j in 1:N_Realizações

            fator_soma += Intensidade_Resultante_NORMALIZADA[2 , i + aux_1]
            aux_1 += N_Sensores

        end

        Intensidade_ESPALHADA_media[i] = fator_soma/N_Realizações

    end


    All_Intensitys = zeros(4,N_Sensores)
    All_Intensitys[1,:] = Intensidade_INCIDENTE_media
    All_Intensitys[3,:] = Intensidade_ESPALHADA_media


    if GEOMETRIA_DA_NUVEM == "DISCO"


        scatter(r[ : ,1],r[ : ,2],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        right_margin = 10Plots.mm,
        ylims = (-4*Radius,4*Radius),
        xlims = (-4*Radius,4*Radius),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        Laser_cima_disc(x) =  (Radius/2)*sqrt((x/(15*Radius/16))^2 + 1)
        x = range(-10*Radius,10*Radius, length=100)
        y = Laser_cima_disc.(x)
        plot!(x,y,lw=3,lab="",color=:black,fill = (0, :black),fillalpha=0.4)

        Laser_baixo_disc(x) = -(Radius/2)*sqrt((x/(15*Radius/16))^2 + 1)
        x = range(-10*Radius,10*Radius, length=100)
        y = Laser_baixo_disc.(x)
        plot!(x,y,lw=3,color=:black,lab=L"\textrm{Laser Gaussiano}",fill = (0, :black),fillalpha=0.4)



        N_Sensores = size(Posição_Sensores,1)

        Intensity = zeros(2*N_Sensores) 

        Intensity[1:N_Sensores] = All_Intensitys[1, : ]
        Intensity[(1+N_Sensores):end] = All_Intensitys[3, : ]

        Todos_SENSORES = zeros(2*N_Sensores,2) 
        Todos_SENSORES[1:N_Sensores,1] = Posição_Sensores[ : ,1]
        Todos_SENSORES[(1+N_Sensores):end,1] = Posição_Sensores[ : ,3]
        Todos_SENSORES[1:N_Sensores,2] = Posição_Sensores[ : ,2]
        Todos_SENSORES[(1+N_Sensores):end,2] = Posição_Sensores[ : ,4]


        # scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],
        # aspect_ratio=:equal,
        # ms = 4,
        # zcolor = Intensity,
        # framestyle =:box,
        # label = "",
        # color = cgrad(categorical = true),
        # colorbar_title = L"\textrm{Intensidade Normalizada}"
        # )

        u = 3*(Intensity)
        v = zeros(2*N_Sensores)

        quiver!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],quiver=(u,v),c=:blue)
        scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],c=:orangered1,lab="Sensores",ms=4)        

    
    elseif GEOMETRIA_DA_NUVEM == "SLAB" 

        scatter(r[ : ,1],r[ : ,2],
        label=L"\textrm{Atomos}",
        size = (tamanho+100, tamanho),
        right_margin = 10Plots.mm,
        ylims = (-3*L,3*L),
        xlims = (-3*L,3*L),
        m = (:green,stroke(1,:black)),
        ms = 5,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        Laser_cima_slab(x) =  (L/4)*sqrt((x/(15*L/32))^2 + 1)
        x = range(-10*L,10*L, length=100)
        y = Laser_cima_slab.(x)
        plot!(x,y,lw=3,lab="",color=:black,fill = (0, :black),fillalpha=0.4)

        Laser_baixo_slab(x) = -(L/4)*sqrt((x/(15*L/32))^2 + 1)
        x = range(-10*L,10*L, length=100)
        y = Laser_baixo_slab.(x)
        plot!(x,y,lw=3,color=:black,lab=L"\textrm{Laser Gaussiano}",fill = (0, :black),fillalpha=0.4)




        N_Sensores = size(Posição_Sensores,1)

        Intensity = zeros(2*N_Sensores) 

        Intensity[1:N_Sensores] = All_Intensitys[1, : ]
        Intensity[(1+N_Sensores):end] = All_Intensitys[3, : ]

        Todos_SENSORES = zeros(2*N_Sensores,2) 
        Todos_SENSORES[1:N_Sensores,1] = Posição_Sensores[ : ,1]
        Todos_SENSORES[(1+N_Sensores):end,1] = Posição_Sensores[ : ,3]
        Todos_SENSORES[1:N_Sensores,2] = Posição_Sensores[ : ,2]
        Todos_SENSORES[(1+N_Sensores):end,2] = Posição_Sensores[ : ,4]


        # scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],
        # aspect_ratio=:equal,
        # ms = 4,
        # zcolor = Intensity,
        # framestyle =:box,
        # label = "",
        # color = cgrad(categorical = true),
        # colorbar_title = L"\textrm{Intensidade Normalizada}"
        # )

        u = 3*(Intensity)
        v = zeros(2*N_Sensores)

        quiver!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],quiver=(u,v),c=:blue)
        scatter!(Todos_SENSORES[ : ,1],Todos_SENSORES[ : ,2],c=:orangered1,lab="Sensores",ms=4)        

        
    end

    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")


end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Vizualização Propriedades de um modo especifico -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Dinamica_dos_atomos(r,β,Tipo_de_kernel,Radius,Δ,L,Lₐ,GEOMETRIA_DA_NUVEM,fase,b₀,tamanho)



    gr()
    theme(:vibrant)

    r_cartesian = r

    x_a = r_cartesian[ : ,1]
    y_b = r_cartesian[ : ,2]
    
    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = sqrt(L^2 + Lₐ^2)

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius
    
    end

    if Tipo_de_kernel == "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar

        z_c = log10.(abs.(β))

        scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(0.7)*Limite,(0.7)*Limite),
        xlims = (-(0.7)*Limite,(0.7)*Limite),
        ms = 6,
        zcolor = z_c,
        framestyle = :box,
        label = " Δ = $Δ , ϕ/π = $fase, b₀ = $b₀ ",
        color = cgrad(categorical =true),
        title =  L"$log_{10}(\beta_n)$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        
        )
    
        # xlabel!(L"\textrm{Posição X}")
        # ylabel!(L"\textrm{Posição Y}")

    elseif Tipo_de_kernel == "Vetorial" 

        N = size(r,1)
        z_c₊ = log10.(abs.(β[1:N]))
        z_c₋ = log10.(abs.(β[(N+1):end]))


        p1 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.2)*Limite,(1.2)*Limite),
        xlims = (-(1.2)*Limite,(1.2)*Limite),
        ms = 6,
        zcolor = z_c₊,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ , ϕ/π = $fase   ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(+)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )
        
        
        p2 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.2)*Limite,(1.2)*Limite),
        xlims = (-(1.2)*Limite,(1.2)*Limite),
        ms = 6,
        zcolor = z_c₋,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ , ϕ/π = $fase   ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(-)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        plot(p1, p2, layout = (2,1))
    end

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------- Vizualização Perfil da Intensidade espalhada e a dinamica dos atomos  -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_perfil_da_Intensidade_e_dinamica_dos_atomos(r,β,Tipo_de_kernel,Δ, All_Intensitys,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,b₀,fator_de_controle,fase,geometria::TwoD,tamanho)


    gr()
    theme(:vibrant)

    r_cartesian = r
    
    x_a = r_cartesian[ : ,1]
    y_b = r_cartesian[ : ,2]
    
    if GEOMETRIA_DA_NUVEM == "SLAB"

        Limite = (2/3)*sqrt(L^2 + Lₐ^2)

    elseif GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = 2.5*Radius
    
    end

    if Tipo_de_kernel == "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar

        z_c = log10.(abs.(β))

        scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        ms = 6,
        zcolor = z_c,
        framestyle = :box,
        label = " Δ = $Δ",
        legendtitle = "b₀ = $b₀ ,ϕ/π = $fase",
        legendtitlefontsize = 20;
        color = cgrad(categorical =true),
        title =  L"$log_{10}(\beta_n)$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        
        )
    
        xlabel!(L"\textrm{Posição X}")
        ylabel!(L"\textrm{Posição Y}")
    

    elseif Tipo_de_kernel == "Vetorial" 

        N = size(r,1)
        z_c₊ = log10.(abs.(β[1:N]))
        z_c₋ = log10.(abs.(β[(N+1):end]))


        p1 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        ms = 6,
        zcolor = z_c₊,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(+)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )
        
        
        p2 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        ms = 6,
        zcolor = z_c₋,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(-)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        plot(p1, p2, layout = (2,1))

        annotate!(xpos, ypos, text("mytext", :red, :right, 3))
    end
    

    plot!(
        Posição_Sensores[ : ,1] .+ (fator_de_controle*All_Intensitys[1, : ]),
        Posição_Sensores[ : ,2],
        c = :orange,
        lab = "",
        lw = 6
    )
    
    plot!(
        Posição_Sensores[ : ,3] .+ (fator_de_controle*All_Intensitys[3, : ]),
        Posição_Sensores[ : ,4],
        c = :orange,
        lab = "",
        lw = 6
    )

    scatter!(
        Posição_Sensores[ : ,1],
        Posição_Sensores[ : ,2],
        c = :black, 
        lab = "",
        ms=4
    )        

    scatter!(
        Posição_Sensores[ : ,3],
        Posição_Sensores[ : ,4],
        c = :green, 
        lab = "",
        ms=4
    )        

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_perfil_da_Intensidade_e_dinamica_dos_atomos(r,β,Tipo_de_kernel,Δ, All_Intensitys,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,b₀,fator_de_controle,fase,geometria::ThreeD,tamanho)


    gr()
    theme(:vibrant)

    r_cartesian = r
    
    x_a = r_cartesian[ : ,1]
    y_b = r_cartesian[ : ,2]
    z_c = r_cartesian[ : ,3]
    
    if GEOMETRIA_DA_NUVEM == "CUBO"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = 2.5*Radius
    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 

        Limite = Lₐ/2

    elseif GEOMETRIA_DA_NUVEM == "TUBO" 

        Limite = Lₐ

    end


    if Tipo_de_kernel == "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar

        t_d = log10.(abs.(β))

        scatter(x_a,y_b,z_c,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.5,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        zlims = (-Limite,Limite),
        ms = 6,
        c = :green,
        # zcolor = t_d,
        framestyle = :box,
        label = "Δ = $Δ",
        # legendtitle = "b₀ = $b₀ ,ϕ/π = $fase",
        legendtitlefontsize = 20;
        # color = cgrad(categorical =true),
        camera = (45,30),
        # title =  L"$log_{10}(\beta_n)$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )
    
        xlabel!(L"\textrm{Posição X}")
        ylabel!(L"\textrm{Posição Y}")
    

    elseif Tipo_de_kernel == "Vetorial" 

        N = size(r,1)
        z_c₊ = log10.(abs.(β[1:N]))
        z_c₋ = log10.(abs.(β[(N+1):end]))


        p1 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        ms = 6,
        zcolor = z_c₊,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(+)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )
        
        
        p2 = scatter(x_a,y_b,
        size = (tamanho+150, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0.3,
        ylims = (-(1.5)*Limite,(1.5)*Limite),
        xlims = (-(1.5)*Limite,(1.5)*Limite),
        ms = 6,
        zcolor = z_c₋,
        framestyle = :box,
        aspect_ratio =:equal,
        label = " Δ = $Δ",
        color = cgrad(categorical = true),
        title =  L"$log_{10}(\beta_n^{(-)})$",
        titlelocation = :right,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        plot(p1, p2, layout = (2,1))

        annotate!(xpos, ypos, text("mytext", :red, :right, 3))
    end
    
    # default(titlefont = (25, "times"), legendfontsize = 18,guidefont = (30, :blue), tickfont = (25, :black), guide = "", framestyle = :zerolines)

    if GEOMETRIA_DA_NUVEM == "CUBO"

        Limite = L

    elseif GEOMETRIA_DA_NUVEM == "ESFERA" 
        
        Limite = 2.5*Radius
    
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        

    elseif GEOMETRIA_DA_NUVEM == "TUBO" 


        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)

    end



    plot!(Posição_Sensores[ : ,1],Posição_Sensores[ : ,2],fator_de_controle*All_Intensitys[1, : ] .+L/2,
    title = "",
    st=:surface,
    clims = (0,30),
    # camera=(-30,20),
    # size = (tamanho+200, tamanho),
    lw=0,
    # left_margin = [5Plots.mm 5Plots.mm], 
    # bottom_margin = 5Plots.mm,xrotation = 90
    )


end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------- Vizualização Perfil da Intensidade espalhada e a dinamica dos atomos  -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_perfil_da_Intensidade_e_da_Cintura_nos_Sensores(σ²,Δ, Intensidade_Normalizada,Posição_Sensores,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,N_Telas,λ,N,fator_controle,tamanho)


    gr()
    theme(:vibrant)

    if PERFIL_DA_INTENSIDADE ≠ "SIM,para obter a cintura do feixe"
       return "NÃO PODE!!!" 
    end

    if GEOMETRIA_DA_NUVEM == "DISCO" 
        
        Limite = Radius

        gr()
        theme(:vibrant)

        p1 = scatter(Posição_Sensores[ : ,1],Posição_Sensores[:,2],
        zcolor = fator_controle*(Intensidade_Normalizada[1, : ]),
        label=L"\textrm{Perfil Luz Incidadente}",
        title = L"$I . 10^8$",
        titlelocation = :right,
        size = (tamanho+100, tamanho),
        left_margin = 5Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ms = 4,
        gridalpha = 0,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 12,     
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        xlabel = L"$kX$",
        ylabel = L"$kY$"

        )

        p2 = scatter(Posição_Sensores[ : ,3],Posição_Sensores[:,4],
        zcolor = fator_controle*(Intensidade_Normalizada[3, : ]),
        label=L"\textrm{Perfil Luz Espahada}",
        title =  L"$I . 10^8$",
        titlelocation = :right,
        size = (tamanho+50, tamanho),
        left_margin = 5Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ms = 4,
        gridalpha = 0,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 12, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        xlabel = L"$kX$",
        ylabel = L"$kY$"
        )  

        # p3 = plot(
        # reverse(σ²[ : ,1]),
        # reverse(σ²[ : ,2]),
        # title = L"\textrm{Perfil da Cintura Antes}",
        # label = " N = $N",
        # size = (tamanho+100, tamanho),
        # left_margin = 10Plots.mm,
        # right_margin = 12Plots.mm,
        # top_margin = 5Plots.mm,
        # bottom_margin = 5Plots.mm,
        # yticks = collect(round(Int64,((minimum(σ²[ : ,2]))-1)):10:(round(Int64,maximum(σ²[ : ,2]))+1)),
        # ylims = (round(Int64,((minimum(σ²[ : ,2]))-1)),(round(Int64,maximum(σ²[ : ,2]))+1)),
        # ms = 0,
        # lw = 4,
        # framestyle = :box,
        # gridalpha = 0.5,
        # legendfontsize = 15,
        # labelfontsize = 20,
        # titlefontsize = 20,
        # tickfontsize = 12, 
        # background_color_legend = :white,
        # background_color_subplot = :white,
        # foreground_color_legend = :black,
        # c = :black,
        # xlabel = L"$kX$",
        # ylabel = L"$\sigma^2$"

        # )

        # p4 = plot(
        # σ²[ : ,3],
        # σ²[ : ,4],
        # title = L"\textrm{Perfil da Cintura Depois}",
        # label = " N = $N",
        # size = (tamanho+100, tamanho),
        # left_margin = 10Plots.mm,
        # right_margin = 12Plots.mm,
        # top_margin = 5Plots.mm,
        # bottom_margin = 5Plots.mm,
        # yticks = collect(round(Int64,((minimum(σ²[ : ,4]))-1)):15:(round(Int64,maximum(σ²[ : ,4]))+1)),
        # ylims = (round(Int64,((minimum(σ²[ : ,4]))-1)),(round(Int64,maximum(σ²[ : ,4]))+1)),
        # ms = 0,
        # lw = 4,
        # framestyle = :box,
        # gridalpha = 0.5,
        # legendfontsize = 15,
        # labelfontsize = 20,
        # titlefontsize = 20,
        # tickfontsize = 15, 
        # background_color_legend = :white,
        # background_color_subplot = :white,
        # foreground_color_legend = :black,
        # c = :black,
        # xlabel = L"$kX$",
        # ylabel = L"$\sigma^2$"

        # )

        # plot(p1, p2, p3, p4, layout = @layout([a b; c d]))

        N_Pontos = size(σ²,1)
        σ²_total = zeros(2*N_Pontos,2)

        σ²_total[1:N_Pontos,1] = reverse(σ²[ : ,1])
        σ²_total[1:N_Pontos,2] = reverse(σ²[ : ,2])    
        σ²_total[(N_Pontos+1):end,1] = σ²[ : ,3]
        σ²_total[(N_Pontos+1):end,2] = σ²[ : ,4]    

        p3 = plot(
            σ²_total[ : ,1],
            σ²_total[ : ,2],
            title = L"\textrm{Evolução da Cintura ao percorrer a nuvem}",
            label = " N = $N",
            size = (tamanho+100, tamanho),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            # yticks = collect(round(Int64,((minimum(σ²_total[ : ,2]))-1)):20:(round(Int64,maximum(σ²_total[ : ,2]))+1)),
            # ylims = (round(Int64,((minimum(σ²_total[ : ,2]))-1)),(round(Int64,maximum(σ²_total[ : ,2]))+1)),
            ms = 0,
            lw = 4,
            framestyle = :box,
            gridalpha = 0.5,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 20,
            tickfontsize = 12, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            c = :black,
            xlabel = L"$kX$",
            ylabel = L"$\sigma^2/L^2$"

        )

        plot(p1, p2, p3, layout = @layout([a b; c ]))

    elseif GEOMETRIA_DA_NUVEM == "SLAB"

        gr()
        theme(:vibrant)

        m1 = round(Int64,maximum(fator_controle*(Intensidade_Normalizada[1, : ])))        
        m2 = round(Int64,maximum(fator_controle*(Intensidade_Normalizada[3, : ])))

        if m1 >= m2

            cota_superior = m1

        elseif m1 < m2
        
            cota_superior = m2
        
        end

        p1 = scatter(Posição_Sensores[ : ,1],Posição_Sensores[:,2],
        zcolor = fator_controle*(Intensidade_Normalizada[1, : ]),
        label=L"\textrm{Perfil Luz Incidadente}",
        title = L"$I . 10^8$",
        titlelocation = :right,
        color = cgrad(:gnuplot2),		
        size = (tamanho+100, tamanho),
        left_margin = 5Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        # clims = (0,cota_superior),
        clims = (0,120),
        ms = 4,
        gridalpha = 0,
        legendfontsize = 15,
        labelfontsize = 15,
        titlefontsize = 20,
        tickfontsize = 12,     
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        xlabel = L"$kX$",
        ylabel = L"$kY$"

        )

        p2 = scatter(Posição_Sensores[ : ,3],Posição_Sensores[:,4],
        zcolor = fator_controle*(Intensidade_Normalizada[3, : ]),
        label=L"\textrm{Perfil Luz Espahada}",
        title =  L"$I . 10^8$",
        titlelocation = :right,
        color = cgrad(:gnuplot2),		
        size = (tamanho+50, tamanho),
        left_margin = 5Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        # clims = (0,cota_superior),
        clims = (0,120),
        ms = 4,
        gridalpha = 0,
        legendfontsize = 15,
        labelfontsize = 15,
        titlefontsize = 20,
        tickfontsize = 12, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        xlabel = L"$kX$",
        ylabel = L"$kY$"
        )  

        # p3 = plot(
        # reverse(σ²[ : ,1]),
        # reverse(σ²[ : ,2]),
        # title = L"\textrm{Perfil da Cintura Antes}",
        # label = " N = $N",
        # size = (tamanho+100, tamanho),
        # left_margin = 10Plots.mm,
        # right_margin = 12Plots.mm,
        # top_margin = 5Plots.mm,
        # bottom_margin = 5Plots.mm,
        # yticks = collect(round(Int64,((minimum(σ²[ : ,2]))-1)):10:(round(Int64,maximum(σ²[ : ,2]))+1)),
        # ylims = (round(Int64,((minimum(σ²[ : ,2]))-1)),(round(Int64,maximum(σ²[ : ,2]))+1)),
        # ms = 0,
        # lw = 4,
        # framestyle = :box,
        # gridalpha = 0.5,
        # legendfontsize = 15,
        # labelfontsize = 20,
        # titlefontsize = 20,
        # tickfontsize = 12, 
        # background_color_legend = :white,
        # background_color_subplot = :white,
        # foreground_color_legend = :black,
        # c = :black,
        # xlabel = L"$kX$",
        # ylabel = L"$\sigma^2$"

        # )

        # p4 = plot(
        # σ²[ : ,3],
        # σ²[ : ,4],
        # title = L"\textrm{Perfil da Cintura Depois}",
        # label = " N = $N",
        # size = (tamanho+100, tamanho),
        # left_margin = 10Plots.mm,
        # right_margin = 12Plots.mm,
        # top_margin = 5Plots.mm,
        # bottom_margin = 5Plots.mm,
        # yticks = collect(round(Int64,((minimum(σ²[ : ,4]))-1)):15:(round(Int64,maximum(σ²[ : ,4]))+1)),
        # ylims = (round(Int64,((minimum(σ²[ : ,4]))-1)),(round(Int64,maximum(σ²[ : ,4]))+1)),
        # ms = 0,
        # lw = 4,
        # framestyle = :box,
        # gridalpha = 0.5,
        # legendfontsize = 15,
        # labelfontsize = 20,
        # titlefontsize = 20,
        # tickfontsize = 15, 
        # background_color_legend = :white,
        # background_color_subplot = :white,
        # foreground_color_legend = :black,
        # c = :black,
        # xlabel = L"$kX$",
        # ylabel = L"$\sigma^2$"

        # )

        # plot(p1, p2, p3, p4, layout = @layout([a b; c d]))

        N_Pontos = size(σ²,1)
        σ²_total = zeros(2*N_Pontos,2)

        σ²_total[1:N_Pontos,1] = reverse(σ²[ : ,1])
        σ²_total[1:N_Pontos,2] = reverse(σ²[ : ,2])    
        σ²_total[(N_Pontos+1):end,1] = σ²[ : ,3]
        σ²_total[(N_Pontos+1):end,2] = σ²[ : ,4]    

        σ²_interno = zeros(2,2)
            
        σ²_interno[1,1] = σ²_total[N_Pontos-1,1]
        σ²_interno[1,2] = σ²_total[N_Pontos-1,2]
        σ²_interno[2,1] = σ²_total[1+N_Pontos,1]
        σ²_interno[2,2] = σ²_total[1+N_Pontos,2]

        L_aproxi = round(L,digits=2)
        
        p3 = plot(
            σ²_total[ : ,1],
            σ²_total[ : ,2],
            title = L"\textrm{Evolução da Cintura ao percorrer a nuvem}",
            label = " N = $N, L = $L_aproxi",
            size = (tamanho+100, tamanho),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            ylims = (100,1000),
            # yscale = :log10,  
            # yticks = [10^2,10^3],
            xlims = (-L,L),          
            ms = 0,
            lw = 4,
            framestyle = :box,
            gridalpha = 0.5,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 20,
            tickfontsize = 12, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            c = :black,
            xlabel = L"$kX$",
            ylabel = L"$\sigma^2$"
        )
        vline!([L/2]; label="", linestyle=:dashdot, color= :green, linewidth=5)
        vline!([-L/2]; label="", linestyle=:dashdot, color= :green, linewidth=5)

        plot(p1, p2, p3, layout = @layout([a b; c ]))


    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------- Vizualização Perfil da Intensidade espalhada e a dinamica dos atomos  -------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_Perfil_da_Cintura_e_Intensidade(Sensores,Intensidades,Ns,b₀s,σ²s,Δ,Variavel_Constante,L,Lₐ,ρs,titulo,tamanho)

    # model_gaussiana_log(x,p) =  -((( x .- p[2]).^2)./(2*(p[3]^2))) .+ p[1]
    # model_gaussiana(x,p) = p[1].*exp.((-( x .- p[2]).^2)/(2*(p[3]^2)))  


    if Variavel_Constante == "N"
       
        gr()
        theme(:vibrant)

        N_Realizações = size(Ns,1)

        σ² = round(σ²s[end],digits = 2) 
        paleta_de_cores = palette(:nipy_spectral, N_Realizações+1)

        plot(Sensores,Intensidades[ : ,end],
        ms = 0,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        linestyle=:dashdot,
        yscale = :log10,
        # ylims = (10^-10,10^-5),
        c= :red,
        lw = 8,
        lab = L"$ N = 0$" ,
        # lab = "" ,
        framestyle = :box,
        legendfontsize = 17,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        # FWHM = get_one_σ_FWHM(Sensores,Intensidades[:,end])
        # σ² = round(FWHM,digits = 2)
        # vline!([-FWHM/2,FWHM/2],lab=L"$ \sigma^2 = %$σ²   $",c=:red,linestyle=:solid,lw=6)

        # xs = Sensores
        # ys = log10.(Intensidades[ : ,end])

        # model = loess(xs, ys, span=0.5)

        # us = range(extrema(xs)...; step = 0.1)
        # vs = exp10.(Loess.predict(model, us))

        # plot!(us,vs,lw=6,lab="",linestyle = :dash,c = :red)


        # p,σ² = get_one_σ_gaussian(Sensores,Intensidades[:,end],SLAB())
        # amplitude,μ,σ = p
        # μ, σ = mean_and_std(Sensores, Weights(Intensidades[:,i] ); corrected = false)
        # plot!(Sensores,model_gaussiana(Sensores,[amplitude,μ,σ]),lw=6,lab=L"$ N = 0, \sigma^2 = %$σ²  $",linestyle = :dash,c = :red)



        for i in 1:N_Realizações
            

            N = Ns[i]
            ρ = round(ρs[i],digits=2)
            σ² = round(σ²s[i],digits = 2) 
            Transmission = round(sum(Intensidades[ : ,i])/sum(Intensidades[ : ,end]),digits =2)

            # p,σ² = get_one_σ_lorentz(Sensores,Intensidades[:,i],SLAB())
            # amplitude,μ,σ = p
            # σ² = round(σ²,digits = 2)
            # # μ, σ = mean_and_std(Sensores, Weights(Intensidades[:,i] ); corrected = false)
            # plot!(Sensores,model_lorenziana(Sensores,[amplitude,μ,σ]),lw=6,lab="",linestyle = :dash,c = paleta_de_cores[i])

            
            xs = Sensores
            ys = log10.(Intensidades[ : ,i])

            model = Loess.loess(xs, ys, span=0.6)
            # model = Smoothers.loess(xs,ys)

            us = range(extrema(xs)...; step = 0.1)
            vs = exp10.(Loess.predict(model, us))
            # vs = exp10.(model.(us))

            plot!(us,vs,lw=6,lab="",linestyle = :dash,c = paleta_de_cores[i])

            # FWHM = get_one_σ_FWHM(Sensores,Intensidades[:,i],SLAB())[1]
            # σ² = round(FWHM,digits = 2)
            # vline!([-FWHM/2,FWHM/2],lab="",c = paleta_de_cores[i],linestyle=:solid,lw=6)


            plot!(Sensores,Intensidades[ : ,i],
            ms = 0,
            size = (tamanho+150, (tamanho+100)/2 + 250),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            yscale = :log10,
            # xscale = :log10,
            ylims = (10^-22,10^-5),
            # xlims = (0,maximum(Sensores)),
            minorgridstyle = :dashdot,
            lw = 5,
            # lab = L"$ N = %$N , \sigma^2 = %$σ², T = %$Transmission,\rho /k^2 =  %$ρ $" ,
            lab = L"$ N = %$N,\rho /k^2 =  %$ρ $" ,
            # lab = "" ,

            title = titulo,
            c = paleta_de_cores[i],
            framestyle = :box,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 25,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black,
            legend =:topright
            )


        end


        xlabel!(L"$ kY$")
        ylabel!(L"\textrm{Intensidade Média}")

    elseif Variavel_Constante == "b₀"
            
        gr()
        theme(:vibrant)

        N_Realizações = size(b₀s,1)

        paleta_de_cores = palette(:rainbow, N_Realizações)

        σ² = round(σ²s[end],digits = 2) 

        plot(Sensores,Intensidades[ : ,end],
        ms = 0,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        linestyle=:dashdot,
        yscale = :log10,
        # xscale = :log10,
        # ylims = (10^-8,10^0),
        ylims = (10^-12,10^-7),
        c= :black,
        lw = 8,
        lab = "b₀ = 0 , σ² = $σ² " ,
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        for i in 1:N_Realizações
            


            b₀ = b₀s[i]
            σ² = round(σ²s[i],digits = 2) 

            plot!(Sensores,Intensidades[ : ,i],
            ms = 0,
            size = (tamanho+150, (tamanho+100)/2 + 250),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 5Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            c = paleta_de_cores[i],
            yscale = :log10,
            # ylims = (10^-8,10^0),
            ylims = (10^-12,10^-7),
            # xscale = :log10,
            minorgridstyle = :dashdot,
            lw = 5,
            lab = "b₀ = $b₀ , σ² = $σ² " ,
            title = "L = $L , Δ =$Δ",
            titlelocation = :right,
            framestyle = :box,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 20,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black

            )        

        end


        xlabel!(L"$ kY$")
        ylabel!(L"\textrm{Intensidade Média}")
    end
            
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização Transmissão em função de espessura optica  ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_Transmission_and_b(Δ_b,Transmission_coerente,Transmission_difuso,b₀,ρ,Escala,tamanho)

    gr()
    theme(:vibrant)

    Lei_de_Beer(x) =  exp(-x)

    if Escala == "LINEAR"

        plot(Δ_b,Transmission_coerente,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :green,
        lab = L"\textrm{Campo Coerente}" ,
        title = "b₀ = $b₀ , ρ = $ρ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        plot!(Δ_b,Transmission_difuso,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :red,
        lab = L"\textrm{Campo Difuso}",
        title = "b₀ = $b₀ , ρ = $ρ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )
        x = range(0, 10, length=1000)
        y = Lei_de_Beer.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"\textrm{Lei de Beer-Lambert}"
        )


    elseif Escala == "LOG"

        plot(Δ_b,Transmission_coerente,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :green,
        lab = L"\textrm{Campo Coerente}" ,
        title = "b₀ = $b₀ , ρ = $ρ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        plot!(Δ_b,Transmission_difuso,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :red,
        lab = L"\textrm{Campo Difuso}",
        title = "b₀ = $b₀ , ρ = $ρ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        x = range(0.01, 10, length=1000)
        y = Lei_de_Beer.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"\textrm{Lei de Beer-Lambert}"
        )
    
    end



    xlabel!(L"$ b_{\delta}$")
    ylabel!(L"$T(I_{coe}/I_{inc})$")

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Transmission_and_b(Δ_b,Transmission,Ls,A_TI,A_TF,Lₐ,Escala,tamanho)

    gr()
    theme(:vibrant)

    Lei_de_Beer(x) =  exp(-x)

    if Escala == "LINEAR"

        plot(Δ_b,Transmission,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :green,
        lab = L"$\phi = [%$A_TI,%$A_TF] $" ,
        title = "b₀ = $b₀ , ρ = $ρ ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        x = range(0, 10, length=1000)
        y = Lei_de_Beer.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"\textrm{Lei de Beer-Lambert}"
        )


    elseif Escala == "LOG"

        N_Realizações = size(Ls,1)
        Ls_Instataneo = Ls[1]
        plot(Δ_b,Transmission[1,:],
        size = (tamanho, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        xscale = :log10,
        ylims = (10^-2,10^0),
        minorgridstyle = :dashdot,
        lw = 4,
        c = :green,
        lab = L"$ kL = %$Ls_Instataneo $" ,
        title = L"Radius = %$Lₐ, \phi = [%$A_TI,%$A_TF]",
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        for i in 2:N_Realizações
            Ls_Instataneo = Ls[i]

            plot!(Δ_b,Transmission[i,:],
            size = (tamanho, tamanho/2),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 10Plots.mm,
            bottom_margin = 10Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            yscale = :log10,
            xscale = :log10,
            ylims = (10^-2,10^0),
            minorgridstyle = :dashdot,
            lw = 4,
            lab = L"$ kL = %$Ls_Instataneo $" ,
            title = L"Radius = %$Lₐ, \phi = [%$A_TI,%$A_TF]",
            framestyle = :box,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 25,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
    
            )
        end

        x = range(0.01, 10, length=1000)
        y = Lei_de_Beer.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"\textrm{Lei Ohm}"
        )
    
    end



    xlabel!(L"$ b_{\delta}$")
    ylabel!(L"$T(I_{coe}/I_{inc})$")

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização Transmissão em função de espessura optica  ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_cintura_por_L(σ²,Range,Escala,ρs,Tipo_de_kernel,titulo,tamanho)
    

    N_Realizações = size(ρs,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    index_modes_inside_range = findall(  (Range .≥ 0))
    
    ξs_fit = zeros(size(ρs,1),3)
    model_1(x, p) = p[1].*x .+ p[3]
    # model_1(x, p) = p[1].*x .+ σ²[1,1]
    # model_1(x, p) = p[1].*x.^(p[2]) .+ p[3]

    for i in 1:size(ρs,1)

        index_modes_inside_range = findall(  (Range .> 0).*(σ²[:,i] .> 0 ) )

        x = Range[index_modes_inside_range]
        y = σ²[index_modes_inside_range,i]

        xdata = x
        ydata = y
        p0 = [1.0,1.0,0.0]

        fit = curve_fit(model_1, xdata, ydata, p0)
        ξs_fit[i,:] = fit.param
    end
    

    if Escala == "LINEAR"

        ρ = ρs[1]
    
        plot(Range,σ²[ : ,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        # yscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"$\rho / k^2$",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

        # x = Range
        # y = σ²[index_modes_inside_range,1]

        # a_sol_plot,b_sol_plot = get_fitting_to_cintura(Range[index_modes_inside_range],σ²[ index_modes_inside_range ,1],Tipo_de_kernel)

        # a,b,c = round.(ξs_fit[1,:],digits=2)
        # plot!(
        #     Range,
        #     model_1(Range,ξs_fit[1,:]),
        #     # lab="",
        #     lab=L"$ %$a x^{%$b} + %$c $",
        #     c = paleta_de_cores[1]
        # ) 

        if Tipo_de_kernel == "Escalar"

            # law_1_ESCALAR(x) = a_sol_plot*x + b_sol_plot
            # y = law_1_ESCALAR.(x)
            # plot!(x,y,
            # lw = 4,
            # linestyle=:dashdot,
            # c = paleta_de_cores[1],
            # lab = L"$ %$a_sol_plot L + %$b_sol_plot $"
            # # lab = ""
            # )

        elseif Tipo_de_kernel == "Vetorial"

            # law_1_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot
            # y = law_1_VETORIAL.(x)
            # plot!(x,y,
            # lw = 4,
            # linestyle=:dashdot,
            # c = paleta_de_cores[1],
            # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
            # # lab = ""
            # )

        end

        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                
                plot!(Range,σ²[ : ,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                # yscale = :log10,
                # ylims = (4,10),
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = titulo,
                # title = L"$\rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 15,
                labelfontsize = 30,
                titlefontsize = 25,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )

                a_sol_plot,b_sol_plot = get_fitting_to_cintura(Range[index_modes_inside_range],σ²[index_modes_inside_range,i],Tipo_de_kernel)

                # a,b,c = round.(ξs_fit[i,:],digits=2)
                # plot!(
                #     Range,
                #     model_1(Range,ξs_fit[i,:]),
                #     # lab="",
                #     lab=L"$ %$a x^{%$b} + %$c $",
                #     c = paleta_de_cores[i]
                # ) 
        
                if Tipo_de_kernel == "Escalar"

                    # law_n_ESCALAR(x) = a_sol_plot*x + b_sol_plot 
                    # y = law_n_ESCALAR.(x)
                    # plot!(x,y,
                    # lw = 4,
                    # linestyle=:dashdot,
                    # c = paleta_de_cores[i],
                    # lab = L"$ %$a_sol_plot x + %$b_sol_plot $"
                    # # lab = ""
                    # )
    
                elseif Tipo_de_kernel == "Vetorial"
    
                    # law_n_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot 
                    # y = law_n_VETORIAL.(x)
                    # plot!(x,y,
                    # lw = 4,
                    # linestyle=:dashdot,
                    # c = paleta_de_cores[i],
                    # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
                    # # lab = ""

                    # )
    
                end
            
            end



        end

        ρ = 0
    
        plot!(Range,σ²[ : ,end],
        ms = 0,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        # linestyle=:dashdot,
        c = :black,
        legend=:topleft,
        # yscale = :log10,
        # xlims = (4,30),
        # ylims = (1000,3000),
        lw = 5,
        lab = L"\rho = 0 ",
        # lab = "",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 30,
        titlefontsize = 25,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

        # law_fit(x) = 10*(x^2) + 1000 
        # y = law_fit.(x)
        # plot!(x,y,
        # lw = 10,
        # linestyle=:dashdot,
        # c = :black,
        # # lab = "$a_sol_plot L² + $b_sol_plot"
        # lab = L"$10 L^{2} + 1000 $"
        # )        

    elseif Escala == "LOG"

        ρ = ρs[1]

        scatter(Range,σ²[:,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        xscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = 30,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

        x = Range[index_modes_inside_range]
        y = σ²_L²[index_modes_inside_range,1]

        a_sol_plot,b_sol_plot = get_fitting_to_cintura(Range[index_modes_inside_range],σ²_L²[ index_modes_inside_range ,1],Tipo_de_kernel)

        if Tipo_de_kernel == "Escalar"

            # law_1_ESCALAR(x) = a_sol_plot*x + b_sol_plot
            # y = law_1_ESCALAR.(x)
            # plot!(x,y,
            # lw = 4,
            # linestyle=:dashdot,
            # c = paleta_de_cores[1],
            # # lab = "$a_sol_plot L + $b_sol_plot"
            # lab = ""
            # )

        elseif Tipo_de_kernel == "Vetorial"

            # law_1_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot
            # y = law_1_VETORIAL.(x)
            # plot!(x,y,
            # lw = 4,
            # linestyle=:dashdot,
            # c = paleta_de_cores[1],
            # # lab = "$a_sol_plot L² + $b_sol_plot"
            # lab = ""
            # )

        end

        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                
                scatter!(Range,σ²[:,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                xscale = :log10,
                # ylims = (60,100),
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = L"$Escalar - 2D - \Delta= Inter - L_a =560 - \rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 25,
                labelfontsize = 30,
                titlefontsize = 30,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )

                a_sol_plot,b_sol_plot = get_fitting_to_cintura(Range[index_modes_inside_range],σ²[ index_modes_inside_range ,i],Tipo_de_kernel)

                if Tipo_de_kernel == "Escalar"

                    # law_n_ESCALAR(x) = a_sol_plot*x + b_sol_plot 
                    # y = law_n_ESCALAR.(x)
                    # plot!(x,y,
                    # lw = 4,
                    # linestyle=:dashdot,
                    # c = paleta_de_cores[i],
                    # # lab = "$a_sol_plot x + $b_sol_plot"
                    # lab = ""
                    # )
    
                elseif Tipo_de_kernel == "Vetorial"
    
                    # law_n_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot 
                    # y = law_n_VETORIAL.(x)
                    # plot!(x,y,
                    # lw = 4,
                    # linestyle=:dashdot,
                    # c = paleta_de_cores[i],
                    # # lab = "$a_sol_plot L² + $b_sol_plot"
                    # lab = ""

                    # )
    
                end
            
            end



        end

        ρ = 0
    
        plot!(Range,σ²[:,end],
        ms = 0,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        # linestyle=:dashdot,
        c = :black,
        # ylims = (0,110),
        legend=:topleft,
        xscale = :log10,
        lw = 5,
        lab = L"\rho = 0 ",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = 30,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )    

    end

    scatter!([minimum(Range),maximum(Range)],[minimum(σ²)-5,maximum(σ²)+5],
    ms = 0,
    zcolor = [log10(ρs[1]),log10(ρs[end])] ,
    lab = "",
    c = cgrad(paleta_de_cores)
    )

    xlabel!(L"$kL$")
    ylabel!(L"$\sigma^2$")
    
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------- Vizualização do perfil da Intensidade para uma fonte pontual  ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_intensidade_e_perfil_do_feixe(N_Colunas,N_pontos,Limite,Ω,Tipo_de_Onda)

    pontos = zeros(N_pontos,2*N_Colunas)
    Intensidade = zeros(N_pontos,N_Colunas)
    Range_x = collect(range(-Limite,Limite,length=N_Colunas))
    Range_y = collect(range(-Limite,Limite,length=N_pontos))
    aux_1 = 0
    aux_2 = 0
    k₀ = 1
    ω₀ = Limite/2


    for j in 1:N_Colunas

        y_pontos = collect(range(-Limite/2,Limite/2,length=N_pontos))
        x_pontos = zeros(N_pontos)
        x_pontos .= Range_x[j]
        pontos[ : ,aux_1+1] = x_pontos
        pontos[ : ,aux_1+2] = y_pontos
        aux_1 += 2
    end


    for j in 1:N_Colunas

        for i in 1:N_pontos



        if Tipo_de_Onda == "Laser Gaussiano" 

            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                pontos[i,aux_2+1],
                pontos[i,aux_2+2],
                Ω,
                ω₀,
                k₀
            )

            Intensidade[i,j] = abs(get_eletric_field_LG_real(entrada_campo))^2

        elseif Tipo_de_Onda == "FONTE PONTUAL"

            Intensidade[i,j] = abs(Ω*SpecialFunctions.besselh(0,1,k₀*norm(pontos[i,aux_2+1:aux_2+2] .- (Limite/2,0))))^2

        elseif Tipo_de_Onda == "Onda Plana"                                                                                                     

            Intensidade[i,j] = abs(Ω*cis(k₀*dot((1,0),(pontos[i,aux_2+1],pontos[i,aux_2+2]))))^2

        end
    
    end
        aux_2 += 2
    end

    tamanho = 1000
    heatmap(Range_x, Range_x, 
    size = (tamanho + 200, 3*tamanho/4),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    color = cgrad(:tempo),
    log10.(reverse(Intensidade)),
    # colorbar_title = L"$log_{10}(\sigma^2_I)$",
    # title = L"\textrm{Diagrama de Fase da Variância}",
    title = L"$log_{10}(I)$",
    titlelocation = :right,
    framestyle = :box,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black,
    tick_direction = :out 
    )
    xlabel!(L"\textrm{Eixo X}")
    ylabel!(L"\textrm{Eixo Y}")
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização Transmissão em função de espessura optica  ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_Cintura_to_N(range_N,cinturas,L,Lₐ,Δ,Escala,tamanho)

    gr()
    theme(:vibrant)

    if Escala == "LINEAR"

        plot(range_N,cinturas,
        size = (tamanho, tamanho/2 ),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        lw = 4,
        c = :green,
        lab = "" ,
        title = "L = $L , Lₐ = $Lₐ , Δ = $Δ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

    elseif Escala == "LOG"

        plot(range_N,cinturas,
        size = (tamanho, tamanho/2 ),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        lw = 4,
        c = :green,
        yscale = :log10,
        lab = "" ,
        title = "L = $L , Lₐ = $Lₐ , Δ = $Δ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )
    
    end



    xlabel!(L"$ N $")
    ylabel!(L"$\sigma^2 / L^2$")

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Vizualização Transmissão em função de espessura optica  ------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_Cintura_to_b₀(b₀s,cinturas,L,Lₐ,Δ,Escala,tamanho)

    gr()
    theme(:vibrant)

    if Escala == "LINEAR"

        plot(b₀s,cinturas,
        size = (tamanho, tamanho/2 ),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        lw = 4,
        c = :green,
        lab = "" ,
        title = "L = $L , Lₐ = $Lₐ , Δ = $Δ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

    elseif Escala == "LOG"

        plot(b₀s,cinturas,
        size = (tamanho, tamanho/2 ),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        lw = 4,
        c = :green,
        yscale = :log10,
        lab = "" ,
        title = "L = $L , Lₐ = $Lₐ , Δ = $Δ",
        framestyle = :box,
        legendfontsize = 16,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )
    
    end



    xlabel!(L"$ b_0 $")
    ylabel!(L"$\sigma^2 / L^2$")

end





#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------- Vizualização da relação Densidade e a Energia dos modos Localizados -----------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_relação_energia_e_densidade(Δs,Range_ρ,Parametros_Antigos,titulo,tamanho)
    
    gr()
    theme(:vibrant)

    itp = LinearInterpolation(Range_ρ,Δs)
    Range_ρ_inter = get_points_in_log_scale(Range_ρ[1],Range_ρ[end],1000) 
    fit_interpolação = itp(Range_ρ_inter)
    plot(fit_interpolação,Range_ρ_inter,
    label=L"Interpolation",
    lw=5,
    c=:red,
    linestyle = :dash
    )
    
    model(x, p) = log.(x).*p[1] .+ p[2] 
    xdata = Range_ρ 
    ydata = Δs
    p0 = [0.25, 1.0]
    
    teste = curve_fit(model, xdata, ydata, p0)
    
    a_sol = teste.param[1] 
    a_sol_plot = round(a_sol,digits = 2)                                                                                                                                                  # Coefiente angular da melhor reta 
    b_sol = teste.param[2]              
    b_sol_plot = round(b_sol,digits = 2)                                                                                                                                    # Coefiente linear da melhor reta
    
    
    
    # Lei_de_Ray(x) =  a_sol*log(x) + b_sol
    # y = range(0.01, 10, length=100)
    # x = Lei_de_Ray.(y)
    # plot!(x,y,
    # lw = 5,
    # color = :blue,
    # linestyle = :dash,
    # lab = L"$ %$a_sol_plot ln(x) + %$b_sol_plot$"
    # )

    if Parametros_Antigos.Escala_do_range == "LINEAR"
    
        scatter!(Range_ρ,Δs,
        size = (tamanho, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0.5,
        ylims = (0,1.6),
        yticks = [0.5,1,1.5,2],
        xlims = (0,10),
        xticks = collect(0:2:10),
        ms = 12,
        m = :star4,
        c = :green,
        framestyle = :box,
        label = L"Espectro",
        legend =:bottomright ,
        # label = "",
        # colorbar_title = L"\textrm{R2}",
        title = titulo,
        # titlelocation = :right,
        legendfontsize = 20,
        legendtitlefontsize = 20;
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        
        )
        xlabel!(L"$\rho / k^2 $")
        ylabel!(L"$\Delta_{LOC}$")

    elseif Parametros_Antigos.Escala_do_range == "LOG"

        scatter!(Δs,Range_ρ,
        size = (tamanho, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0.5,
        # yscale = :log10,
        xlims = (-10,3),
        # xticks = [0.5,1,1.5,2],
        # ylims = (0.1,8),
        # yticks = [10^1,10^0,10^-1,10^-2],
        ms = 12,
        m = :star4,
        c = :green,
        framestyle = :box,
        # label = "g = $number_thouless",
        label = L"Espetro",
        legend =:topleft ,
        # colorbar_title = L"\textrm{R2}",
        title = titulo,
        # titlelocation = :right,
        legendfontsize = 20,
        legendtitlefontsize = 20;
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        
        )
        ylabel!(L"$\rho / k^2 $")
        xlabel!(L"$\Delta_{LOC}$")

    
    end

end













# tamanho = 1000

# C_Máximo(x) = real(((1-1im)*exp(1im*x)/sqrt(x))) 
# x = range(1, 10000, length=100)
# y = C_Máximo.(x)
# plot(x,y,lw = 3,color = :black,lab = "Carlos Maximo",    title = "Comportamento assintótico parte Real",
# size = (tamanho, 100 + tamanho/2),
# framestyle = :box,
# legendfontsize = 25,
# labelfontsize = 20,
# titlefontsize = 25,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white
# )
# xlabel!("x")
# ylabel!("")


# Hankel_0(x) =  real(SpecialFunctions.besselh(0,2,x))
# x = range(1, 10000, length=100)
# y = Hankel_0.(x)
# plot!(x,y,lw = 3,color = :blue,lab = "Função de Hankel")
# # savefig("Romain_203.png")



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------- Vizualização Histograma das Condutancias ------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Histograma_das_gc(Histograma,tamanho)
    
    gr()
    theme(:wong)

    x_a = collect(Histograma.edges[1])
    y_b = Histograma.weights
    
    plot(x_a[2:end],y_b,
    seriestype = :bar,
    yscale = :log10,
    gridalpha = 0  ,
    c = :blue,
    xlims = (0,21),
    size = (tamanho+100, 50 + 3(tamanho+100)/4),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    framestyle=:box,
    # label = L"\textrm{Quantidades em cada nivel de gc}",
    label = "",
    title = L"$\rho \lambda^3 = 40 - \Delta = 1 - Escalar $",
    xrotation = 45,
    legendfontsize = 20,
    labelfontsize = 25,
    titlefontsize = 30,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    )
    xlabel!(L"$g_C$")
    ylabel!(L"\textrm{Quantidade de dados por gc}")

end 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades de g_c ----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_de_gc(Histograma,titulo,tamanho)
    
    gr()
    theme(:vibrant)
    h_normalizado = normalize(Histograma; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights

    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 12,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    # xlims = (0,21),
    ylims = (0.001,1),
    lw = 5,
    framestyle=:box,
    # yticks = [10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    title = titulo,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    
    )
    xlabel!(L"$g_C$" )
    ylabel!(L"$P (g_C)$")





end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades de ln(g_c) ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Distribuição_de_Probabilidades_de_lngc(Histograma,titulo,tamanho)
    
    gr()
    theme(:vibrant)
    h_normalizado = normalize(Histograma; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights

    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 12,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    # yscale = :log10,
    xlims = (-3,3),
    ylims = (0,0.6),
    lw = 5,
    framestyle=:box,
    yticks = [0.6,0.4,0.2,0],
    lab = L"\textrm{Númerico}",
    title = titulo,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    
    )
    xlabel!(L"$ln(g_C)$" )
    ylabel!(L"$P (ln(g_C))$")





end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------- Vizualização Distribuição das Probababilidades da Intensidade ---------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_vom_rossum(h,titulo,tamanho)
    
    gr()
    theme(:vibrant)
    h_normalizado = normalize(h; mode=:pdf)

    x_a = collect(h_normalizado.edges[1])
    y_b = h_normalizado.weights

    scatter(x_a[2:end],y_b,
    m = :star4,
    ms = 15,
    size = (tamanho, tamanho/2),
    left_margin = 10Plots.mm,
    right_margin = 10Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    c = :blue,
    gridalpha = 0.8,
    minorgrid = true,
    minorgridalpha = 0.5,
    minorgridstyle = :dashdot,
    yscale = :log10,
    xscale = :log10,
    xlims = (10^-1,10^2),
    xticks = [10^-1,10^0,10^1,10^2],
    ylims = (10^-6,10^1),
    yticks = [10^1,10^0,10^-2,10^-4,10^-6],
    lab = L"\textrm{Númerico}",
    title = titulo,
    framestyle = :box,
    legendfontsize = 15,
    labelfontsize = 25,
    titlefontsize = 20,
    tickfontsize = 15,
    legendtitlefontsize = 12; 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black
    
    
    )
    xlabel!(L"$g_C$")
    ylabel!(L"$P(g_C)$")
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------ Vizualização do Diagrama de Fase da Variancia das Estatisticas de gc  --------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Diagrama_de_Fase_das_Estatisticas_de_gc(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Variavel_Constante,titulo,tamanho)

    if Escala_de_Densidade == "LINEAR"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = range(ρ_Inicial,ρ_Final,length=N_div_Densidade)
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 7Plots.mm,
        bottom_margin = 10Plots.mm,
        # color = cgrad(:tempo,rev=:true),
        # color = cgrad(:tempo,[0.3,0.35]),
        # color = cgrad(:tempo,[0.05,0.15,0.25],rev=:true),
        # color = cgrad(:PuOr_11),
        # color = cgrad(:Paired_10),
        color = cgrad(),
        # color = cgrad(:rainbow),        
        # color = cgrad(:cividis),
        # log10.(abs.(A)),
        A,
        # clims = (0,25),
        # colorbar_title = L"$log_{10}(\sigma^2_I)$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        # title = L"$log_{10}(\sigma_I)$",
        title = titulo,
        # legendtitle = "kR = $kR",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho \lambda^3 $")

    
        
    elseif Escala_de_Densidade == "LOG"

        gr()
        theme(:vibrant)

        # x_a = Dados_Resultantes[ : ,1]
        # y_b = Dados_Resultantes[ : ,2]
        # z_c = Dados_Resultantes[ : ,3]

        # tamanho = 600
        # scatter(x_a,y_b,size = (tamanho+100, tamanho),gridalpha=0,ylims=(0,1),xlims=(-10,10),ms=10,zcolor=z_c,framestyle=:box,label="",color=palette(:turbo))

        A = zeros(N_div_Densidade,N_div_Deturn)

        contador_1 = 1
        contador_2 = 1

        while true

            A[contador_1, : ] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
            contador_2 = contador_2 + N_div_Deturn


            if contador_1 == N_div_Densidade
                break
            else
                contador_1 = contador_1 + 1
            end
        end

        y = get_points_in_log_scale(ρ_Inicial,ρ_Final,N_div_Densidade) 
        x = Dados_Resultantes[1:N_div_Deturn,1]


        heatmap(x, y, 
        size = (tamanho + 200, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 10Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        color = cgrad(:tempo,[0.7,0.75]),
        # color = cgrad(:cividis),
        # color = cgrad([:yellow,:yellow1,:yellow2,:yellow3,:gold,:darkgoldenrod2,:orange,:darkorange1,:green4,:deepskyblue3,:royalblue3,:darkblue], [0.02,0.023,0.03,0.04,0.045,0.1,0.2,0.33,0.41,0.47,0.5,0.51,0.6,0.7]), 
        log10.(A),
        title =  L"$log_{10}(\sigma_I)$",
        # legendtitle = "kR = $kR",
        titlelocation = :right,
        # colorbar_title = L"$\sigma^2_I$",
        # title = L"\textrm{Diagrama de Fase da Variância}",
        framestyle = :box,
        yaxis = :log,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 25,
        tickfontsize = 15,
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
        )
        xlabel!(L"$\Delta / \Gamma_0$")
        ylabel!(L"$\rho \lambda^3 $")
    end

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_Transmission_and_b_especific_angle(Δ_b,Intensidade,Ls,A_TI,A_TF,Escala,Geometria,N_pontos,N_Realizações,tamanho)
    Transmission = zeros(size(Ls,1),N_pontos)
    N_rep = size(Ls,1)
    for j in 1:N_rep
        for i in 1:N_pontos
            Transmission[j,i] = get_Tramission(Intensidade[j,i,:,:],N_Sensores,N_Realizações,A_TI,A_TF,Geometria)
        end
    end

    theme(:vibrant)

    Lei_de_Beer(x) =  exp(-x)
    Lei_de_Ohm(x) =  (1.7104)/(x + 1.4208)
    Lei_de_b(x) = 1/x
    
    # Lei_de_Ohm(y) = quadgk(x -> (1-exp(-x))/x ,0,y,rtol=1e-16)[1]
    # Lei_de_Ohm(y) = y*(1 + quadgk(x -> ((-y)^x)/((1+x)*factorial(1+x)) ,1,Inf,rtol=1e-16)[1] )
    # Lei_de_Ohm(y) = y*(1 + sum() ((-y)^x)/((1+x)*factorial(1+x)) ,1,Inf,rtol=1e-16)[1] )

    # Lei_de_Ohm(0.01)

    if Escala == "LINEAR"

        N_rep = size(Ls,1)
        Ls_Instataneo = Ls[1]
        scatter(Δ_b,Transmission[1,:],
        size = (tamanho, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        # xlims = (0,5),
        # xscale = :log10,
        # ylims = (10^-2,10^0.5),
        minorgridstyle = :dashdot,
        m = :star4,
        ms = 10,
        c = :green,
        lab = L"$ kL = %$Ls_Instataneo $" ,
        title = L"Radius = %$Lₐ, \theta = [%$A_TI,%$A_TF]",
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        for i in 2:N_rep
            Ls_Instataneo = Ls[i]

            scatter!(Δ_b,Transmission[i,:],
            size = (tamanho, tamanho/2),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 10Plots.mm,
            bottom_margin = 10Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            yscale = :log10,
            # xscale = :log10,
            # ylims = (10^-1,10^0.5),
            # xlims = (0.01,100),
            minorgridstyle = :dashdot,
            m = :star4,
            ms = 10,
            lab = L"$ kL = %$Ls_Instataneo $" ,
            title = L"Radius = %$Lₐ, \theta = [%$A_TI,%$A_TF]",
            framestyle = :box,
            legend =:bottomleft,
            legendfontsize = 15,
            labelfontsize = 20,
            titlefontsize = 25,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
    
            )
        end

        x = range(0.01, 10, length=1000)
        y = Lei_de_Ohm.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"$(1 + \xi)/(b + 2\xi) $"
        )

        y = Lei_de_b.(x)
        plot!(x,y,
        lw = 4,
        color = :red,
        linestyle = :dash,
        lab = L"$1/b_0$"
        )


   
    elseif Escala == "LOG"

        N_rep = size(Ls,1)
        Ls_Instataneo = Ls[1]
        scatter(Δ_b,Transmission[1,:]/maximum(Transmission[1,:]),
        size = (tamanho, tamanho/2),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 10Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        xlims = (1,30),
        xscale = :log10,
        ylims = (10^-1,10^0.5),
        minorgridstyle = :dashdot,
        m = :star4,
        ms = 10,
        c = :green,
        lab = L"$ kL = %$Ls_Instataneo $" ,
        title = L"Radius = %$Lₐ, \theta = [%$A_TI,%$A_TF]",
        framestyle = :box,
        legendfontsize = 12,
        labelfontsize = 20,
        titlefontsize = 25,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )

        for i in 2:N_rep
            Ls_Instataneo = Ls[i]

            scatter!(Δ_b,Transmission[i,:]/maximum(Transmission[i,:]),
            size = (tamanho, tamanho/2),
            left_margin = 10Plots.mm,
            right_margin = 12Plots.mm,
            top_margin = 10Plots.mm,
            bottom_margin = 10Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            yscale = :log10,
            xscale = :log10,
            ylims = (10^-1,10^0.1),
            xlims = (1,30),
            minorgridstyle = :dashdot,
            m = :star4,
            ms = 10,
            lab = L"$ kL = %$Ls_Instataneo $" ,
            title = L"Radius = %$Lₐ, \theta = [%$A_TI,%$A_TF]",
            framestyle = :box,
            legend =:bottomleft,
            # legend =:topright,
            legendfontsize = 12,
            labelfontsize = 20,
            titlefontsize = 25,
            tickfontsize = 15, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
    
            )
        end

        x = range(0.01, 100, length=1000)
        y = Lei_de_Ohm.(x)
        plot!(x,y,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"$(1 + \xi)/(b + 2\xi) $"
        )
    
        y = Lei_de_b.(x)
        plot!(x,y,
        lw = 4,
        color = :red,
        linestyle = :dash,
        lab = L"$1/b_0$"
        )
    
        # y = Lei_de_Beer.(x)
        # plot!(x,y,
        # lw = 4,
        # color = :green,
        # linestyle = :dash,
        # lab = L"$Beer-Lambert$"
        # )
        
    end



    xlabel!(L"$ b_{0}$")
    ylabel!(L"$T_{diff}/T_max$")

end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function vizualizar_degrees_of_Beer(Intensidade_Resultante_PURA,b₀,N_Realizações,angulo_intervalo,titulo,tamanho)

    Intensidade_media_espalhada = zeros(N_Sensores)

    for i in 1:N_Sensores
        fator_soma = 0
        aux_1 = 0
        for c in 1:N_Realizações
            fator_soma += Intensidade_Resultante_PURA[1,i + aux_1]
            aux_1 += N_Sensores
        end
        Intensidade_media_espalhada[i] = fator_soma/N_Realizações
    end

    Intensidade_media_incidente = zeros(N_Sensores)

    for i in 1:N_Sensores
        fator_soma = 0
        aux_1 = 0
        for c in 1:N_Realizações
            fator_soma += Intensidade_Resultante_PURA[2,i + aux_1]
            aux_1 += N_Sensores
        end
        Intensidade_media_incidente[i] = fator_soma/N_Realizações
    end

    ϕ = round.(Intensidade_Resultante_PURA[3,1:N_Sensores])
    θ = round.(Intensidade_Resultante_PURA[4,1:N_Sensores])
    θ[findall((isnan.(θ)))] .= 360

    beer_relation = zeros(2,angulo_intervalo)
    angulos = (1:1:angulo_intervalo)*(1/180)
    angulo_base = 6

    for i in 1:angulo_intervalo

        index_sensor_ang_fixo_maximo = findall(  (ϕ[ : ] .== i).*(θ[ : ] .> (360 - angulo_base) ))
        index_sensor_ang_fixo_minimo = findall(  (ϕ[ : ] .== i).*(θ[ : ] .< (0 + angulo_base) ))

        if size(index_sensor_ang_fixo_maximo,1) >= 1 
            index_sensor_ang = index_sensor_ang_fixo_maximo 
        elseif size(index_sensor_ang_fixo_minimo,1) >= 1
            index_sensor_ang = index_sensor_ang_fixo_minimo            
        end

        Intensidade_espalhada_angulo =  Intensidade_media_espalhada[index_sensor_ang[1]]
        Intensidade_incidente_angulo =  Intensidade_media_incidente[index_sensor_ang[1]]

        beer_relation[1,i] = Intensidade_espalhada_angulo
        beer_relation[2,i] = Intensidade_incidente_angulo*exp(-b₀)

    end

    beer_relation[findall((isnan.(beer_relation)))] .= 10^(-10)
    beer_relation[findall(beer_relation .== 0)] .= 10^(-10)


    gr()
    theme(:vibrant)

    plot(angulos,beer_relation[1, : ],
    size = (tamanho, (tamanho)/2 ),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    yscale = :log10,
    # ylims = (10^-2,10^0),
    minorgridstyle = :dashdot,
    lw = 4,
    c = :green,
    lab = L"$ I_{esp} $" ,
    title = titulo,
    legend =:bottomright,
    framestyle = :box,
    legendfontsize = 16,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

    plot!(angulos,beer_relation[2, : ],
    size = (tamanho, (tamanho)/2 ),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 10Plots.mm,
    bottom_margin = 10Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    yscale = :log10,
    xlims = (0,0.5),
    minorgridstyle = :dashdot,
    lw = 4,
    c = :blue,
    lab = L"$ I_{inc}e^{-b_0}$" ,
    title = titulo,
    legend =:bottomright,
    framestyle = :box,
    legendfontsize = 16,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

    xticks!(0:0.125:0.5,[L"$0$",L"$\pi/8$",L"$\pi/4$",L"$3\pi/8$",L"$\pi/2$"])


    xlabel!(L"$ \phi $")
    ylabel!(L"$ Intensity $")

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------- Vizualização Perfil da Transmissão espalhada ------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_Perfil_da_Cintura_e_Transmissao(Sensores,Intensidades,Ns,b₀s,σ²s,Δ,Variavel_Constante,L,Lₐ,tamanho)
       
    gr()
    theme(:vibrant)

    N_sensores = size(Sensores,1)
    N_Realizações = size(Ns,1)
    angulos = zeros(N_sensores)
    Transmission = zeros(N_sensores,N_Realizações)

    for i in 1:N_sensores
        angulo_temp = get_degree_azimut(Sensores[i,:])

        if angulo_temp <= 90

            angulos[i] = angulo_temp

        else
            angulos[i] = -(360 - angulo_temp)
        end
    end

    
    for i in 1:N_Realizações
        for j in 1:N_sensores
            Transmission[j,i] = Intensidades[j,i]/Intensidades[j,end]
        end
    end
    
    σ² = round(σ²s[1],digits = 2) 
    N = Ns[1]

    plot(angulos,Transmission[ : ,1],
    ms = 0,
    size = (tamanho, (tamanho)/2),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    # linestyle=:dashdot,
    yscale = :log10,
    # ylims = (10^0,10^2),
    # c= :black,
    lw = 8,
    lab = L"$N = %$N , \sigma^2 = %$σ²$" ,
    framestyle = :box,
    legendfontsize = 15,
    labelfontsize = 20,
    titlefontsize = 20,
    tickfontsize = 15, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

    for i in 1:N_Realizações
        

        N = Ns[i]
        σ² = round(σ²s[i],digits = 2) 

        plot!(angulos,Transmission[ : ,i],
        ms = 0,
        size = (tamanho+150, (tamanho+100)/2 + 250),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        yscale = :log10,
        ylims = (10^-2,10^2),
        xlims = (-40,40),
        minorgridstyle = :dashdot,
        lw = 5,
        lab = L"$ N = %$N , \sigma^2 = %$σ²$" ,
        title = L"$L = %$L , \Delta = %$Δ ,L_a = %$Lₐ$",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 15,
        labelfontsize = 20,
        titlefontsize = 20,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black

        )        

    end


        xlabel!(L"$ \theta $")
        ylabel!(L"\textrm{Transmissão}")

         
end



function vizualizar_modulo_optical_forces(r, optical_forces,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,tamanho)
                                            
    norm_optical_forces = zeros(size(r,1))
    for i in 1:size(r,1)
        norm_optical_forces[i] = norm(optical_forces[i,:])
    end

    gr()
    theme(:vibrant)
    
    if GEOMETRIA_DA_NUVEM == "ESFERA"

        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-1.5*Radius,1.5*Radius),
        xlims = (-1.5*Radius,1.5*Radius),
        zlims = (-1.5*Radius,1.5*Radius),
        zcolor = log10.(norm_optical_forces),
        color = cgrad(),
        framestyle =:box,
        aspect_ratio =:equal,
        ms = 5,
        gridalpha = 0.4,
        camera = (45,45),
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

    
    elseif GEOMETRIA_DA_NUVEM == "CUBO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-0.7*L,0.7*L),
        xlims = (-0.7*L,0.7*L),
        zlims = (-0.7*L,0.7*L),
        camera = (45,45),
        zcolor = log10.(norm_optical_forces),
        color = cgrad(),
        aspect_ratio =:equal,
        framestyle =:box,
        ms = 6,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        Lₐs = fill(L,(100,1))
        Ls = fill(L,(100,1))

        Lₐs_r = range(-L/2,L/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)

        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        
        
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        xlims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        zlims = (-(3/4)*L,(3/4)*L),
        zcolor = log10.(norm_optical_forces),
        color = cgrad(),
        framestyle =:box,
        ms = 6,
        camera = (45,45),
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )


        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))

        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)

        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        


    elseif GEOMETRIA_DA_NUVEM == "TUBO" 
        
        Limite = sqrt(L^2 + Lₐ^2)

        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-(0.7)*Limite,(0.7)*Limite),
        xlims = (-(0.7)*Limite,(0.7)*Limite),
        zlims = (-(0.7)*Limite,(0.7)*Limite),
        camera = (45,45),
        zcolor = log10.(norm_optical_forces),
        color = cgrad(),
        framestyle =:box,
        ms = 6,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)

    end



    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")

end



function vizualizar_vetores_optical_forces(r, optical_forces,Radius,L,Lₐ,GEOMETRIA_DA_NUVEM,escala,tamanho)
                                            
    gr()
    theme(:vibrant)
    
    if GEOMETRIA_DA_NUVEM == "ESFERA"

        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-1.5*Radius,1.5*Radius),
        xlims = (-1.5*Radius,1.5*Radius),
        zlims = (-1.5*Radius,1.5*Radius),
        c = :green,
        framestyle =:box,
        aspect_ratio =:equal,
        ms = 5,
        gridalpha = 0.4,
        camera = (45,45),
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        fator_controle = 10^7
        u = fator_controle*norm.(optical_forces[:,1])
        v = fator_controle*norm.(optical_forces[:,2])
        k = fator_controle*norm.(optical_forces[:,3])

        quiver!(r[ : ,1],r[ : ,2],r[ : ,3],quiver=(u,v,k),c=:blue,lw = 2)
    
    elseif GEOMETRIA_DA_NUVEM == "CUBO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-0.7*L,0.7*L),
        xlims = (-0.7*L,0.7*L),
        zlims = (-0.7*L,0.7*L),
        camera = (45,45),
        c = :green,
        aspect_ratio =:equal,
        framestyle =:box,
        ms = 6,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        fator_controle = 10^10
        u = fator_controle*norm.(optical_forces[:,1])
        v = fator_controle*norm.(optical_forces[:,2])
        k = fator_controle*norm.(optical_forces[:,3])

        quiver!(r[ : ,1],r[ : ,2],r[ : ,3],quiver=(u,v,k),c=:blue,lw = 2)

        Lₐs = fill(L,(100,1))
        Ls = fill(L,(100,1))

        Lₐs_r = range(-L/2,L/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)

        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        
        
    elseif GEOMETRIA_DA_NUVEM == "PARALELEPIPEDO" 
        
        scatter(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        xlims = (-(3/4)*Lₐ,(3/4)*Lₐ),
        zlims = (-(3/4)*L,(3/4)*L),
        c = :green,
        framestyle =:box,
        ms = 6,
        camera = (45,45),
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
        )

        fator_controle = 10^10
        u = fator_controle*norm.(optical_forces[:,1])
        v = fator_controle*norm.(optical_forces[:,2])
        k = fator_controle*norm.(optical_forces[:,3])

        quiver!(r[ : ,1],r[ : ,2],r[ : ,3],quiver=(u,v,k),c=:blue,lw = 2)


        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))

        Lₐs_r = range(-Lₐ/2,Lₐ/2, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)

        plot!(Lₐs_r,.-Lₐs./2,.-Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,.-Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r,Lₐs./2,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,Lₐs_r,Ls./2,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs_r,-Ls./2,label = "",c=:black,lw=3)
        plot!(-Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,-Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        plot!(Lₐs./2,Lₐs./2,Ls_r,label = "",c=:black,lw=3)
        


    elseif GEOMETRIA_DA_NUVEM == "TUBO" 
        
        Limite = sqrt(L^2 + Lₐ^2)

        # fator_controle = escala
        fator_controle = 20/maximum(norm.(optical_forces))

        u = fator_controle*optical_forces[:,1]
        v = fator_controle*optical_forces[:,2]
        k = fator_controle*optical_forces[:,3]

        quiver(r[ : ,1],r[ : ,2],r[ : ,3],quiver=(u,v,k),c=:blue,lw = 3,label=L"$ Optical Forces $",marker =:utriangle)

        scatter!(r[ : ,1],r[ : ,2],r[ : ,3],
        label="",
        size = (tamanho+100, tamanho),
        left_margin = 10Plots.mm,
        right_margin = 12Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 5Plots.mm,
        ylims = (-1.5*Lₐ,1.5*Lₐ),
        xlims = (-1.5*Lₐ,1.5*Lₐ),
        zlims = (-1.5*L,1.5*L),
        camera = (45,45),
        c = :green,
        framestyle =:box,
        ms = 10,
        gridalpha = 0.8,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 30,
        tickfontsize = 15, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        lab = L"$ Atomos $"
        )


        Lₐs = fill(Lₐ,(100,1))
        Ls = fill(L,(100,1))
    
        Lₐs_r = range(-Lₐ,Lₐ, length = 100)
        Ls_r = range(-L/2,L/2, length = 100)
    
        
        curva_1(x) = sqrt((Lₐ)^2 - x^2)
        curva_2(x) = - sqrt((Lₐ)^2 - x^2)
        c_L_1 =  curva_1.(Lₐs_r)
        c_L_2 =  curva_2.(Lₐs_r)


        plot!(Lₐs_r[85:end],c_L_1[85:end],.-Ls[85:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[15:50],c_L_2[15:50],.-Ls[15:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],.-Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_1[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_1[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)

        plot!(Lₐs_r[1:50],c_L_2[1:50],Ls[1:50]./2,label = "",c=:black,lw=3)
        plot!(Lₐs_r[50:end],c_L_2[50:end],Ls[50:end]./2,label = "",c=:black,lw=3)    

        plot!(-(sqrt(2)/2)*Lₐs,-(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)
        plot!((sqrt(2)/2)*Lₐs,(sqrt(2)/2)*Lₐs,Ls_r,label = "",c=:black,lw=3)

    end



    xlabel!(L"Eixo X")
    ylabel!(L"Eixo Y")

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------- Vizualização Histograma das Condutancias ------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_PR_por_W(PR,Range,Escala,b₀s,Tipo_de_kernel,titulo,tamanho)


    N_Realizações = size(b₀s,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    index_modes_inside_range = findall(  (Range .≥ 0))



    ρ = b₀s[1]

    scatter(Range,PR[ : ,1],
    ms = 10,
    m =  :circle,
    size = (tamanho + 200, (3/4)tamanho),
    left_margin = 10Plots.mm,
    right_margin = 15Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 7Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    c = paleta_de_cores[1],
    markerstrokecolor = paleta_de_cores[1],
    legend=:topleft,
    yscale = :log10,
    xscale = :log10,
    # ylims = (0,200),
    lw = 5,
    # lab = "ρ = $ρ ",
    lab = "",
    title = L"$\rho / k^2$",
    titlelocation = :right,
    framestyle = :box,
    legendfontsize = 25,
    labelfontsize = 30,
    titlefontsize = 25,
    tickfontsize = 25, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

    if Tipo_de_kernel == "Escalar"

        # law_1_ESCALAR(x) = a_sol_plot*x + b_sol_plot
        # y = law_1_ESCALAR.(x)
        # plot!(x,y,
        # lw = 4,
        # linestyle=:dashdot,
        # c = paleta_de_cores[1],
        # lab = L"$ %$a_sol_plot L + %$b_sol_plot $"
        # # lab = ""
        # )

    elseif Tipo_de_kernel == "Vetorial"

        # law_1_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot
        # y = law_1_VETORIAL.(x)
        # plot!(x,y,
        # lw = 4,
        # linestyle=:dashdot,
        # c = paleta_de_cores[1],
        # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
        # # lab = ""
        # )

    end

    if N_Realizações > 1

        for i in 2:N_Realizações

            ρ = b₀s[i]

            
            scatter!(Range,PR[ : ,i],
            ms = 10,
            m =  :circle,
            size = (tamanho + 200, (3/4)tamanho),
            left_margin = 10Plots.mm,
            right_margin = 15Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 7Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            minorgridstyle = :dashdot,
            legend=:topleft,
            yscale = :log10,
            xscale = :log10,
            # xlims = (60,100),
            c = paleta_de_cores[i],
            markerstrokecolor = paleta_de_cores[i],
            lw = 5,
            # lab = "ρ = $ρ ",
            lab = "",
            title = titulo,
            # title = L"$\rho / k^2$",
            titlelocation = :right,
            framestyle = :box,
            legendfontsize = 25,
            labelfontsize = 30,
            titlefontsize = 20,
            tickfontsize = 25, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
        
            )

            if Tipo_de_kernel == "Escalar"

                # law_n_ESCALAR(x) = a_sol_plot*x + b_sol_plot 
                # y = law_n_ESCALAR.(x)
                # plot!(x,y,
                # lw = 4,
                # linestyle=:dashdot,
                # c = paleta_de_cores[i],
                # lab = L"$ %$a_sol_plot x + %$b_sol_plot $"
                # # lab = ""
                # )

            elseif Tipo_de_kernel == "Vetorial"

                # law_n_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot 
                # y = law_n_VETORIAL.(x)
                # plot!(x,y,
                # lw = 4,
                # linestyle=:dashdot,
                # c = paleta_de_cores[i],
                # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
                # # lab = ""

                # )

            end
        
        end



    end



    scatter!([0.01,1],[maximum(PR[ : ,end]),maximum(PR[ : ,end])],
    ms = 0,
    zcolor = [b₀s[1],b₀s[end]] ,
    lab = "",
    c = cgrad(paleta_de_cores,scale = :log)
    )

    xlabel!(L"$W / b_0$")
    ylabel!(L"$ PR $")

end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------- Vizualização Histograma das Condutancias ------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function vizualizar_PR_por_W_transition(PR,Range,Escala,b₀s,Tipo_de_kernel,Radius,titulo,tamanho)


    N_Realizações = size(b₀s,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    index_modes_inside_range = findall(  (Range .≥ 0))

    PR_especial = similar(PR)
    for i in 1:N_Realizações
        N_Instantaneo = round(Int64,(π*Radius*b₀s[i])/16)
        for j in 1:size(PR_medio_superradiante,1)     
            PR_especial[j,i] = (PR[j,i] - 1)/N_Instantaneo
        end
    end

    ρ = b₀s[1]

    scatter(Range,PR_especial[ : ,1],
    ms = 10,
    m =  :circle,
    size = (tamanho + 200, (3/4)tamanho),
    left_margin = 10Plots.mm,
    right_margin = 15Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 7Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    c = paleta_de_cores[1],
    markerstrokecolor = paleta_de_cores[1],
    legend=:topleft,
    yscale = :log10,
    xscale = :log10,
    # ylims = (0,200),
    lw = 5,
    # lab = "ρ = $ρ ",
    lab = "",
    title = L"$\rho / k^2$",
    titlelocation = :right,
    framestyle = :box,
    legendfontsize = 25,
    labelfontsize = 30,
    titlefontsize = 25,
    tickfontsize = 25, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )

    if Tipo_de_kernel == "Escalar"

        # law_1_ESCALAR(x) = a_sol_plot*x + b_sol_plot
        # y = law_1_ESCALAR.(x)
        # plot!(x,y,
        # lw = 4,
        # linestyle=:dashdot,
        # c = paleta_de_cores[1],
        # lab = L"$ %$a_sol_plot L + %$b_sol_plot $"
        # # lab = ""
        # )

    elseif Tipo_de_kernel == "Vetorial"

        # law_1_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot
        # y = law_1_VETORIAL.(x)
        # plot!(x,y,
        # lw = 4,
        # linestyle=:dashdot,
        # c = paleta_de_cores[1],
        # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
        # # lab = ""
        # )

    end

    if N_Realizações > 1

        for i in 2:N_Realizações

            ρ = b₀s[i]

            
            scatter!(Range,PR_especial[ : ,i],
            ms = 10,
            m =  :circle,
            size = (tamanho + 200, (3/4)tamanho),
            left_margin = 10Plots.mm,
            right_margin = 15Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 7Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            minorgridstyle = :dashdot,
            legend=:topleft,
            yscale = :log10,
            xscale = :log10,
            ylims = (10^-4,10^-0),
            c = paleta_de_cores[i],
            markerstrokecolor = paleta_de_cores[i],
            lw = 5,
            # lab = "ρ = $ρ ",
            lab = "",
            title = titulo,
            # title = L"$\rho / k^2$",
            titlelocation = :right,
            framestyle = :box,
            legendfontsize = 25,
            labelfontsize = 30,
            titlefontsize = 20,
            tickfontsize = 25, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
        
            )

            if Tipo_de_kernel == "Escalar"

                # law_n_ESCALAR(x) = a_sol_plot*x + b_sol_plot 
                # y = law_n_ESCALAR.(x)
                # plot!(x,y,
                # lw = 4,
                # linestyle=:dashdot,
                # c = paleta_de_cores[i],
                # lab = L"$ %$a_sol_plot x + %$b_sol_plot $"
                # # lab = ""
                # )

            elseif Tipo_de_kernel == "Vetorial"

                # law_n_VETORIAL(x) = a_sol_plot*(x^2) + b_sol_plot 
                # y = law_n_VETORIAL.(x)
                # plot!(x,y,
                # lw = 4,
                # linestyle=:dashdot,
                # c = paleta_de_cores[i],
                # lab = L"$ %$a_sol_plot L^2 + %$b_sol_plot $"
                # # lab = ""

                # )

            end
        
        end



    end



    scatter!([0.01,1],[maximum(PR_especial[ : ,end]),maximum(PR_especial[ : ,end])],
    ms = 0,
    zcolor = [b₀s[1],b₀s[end]] ,
    lab = "",
    c = cgrad(paleta_de_cores,scale = :log)
    )

    xlabel!(L"$W / b_0$")
    ylabel!(L"$ (PR-1)/N $")

end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase da ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_Diagrama_de_Fase_ξ(Dados_Resultantes,N_div_Densidade,N_div_Deturn,ρ_Inicial,ρ_Final,Δ_Inicial,Δ_Final,titulo,tamanho)

    gr()
    theme(:vibrant)

    index_valid = findall(Dados_Resultantes[ : ,3] .≠ 0 )

    x_a = Dados_Resultantes[ index_valid ,1]
    y_b = Dados_Resultantes[ index_valid ,2]
    z_c = log10.(abs.(Dados_Resultantes[ index_valid ,3]))

    scatter(x_a,y_b,
        size = (1200, 600),
        left_margin = 10Plots.mm,
        right_margin = 5Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 10Plots.mm,
        gridalpha=0,
        xlims = (-0.05,4.9),
        ylims = (0.1,1),
        ms=12,
        clims=(-1,4),
        m =:square,
        zcolor=z_c,
        label="",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 25,
        titlefontsize = 20,
        tickfontsize = 15, 
        legendtitlefontsize = 20,
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black,
        tick_direction = :out 
    )
    xlabel!(L"$\Delta / \Gamma_0$")
    ylabel!(L"$\rho / k^2$")

    

    # Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-June-25}_Dados_Antigos_CLUSTER_C6.jld2", "SAIDA")
    # Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/TEMPORARIO_C16.jld2", "SAIDA")
    Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C16/DATA={2022-August-27}_Dados_Antigos_CLUSTER_C16.jld2", "SAIDA")
    

    Range_ρ = Dados_Antigos[1][1:end-1]
    Δs = Dados_Antigos[2][1:end-1]

    Range_ρ = Range_ρ[findall(iszero.(Δs) .== false)]
    Δs = Δs[findall(iszero.(Δs) .== false)]

    itp = LinearInterpolation(Range_ρ,Δs)
    Range_ρ_inter = get_points_in_log_scale(Range_ρ[1],Range_ρ[end],1000) 
    fit_interpolação = itp(Range_ρ_inter)
    plot!(fit_interpolação,Range_ρ_inter,
        label=L"Interpolation",
        lw=5,
        c=:red,
        linestyle = :dash
    )

    scatter!(Δs,Range_ρ,
        ms = 12,
        m = :star4,
        c = :green,
        label = L"Espectro",
        legend =:topright ,
        title = titulo,
    )

    # A = zeros(N_div_Densidade,N_div_Deturn)

    # contador_1 = 1
    # contador_2 = 1

    # while true

    #     A[ : ,contador_1] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
    #     contador_2 = contador_2 + N_div_Deturn


    #     if contador_1 == N_div_Densidade
    #         break
    #     else
    #         contador_1 = contador_1 + 1
    #     end
    # end

    # # while true

    # #     A[:,contador_1] = Dados_Resultantes[contador_2:(contador_2+N_div_Deturn-1),3]
    # #     contador_2 += N_div_Deturn


    # #     if contador_1 == N_div_Densidade
    # #         break
    # #     else
    # #         contador_1 += 1
    # #     end
    # # end



    # y = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade)
    # x = range(Δ_Inicial,Δ_Final,length=N_div_Deturn) 


    # heatmap(x, y, 
    # size = (tamanho + 200, tamanho/2),
    # left_margin = 10Plots.mm,
    # right_margin = 10Plots.mm,
    # top_margin = 10Plots.mm,
    # bottom_margin = 10Plots.mm,
    # # color = cgrad(:tempo,rev=:true),
    # # color = cgrad(:tempo,[0.3,0.35]),
    # # color = cgrad(:tempo,[0.05,0.15,0.25],rev=:true),
    # # color = cgrad(:PuOr_11),
    # # color = cgrad(:Paired_10),
    # # color = cgrad(:rainbow),        
    # # color = cgrad(:cividis),
    # log.(A),
    # # A,
    # # clims = (0,3),
    # # colorbar_title = L"$log_{10}(\sigma^2_I)$",
    # # title = L"\textrm{Diagrama de Fase da Variância}",
    # # title = L"$log_{10}(\sigma_I)$",
    # title = titulo,
    # # legendtitle = "kR = $kR",
    # titlelocation = :right,
    # framestyle = :box,
    # legendfontsize = 20,
    # labelfontsize = 25,
    # titlefontsize = 20,
    # tickfontsize = 15, 
    # legendtitlefontsize = 20,
    # background_color_legend = :white,
    # background_color_subplot = :white,
    # foreground_color_legend = :black,
    # tick_direction = :out 
    # )
    # xlabel!(L"$\Delta / \Gamma_0$")
    # ylabel!(L"$\rho / k^2$")


        
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase da ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_transmissão_por_L(T,Range,Escala,ρs,Tipo_de_kernel,titulo,tamanho)


    N_Realizações = size(ρs,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    
    ξs_fit = zeros(size(ρs,1))
    model_exp(x, p) = exp.(-1 .* (x./p))
    model_log(x, p) = (-1) .* (x./p)

    for i in 1:size(ρs,1)

        index_modes_inside_range = findall(  (Range .> 0).*(T[:,i] .> 10^-7 ) )

        x = Range_L[index_modes_inside_range]
        y = T[index_modes_inside_range,i]

        xdata = x
        ydata = similar(xdata)
        ydata_preciso = log.(y)

        for k in 1:size(ydata_preciso,1)
            valor_preciso = ydata_preciso[k]
            string = "$valor_preciso"
            ydata[k] = parse(Float64,string[1:end])
        end

        p0 = [0.01]

        fit = curve_fit(model_log, xdata, ydata, p0)
        ξs_fit[i] = fit.param[1]
    end

    if Escala == "LINEAR"

        ρ = ρs[1]

        plot(Range,T[ : ,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        # yscale = :log10,
        # ylims = (0,200),
        yscale = :log10,
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"$\rho / k^2$",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 30,
        titlefontsize = 25,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )
        # ξ = round.(ξs_fit[1],digits=2)
        # plot!(
        #     Range_L,
        #     model_exp(Range_L,ξs_fit[1]),
        #     lab="",
        #     # lab=L"$\xi = %$ξ $",
        #     c = paleta_de_cores[1]
        # ) 

        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                plot!(Range,T[ : ,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topright,
                yscale = :log10,
                # xscale = :log10,
                ylims = (10^-12,2),
                xticks = [0,5,10,15,20,25],
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = titulo,
                # title = L"$\rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 20,
                labelfontsize = 30,
                titlefontsize = 25,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black,
                )

                # ξ = round.(ξs_fit[i],digits=2)
                # plot!(
                #     Range_L,
                #     model_exp(Range_L,ξs_fit[i]),
                #     lab="",
                #     # lab=L"$\xi = %$ξ $",
                #     c = paleta_de_cores[i]
                # ) 
    
            end


        end

    elseif Escala == "LOG"

        model_loglog(x,p) = p[1].*x .+ p[2]
        taxa_fit = zeros(size(ρs,1),2)

        # for i in 1:size(ρs,1)

        #     index_modes_inside_range = findall(  (Range_L .< 12) )
    
        #     x = Range_L[index_modes_inside_range]
        #     y = T[index_modes_inside_range,i]
    
        #     xdata = log.(x)
        #     ydata = log.(y)
        #     p0 = [0.01,0.0]
    
        #     fit = curve_fit(model_loglog, xdata, ydata, p0)
        #     taxa_fit[i,:] = fit.param
        # end

        

        ρ = ρs[1]
    
        plot(Range,T[ : ,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        # yscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"$\rho / k^2$",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 20,
        labelfontsize = 30,
        titlefontsize = 25,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )
        α = round.(taxa_fit[1,1],digits=2)
        # plot!(
        #     Range_L,
        #     exp.(model_loglog(log.(Range_L),taxa_fit[1,:])),
        #     # lab="",
        #     lab=L"$\alpha = %$α $",
        #     c = paleta_de_cores[1]
        # ) 


        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                
                plot!(Range,T[ : ,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:bottomleft,
                yscale = :log10,
                xscale = :log10,
                ylims = (15^-2,10^0),
                xlims = (3.5,10^1.45),
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = titulo,
                # title = L"$\rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 20,
                labelfontsize = 30,
                titlefontsize = 25,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black,
                )
                α = round.(taxa_fit[i,1],digits=2)
                # plot!(
                #     Range_L,
                #     exp.(model_loglog(log.(Range_L),taxa_fit[i,:])),
                #     # lab="",
                #     lab=L"$\alpha = %$α $",
                #     c = paleta_de_cores[i]
                # ) 
            end


        end
        
        potencia_1(x) = (1/x) 
        # potencia_2(x) = 10*(1/(x^2))   
        # potencia_3(x) = 100*(1/(x^3)) 
        x = range(0.01, 10^1.5, length=100)
        y_1 = potencia_1.(x)
        # y_2 = potencia_2.(x)
        # y_3 = potencia_3.(x)

        plot!(x,y_1,
        lw = 4,
        color = :black,
        linestyle = :dash,
        lab = L"$1 / kL $"
        )
        # plot!(x,y_2,
        # lw = 4,
        # color = :red,
        # linestyle = :dash,
        # lab = L"$10 / kL^{2} $"
        # )
        # plot!(x,y_3,
        # lw = 4,
        # color = :green,
        # linestyle = :dash,
        # lab = L"$100 / kL^{3} $"
        # )



    end

    scatter!([minimum(Range),maximum(Range)],[minimum(T)-5,maximum(T)+5],
    ms = 0,
    zcolor = [log10(ρs[1]),log10(ρs[end])] ,
    lab = "",
    c = cgrad(paleta_de_cores)
    )

    xlabel!(L"$kL$")
    # ylabel!(L"$ exp (\langle log(T) \rangle )$")
    ylabel!(L"$Transmission$")
    
end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase da ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_transmissão_por_b0(T,Range,Escala,ρs,Tipo_de_kernel,Lₐ,titulo,tamanho)


    N_Realizações = size(ρs,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    
    if Escala == "LINEAR"

        b₀s = zeros(size(Range,1))
        for n in 1:size(Range,1)
            if Tipo_de_kernel == "Escalar"            
                b₀s[n] =  (4*(ρs[1]*Lₐ*Range[n]))/(Lₐ)
            elseif Tipo_de_kernel == "Vetorial"        
                b₀s[n] =  (8*(ρs[1]*Lₐ*Range[n]))/(Lₐ)
            end
        end

        ρ = ρs[1]
    
        scatter(b₀s,T[ : ,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        # yscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"$\rho / k^2$",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = 25,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

        if N_Realizações > 1

            for i in 2:N_Realizações

                b₀s = zeros(size(Range,1))
                for n in 1:size(Range,1)
                    if Tipo_de_kernel == "Escalar"            
                        b₀s[n] =  (4*(ρs[i]*Lₐ*Range[n]))/(Lₐ)
                    elseif Tipo_de_kernel == "Vetorial"        
                        b₀s[n] =  (8*(ρs[i]*Lₐ*Range[n]))/(Lₐ)
                    end
                end

                ρ = ρs[i]

                
                scatter!(b₀s,T[ : ,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                yscale = :log10,
                ylims = (minimum(T),1),
                xscale = :log10,
                xlims = (0.1,100),
                xticks = [10^-1,10^0,10^1,10^2],
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = titulo,
                # title = L"$\rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 25,
                labelfontsize = 30,
                titlefontsize = 25,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )            
            end



        end

    elseif Escala == "LOG"

        ρ = ρs[1]

        scatter(Range,T[:,1],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        xscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = 30,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )


        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                
                scatter!(Range,T[:,i],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                xscale = :log10,
                # ylims = (60,100),
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = L"$Escalar - 2D - \Delta= Inter - L_a =560 - \rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 25,
                labelfontsize = 30,
                titlefontsize = 30,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )


            end



        end

    end
    
    law_BL(x) = exp(-x)
    x = get_points_in_log_scale(0.1,200,60)
    y = law_BL.(x)
    plot!(x,y,
    lw = 4,
    linestyle=:dashdot,
    c = :black,
    label=L"\textrm{Lambert Beer}",
    legend =:bottomleft,
    legendfontsize = 15
    )

    Lei_de_Ohm(x) =  (1.7104)/(x + 1.4208)
    x = get_points_in_log_scale(0.01, 200,60)
    y = Lei_de_Ohm.(x)
    plot!(x,y,
    lw = 4,
    color = :red,
    linestyle = :dash,
    lab = L"$(1 + \xi)/(b + 2\xi) $"
    )


    scatter!([minimum(Range),maximum(Range)],[minimum(T)-5,maximum(T)+5],
    ms = 0,
    zcolor = [log10(ρs[1]),log10(ρs[end])] ,
    lab = "",
    c = cgrad(paleta_de_cores)
    )

    xlabel!(L"$b_0$")
    ylabel!(L"$ T $")
    
end




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase da ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_transmissão_por_b0(T_interrese,b₀s,Escala,ρs,tamanho_titulo,titulo,tamanho)


    N_Realizações = size(ρs,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    
    if Escala == "LINEAR"

        ρ = ρs[1]
    
        plot(b₀s,T_interrese[ 1 , : ],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        # yscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = titulo,
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = tamanho_titulo,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )

        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                plot!(b₀s,T_interrese[ i , : ],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                # yscale = :log10,
                ylims = (0,1),
                xscale = :log10,
                xlims = (0.01,10),
                # xticks = [10^1,10^1.5,10^2],
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = titulo,
                # title = L"$\rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 25,
                labelfontsize = 30,
                titlefontsize = tamanho_titulo,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )            
            end



        end

    elseif Escala == "LOG"

        ρ = ρs[1]

        scatter(b₀s,T[1,:],
        ms = 10,
        m =  :circle,
        size = (tamanho + 200, (3/4)tamanho),
        left_margin = 10Plots.mm,
        right_margin = 15Plots.mm,
        top_margin = 5Plots.mm,
        bottom_margin = 7Plots.mm,
        gridalpha = 0,
        minorgrid = true,
        minorgridalpha = 0,
        minorgridstyle = :dashdot,
        c = paleta_de_cores[1],
        markerstrokecolor = paleta_de_cores[1],
        legend=:topleft,
        xscale = :log10,
        # ylims = (0,200),
        lw = 5,
        # lab = "ρ = $ρ ",
        lab = "",
        title = L"",
        titlelocation = :right,
        framestyle = :box,
        legendfontsize = 25,
        labelfontsize = 30,
        titlefontsize = tamanho_titulo,
        tickfontsize = 25, 
        background_color_legend = :white,
        background_color_subplot = :white,
        foreground_color_legend = :black
    
        )


        if N_Realizações > 1

            for i in 2:N_Realizações

                ρ = ρs[i]

                
                scatter!(b₀s,T[i,:],
                ms = 10,
                m =  :circle,
                size = (tamanho + 200, (3/4)tamanho),
                left_margin = 10Plots.mm,
                right_margin = 15Plots.mm,
                top_margin = 5Plots.mm,
                bottom_margin = 7Plots.mm,
                gridalpha = 0,
                minorgrid = true,
                minorgridalpha = 0,
                minorgridstyle = :dashdot,
                legend=:topleft,
                xscale = :log10,
                # ylims = (60,100),
                c = paleta_de_cores[i],
                markerstrokecolor = paleta_de_cores[i],
                lw = 5,
                # lab = "ρ = $ρ ",
                lab = "",
                title = L"$Escalar - 2D - \Delta= Inter - L_a =560 - \rho / k^2$",
                titlelocation = :right,
                framestyle = :box,
                legendfontsize = 25,
                labelfontsize = 30,
                titlefontsize = tamanho_titulo,
                tickfontsize = 25, 
                background_color_legend = :white,
                background_color_subplot = :white,
                foreground_color_legend = :black
            
                )


            end



        end

    end
    
    law_BL(x) = exp(-x)
    x = get_points_in_log_scale(0.01,10,60)
    y = law_BL.(x)
    plot!(x,y,
    lw = 4,
    linestyle=:dashdot,
    c = :black,
    label=L"\textrm{Beer-Lambert Law}",
    legend =:bottomleft,
    legendfontsize = 15
    )
    
    Lei_de_Ohm(x) =  (1.7104)/(x + 1.4208)
    x = get_points_in_log_scale(0.01, 200,60)
    y = Lei_de_Ohm.(x)
    plot!(x,y,
    lw = 4,
    color = :red,
    linestyle = :dash,
    lab = L"$(1 + \xi)/(b + 2\xi) $"
    )


    scatter!([minimum(b₀s),maximum(b₀s)],[minimum(T_interrese)-5,maximum(T_interrese)+5],
    ms = 0,
    zcolor = [log10(ρs[1]),log10(ρs[end])] ,
    lab = "",
    c = cgrad(paleta_de_cores)
    )

    xlabel!(L"$b_{\delta}$")
    ylabel!(L"$ T_{dif} $")
    
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------ Vizualização do Diagrama de Fase da ξ ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function vizualizar_β_por_L(βs_r,Range,ρs,titulo,tamanho)

    gr()
    theme(:vibrant)

    N_Realizações = size(ρs,1)
    paleta_de_cores = palette(:rainbow, N_Realizações)
    


    ρ = ρs[1]

    xdata = zeros(size(βs_r[1,1],1))
    xdata .= βs_r[1,1]

    ydata = zeros(size(βs_r[2,1],1))
    ydata .= βs_r[2,1]

    xs = xdata
    ys = log10.(ydata)

    model = Loess.loess(xs, ys, span=0.5)

    xdata_loess = range(extrema(xs)...; step = 0.1)
    ydata_loess = exp10.(Loess.predict(model, xdata_loess))


    plot(xdata_loess,ydata_loess,
    # ms = 10,
    # m =  :circle,
    size = (tamanho + 200, (3/4)tamanho),
    left_margin = 10Plots.mm,
    right_margin = 12Plots.mm,
    top_margin = 5Plots.mm,
    bottom_margin = 5Plots.mm,
    gridalpha = 0,
    minorgrid = true,
    minorgridalpha = 0,
    minorgridstyle = :dashdot,
    c = paleta_de_cores[1],
    markerstrokecolor = paleta_de_cores[1],
    legend=:topleft,
    yscale = :log10,
    # ylims = (0,200),
    lw = 5,
    # lab = "ρ = $ρ ",
    lab = "",
    title = L"$\rho / k^2$",
    titlelocation = :right,
    framestyle = :box,
    legendfontsize = 25,
    labelfontsize = 30,
    titlefontsize = 25,
    tickfontsize = 25, 
    background_color_legend = :white,
    background_color_subplot = :white,
    foreground_color_legend = :black

    )


    if N_Realizações > 1

        for i in 2:N_Realizações

            xdata = zeros(size(βs_r[1,i],1))
            xdata .= βs_r[1,i]
        
            ydata = zeros(size(βs_r[1,i],1))
            ydata .= βs_r[2,i]
        
            xs = xdata
            ys = log10.(ydata)
        
            model = Loess.loess(xs, ys, span=0.5)
        
            xdata_loess = range(extrema(xs)...; step = 0.1)
            ydata_loess = exp10.(Loess.predict(model, xdata_loess))
        
            ρ = ρs[i]

            
            plot!(xdata_loess,ydata_loess,
            # ms = 10,
            # m =  :circle,
            size = (tamanho + 200, (3/4)tamanho),
            left_margin = 10Plots.mm,
            right_margin = 15Plots.mm,
            top_margin = 5Plots.mm,
            bottom_margin = 7Plots.mm,
            gridalpha = 0,
            minorgrid = true,
            minorgridalpha = 0,
            minorgridstyle = :dashdot,
            legend=:topleft,
            yscale = :log10,
            # xscale = :log10,
            # xlims = (10^0,15),            
            ylims = (10^-8,10^-5.3),
            c = paleta_de_cores[i],
            markerstrokecolor = paleta_de_cores[i],
            lw = 5,
            # lab = "ρ = $ρ ",
            lab = "",
            title = titulo,
            # title = L"$\rho / k^2$",
            titlelocation = :right,
            framestyle = :box,
            legendfontsize = 25,
            labelfontsize = 30,
            titlefontsize = 25,
            tickfontsize = 25, 
            background_color_legend = :white,
            background_color_subplot = :white,
            foreground_color_legend = :black
        
            )            
        end



    end

    scatter!([-maximum(Range)/2,maximum(Range)/2],[-15,-4],
    ms = 0,
    zcolor = [log10(ρs[1]),log10(ρs[end])] ,
    lab = "",
    c = cgrad(paleta_de_cores),
    xlabel = L"$ kZ $", 
    ylabel = L"$ | \beta_n |^2 $"
    )

    # pot1(x) = (1/x) 
    # x = 0.1:0.1:15
    # y = pot1.(x)
    # plot!(x,y,
    # lw = 4,
    # color = :red,
    # linestyle = :dash,
    # lab = L"$  1/kZ $"
    # )

end
