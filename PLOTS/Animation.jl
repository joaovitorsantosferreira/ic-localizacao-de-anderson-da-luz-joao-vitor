#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Animar Perfil da Distribuição de Probabilidade da Intensidade -------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function animar_comportamento_da_Intensidade_Media(Parametros::R1_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number)

    anim = Animation()

    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 
       
        
        Δ = i

        entrada_rotina = R1_ENTRADA(
            Parametros.Γ₀,
            Parametros.ωₐ,
            Parametros.Ω,
            Parametros.k,
            Parametros.N,
            Parametros.Radius,
            Parametros.ρ,
            Parametros.rₘᵢₙ,
            Parametros.Angulo_da_luz_incidente,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Parametros.λ,
            Parametros.ωₗ,
            Δ,
            Parametros.Tipo_de_kernel,
            Parametros.Tipo_de_Onda,
            Parametros.N_Sensores,
            Parametros.Distancia_dos_Sensores,
            0,
            360,
            Parametros.Tipo_de_beta,
            Parametros.N_Realizações,
            Parametros.N_div,
            Parametros.Desordem_Diagonal,
            Parametros.W,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.L,
            Parametros.Lₐ,
            Parametros.PERFIL_DA_INTENSIDADE
        )
        
        Dados = ROTINA_1__Estatisticas_da_Intensidade(entrada_rotina)
        
        Intensidade_Resultante_NORMALIZADA = Dados.Intensidade_Resultante_NORMALIZADA
        Intensidade_Resultante_PURA = Dados.Intensidade_Resultante_PURA    
        vizualizar_media_da_Intentensidade_ao_longo_dos_angulos(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Parametros.N_Sensores,Parametros.N_Realizações,Δ,Parametros.ρ,1000)
        frame(anim)
    
    end

    return anim
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------- Animar Perfil da Intensidade Média ao longo dos ângulos -----------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function animar_perfil_da_probabilidade_para_diferentes_Δs(Parametros,Range_1,espaçamento,Range_2)

    anim = Animation()

    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 
       
        
        Δ = i

        entrada_rotina = R1_ENTRADA(
            Parametros.Γ₀,
            Parametros.ωₐ,
            Parametros.Ω,
            Parametros.k,
            Parametros.N,
            Parametros.Radius,
            Parametros.ρ,
            Parametros.rₘᵢₙ,
            Parametros.Angulo_da_luz_incidente,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Parametros.λ,
            Parametros.ωₗ,
            Δ,
            Parametros.Tipo_de_kernel,
            Parametros.Tipo_de_Onda,
            Parametros.N_Sensores,
            Parametros.Distancia_dos_Sensores,
            Parametros.Angulo_de_variação_da_tela_circular_1,
            Parametros.Angulo_de_variação_da_tela_circular_2,
            Parametros.Tipo_de_beta,
            Parametros.N_Realizações,
            Parametros.N_div,
            Parametros.Desordem_Diagonal,
            Parametros.W,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.L,
            Parametros.Lₐ,
            Parametros.PERFIL_DA_INTENSIDADE
        )
            
        Dados = ROTINA_1__Estatisticas_da_Intensidade(entrada_rotina)
        Intensidade_Resultante_NORMALIZADA = Dados.Intensidade_Resultante_NORMALIZADA  
        variancia = Dados.variancia
        Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],40)
        vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_especial(Histograma_LOG,variancia,Δ,1000)
        frame(anim)

    
    end

    return anim
end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Influência da Desordem Diagonal no Espectro de Autovalores -----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function animar_a_Influencia_da_Desordem_Diagonal_Espectro(Parametros,Range_1,espaçamento,Range_2)
    

    anim = Animation()
    PERFIL_DA_INTENSIDADE = "NAO"
    N_Telas = 1

    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 

        W = i

        Entrada = E1_ENTRADA(
            Parametros.Γ₀,
            Parametros.ωₐ,
            Parametros.Ω,
            Parametros.k,
            Parametros.kR,
            Parametros.N,
            Parametros.Radius,
            Parametros.ρ,
            Parametros.rₘᵢₙ,
            Parametros.Angulo_da_luz_incidente,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Parametros.λ,
            Parametros.ωₗ,
            Parametros.Δ,
            Parametros.Tipo_de_kernel,
            Parametros.Tipo_de_Onda,
            Parametros.N_Sensores,
            Parametros.Distancia_dos_Sensores,
            Parametros.Angulo_de_variação_da_tela_circular_1,
            Parametros.Angulo_de_variação_da_tela_circular_2,
            Parametros.Tipo_de_beta,
            Parametros.Desordem_Diagonal,
            W,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.L,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )

        Dados = E1_extração_de_dados_Geral(Entrada)

        propriedades_fisicas_global = Dados.propriedades_fisicas_global
        IPRs = propriedades_fisicas_global.IPRs                                                                                                                                # Indice de parcipação de cada modo
        γₙ = propriedades_fisicas_global.γₙ                                                                                                                                     # Tempo de vida de cada modo
        ωₙ = propriedades_fisicas_global.ωₙ                                                                                                                                     # Frequência de cada modo

        g = get_number_thouless(γₙ ,ωₙ ,Parametros.Δ, Parametros.Γ₀)

        vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Parametros.Δ,Parametros.Γ₀,g,Parametros.Tipo_de_kernel,Parametros.Desordem_Diagonal,W,1000)
        frame(anim)

    
    end

    return anim

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Animar Perfil da Distribuição de Probabilidade da Intensidade -------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function animar_a_evolução_da_dinamica_atomica(Parametros::E1_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number)

    anim = Animation()

    entrada_cloud = Cloud_ENTRADA(
        Parametros.N,
        Parametros.ρ,
        Parametros.k,
        Parametros.Γ₀,
        Parametros.Δ,
        Parametros.Radius,
        Parametros.rₘᵢₙ,
        Parametros.Tipo_de_kernel,
        Parametros.Desordem_Diagonal,
        Parametros.W,
        Parametros.GEOMETRIA_DA_NUVEM,
        Parametros.L,
        Parametros.Lₐ,
        Parametros.Geometria
    )  
    
    cloud = Cloud_CLEAN(entrada_cloud)
    
    b₀ = 4*Parametros.N/(Parametros.Lₐ)

    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 

        Δ = i

        Entrada = E5_ENTRADA(
            Parametros.Γ₀, 
            Parametros.Ω ,
            Parametros.k,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Δ,
            Parametros.Tipo_de_kernel,
            Parametros.Tipo_de_Onda,
            Parametros.Tipo_de_beta,
            Parametros.Desordem_Diagonal,
            cloud,
            Parametros.L,
            Parametros.Radius,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.Geometria
        )

        βₙ = E5_extração_de_dados_Dinamica_dos_Atomos(Entrada)
        
        if Parametros.GEOMETRIA_DA_NUVEM == "SLAB"
            fase = real(-Parametros.ρ/(8*Parametros.k*(2*(Δ/Parametros.Γ₀) + 1im)))*Parametros.k*Parametros.L
        elseif Parametros.GEOMETRIA_DA_NUVEM == "DISCO"
            fase = real(-Parametros.ρ/(8*Parametros.k*(2*(Δ/Parametros.Γ₀) + 1im)))*Parametros.k*Parametros.Radius
        end

    
        vizualizar_Dinamica_dos_atomos(cloud.r,βₙ,Parametros.Tipo_de_kernel,Parametros.Radius,trunc(Δ),Parametros.L,Parametros.Lₐ,Parametros.GEOMETRIA_DA_NUVEM,round((fase/π),digits = 2),trunc(b₀),1000)        
        frame(anim)

    
    end

    return anim

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------- Animar Perfil da Intensidade e da dinâmcica dos atomos -------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function animar_a_evolução_do_perfil_da_Intensidade(Parametros::R1_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number,fator_controle::Number)
    
    anim = Animation()
    Δ₀ = 0
    N_Telas = 1

    entrada_cloud = Cloud_ENTRADA(
        Parametros.N,
        Parametros.ρ,
        Parametros.k,
        Parametros.Γ₀,
        Δ₀,
        Parametros.Radius,
        Parametros.rₘᵢₙ,
        Parametros.Tipo_de_kernel,
        Parametros.Desordem_Diagonal,
        Parametros.W,
        Parametros.GEOMETRIA_DA_NUVEM,
        Parametros.L,
        Parametros.Lₐ,
        Parametros.Geometria        
    )  

    cloud = Cloud_CLEAN(entrada_cloud)
    PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"

    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Parametros.Radius,
        Parametros.N_Sensores,
        Parametros.Distancia_dos_Sensores,
        Parametros.Angulo_da_luz_incidente,
        Parametros.Angulo_de_variação_da_tela_circular_1,
        Parametros.Angulo_de_variação_da_tela_circular_2,
        Parametros.GEOMETRIA_DA_NUVEM,
        Parametros.L,
        Parametros.Lₐ,  
        PERFIL_DA_INTENSIDADE,
        N_Telas
    )
    # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
    Posição_Sensores = get_cloud_sensors(entrada_Sensores,Parametros.Geometria) 
    
    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 
        
        Δ = i

        Entrada = E6_ENTRADA(
            Parametros.Γ₀, 
            Parametros.Ω ,
            Parametros.k,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Δ,
            Parametros.Tipo_de_kernel,
            Parametros.Tipo_de_Onda,
            Parametros.Tipo_de_beta,
            Parametros.Desordem_Diagonal,
            PERFIL_DA_INTENSIDADE,
            cloud,
            Posição_Sensores,
            N_Telas,
            Parametros.L,
            Parametros.Radius,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.Geometria
        )

        All_Intensitys , βₙ = E6_extração_de_dados_Dinamica_dos_Atomos_Intensidade(Entrada)

        if Parametros.GEOMETRIA_DA_NUVEM == "SLAB"
            fase = real(- Parametros.ρ/(8*Parametros.k*(2*(Δ/Parametros.Γ₀) + 1im)))*Parametros.k*Parametros.L
        elseif Parametros.GEOMETRIA_DA_NUVEM == "DISCO"
            fase = real(-Parametros.ρ/(8*Parametros.k*(2*(Δ/Parametros.Γ₀) + 1im)))*Parametros.k*Parametros.Radius
        else
            fase = 0
        end

        vizualizar_perfil_da_Intensidade_e_dinamica_dos_atomos(
            cloud.r,
            βₙ,
            Parametros.Tipo_de_kernel,
            round(Int64,Δ), 
            All_Intensitys,
            Posição_Sensores,
            Parametros.Radius,
            Parametros.L,
            Parametros.Lₐ,    
            Parametros.GEOMETRIA_DA_NUVEM,
            PERFIL_DA_INTENSIDADE,
            round(Int64,cloud.b₀),
            fator_controle,
            round((fase/π),digits = 2),
            Parametros.Geometria,
            1000
        )

        frame(anim)
    end

    return anim

end

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------- Animar Perfil da Intensidade e da Cintura do Laser ------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



function animar_a_evolução_do_perfil_da_Cintura_do_Laser(Parametros::R1_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number,N_Telas::Integer,fator_controle::Number)

    range_N = collect(Range_1:espaçamento:Range_2)
    Ns = round(Int64,size(range_N,1))
    cinturas = zeros(Ns)

    anim = Animation()
    aux_1 = 1
    Δ₀ = 0
    PERFIL_DA_INTENSIDADE = "SIM,para obter a cintura do feixe"

    entrada_Sensores = get_cloud_sensors_ENTRADA(
        Parametros.Radius,
        Parametros.N_Sensores,
        Parametros.Distancia_dos_Sensores,
        Parametros.Angulo_da_luz_incidente,
        Parametros.Angulo_de_variação_da_tela_circular_1,
        Parametros.Angulo_de_variação_da_tela_circular_2,
        Parametros.GEOMETRIA_DA_NUVEM,
        Parametros.L,
        Parametros.Lₐ,
        PERFIL_DA_INTENSIDADE,
        N_Telas
    )
    Posição_Sensores = get_cloud_sensors(entrada_Sensores) 


    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 

        if i == 1
            N = i
        else
            N = i - 1
        end  
    
        if Parametros.GEOMETRIA_DA_NUVEM == "DISCO"
            ρ = N/(π*Parametros.Radius^2)
            rₘᵢₙ = 1/(10*sqrt(ρ))     
        elseif Parametros.GEOMETRIA_DA_NUVEM == "SLAB"
            ρ = N/(Parametros.L*Parametros.Lₐ)
            rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
        end

        Intensidade_Temp = zeros(N_Realizações,2*Parametros.N_Sensores*N_Telas)

        for n in 1:Parametros.N_Realizações

            entrada_cloud = Cloud_ENTRADA(
                N,
                ρ,
                Parametros.k,
                Parametros.Γ₀,
                Δ₀,
                Parametros.Radius,
                rₘᵢₙ,
                Parametros.Tipo_de_kernel,
                Parametros.Desordem_Diagonal,
                Parametros.W,
                Parametros.GEOMETRIA_DA_NUVEM,
                Parametros.L,
                Parametros.Lₐ
            )  
            cloud = Cloud_CLEAN(entrada_cloud)


            Entrada = E7_ENTRADA(
                Parametros.Γ₀, 
                Parametros.Ω ,
                Parametros.k,
                Parametros.vetor_de_onda,
                Parametros.ω₀,
                Δ₀,
                Parametros.Tipo_de_kernel,
                Parametros.Tipo_de_Onda,
                Parametros.Tipo_de_beta,
                Parametros.Desordem_Diagonal,
                PERFIL_DA_INTENSIDADE,
                cloud,
                Posição_Sensores,
                N_Telas,
                Parametros.L,
                Parametros.Radius,
                Parametros.GEOMETRIA_DA_NUVEM
            )

        
            All_Intensitys = E7_extração_de_dados_Cintura_e_Intensidade(Entrada)

            Intensidade_Temp[n, 1:Parametros.N_Sensores*N_Telas] = All_Intensitys[1, : ]
            Intensidade_Temp[n,(Parametros.N_Sensores*N_Telas+1):end] = All_Intensitys[3, : ]
        
        end

        Intensidade_Media = zeros(4,Parametros.N_Sensores*N_Telas)

        for j in 1:Parametros.N_Sensores*N_Telas
        
            Intensidade_Media[1,j] = mean(Intensidade_Temp[:,j])
            Intensidade_Media[3,j] = mean(Intensidade_Temp[:,j+Parametros.N_Sensores*N_Telas])
        
        end

        entrada_Cintura = get_waist_in_screens_ENTRADA(
        Posição_Sensores,
        Intensidade_Media,
        N_Telas
        )
        # Guardo os valores de Cintura para diferentes distancia ao longo da nuvem 
        σ² = get_waist_in_screens(entrada_Cintura)

        N_Pontos = size(σ²,1)
        σ²_total = zeros(2*N_Pontos,2)

        σ²_total[1:N_Pontos,1] = reverse(σ²[ : ,1])
        σ²_total[1:N_Pontos,2] = reverse(σ²[ : ,2])    
        σ²_total[(N_Pontos+1):end,1] = σ²[ : ,3]
        σ²_total[(N_Pontos+1):end,2] = σ²[ : ,4]    
            
        cinturas[aux_1] = σ²_total[1+N_Pontos,2]
        
        vizualizar_perfil_da_Intensidade_e_da_Cintura_nos_Sensores(
            σ²,
            Δ₀, 
            Intensidade_Media,
            Posição_Sensores,
            Parametros.Radius,
            Parametros.L,
            Parametros.Lₐ,
            Parametros.GEOMETRIA_DA_NUVEM,
            PERFIL_DA_INTENSIDADE,
            N_Telas,
            Parametros.Distancia_dos_Sensores,
            N,
            fator_controle,
            1000
        )

        frame(anim)
        aux_1 += 1
    end


    return anim,range_N,cinturas

end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Animar o Comportamneto de Σ² no espectro -----------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function animar_o_estudo_do_espectro(Parametros::E1_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number,N_Realizações::Integer)

    anim = Animation()

    espectro = Any[]

    if Parametros.Tipo_de_kernel == "Escalar"
        IPRs_tot = zeros(Parametros.N*N_Realizações)
    elseif Parametros.Tipo_de_kernel == "Vetorial"
        IPRs_tot = zeros(Parametros.N*3*N_Realizações)
    end
 
    for i in 1:N_Realizações
        entrada_cloud = Cloud_ENTRADA(
            Parametros.N,
            Parametros.ρ,
            Parametros.k,
            Parametros.Γ₀,
            Parametros.Δ,
            Parametros.Radius,
            Parametros.rₘᵢₙ,
            Parametros.Tipo_de_kernel,
            Parametros.Desordem_Diagonal,
            Parametros.W,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.L,
            Parametros.Lₐ,
            Parametros.Geometria
        )  
        
        cloud = Cloud_COMPLETO(entrada_cloud)
        IPRs,q,Sₑ = get_Statistics_profile_of_modes(cloud.ψ,Parametros.Tipo_de_kernel,Parametros.Geometria) 
        if Parametros.Tipo_de_kernel == "Escalar"
            IPRs_tot[((i-1)*Parametros.N+1):i*Parametros.N] = IPRs
        elseif Parametros.Tipo_de_kernel == "Vetorial"
            IPRs_tot[((i-1)*3*Parametros.N+1):3*i*Parametros.N] = IPRs
        end
        append!(espectro,cloud.λ)
    end

    γₙ = abs.(real.(espectro))
    ωₙ = imag.(espectro)


    ρ_titulo = Parametros.ρ*(2*π)^3
    L_titulo = round(Parametros.L,digits=2)
    g_tot,Σ²_tot = zeros(size(Range_1:espaçamento:Range_2,1)),zeros(size(Range_1:espaçamento:Range_2,1))
    aux = 1
    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 

        Δ = i
        g,Σ² = get_number_thouless(γₙ ,ωₙ ,Δ, Parametros.Γ₀), round(get_Σ2(γₙ ,ωₙ ,Δ, Parametros.Γ₀,N_Realizações,Parametros.Tipo_de_kernel),digits = 2)
        g_tot[aux],Σ²_tot[aux] = g,Σ²

        vizualizar_relação_entre_gamma_e_omega_IPR(ωₙ,γₙ,IPRs_tot,Δ,Γ₀,g,Σ²,Tipo_de_kernel,Desordem_Diagonal,W,b₀,L"$ \rho\lambda^3 =%$ρ_titulo,Geometria = %$Geometria,kL = %$L_titulo,kR = %$Lₐ, %$Tipo_de_kernel  - IPR $",1000)
        frame(anim)    
        aux += 1
    end

    return anim,g_tot,Σ²_tot,Range_1:espaçamento:Range_2

end



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Animar Perfil da Distribuição de Probabilidade da Intensidade -------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function animar_a_evolução_das_forças_opticas(Parametros::E11_ENTRADA,Range_1::Number,espaçamento::Number,Range_2::Number)

    anim = Animation()
    mean_force = zeros(size(Range_1:espaçamento:Range_2,1))
    aux_1 = 1
    
    ρ = Parametros.ρ
    densidade_plot = ρ*λ^3

    entrada_cloud = Cloud_ENTRADA(
        Parametros.N,
        Parametros.ρ,
        Parametros.k,
        Parametros.Γ₀,
        0,
        Parametros.Radius,
        Parametros.rₘᵢₙ,
        Parametros.Tipo_de_kernel,
        Parametros.Desordem_Diagonal,
        Parametros.W,
        Parametros.GEOMETRIA_DA_NUVEM,
        Parametros.L,
        Parametros.Lₐ,
        Parametros.Geometria
    )  
    cloud = Cloud_CLEAN(entrada_cloud)
    # Rij = Distances.pairwise(Euclidean(), cloud.r, cloud.r, dims = 1) 
    # rij = round(Rij[1,2],digits=1)

    ProgressMeter.@showprogress 1 "Preparando Animação :) ===>" for i in Range_1:espaçamento:Range_2 

        entrada_beta = get_all_beta_to_system_ENTRADA(
            cloud.G,
            i,
            Parametros.Γ₀,
            cloud.r,
            Parametros.Ω,
            Parametros.vetor_de_onda,
            Parametros.ω₀,
            Parametros.Tipo_de_Onda,
            Parametros.Tipo_de_beta,
            Parametros.k,
            Parametros.Tipo_de_kernel,
            Parametros.Desordem_Diagonal,
            Parametros.GEOMETRIA_DA_NUVEM,
            Parametros.Radius,
            Parametros.L
        )
        # Guardo a dinâmica dos atomos em uma matriz    
        βₙ = get_all_beta_to_system(entrada_beta,Parametros.Geometria)                           
        
        Fₙ_temp = get_force_in_atoms(cloud.r,Parametros.vetor_de_onda,βₙ,Parametros.Γ₀,Parametros.Ω,cloud.G)
        Fₙ_temp_modulo = zeros(size(Fₙ_temp,1))
        for i in 1:size(Fₙ_temp,1)
            Fₙ_temp_modulo[i] = norm(Fₙ_temp[i,:])  
        end
        Fₙ_temp_medio = round(mean((Fₙ_temp_modulo/mean(Fₙ_temp_modulo)).^2) - mean(Fₙ_temp_modulo/mean(Fₙ_temp_modulo))^2,digits=2)
        # Fₙ_temp_medio = round(mean((Fₙ_temp_modulo/maximum(Fₙ_temp_modulo))),digits=2)

        mean_force[aux_1] = Fₙ_temp_medio
        aux_1 += 1
        
        vizualizar_vetores_optical_forces(cloud.r, Fₙ_temp,Parametros.Radius,Parametros.L,Parametros.Lₐ,Parametros.GEOMETRIA_DA_NUVEM,10^8,1000)
        title!(L"$ \Delta_0 = %$i,\rho \lambda^3 =%$densidade_plot,\sigma (F_n) = %$Fₙ_temp_medio ,\mathbf{F_n}/\mathbf{F_{max}}  $", titlefontsize = 20)

        frame(anim)
    end

    return anim,mean_force
end






