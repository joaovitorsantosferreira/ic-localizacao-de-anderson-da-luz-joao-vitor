###############################################################################################################################################################################################################
################################################################ Programa de Controle 12 - Contempla simulações Voltadas a Transmissão da Luz #################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E9.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R10.jl")


include("CODIGO_FONTE_PC3/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Extração/E9.jl")
include("PLOTS/Plots.jl")
include("CODIGO_FONTE_PC3/CORPO/Rotinas/[5]---Transmissão/R10.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


k = 1                                                                                                                                                   # Número de Onda
ρ_normalizado = 0.1                                                                                                                                    # Densidade normalidade, ou ρ/k²
b₀ = 20

ρ = ρ_normalizado*(k^2)                                                                                                                                 # Densidade de atomos no sistema
L = b₀/(4*k*ρ)
Lₐ = 280
N = round(Int64,ρ*(k*L)^2)                                                                                                                              # Número de Atomos 
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
rₘᵢₙ = 1
ω₀ = L/20                                                                                                                                               # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial

######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  

################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Sensores = 3600                                                                                                             # Quantidade de Sensores ao redor da amostra
comprimento_de_ray = (k*(ω₀^2))/2                                                                                             # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
Distancia_dos_Sensores = 5*L + comprimento_de_ray                                                                             # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                                                     # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 360                                                                                   # Define o angulo final da tela circular de Sensores
angulo_coerente = 30                                                         


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

########################################################################### DEFINIÇÂO 5 - Escala do Eixo Y referente a Densidade ##############################################################################
Escala_de_b = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
# Escala_de_b = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################


N_Realizações = 5                                                                                                         # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
b_INICIAL = 0.01 
b_FINAL = 10
N_pontos = 10


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

entrada_rotina = R10_ENTRADA(
    Γ₀,  
    ωₐ,
    Ω,
    N,
    ρ,
    k,
    rₘᵢₙ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    angulo_coerente,
    Tipo_de_beta,
    N_pontos,
    N_Realizações,
    b_INICIAL,
    b_FINAL,
    Escala_de_b,
    Desordem_Diagonal,
    W,
    L,
    Lₐ,
    b₀
)

Dados_C12 = ROTINA_10__Estatisticas_da_Transmission(entrada_rotina)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Δ_b = Dados_C12[1]
T_DIFUSO = Dados_C12[2][:,1]
T_COERENTE = Dados_C12[2][:,2]


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

vizualizar_Transmission_and_b(Δ_b,T_COERENTE,T_DIFUSO,b₀,ρ,Escala_de_b,1000)
# savefig("Transmission_NS={$N_Sensores}__NR={$N_Realizações}_Kernel={$Tipo_de_kernel}__ρ={$ρ}___b0={$b₀}__2.png")

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C12.jld2", Dict("SAIDA" => Dados_C12,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("_Dados_Antigos.jld2", "SAIDA")
# Parametros_Antigos = load("_Dados_Antigos.jld2", "Parametros")












#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#





# bins
# aux_1 = 0

# for i in 1:(N_Sensores*N_Realizações)

#     if Intensidade_Normalizada[i] < 0.1
#         aux_1 += 1
#     end

# end
# aux_1


# vizualizar_Histograma_das_Intensidade_log(h,1000)
# savefig("Romain_227.png")






