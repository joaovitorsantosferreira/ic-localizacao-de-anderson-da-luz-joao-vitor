###############################################################################################################################################################################################################
##################################################################### Programa de Controle 1 - Contempla todas as funcionalidades do codigo ###################################################################
###############################################################################################################################################################################################################




###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E5.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_PC3/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Extração/E1.jl")
include("CODIGO_FONTE_PC3/CORPO/Extração/E5.jl")
include("PLOTS/Plots.jl")
include("PLOTS/Animation.jl") 

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

k = 1                                                                                                                                                   # Número de Onda
ρ_normalizado = 0.125                                                                                                                                      # Densidade normalidade, ou ρ/k²
ρ = ρ_normalizado*(k^2)                                                                                                                                 # Densidade de atomos no sistema


#----------------------------------------------------------------------------------------- Variaveis complementares ------------------------------------------------------------------------------------------#
Radius = 1 
L = 1
Lₐ = 280
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




if GEOMETRIA_DA_NUVEM == "DISCO"
############################################################################################# Geometria de Disco #############################################################################################
#----------------------------------------------------------------------------------------- Numero de atomos fixado ------------------------------------------------------------------------------------------#
N = 100                                                                                                                                                # Número de Atomos 
kR = sqrt(N/(π*ρ))                                                                                                                                      # Raio Normalidado
Radius = kR/k                                                                                                                                           # Raio do Sistema
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
ω₀ = Radius/2                                                                                                                                           # Cintura do Laser Gaussiano 
#----------------------------------------------------------------------------------------- Raio do sistema fixado -------------------------------------------------------------------------------------------#
# kR = 50                                                                                                                                                # Raio Normalidado
# Radius = kR/k                                                                                                                                          # Raio do Sistema
# N = round(Int64,ρ_normalizado*π*(kR^2))                                                                                                                # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
# ω₀ = Radius/2                                                                                                                                          # Cintura do Laser Gaussiano 


elseif GEOMETRIA_DA_NUVEM == "SLAB"
############################################################################################### Geometria de SLAB #############################################################################################
#------------------------------------------------------------------------------------------- Numero de atomos fixado -----------------------------------------------------------------------------------------#
N = 2000                                                                                                                                                # Número de Atomos 
L = (N/(ρ*Lₐ))                                                                                                                                         # Espessura da Nuvem 
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
ω₀ = Lₐ/20                                                                                                                                                # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano
#----------------------------------------------------------------------------------------- Expessura da Nuvem fixada -----------------------------------------------------------------------------------------#
# L = 10                                                                                                                                                   # Comprimento de lado da nuvem 
# N = round(Int64,ρ_normalizado*(L*k)^2)                                                                                                                   # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     # Distância minima entre os atomos  
# ω₀ = L/4                                                                                                                                                 # Cintura do Laser Gaussiano


end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 1 - Tipo de Kernel ###################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial

######################################################################################## DEFINIÇÂO 2 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"
################################################################################### DEFINIÇÂO 3 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ = 24                                                                                                                        # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

################################################################################### DEFINIÇÂO 5 - Regime das Fases da dinâmica atomica ########################################################################
# PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"                               
# PERFIL_DA_INTENSIDADE = "SIM,para obter a cintura do feixe"                               
PERFIL_DA_INTENSIDADE = "NAO"                               
###############################################################################################################################################################################################################


N_Sensores = 360         
N_Telas = 100                                                                                                    # Quantidade de Sensores ao redor da amostra
comprimento_de_ray = (k*(ω₀^2))/2                                                                                           # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
Distancia_dos_Sensores = 0                                                                                                 # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 0                                                                                   # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 360                                                                                 # Define o angulo final da tela circular de Sensores





#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 5                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Gerando e Extraindo Dados da Nuvem -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Entrada = E1_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    PERFIL_DA_INTENSIDADE,
    N_Telas
)

# @time Dados_C9 = extração_de_dados_Geral(Entrada)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:

# cloud = Dados_C9.cloud
# r = cloud.r                                                                                                                                # Posições de todos os atomos
# λ = cloud.λ                                                                                                                                # Autovalores da Matriz de Green
# ψ = cloud.ψ                                                                                                                                # Autovetores da Matriz de Green
# R_jk = cloud.R_jk                                                                                                                          # Matriz que guarda a distância entre os atomos
# G = cloud.G                                                                                                                                # Matriz de Green do sistema 
# b₀ = cloud.b₀                                                                                                                              # Densidade Otica 
# β = Dados_C9.βₙ                                                                                                                             # Matriz que guarda a dinâmica dos atomos

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Graficos e Vizualização do Sistema -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# vizualizar_distribuição_dos_atomos(r,1000)
# savefig("fig1__N={$N}__Densidade={$ρ}__K={$k}.png")


# vizualizar_Dinamica_dos_atomos(r,β,Tipo_de_kernel,Radius,Δ,L,GEOMETRIA_DA_NUVEM,1000)
# savefig("fig_Romain__N={$N}__Densidade={$ρ}__K={$k}__3.png")


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------------------------- Gif"s -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------- Influência do Deturn na Dinâmica dos Atomos  -----------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Range_1 = 0
espaçamento = 0.1
Range_2 = 10
frames = animar_a_evolução_da_dinamica_atomica(Entrada,Range_1,espaçamento,Range_2)
# gif(frames,"animação_da_dinamica_dos_atoms_43.gif",fps=10)


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C9.jld2", Dict("SAIDA" => Dados_C9,"Parametros" => Entrada))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("_Dados_Antigos.jld2", "SAIDA")
# Parametros_Antigos = load("_Dados_Antigos.jld2", "Parametros")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#





# anim = Animation()
# for i in -4:0.1:3 
#     Δ = i
#     g = get_Thouless_Number(γₙ,ωₙ,Δ,Γ₀)
#     vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,1000)
#     frame(anim)
# end
# gif(anim,"animação_Deturn_3.gif",fps=10)


# anim = Animation()
# for i in 1:1:2*N 
#     RG_Modos = i

#     propriedades_fisicas_modo_especifico = get_propriedade_to_specific_mode(cloud,propriedades_fisicas_global,Tipo_de_kernel,RG_Modo)
    
#     IPRₑ = propriedades_fisicas_modo_especifico.IPRᵦ                                                                                                                             
#     ξₑ = propriedades_fisicas_modo_especifico.ξᵦ                                                                                                                                   
#     Eₑ = propriedades_fisicas_modo_especifico.Eᵦ                                                                                                                                 
#     γₑ = propriedades_fisicas_modo_especifico.γᵦ                                                                                                                                 
#     Γₑ = propriedades_fisicas_modo_especifico.Γᵦ                                                                                                                                 
#     ωₑ = propriedades_fisicas_modo_especifico.ωᵦ 
#     Tₑ = propriedades_fisicas_modo_especifico.Tᵦ
#     Dcmₑ = propriedades_fisicas_modo_especifico.Dcmᵦ
#     Rcmₑ = propriedades_fisicas_modo_especifico.Rcmᵦ                                                                                                                             
#     ψₑ² = propriedades_fisicas_modo_especifico.ψᵦ²
    
    
#     vizualizar_o_modo_especifico_no_disco(r,ψₑ²,Radius,IPRₑ,RG_Modo,1000)    
#     frame(anim)
# end

# gif(anim,"animação_Deturn_4.gif",fps=2)


# progresso_global = 0
# progresso_global = Progress(8, 1,"Geração da Nuvem")
# etapa = Progress(8, 2,"Etapas") 



