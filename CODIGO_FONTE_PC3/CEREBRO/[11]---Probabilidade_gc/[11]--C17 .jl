###############################################################################################################################################################################################################
################################################################# Programa de Controle 2 - Contempla simulações Voltadas a Estatistica da Luz #################################################################
###############################################################################################################################################################################################################

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Adicionando Processadores ao Sistema  ----------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distributed
addprocs(7)

@everywhere begin




###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R15.jl")


include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P1.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P2.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P3.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P4.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P5.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Programas Base/P6.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Extração/E2.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/PLOTS/Plots.jl")
include("/home/pc3/Joao/CODIGOS/Versão 2.5/master/CODIGO_FONTE_PC3/CORPO/Rotinas/[9]---Estatistiscas__de__gc/R15.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


ωₐ = 1                                                                                                                  # Tempo de decaimento atomico
Γ₀ = 1                                                                                                                  # Frequencia natural dos atomos 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define a geometria da nuvem como Disco
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define a geometria da nuvem como Slab
# GEOMETRIA_DA_NUVEM = "CUBO"                                                                                                 # Define a geometria da nuvem como Cubo
# GEOMETRIA_DA_NUVEM = "ESFERA"                                                                                               # Define a geometria da nuvem como Esfera 
# GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"                                                                                       # Define a geometria da nuvem como Paralelepipedo
GEOMETRIA_DA_NUVEM = "TUBO"                                                                                                 # Define a geometria da nuvem como Tubo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Geometria = get_geometria(GEOMETRIA_DA_NUVEM)                                                                                # Define o struct que representa a dimensão e geometria da nuvem
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###############################################################################################################################################################################################################

k = 1                                                                                                                        # Número de Onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ρ = 33/λ^3                                                                                                                    # Densidade normalidade, ou ρ/k²
Lₐ = 20                                                                                                                      # Dimensão Fixa Complementar 

############################################################################################## Parametros da Nuvem #############################################################################################
#-------------------------------------------------------------------------------------------- Número de Atomos Fixo ------------------------------------------------------------------------------------------#
N = 200                                                                                                                    # Número de Átomos da Nuvem 
Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(N,ρ,Lₐ,GEOMETRIA_DA_NUVEM)
#------------------------------------------------------------------------------------------------ Dimensão Fixa ----------------------------------------------------------------------------------------------#
# L = 25                                                                                                                        # Espessura da Nuvem, tanto para o 2D, quanto para o 3D 
# Radius = 10                                                                                                                  # Raio da Nuvem, funciona apenas para a geometria de Esfera e Disco
# N,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(L,Radius,Lₐ,ρ,GEOMETRIA_DA_NUVEM)
###############################################################################################################################################################################################################




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ = 0.9                                                                                                                       # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
angulo_da_luz_incidente = 0                                                                                                  # Angulo deIncidencia do Laser
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

N_Sensores = 20*720                                                                                                               # Quantidade de Sensores ao redor da amostra
comprimento_de_ray = (k*(ω₀^2))/2                                                                                             # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
# Distancia_dos_Sensores = 10*Radius + comprimento_de_ray                                                                     # Define a distacia entre os sensores e os sistema
Distancia_dos_Sensores = 100*sqrt((L/2)^2+Lₐ^2) 
# Distancia_dos_Sensores = 10*Radius                                                                                          # Define a distacia entre os sensores e os sistema
# Distancia_dos_Sensores = 0                                                                                                    # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = 50                                                                                    # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 70                                                                                    # Define o angulo final da tela circular de Sensores
                                                             

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


N_Realizações_Intensidade = 1                                                # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
N_Realizações_gc = 1000                                                # Define o número de Nuvens geradas para acumular dados e extrair as estatisticas da luz espalhada 
N_div = 20                                                         # Define o número de divisões do histograma, esse valor pode ser alterado apos a simulação no modulo "Adaptação dos Histogramas" logo abaixo

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 5 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################

W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Variaveis_de_Entrada = R15_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    N_Realizações_Intensidade,
    N_Realizações_gc,
    N_div,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    Geometria
)

end

Dados_C17 = ROTINA_15__Estatisticas_de_gc(Variaveis_de_Entrada)



###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
gc_von_rossum = Dados_C17


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Adaptação dos Histogramas ----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Histograma_LINEAR = get_dados_histograma_LINEAR(gc_von_rossum,50)
# Histograma_LOG = get_dados_histograma_LOG(gc_von_rossum,50)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------ Caso Linear ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


var_gc = std(gc_von_rossum)
mean_gc = mean(gc_von_rossum)
median_gc = median(gc_von_rossum)

var = round(var_gc,digits = 2)
media = round(mean_gc,digits = 2)

include("PLOTS/Plots.jl")
vizualizar_Histograma_das_gc(Histograma_LINEAR,1000)
# savefig("Distri_gc_NRgc={$N_Realizações_gc}__N={$N}__ρ={$ρ}__kR={$Radius}__Δ={$Δ}.png")

include("PLOTS/Plots.jl")
vizualizar_Distribuição_de_Probabilidades_de_gc(Histograma_LINEAR,L"$ \sigma^2_{gc} = %$var -  \langle gc \rangle = %$media - \rho \lambda^3 = %$ρ - \Delta = %$Δ $",1000)
# savefig("DistriProba__NRgc={$N_Realizações_gc}__N={$N}__ρ={$ρ}__kR={$Radius}__Δ={$Δ}_LINEAR.png")
# savefig("Relario_Cientifico DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__LINEAR.png")

include("PLOTS/Plots.jl")
vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_vom_rossum(Histograma_LOG,L"$ \sigma^2_{gc} = %$var -  \langle gc \rangle = %$media - \rho \lambda^3 = %$ρ - \Delta = %$Δ $",1000)
# savefig("DistriProba__NRgc={$N_Realizações_gc}__N={$N}__ρ={$ρ}__kR={$Radius}__Δ={$Δ}_LOG.png")



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------- Caso Log --------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# vizualizar_Histograma_das_Intensidade(Histograma_LOG,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}6.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade(Histograma_LOG,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}5.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Shnerb_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)
# savefig("DistriProba__Shnerb_Law__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_desvio_19.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_especial(Histograma_LOG,variancia,Δ,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_especial_desvio_1.png")


# vizualizar_Distribuição_de_Probabilidades_das_Intensidade_com_desvio_log_Kogan_Law(Histograma_LOG,Intensidade_Resultante_NORMALIZADA,1000)
# savefig("DistriProba__N ={$N}__ρ = {$ρ}__NS={$N_Sensores}__NR={$N_Realizações}__Δ={$Δ}_GEOMETRIA_DA_NUVEM={$GEOMETRIA_DA_NUVEM}__Desvio_Kogan_1.png")




###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C2.jld2", Dict("SAIDA" => Dados_C2,"Parametros" => entrada_rotina))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Dados_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-August-9}_Dados_Antigos_C2.jld2", "SAIDA")
# Parametros_Antigos = load("RESULTADOS/DADOS ANTIGOS/C2/DATA={2021-August-9}_Dados_Antigos_C2.jld2", "Parametros")


