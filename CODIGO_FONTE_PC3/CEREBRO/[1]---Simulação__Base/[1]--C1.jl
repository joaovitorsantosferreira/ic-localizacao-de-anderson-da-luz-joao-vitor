###############################################################################################################################################################################################################
##################################################################### Programa de Controle 1 - Contempla todas as funcionalidades do codigo ###################################################################
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------ Adicionando os processadores a serem utilizados ------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################



using Distances
using LinearAlgebra
using LaTeXStrings
using Random
using Statistics
using Plots
using SpecialFunctions
using QuadGK
using PlotThemes
using StatsBase
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R1.jl")


include("CODIGO_FONTE_PC3/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Extração/E1.jl")
include("PLOTS/Plots.jl")
include("PLOTS/Animation.jl") 


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Propriedades dos atomos ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

Γ₀ = 1                                                                                                                                                   # Tempo de decaimento atomico
ωₐ = 1                                                                                                                                                   # Frequencia natural dos atomos 

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Caracteristicas da Nuvem -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
# GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################

k = 1                                                                                                                                                   # Número de Onda
ρ_normalizado = 1                                                                                                                                      # Densidade normalidade, ou ρ/k²
ρ = ρ_normalizado*(k^2)                                                                                                                                 # Densidade de atomos no sistema


#----------------------------------------------------------------------------------------- Variaveis complementares ------------------------------------------------------------------------------------------#
Radius = 1 
L = 1
Lₐ = 50
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


if GEOMETRIA_DA_NUVEM == "DISCO"
############################################################################################# Geometria de Disco #############################################################################################
#----------------------------------------------------------------------------------------- Numero de atomos fixado ------------------------------------------------------------------------------------------#
N = 1000                                                                                                                                                # Número de Atomos 
kR = sqrt(N/(π*ρ))                                                                                                                                      # Raio Normalidado
Radius = kR/k                                                                                                                                           # Raio do Sistema
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                    # Distância minima entre os atomos  
ω₀ = Radius/2                                                                                                                                           # Cintura do Laser Gaussiano 
#----------------------------------------------------------------------------------------- Raio do sistema fixado -------------------------------------------------------------------------------------------#
# kR = 10                                                                                                                                                # Raio Normalidado
# Radius = kR/k                                                                                                                                          # Raio do Sistema
# N = round(Int64,ρ_normalizado*π*(kR^2))                                                                                                                # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                   # Distância minima entre os Atomos
# ω₀ = Radius/2                                                                                                                                          # Cintura do Laser Gaussiano 


elseif GEOMETRIA_DA_NUVEM == "SLAB"
############################################################################################### Geometria de SLAB #############################################################################################
#------------------------------------------------------------------------------------------- Numero de atomos fixado -----------------------------------------------------------------------------------------#
N = 1000                                                                                                                                                   # Número de Atomos 
L = N/(Lₐ*ρ)                                                                                                                                               # Espessura da Nuvem 
rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                       # Distância minima entre os atomos  
ω₀ = Lₐ/4                                                                                                                                                  # Comprimento de lado da nuvem                                                                                                                                          # Cintura do Laser Gaussiano
#----------------------------------------------------------------------------------------- Expessura da Nuvem fixada -----------------------------------------------------------------------------------------#
# L = 15                                                                                                                                                   # Comprimento de lado da nuvem 
# N = round(Int64,ρ_normalizado*(k^2)*L*Lₐ)                                                                                                                # Número de Atomos
# rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     # Distância minima entre os atomos  
# ω₀ = Lₐ/4                                                                                                                                                # Cintura do Laser Gaussiano


end
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
# Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Δ = 0                                                                                                                        # Diferença de Frequência entre o laser e a frequencia natural dos atomos
Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
Angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
vetor_de_onda = (k*cosd(Angulo_da_luz_incidente),k*sind(Angulo_da_luz_incidente))                                            # Vetor de propagação da onda
λ = (2*π)/k                                                                                                                  # Comprimento de onda 
ωₗ = Δ + ωₐ                                                                                                                   # Frequencia do laser Incidente 


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

################################################################################### DEFINIÇÂO 5 - Regime das Fases da dinâmica atomica ########################################################################
# PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"                               
# PERFIL_DA_INTENSIDADE = "SIM,para obter a cintura do feixe"                               
PERFIL_DA_INTENSIDADE = "NAO"                               
###############################################################################################################################################################################################################


N_Sensores = 360                                                                                                             # Quantidade de Sensores ao redor da amostra
N_Telas = 100         
comprimento_de_ray = (k*(ω₀^2))/2                                                                                           # Comprimento de Rayleigh, define uma distância mínima Para efeitos oticos 
Distancia_dos_Sensores = 15                                                                                                  # Define a distacia entre os sensores e os sistema
Angulo_de_variação_da_tela_circular_1 = -90                                                                                   # Define o angulo inicial da tela circular de Sensores
Angulo_de_variação_da_tela_circular_2 = 90                                                                               # Define o angulo final da tela circular de Sensores


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 4 - Desordem Diagonal ######################################################################################|
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Gerando e Extraindo Dados da Nuvem -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

Entrada = E1_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    Radius,
    ρ,
    rₘᵢₙ,
    Angulo_da_luz_incidente,
    vetor_de_onda,
    ω₀,
    λ,
    ωₗ,
    Δ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Distancia_dos_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM,
    L,
    Lₐ,
    PERFIL_DA_INTENSIDADE,
    N_Telas
)



@time Dados_C1 = E1_extração_de_dados_Geral(Entrada)


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# Resultados:

cloud = Dados_C1.cloud
r = cloud.r                                                                                                                                # Posições de todos os atomos
λ = cloud.λ                                                                                                                                # Autovalores da Matriz de Green
ψ = cloud.ψ                                                                                                                                # Autovetores da Matriz de Green
R_jk = cloud.R_jk                                                                                                                          # Matriz que guarda a distância entre os atomos
G = cloud.G                                                                                                                                # Matriz de Green do sistema 
b₀ = cloud.b₀                                                                                                                              # Densidade Otica 

propriedades_fisicas_global = Dados_C1.propriedades_fisicas_global
IPRs = propriedades_fisicas_global.IPRs                                                                                                    # Indice de parcipação de cada modo
ξₙ = propriedades_fisicas_global.ξₙ                                                                                                         # Comprimento de localização de cada modo
Eₙ = propriedades_fisicas_global.Eₙ                                                                                                         # Erro cometido em cada comprimento de localização
γₙ = propriedades_fisicas_global.γₙ                                                                                                         # Tempo de vida de cada modo
ωₙ = propriedades_fisicas_global.ωₙ                                                                                                         # Frequência de cada modo
qₙ = propriedades_fisicas_global.q                                                                                                          # Tempo de decaimento de cada atomo
Sₑ = propriedades_fisicas_global.Sₑ                                                                                                        # Tempo de vida de cada modo  
R_cm_n = propriedades_fisicas_global.R_cm_n                                                                                                # Centro de Massa de cada modo


propriedades_fisicas_best_mode = Dados_C1.propriedades_fisicas_best_mode
IPRᵦ = propriedades_fisicas_best_mode.IPRᵦ                                                                                                 # Indice de parcipação do melhor modo
ξᵦ = propriedades_fisicas_best_mode.ξᵦ                                                                                                     # Comprimento de localização do melhor modo  
Eᵦ = propriedades_fisicas_best_mode.Eᵦ                                                                                                     # Erro cometido no comprimento de localização do melhor modo
γᵦ = propriedades_fisicas_best_mode.γᵦ                                                                                                     # Tempo de vida do melhor modo
ωᵦ = propriedades_fisicas_best_mode.ωᵦ 
qᵦ = propriedades_fisicas_best_mode.qᵦ
Sₑᵦ = propriedades_fisicas_best_mode.Sₑᵦ                                                                                                     # Tempo de decaimento do melhor modo
RG_best = propriedades_fisicas_best_mode.RG_best                                                                                           # Indexação do modo mais subradiante
Dcmᵦ = propriedades_fisicas_best_mode.Dcmᵦ
Rcmᵦ = propriedades_fisicas_best_mode.Rcmᵦ                                                                                                 # Centro massa do melhor modo
ψᵦ² = propriedades_fisicas_best_mode.ψᵦ²

βₙ = Dados_C1.βₙ                                                                                                                            # Matriz que guarda a dinâmica dos atomos

Posição_Sensores = Dados_C1.Posição_Sensores                                                                                               # Posição dos Sensores

All_Intensitys = Dados_C1.All_Intensitys                                                                                                   # Intensidade da luz expalhada nas direções definidas


if PERFIL_DA_INTENSIDADE == "NAO"

    Intensidade_Normalizada = zeros(2,N_Sensores)
    Intensidade_Normalizada[1, : ] = All_Intensitys[1, : ]/mean(All_Intensitys[1, : ])                                                         # Intensidade Normalizada
    Intensidade_Normalizada[2, : ] = All_Intensitys[2, : ]

elseif  PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada" 

    Intensidade_Normalizada = zeros(4,N_Sensores)
    Intensidade_Normalizada[1, : ] = All_Intensitys[1, : ]/mean(All_Intensitys[1, : ])                                                         # Intensidade Normalizada
    Intensidade_Normalizada[2, : ] = All_Intensitys[2, : ]
    Intensidade_Normalizada[3, : ] = All_Intensitys[3, : ]/mean(All_Intensitys[3, : ])                                                         # Intensidade Normalizada
    Intensidade_Normalizada[4, : ] = All_Intensitys[4, : ]

elseif  PERFIL_DA_INTENSIDADE == "SIM,para obter a cintura do feixe" 

    Intensidade_Normalizada = zeros(4,N_Telas*N_Sensores)
    Intensidade_Normalizada[1, : ] = All_Intensitys[1, : ]/mean(All_Intensitys[1, : ])                                                         # Intensidade Normalizada
    Intensidade_Normalizada[2, : ] = All_Intensitys[2, : ]
    Intensidade_Normalizada[3, : ] = All_Intensitys[3, : ]/mean(All_Intensitys[3, : ])                                                         # Intensidade Normalizada
    Intensidade_Normalizada[4, : ] = All_Intensitys[4, : ]

end



g = get_number_thouless(γₙ ,ωₙ ,Δ, Γ₀)                                                                        # Número de Thouless definido para a banda de valores selecionada pelo Deturn: [Δ - Γ₀/2,Δ + Γ₀/2]


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Graficos e Vizualização do Sistema -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# vizualizar_distribuição_dos_atomos(r,Radius,Lₐ,GEOMETRIA_DA_NUVEM,1000)
# savefig("fig1__N={$N}__Densidade={$ρ}__K={$k}.png")

# vizualizar_o_modo_mais_subrandiante_no_disco(r,ψᵦ²,Radius,Lₐ,GEOMETRIA_DA_NUVEM,IPRᵦ,1000)
# savefig("fig2__N={$N}__Densidade={$ρ}__K={$k}__4.png")

# vizualizar_fit_modo_mais_subradiante(Dcmᵦ,ψᵦ²,Radius,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,RG_best,ψ,N,1000)
# savefig("fig5__N={$N}__Densidade={$ρ}__K={$k}__4.png")

# vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmᵦ,ψᵦ²,Radius,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,1000)
# savefig("distribu_Intensidades__N={$N}__Densidade={$ρ}__K={$k}_8.png")

# vizualizar_o_modo_mais_subrandiante_no_disco_3D(r,ψᵦ²,1000)
# savefig("perfil_3D___1.png")

vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,1000)
# savefig("fig3__N={$N}__Densidade={$ρ}__K={$k}_18.png")

# vizualizar_relação_entre_gamma_e_comprimento_de_localização_R_cm(ξₙ,γₙ,R_cm_n,Radius,Lₐ,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel ,1000)
# savefig("fig4__N={$N}__Densidade={$ρ}__K={$k}_3.png")

# vizualizar_relação_entre_gamma_e_R_cm(γₙ,R_cm_n,Tipo_de_kernel,1000)
# savefig("fig4__8__N={$N}__Densidade={$ρ}__K={$k}_3.png")

# vizualizar_relação_entre_gamma_e_comprimento_de_localização_R²(ξₙ,γₙ,Eₙ,Radius,Lₐ,GEOMETRIA_DA_NUVEM,k,Tipo_de_kernel,1000)
# savefig("fig4__7__N={$N}__Densidade={$ρ}__K={$k}_4.png")

# vizualizar_Intensidade_nos_sensores(r, Intensidade_Normalizada,Posição_Sensores,Radius,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,4,1000)
# savefig("fig6__N={$N}__Densidade={$ρ}__N={$N}_Δ={$Δ}_Kernel={$Tipo_de_kernel}_25.png")

# vizualizar_Intensidade_ao_longo_dos_angulos(Intensidade_Normalizada,PERFIL_DA_INTENSIDADE,1000)
# savefig("distribu_Intensidades__N={$N}__Densidade={$ρ}__K={$k}_6.png")

# vizualizar_relação_entre_q_e_Sₑ(qₙ,Sₑ,IPRs,Δ,Γ₀,g,Tipo_de_kernel,Desordem_Diagonal,W,1000)
# savefig("Metodo_de_Pipek__N={$N}__Densidade={$ρ}__GEO={$GEOMETRIA_DA_NUVEM}__Kernel={$Tipo_de_kernel}__5.png")

# vizualizar_Perfil_da_Cintura_nos_sensores(r, Intensidade_Normalizada,Posição_Sensores,Radius,Lₐ,GEOMETRIA_DA_NUVEM,PERFIL_DA_INTENSIDADE,4,1000)
# savefig("Perfil_da_Cintura_do_Laser__N={$N}__Densidade={$ρ}__N={$N}_Δ={$Δ}_Kernel={$Tipo_de_kernel}_24.png")




#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------ Vizualizar Modo Especifico ---------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

# Define o modo a ser observado, deve ser selecionado um número inteiro
# Modo_de_Interresse = 20


# propriedades_fisicas_modo_especifico = get_propriedade_to_specific_mode(cloud,propriedades_fisicas_global,Tipo_de_kernel,Modo_de_Interresse)

# IPRₑ = propriedades_fisicas_modo_especifico.IPRᵦ                                                                                                                             
# Dcmₑ = propriedades_fisicas_modo_especifico.Dcmᵦ
# ψₑ² = propriedades_fisicas_modo_especifico.ψᵦ²


# vizualizar_o_modo_especifico_no_disco(cloud.r,ψₑ²,Radius,Lₐ,GEOMETRIA_DA_NUVEM,IPRₑ,Modo_de_Interresse,1000)

# vizualizar_o_modo_mais_subrandiante_no_disco_3D(cloud.r,ψₑ²,1000)

# vizualizar_fit_modo_mais_subradiante(Dcmₑ,ψₑ²,Radius,Lₐ,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,Modo_de_Interresse,cloud.ψ,N,1000)

# vizualizar_fiting_and_spatial_profile_to_one_mode(Dcmₑ,ψₑ²,Radius,L,GEOMETRIA_DA_NUVEM,Tipo_de_kernel,r,1000)


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------- Vizualizar Perfil dos Lasers ---------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# N_Colunas = 1000
# N_pontos = 1000
# Limite = 20
# Ω = 1
# Tipo_de_Onda = "Laser Gaussiano"                                                                                                  
# Tipo_de_Onda = "Onda Plana"                                                                                                     
# Tipo_de_Onda = "FONTE PONTUAL"


# vizualizar_intensidade_e_perfil_do_feixe(N_Colunas,N_pontos,Limite,Ω,Tipo_de_Onda)
# savefig("Perfil_do_Laser_pontutal_1.png")


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------------------------------- Gif"s -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------- Influência da Desordem Diagonal no Espectro de Autovalores -----------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


# Range_1 = 0
# espaçamento = 0.2
# Range_2 = 10
# frames = animar_a_Influencia_da_Desordem_Diagonal_Espectro(Entrada,Range_1,espaçamento,Range_2)
# gif(frames,"animação_do_plano_complexo__1.gif",fps=3)


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C1.jld2", Dict("SAIDA" => Dados_C1,"Parametros" => Entrada))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("_Dados_Antigos.jld2", "SAIDA")
# Parametros_Antigos = load("_Dados_Antigos.jld2", "Parametros")



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#







# G[LinearAlgebra.diagind(G)] = real.(G[LinearAlgebra.diagind(G)])
# imag.(G[LinearAlgebra.diagind(G)])

# G[LinearAlgebra.diagind(G)] = G[LinearAlgebra.diagind(G)] + 1im*ones(N) 















# anim = Animation()
# for i in -4:0.1:3 
#     Δ = i
#     g = get_Thouless_Number(γₙ,ωₙ,Δ,Γ₀)
#     vizualizar_relação_entre_gamma_e_omega(ωₙ,γₙ,IPRs,Δ,Γ₀,g,1000)
#     frame(anim)
# end
# gif(anim,"animação_Deturn_3.gif",fps=10)


# anim = Animation()
# for i in 1:1:2*N 
#     RG_Modos = i

#     propriedades_fisicas_modo_especifico = get_propriedade_to_specific_mode(cloud,propriedades_fisicas_global,Tipo_de_kernel,RG_Modo)
    
#     IPRₑ = propriedades_fisicas_modo_especifico.IPRᵦ                                                                                                                             
#     ξₑ = propriedades_fisicas_modo_especifico.ξᵦ                                                                                                                                   
#     Eₑ = propriedades_fisicas_modo_especifico.Eᵦ                                                                                                                                 
#     γₑ = propriedades_fisicas_modo_especifico.γᵦ                                                                                                                                 
#     Γₑ = propriedades_fisicas_modo_especifico.Γᵦ                                                                                                                                 
#     ωₑ = propriedades_fisicas_modo_especifico.ωᵦ 
#     Tₑ = propriedades_fisicas_modo_especifico.Tᵦ
#     Dcmₑ = propriedades_fisicas_modo_especifico.Dcmᵦ
#     Rcmₑ = propriedades_fisicas_modo_especifico.Rcmᵦ                                                                                                                             
#     ψₑ² = propriedades_fisicas_modo_especifico.ψᵦ²
    
    
#     vizualizar_o_modo_especifico_no_disco(r,ψₑ²,Radius,IPRₑ,RG_Modo,1000)    
#     frame(anim)
# end

# gif(anim,"animação_Deturn_4.gif",fps=2)


# progresso_global = 0
# progresso_global = Progress(8, 1,"Geração da Nuvem")
# etapa = Progress(8, 2,"Etapas") 

# boxplot is defined in StatsPlots
# using StatsPlots, StatsPlots.PlotMeasures
# gr(leg = false, bg = :lightgrey)

# # Create a filled contour and boxplot side by side.
# plot(contourf(randn(10, 20)), boxplot(rand(1:4, 1000), randn(1000)))

# # Add a histogram inset on the heatmap.
# # We set the (optional) position relative to bottom-right of the 1st subplot.
# # The call is `bbox(x, y, width, height, origin...)`, where numbers are treated as
# # "percent of parent".
# histogram!(
#     randn(1000),
#     inset = (1, bbox(0.05, 0.05, 0.5, 0.25, :bottom, :right)),
#     ticks = nothing,
#     subplot = 3,
#     bg_inside = nothing
# )

# # Add sticks floating in the window (inset relative to the window, as opposed to being
# # relative to a subplot)
# sticks!(
#     randn(100),
#     inset = bbox(0, -0.2, 200px, 100px, :center),
#     ticks = nothing,
#     subplot = 4
# )

# using Plots
# gr()
# layout = @layout [grid(1,2); grid(1,2); grid(1,1)]
# p = plot(layout = layout)

# plot!(p,1:10,rand(10),subplot=2)

# # first we take the second subplot from the subplot array with in 'p', cause this is the subplot 
# # we want to change, you can take an other subplot you want
# a = p.subplots[2]

# # then we take from the series_list array the first cell cause we have only one line with in the 
# # plot, if we had more then one, you need to know which line to take according to the order
# # you plotted them.
# # then we over write the 'y' values. you can over write the 'x' value by using ':x' instead of ':y'
# a.series_list[1][:y] = rand(10)

# # finally we redraw the plot 
# display(p)

# f(x,y) = x*y
# x = y = -10:10

# # a plot with an inset
# p1 = plot(x, y, f, 
#     seriestype = [:wireframe, :surface], 
#     inset_subplots = bbox(0, 0, 0.5, 0.5, :bottom),
#     title = ["Look At" "My Titles"],
#     camera = [(10, 45) (60, 0)],
#     colorbar = false)


# # a second plot
# p2 = heatmap(x, y, f, c = :viridis)

# # two subplots where one of them has an inset
# plot(p1, p2, layout = (2, 1))

# plot( 1:5 )
# plot!( -5:8, (-5:8).^2, inset = (1, bbox(0.1,0.0,0.3,0.3)), subplot = 2)

# tamanho = 1000
# A = range(0.16, 3.14, length = 20)
# B = [
# 2,
# 2.02,
# 2.05,
# 2.09,
# 2.11,
# 2.14,
# 2.19,
# 2.24,
# 2.31,
# 2.39,
# 2.48,
# 2.58,
# 2.72,
# 2.87,
# 3.06,
# 3.32,
# 3.67,
# 4.18,
# 5.22,
# 11.15
# ]
# gr()
# theme(:vibrant)


# A_total = zeros(80)
# B_total = zeros(80)

# A_total[61:80] = A.+(3*3.14)
# B_total[61:80] = reverse(B)
# B_total[41:60] = B

# x_a = A_total
# y_b = B_total


# scatter!(-x_a/pi,y_b,
# size = (tamanho, tamanho/2),
# left_margin = 10Plots.mm,
# right_margin = 12Plots.mm,
# top_margin = 10Plots.mm,
# bottom_margin = 10Plots.mm,
# c = :green,
# framestyle = :box,
# gridalpha = 0.3,
# ylims = (0,12),
# xlims = (-4,4),
# ms = 8,
# label = "",
# title = "",
# legendfontsize = 20,
# labelfontsize = 15,
# titlefontsize = 30,
# tickfontsize = 15, 
# background_color_legend = :white,
# background_color_subplot = :white,
# foreground_color_legend = :black
# )

# xlabel!(L"$\theta_0/\pi$")
# ylabel!(L"$T$")

# # savefig("Desafio_4.png")

