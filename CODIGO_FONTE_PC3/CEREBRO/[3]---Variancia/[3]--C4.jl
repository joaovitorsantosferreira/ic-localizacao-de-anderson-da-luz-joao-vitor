###############################################################################################################################################################################################################
######################################################## Programa de Controle 4 - Contempla simulações Voltadas a Variância para diferentes Densidades e Δ ####################################################
##############################################################################################################################################################################################################

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Adicionando Processadores ao Sistema  ----------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

using Distributed
addprocs(7)

@everywhere begin


###############################################################################################################################################################################################################
#----------------------------------------------------------------------------- Inicialização dos Pacotes utilizados pelo Programa ----------------------------------------------------------------------------#
###############################################################################################################################################################################################################


using Distances                                                                                                                 
using LinearAlgebra                                                                                                             
using LaTeXStrings                                                                                                               
using Random                                                                                                                    
using Statistics                                                                                                                
using Plots                                                                                                                     
using SpecialFunctions                                                                                                          
using QuadGK                                                                                                                    
using PlotThemes                                                                                                                
using StatsBase 
using LsqFit
using Optim
using ProgressMeter
using FileIO
using JLD2
using Dates


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Inclusão dos programas secundarios  ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Programas_Base/ES-P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Extratoras/ES-E2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R2.jl")
include("CODIGO_FONTE_PC3/CORPO/Estrutura_dos_Dados/Rotinas/ES-R3.jl")


include("CODIGO_FONTE_PC3/CORPO/Programas Base/P1.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P2.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P3.jl")
include("CODIGO_FONTE_PC3/CORPO/Programas Base/P4.jl")
include("CODIGO_FONTE_PC3/CORPO/Extração/E2.jl")
include("CODIGO_FONTE_PC3/CORPO/Rotinas/[2]---Variancia/R2.jl")  
include("CODIGO_FONTE_PC3/CORPO/Rotinas/[2]---Variancia/R3.jl") 
include("PLOTS/Plots.jl")



###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------- Variaveis de entrada -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------- Propriedades dos atomos -----------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


Γ₀ = 1                                                                                                                        # Tempo de decaimento atomico 
ωₐ = 1                                                                                                                        # Frequencia natural dos atomos


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------- Caracteristicas da Nuvem ------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

###################################################################################### DEFINIÇÂO 1 - Geometria do Sistema #####################################################################################
GEOMETRIA_DA_NUVEM = "DISCO"                                                                                                # Define que a analise é feita com a luz escalar
# GEOMETRIA_DA_NUVEM = "SLAB"                                                                                                 # Define que a analise é feita com a luz  vetorial
###############################################################################################################################################################################################################


N = 100                                                                                                                        # Número de atomos caso N seja fixado
kR = 1                                                                                                                        # Raio normalizado do sistema caso kR seja fixado
L = 3
Lₐ = 50

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------- Propriedades da luz incidadente --------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################################# DEFINIÇÂO 2 - Tipo de Kernel ####################################################################################
Tipo_de_kernel = "Escalar"                                                                                                 # Define que a analise é feita com a luz escalar
# Tipo_de_kernel = "Vetorial"                                                                                              # Define que a analise é feita com a luz  vetorial
######################################################################################## DEFINIÇÂO 3 - Perfil da Onda Incidadente #############################################################################
Tipo_de_Onda = "Laser Gaussiano"                                                                                                  # Define que a onda incidente tem um perfil de um Laser Gaussiano
# Tipo_de_Onda = "Onda Plana"                                                                                                     # Define que a onda incidente tem um perfil de Onda Plana  
# Tipo_de_Onda = "FONTE PONTUAL"                                                                                                  # Define que a onda incidente tem um perfil de Fonte Pontual
################################################################################### DEFINIÇÂO 4 - Regime das Fases da dinâmica atomica ########################################################################
Tipo_de_beta = "Estacionário"                             # Define o regime de analise das equações diferencias como estacionario, ou seja, a dinâmica dos atomos ao longo do tempo praticamente não é alterada  
# Tipo_de_beta = "Fases Aleatórias"                       # Estabelece que a dinâmica dos atomos é estacionaria e aleatória
###############################################################################################################################################################################################################


Ω = Γ₀/1000                                                                                                                  # Frequência de Rabi
k = 1                                                                                                                        # Número de Onda
angulo_da_luz_incidente = 0                                                                                                  # Angulo de incidadencia da luz em graus
λ = (2*π)/k                                                                                                                  # Comprimento de onda da luz incidente 
vetor_de_onda = (k*cosd(angulo_da_luz_incidente),k*sind(angulo_da_luz_incidente))                                            # Vetor de Onda da frente de onda                                            


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------- Sensores ------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

N_Sensores = 45                                                                                                              # Quantidade de Sensores ao redor da amostra
Angulo_de_variação_da_tela_circular_1 = 40                                                                                   # Define o angulo inicial da Tela Circular
Angulo_de_variação_da_tela_circular_2 = 85                                                                                   # Define o angulo final da Tela Circular


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Variaveis de Controle das realizações -----------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


############################################################################ DEFINIÇÂO 5 - Variavel constante no Diagrama de Fase #############################################################################
# Variavel_Constante = "N"                                                                      # Define que o número de atomos será a quantidade fixada nas realizações
Variavel_Constante = "kR"                                                                     # Define que o raio do sistma será a quantidade fixada nas realizações, presupondo uma geometria de DISCO
# Variavel_Constante = "L"                                                                      # Define que o as dimensões do SLAB serão a quantidade fixada nas realizações, presupondo uma geometria de SLAB 
########################################################################### DEFINIÇÂO 6 - Escala do Eixo Y referente a Densidade ##############################################################################
# Escala_de_Densidade = "LOG"                                                                                                      # Define que a escala no eixo Y será logaritimica
Escala_de_Densidade = "LINEAR"                                                                                                   # Define que a escala no eixo Y será Linear
###############################################################################################################################################################################################################



N_Realizações = 100                                                                                               # Define a quantidade de realizações por locus no diagrama de Fase
N_div_Densidade = 20                                                                                            # Define a quatidade de divisões no eixo y do Diagrama, que é a densidade normalizada do sistema
N_div_Deturn = 10                                                                                               # Define a quatidade de divisões no eixo x do Diagrama, que é o Deturn do sistema

Δ_Inicial = -5                                                                                                  # Define o Valor Inicial do Deturn
Δ_Final = 2.4                                                                                                     # Define o Valor Final do Deturn
Densidade_Inicial = 1                                                                                           # Define o Valor Inicial da Densidade
Densidade_Final = 10                                                                                             # Define o Valor Final da Densidade


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------- Desordem Diagonal -------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


######################################################################################## DEFINIÇÂO 7 - Desordem Diagonal ######################################################################################
# Desordem_Diagonal = "PRESENTE"                                                   # Define que ao gerar o sistema exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels  
Desordem_Diagonal = "AUSENTE"                                                      # Define que ao gerar o sistema não exista desordem nos termos da diagonal principal da Matriz de Green em ambos os Kernels
###############################################################################################################################################################################################################


W = 10                                                                                                                      # Amplitude de Desordem


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------- Execução dos Programas e Extração dos Dados  -------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


Variaveis_de_Entrada = R3_ENTRADA(
    Γ₀,
    ωₐ,
    Ω,
    k,
    N,
    kR,
    L,
    Lₐ,
    angulo_da_luz_incidente,
    vetor_de_onda,
    λ,
    Tipo_de_kernel,
    Tipo_de_Onda,
    N_Sensores,
    Angulo_de_variação_da_tela_circular_1,
    Angulo_de_variação_da_tela_circular_2,
    Tipo_de_beta,
    N_Realizações,
    N_div_Densidade,
    N_div_Deturn,
    Δ_Inicial,
    Δ_Final,
    Densidade_Inicial,
    Densidade_Final,
    Variavel_Constante,
    Escala_de_Densidade,
    Desordem_Diagonal,
    W,
    GEOMETRIA_DA_NUVEM
)

end

Dados_C4 = ROTINA_3__Diagrama_de_Fase_Variancia(Variaveis_de_Entrada)

###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------------ Resultados da Simulção -------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Resultados:
Histograma_Diagrama_de_Fase = Dados_C4                                                                                      # Define o histograma que contem a variancia calculada para diferentes 

###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------------------- Graficos -------------------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

vizualizar_Diagrama_de_Fase_Variancia(Histograma_Diagrama_de_Fase,N_div_Densidade,N_div_Deturn,Densidade_Inicial,Densidade_Final,Escala_de_Densidade,kR,1000)
# savefig("Diagrama_Variancia__NR={$N_Realizações}__N={$N}_norm_21.png")
# savefig("Diagrama_Variancia__NR={$N_Realizações}__kR={$kR}_15.png")
# savefig("Romain_302.png")


###############################################################################################################################################################################################################
#-------------------------------------------------------------------------------------- Gravação os Dados da Simulação  --------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################

# DATA = Dates.now()
# ano = Dates.year(DATA)
# mes = Dates.monthname(DATA)
# dia = Dates.day(DATA)

# save("DATA={$ano-$mes-$dia}_Dados_Antigos_C4.jld2", Dict("SAIDA" => Dados_C4,"Parametros" => Variaveis_de_Entrada))


###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------- Resgatar Dados de Outra SImulação -------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


# Dados_Antigos = load("_Dados_Antigos.jld2", "SAIDA")
# Parametros_Antigos = load("_Dados_Antigos.jld2", "Parametros")


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------------------------------------------------------- Testes -------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#