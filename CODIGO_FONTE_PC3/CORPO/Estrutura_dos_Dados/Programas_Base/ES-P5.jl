mutable struct E1_ENTRADA
    Γ₀::Any 
    ωₐ::Any
    Ω ::Any
    k::Any
    kR::Any
    N::Integer
    Radius::Any
    ρ::Any
    rₘᵢₙ::Any
    angulo_da_luz_incidente::Integer
    vetor_de_onda::Any
    ω₀::Any
    λ::Any
    ωₗ::Any
    Δ ::Any
    Tipo_de_kernel::String
    Tipo_de_onda::String
    N_Sensores::Integer
    Tipo_de_sensores::String
    Tipo_de_intensidade_nos_Sensores::String
    Sensores_em_ambos_sentidos::String
    Distancia_dos_Sensores::Any
    Angulo_da_tela_de_Sensores::Any
    Tamanho_da_tela::Any
    Angulo_de_variação_da_tela_circular_1::Any
    Angulo_de_variação_da_tela_circular_2::Any
    tipo_de_beta::String
end

mutable struct Cloud_Entrada

    N::Integer
    ρ::Any
    k::Any
    Γ₀::Any
    Δ::Any
    Radius::Any
    rₘᵢₙ::Any
    Tipo_de_kernel::String

end

mutable struct Cloud_Saida

    r::Array
    λ::Any
    ψ::Any 
    R_jk::Any
    G::Array
    b₀::Number

end

mutable struct Caracteristicas_dos_Modos_Entrada
    cloud::Cloud_Saida 
    Radius::Number
    Δ::Number
    Γ₀::Number
end


mutable struct Caracteristicas_dos_Modos_Saida
    IPRs::Array
    ξₙ::Array
    Eₙ::Array
    γₙ::Array
    Γₙ::Array
    ωₙ::Array
    Tₙ::Array
    R_cm_n::Array
end