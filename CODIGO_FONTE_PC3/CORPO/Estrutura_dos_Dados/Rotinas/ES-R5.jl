mutable struct R5_ENTRADA
    Γ₀::Number
    k::Number
    N::Integer
    kR::Number
    L::Number
    Lₐ::Number
    Tipo_de_kernel::String
    N_div_Densidade::Integer
    N_div_Deturn::Integer
    Densidade_Inicial::Number
    Densidade_Final::Number
    Δ_Inicial::Number
    Δ_Final::Number
    Variavel_Constante::String
    Escala_de_Densidade::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String

end

# entrada_rotina = R6_ENTRADA(
#     Γ₀,
#     k,
#     N,
#     kR,
#     Tipo_de_kernel,
#     N_div_Densidade,
#     N_div_Deturn,
#     Densidade_Inicial,
#     Densidade_Final,
#     Δ_Inicial,
#     Δ_Final,
#     Variavel_Constante,
#     Escala_de_Densidade
# )



