mutable struct R7_ENTRADA
    Γ₀::Any
    ωₐ::Number 
    k::Any
    N::Integer
    Radius::Any
    ρ::Any
    rₘᵢₙ::Any
    Tipo_de_kernel::String
    N_div_Deturn::Integer
    N_Realizações::Integer
    Δ_Inicial::Number
    Δ_Final::Number
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
end

# entrada_rotina = R8_ENTRADA(
#     Γ₀,
#     ωₐ,
#     k,
#     N,
#     Radius,
#     ρ,
#     rₘᵢₙ,
#     Tipo_de_kernel,
#     N_div_Deturn,
#     N_Realizações,
#     Δ_Inicial,
#     Δ_Final
# )
