mutable struct R10_ENTRADA
    Γ₀::Number  
    ωₐ::Number
    Ω ::Number
    N::Integer
    ρ::Number
    k::Number
    rₘᵢₙ::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    ω₀::Number
    λ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
    angulo_coerente::Number
    Tipo_de_beta::String
    N_pontos::Integer
    N_Realizações::Integer
    b_INICIAL::Number
    b_FINAL::Number
    Escala_de_b::String
    Desordem_Diagonal::String
    W::Number
    L::Number
    Lₐ::Number
    b₀::Number

end
