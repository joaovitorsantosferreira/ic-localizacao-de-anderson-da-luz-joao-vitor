mutable struct R2_ENTRADA
    Γ₀::Number  
    ωₐ::Number
    Ω ::Number
    k::Number
    N::Integer
    Radius::Number
    ρ::Number
    rₘᵢₙ::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    ω₀::Number
    λ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
    Tipo_de_beta::String
    N_div_Deturn::Integer
    N_Realizações::Integer
    Δ_Inicial::Number
    Δ_Final::Number
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
end

mutable struct R2_SAIDA
    Deturn::Any
    Variacias_SIMULADAS::Any
    Variacias_TEORICAS::Any   
end
