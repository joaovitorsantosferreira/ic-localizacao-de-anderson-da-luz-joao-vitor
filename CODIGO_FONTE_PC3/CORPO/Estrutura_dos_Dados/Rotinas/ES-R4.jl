mutable struct R4_ENTRADA
    Γ₀::Number
    k::Number
    Δ::Number
    Ns::Any
    kRs::Any
    Ls::Any
    Lₐ::Number
    Tipo_de_kernel::String
    N_div_Densidade::Integer
    Densidade_Inicial::Number
    Densidade_Final::Number
    Variavel_Constante::String
    Escala_de_Densidade::String
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
end

# entrada_rotina = R5_ENTRADA(
#     Γ₀,
#     k,
#     Δ,
#     Ns,
#     kRs,
#     Tipo_de_kernel,
#     N_div_Densidade,
#     Densidade_Inicial,
#     Densidade_Final,
#     Variavel_Constante,
#     Escala_de_Densidade
# )