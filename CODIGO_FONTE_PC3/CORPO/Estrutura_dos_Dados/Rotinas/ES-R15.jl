mutable struct R15_ENTRADA
    Γ₀::Number 
    ωₐ::Number
    Ω ::Number
    k::Number
    N::Integer
    Radius::Number
    ρ::Number
    rₘᵢₙ::Number
    Angulo_da_luz_incidente::Number
    vetor_de_onda::Any
    ω₀::Number
    λ::Number
    ωₗ::Number
    Δ::Number
    Tipo_de_kernel::String
    Tipo_de_Onda::String
    N_Sensores::Integer
    Distancia_dos_Sensores::Number
    Angulo_de_variação_da_tela_circular_1::Number
    Angulo_de_variação_da_tela_circular_2::Number
    Tipo_de_beta::String
    N_Realizações_Intensidade::Integer
    N_Realizações_gc::Integer
    N_div::Integer
    Desordem_Diagonal::String
    W::Number
    GEOMETRIA_DA_NUVEM::String
    L::Number
    Lₐ::Number
    Geometria::Dimension
end

mutable struct R1_SAIDA_PROBABILIDADE
    Intensidade_Resultante_PURA::Array
    Intensidade_Resultante_NORMALIZADA::Array
    Histograma_LINEAR::Any
    Histograma_LOG::Any
    variancia::Number
end

mutable struct R1_SAIDA_PERFIL
    Intensidade_Resultante_PURA::Array
    Intensidade_Resultante_NORMALIZADA::Array
    Posição_Sensores::Array
    r::Array
end

