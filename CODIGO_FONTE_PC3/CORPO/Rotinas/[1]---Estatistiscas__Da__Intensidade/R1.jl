###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_1__Estatisticas_da_Intensidade(Variaveis_de_entrada::R1_ENTRADA)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    N_Realizações = Variaveis_de_entrada.N_Realizações
    N_div = Variaveis_de_entrada.N_div

    Q_total = N_Sensores*N_Realizações
    
    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"


        if typeof(Variaveis_de_entrada.Geometria) <: TwoD 
            Intensidade_Resultante_PURA = zeros(2,Q_total)
        elseif typeof(Variaveis_de_entrada.Geometria) <: ThreeD 
            Intensidade_Resultante_PURA = zeros(3,Q_total)
        end

    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        Intensidade_Resultante_PURA = zeros(4,Q_total)

    else 
    
        return "NÃO PODE!!!!"
    end

    auxiliar = 0
    N_Telas = 1

    tuplasResultados = ProgressMeter.@showprogress 1 "Extraido Estatisticas da Luz===>" pmap(1:N_Realizações) do R_instantaneo

        entrada_Extratora = E2_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            Variaveis_de_entrada.ωₗ,
            Variaveis_de_entrada.Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
            N_Telas,
            Variaveis_de_entrada.Geometria
        )

        All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)                                                                                                    

        valor_retorno = All_Intensitys
    end

    for n in 1:N_Realizações

        if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

            if typeof(Variaveis_de_entrada.Geometria) <: TwoD 
                result = tuplasResultados[n]
                Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = result[1 , : ]
                Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = result[2 , : ]
                auxiliar +=  N_Sensores
    
            elseif typeof(Variaveis_de_entrada.Geometria) <: ThreeD 
                result = tuplasResultados[n]
                Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = result[1 , : ]
                Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = result[2 , : ]
                Intensidade_Resultante_PURA[3,(auxiliar + 1):(auxiliar+N_Sensores)] = result[3 , : ]
                auxiliar +=  N_Sensores
    
            end
    
        elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"
    
            result = tuplasResultados[n]
            Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = result[1 , : ]
            Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = result[2 , : ]
            Intensidade_Resultante_PURA[3,(auxiliar + 1):(auxiliar+N_Sensores)] = result[3 , : ]
            Intensidade_Resultante_PURA[4,(auxiliar + 1):(auxiliar+N_Sensores)] = result[4 , : ]
            auxiliar +=  N_Sensores

        end
    end
    


    if Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "NAO"

        # Intensidade_Resultante_NORMALIZADA = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        # Histograma_LINEAR = get_dados_histograma_LINEAR(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        # Histograma_LOG = get_dados_histograma_LOG(Intensidade_Resultante_NORMALIZADA[ : ],N_div)
        # variancia = get_variancia(Intensidade_Resultante_NORMALIZADA)
    
        # return R1_SAIDA_PROBABILIDADE(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Histograma_LINEAR,Histograma_LOG,variancia)
        return Intensidade_Resultante_PURA
        
    elseif Variaveis_de_entrada.PERFIL_DA_INTENSIDADE == "SIM,para distancia fixada"

        entrada_Sensores = get_cloud_sensors_ENTRADA(
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
            N_Telas
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_entrada.Geometria) 
    
        

        Intensidade_Resultante_NORMALIZADA = zeros(2,Q_total)
        Intensidade_Resultante_NORMALIZADA[1, : ] = Intensidade_Resultante_PURA[1 , : ]/mean(Intensidade_Resultante_PURA[1 , : ])
        Intensidade_Resultante_NORMALIZADA[2, : ] = Intensidade_Resultante_PURA[3 , : ]/mean(Intensidade_Resultante_PURA[3 , : ])

        r_exemplo = getAtoms_distribution(getAtoms_distribution_ENTRADA(Variaveis_de_entrada.N, Variaveis_de_entrada.Radius, Variaveis_de_entrada.rₘᵢₙ,Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,Variaveis_de_entrada.L,Variaveis_de_entrada.Lₐ),Variaveis_de_entrada.Geometria)                                                                   # Gera o disco e guarda as posições em r

        return R1_SAIDA_PERFIL(Intensidade_Resultante_PURA,Intensidade_Resultante_NORMALIZADA,Posição_Sensores,r_exemplo)

    end
end

function get_dados_histograma_LINEAR(Intensidade_Normalizada,N_div)

    sep = maximum(Intensidade_Normalizada)/N_div
    bins = 0:sep:(maximum(Intensidade_Normalizada))
    h = fit(Histogram, Intensidade_Normalizada,bins) 

    return h
end

function get_dados_histograma_LOG(Intensidade_Normalizada,N_div)
    a = minimum(Intensidade_Normalizada)
    b = maximum(Intensidade_Normalizada)

    bins = get_points_in_log_scale(a,b,N_div)
    h = fit(Histogram, Intensidade_Normalizada[ : ],bins)

    return h
end


function get_variancia(Intensidade_Normalizada)

    # variancia_sim = mean(Intensidade_Normalizada.^2) - mean(Intensidade_Normalizada)^2
    variancia_sim = Statistics.var(Intensidade_Normalizada,corrected=:false)

    return variancia_sim
end

function get_points_in_log_scale(Valor_Inicial,Valor_Final,N_div)
    
    a = log10(Valor_Inicial)
    b = log10(Valor_Final)

    sep = abs(b-a) / (N_div-1)

    aux_1 = 1
    pontos = zeros(N_div)

    for i in 0:N_div-1
        pontos[aux_1] = 10.0^(a + sep*i)
        aux_1 += 1
    end

    return pontos
end
