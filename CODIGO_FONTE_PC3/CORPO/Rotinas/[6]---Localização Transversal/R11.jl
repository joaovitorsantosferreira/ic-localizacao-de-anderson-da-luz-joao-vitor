#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_11__Cintura_e_L(Variaveis_de_Entrada::R11_ENTRADA,Geometria::TwoD)
    

    if Variaveis_de_Entrada.Variavel_Constante == "rmin"
        
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.rₘᵢₙs,1)
        aux_1 = 1
        ω₀ = Lₐ/10

        σ²_L² = zeros(N_pontos,N_Curvas)
        
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$(Variaveis_de_Entrada.rₘᵢₙs[aux_1])}===>" pmap(Range_L) do L_instantaneo

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                
                ρ = 1/((rₘᵢₙs[j]*10)^2)
                N = round(Int64,ρ*Lₐ*(L_instantaneo*rₘᵢₙs[j]))
                
                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        ρ,
                        rₘᵢₙs[j],
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        L_instantaneo*rₘᵢₙs[j],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for k in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    1,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    0,
                    1,
                    GEOMETRIA_DA_NUVEM,
                    L_instantaneo*rₘᵢₙs[j],
                    Variaveis_de_Entrada.Lₐ,
                    "SIM,para distancia fixada",
                    1
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]
                # σ²_L²[k,j] = σ²/((Range_L[k]*rₘᵢₙs[j])^2) 
                σ²_L²[k,j] = σ²


            end
            aux_1 += 1
        end

    elseif Variaveis_de_Entrada.Variavel_Constante == "ρ"
      
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
           
        Lₐ = Variaveis_de_Entrada.Lₐ
        ρs = Variaveis_de_Entrada.ρs
    
        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        

        N_Curvas = size(Variaveis_de_Entrada.ρs,1)

        σ²_L² = zeros(N_pontos,N_Curvas+1)
    

        for j in 1:N_Curvas 

            ρ = ρs[j]
            Δ = 0.25*log(ρ) + 0.95
            tuplasResultados = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$ρ}===>" pmap(Range_L) do L_instantaneo

                Intensidade_Resultante_PURA = Any[]
                
                # ωᵦ = Lₐ/10
                # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
                # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

                ωᵦ = Lₐ/20
                ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo/2)^2))/2)
                
                # ω₀ = Lₐ/10
                
                rₘᵢₙ = 1/(10*sqrt(ρ))
                N = round(Int64,ρ*(L_instantaneo)*Lₐ)                
                

                for i in 1:N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        1,
                        ρ,
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        0,
                        1,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        "SLAB",
                        L_instantaneo,
                        Variaveis_de_Entrada.Lₐ,
                        "SIM,para distancia fixada",
                        1,
                        Variaveis_de_Entrada.Geometria
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    append!(Intensidade_Resultante_PURA,All_Intensitys[3 , : ])

                end

                Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
            

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    1,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    0,
                    1,
                    "SLAB",
                    L_instantaneo,
                    Variaveis_de_Entrada.Lₐ,
                    "SIM,para distancia fixada",
                    1
                )
                
                Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
                
                valor_retorno = σ²
            end

            for n in 1:N_pontos
                σ²_L²[n,j] = tuplasResultados[n]
            end
        end
        
        tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_L) do L_instantaneo

            # ωᵦ = Lₐ/10
            # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
            # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

            ωᵦ = Lₐ/20
            ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo)^2))/2)

            # ω₀ = Lₐ/10

            entrada_Sensores = get_cloud_sensors_ENTRADA(
                1,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                0,
                1,
                "SLAB",
                L_instantaneo,
                Variaveis_de_Entrada.Lₐ,
                "SIM,para distancia fixada",
                1
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

            Intensidade_PURA = zeros(N_Sensores)
            for i in 1:N_Sensores
                
                entrada_campo = get_eletric_field_LG_real_ENTRADA(
                    Sensores[i,3:4],
                    Variaveis_de_Entrada.Ω,
                    ω₀,
                    Variaveis_de_Entrada.k,
                    Variaveis_de_Entrada.Δ
                )
                campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
                
                Intensidade_PURA[i] = abs(campo_laser^2) 
            end

            σ² = get_one_σ(Sensores[ : ,3:4],Intensidade_PURA,Variaveis_de_Entrada.Geometria)[2]

            valor_retorno = σ²
        end

        for n in 1:N_pontos
            σ²_L²[n,end] = tuplasResultados_caso_vazio[n]
        end

    elseif Variaveis_de_Entrada.Variavel_Constante == "L"
    
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        b₀_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        b₀_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_b₀ = get_points_in_log_scale(b₀_INICIAL,b₀_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_b₀ = range(b₀_INICIAL,b₀_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.Ls,1)
        aux_1 = 1
        ω₀ = Lₐ/10

        σ²_L² = zeros(N_pontos,N_Curvas)
        
        for j in 1:N_Curvas 
                
            ProgressMeter.@showprogress 2 "Calculo_Cintura_L, L = {$(Variaveis_de_Entrada.Ls[aux_1])}===>" pmap(Range_b₀) do b₀_instantaneo

                Intensidade_Resultante_PURA = zeros(Q_total)
                aux_2 = 0
                

                N = round(Int64,(b₀_instantaneo*Variaveis_de_Entrada.k*Variaveis_de_Entrada.Lₐ)/4)
                ρ = N/(Variaveis_de_Entrada.Ls[j]*Variaveis_de_Entrada.Lₐ)
                rₘᵢₙ = 1/(10*sqrt(ρ))
                

                for i in 1:Variaveis_de_Entrada.N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        Radius,
                        ρ,
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        Angulo_de_variação_da_tela_circular_1,
                        Angulo_de_variação_da_tela_circular_2,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        GEOMETRIA_DA_NUVEM,
                        Variaveis_de_Entrada.Ls[j],
                        Variaveis_de_Entrada.Lₐ,
                        PERFIL_DA_INTENSIDADE,
                        N_Telas
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    N_Sensores = Variaveis_de_Entrada.N_Sensores
                    Intensidade_Resultante_PURA[(aux_2 + 1):(aux_2+N_Sensores)] = All_Intensitys[3 ,1:N_Sensores]
                    aux_2 +=  N_Sensores

                end
            
                Intensidade_media = zeros(N_Sensores)

                for i in 1:N_Sensores
            
                    fator_soma = 0
                    aux_3 = 0
            
                    for k in 1:N_Realizações
            
                        fator_soma += Intensidade_Resultante_PURA[i + aux_3]
                        aux_3 += N_Sensores
            
                    end
            
                    Intensidade_media[i] = fator_soma/N_Realizações
            
                end

                
                kR = 1

                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    kR,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Angulo_de_variação_da_tela_circular_1,
                    Angulo_de_variação_da_tela_circular_2,
                    GEOMETRIA_DA_NUVEM,
                    Variaveis_de_Entrada.Ls[j],
                    Variaveis_de_Entrada.Lₐ,
                    PERFIL_DA_INTENSIDADE,
                    N_Telas
                )
                # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
                Posição_Sensores = get_cloud_sensors(entrada_Sensores) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]
                # σ²_L²[k,j] = σ²/((Variaveis_de_Entrada.Ls[j])^2) 
                σ²_L²[k,j] = σ²


            end
            aux_1 += 1
        end

    elseif Variaveis_de_Entrada.Variavel_Constante == "b₀"
    

        kR = 1
        N_Sensores = Variaveis_de_Entrada.N_Sensores    
        L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
        L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
        N_pontos = Variaveis_de_Entrada.N_pontos
        N_Realizações = Variaveis_de_Entrada.N_Realizações
        GEOMETRIA_DA_NUVEM = "SLAB"
        Radius = 1
        PERFIL_DA_INTENSIDADE = "SIM,para distancia fixada"   
        Angulo_de_variação_da_tela_circular_1 = 0 
        Angulo_de_variação_da_tela_circular_2 = 1
        Lₐ = Variaveis_de_Entrada.Lₐ
        ρs = Variaveis_de_Entrada.ρs

        Q_total = N_Sensores*N_Realizações
        N_Telas = 1

        if Variaveis_de_Entrada.Escala_do_range == "LOG"

            Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
        
        elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

            Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
        
        end
        
        
        N_Curvas = size(Variaveis_de_Entrada.b₀s,1)

        σ²_L² = zeros(N_pontos,N_Curvas+1)
        
        for j in 1:N_Curvas 
                
            b₀ = b₀s[j]

            tuplasResultados = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, b₀ = {$b₀}===>" pmap(Range_L) do L_instantaneo

                Intensidade_Resultante_PURA = Any[]
                
                ωᵦ = Lₐ/10
                find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
                ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]
                    
                if Variaveis_de_Entrada.Tipo_de_kernel == "Escalar"
                    N = round(Int64,(b₀*Variaveis_de_Entrada.k*Lₐ)/4)
                    ρ = N/(L_instantaneo*Lₐ)
                    rₘᵢₙ = 1/(10*sqrt(ρ))
                elseif Variaveis_de_Entrada.Tipo_de_kernel == "Vetorial"
                    N = round(Int64,(b₀*Variaveis_de_Entrada.k*Lₐ)/8)
                    ρ = N/(L_instantaneo*Lₐ)
                    rₘᵢₙ = 1/(10*sqrt(ρ))
                end


                
                for i in 1:N_Realizações

                    entrada_extratora = E2_ENTRADA(
                        Variaveis_de_Entrada.Γ₀,
                        Variaveis_de_Entrada.ωₐ,
                        Variaveis_de_Entrada.Ω,
                        Variaveis_de_Entrada.k,
                        N,
                        1,
                        ρ,
                        rₘᵢₙ,
                        Variaveis_de_Entrada.Angulo_da_luz_incidente,
                        Variaveis_de_Entrada.vetor_de_onda,
                        ω₀,
                        Variaveis_de_Entrada.λ,
                        Variaveis_de_Entrada.ωₗ,
                        Variaveis_de_Entrada.Δ,
                        Variaveis_de_Entrada.Tipo_de_kernel,
                        Variaveis_de_Entrada.Tipo_de_Onda,
                        Variaveis_de_Entrada.N_Sensores,
                        Variaveis_de_Entrada.Distancia_dos_Sensores,
                        0,
                        1,
                        Variaveis_de_Entrada.Tipo_de_beta,
                        Variaveis_de_Entrada.Desordem_Diagonal,
                        Variaveis_de_Entrada.W,
                        "SLAB",
                        L_instantaneo,
                        Variaveis_de_Entrada.Lₐ,
                        "SIM,para distancia fixada",
                        1
                    )

                    All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                    append!(Intensidade_Resultante_PURA,All_Intensitys[3 ,1:N_Sensores])

                end

                Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
                
                entrada_Sensores = get_cloud_sensors_ENTRADA(
                    1,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    0,
                    1,
                    "SLAB",
                    L_instantaneo,
                    Variaveis_de_Entrada.Lₐ,
                    "SIM,para distancia fixada",
                    1
                )

                Posição_Sensores = get_cloud_sensors(entrada_Sensores) 

                σ² = get_one_σ(Posição_Sensores[ : ,3:4],Intensidade_media)[2]

                valor_retorno = σ²
            end

            for n in 1:N_pontos
                σ²_L²[n,j] = tuplasResultados[n]
            end

        end

        tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_L) do L_instantaneo

            ωᵦ = Lₐ/10
            find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
            ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

            entrada_Sensores = get_cloud_sensors_ENTRADA(
                1,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                0,
                1,
                "SLAB",
                L_instantaneo,
                Variaveis_de_Entrada.Lₐ,
                "SIM,para distancia fixada",
                1
            )
            # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
            Sensores = get_cloud_sensors(entrada_Sensores)

            Intensidade_PURA = zeros(N_Sensores)
            for i in 1:N_Sensores
                
                entrada_campo = get_eletric_field_LG_real_ENTRADA(
                    Sensores[i,3],
                    Sensores[i,4],
                    Variaveis_de_Entrada.Ω,
                    ω₀,
                    Variaveis_de_Entrada.k
                )
                campo_laser = get_eletric_field_LG_real(entrada_campo)
                
                Intensidade_PURA[i] = abs(campo_laser^2) 
            end

            σ² = get_one_σ(Sensores[ : ,3:4],Intensidade_PURA)[2]

            valor_retorno = σ²
        end

        for n in 1:N_pontos
            σ²_L²[n,end] = tuplasResultados_caso_vazio[n]
        end

        
    end

    if Variaveis_de_Entrada.Variavel_Constante == "rmin"

        Range  = Range_L        
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "ρ"        
    
        Range  = Range_L
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "L"
    
        Range = Range_b₀
    
    elseif Variaveis_de_Entrada.Variavel_Constante == "b₀"
    
        Range = Range_L
    
    end
    
    return σ²_L²,Range
end


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#


function ROTINA_11__Cintura_e_L(Variaveis_de_Entrada::R11_ENTRADA,Geometria::ThreeD)


    N_Sensores = Variaveis_de_Entrada.N_Sensores    
    L_INICIAL = Variaveis_de_Entrada.RANGE_INICIAL 
    L_FINAL = Variaveis_de_Entrada.RANGE_FINAL
    N_pontos = Variaveis_de_Entrada.N_pontos
    N_Realizações = Variaveis_de_Entrada.N_Realizações

    Lₐ = Variaveis_de_Entrada.Lₐ
    ρs = Variaveis_de_Entrada.ρs


    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_L = get_points_in_log_scale(L_INICIAL,L_FINAL,N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Range_L = range(L_INICIAL,L_FINAL,length=N_pontos) 
    
    end
    

    N_Curvas = size(Variaveis_de_Entrada.ρs,1)

    σ²_L² = zeros(N_pontos,N_Curvas+1)


    for j in 1:N_Curvas 

        ρ = ρs[j]

        tuplasResultados = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = {$ρ}===>" pmap(Range_L) do L_instantaneo

            Intensidade_Resultante_PURA = Any[]
            
            # ωᵦ = Lₐ/10
            # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
            # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

            ωᵦ = Lₐ/5
            ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo/2)^2))/2)
            
            rₘᵢₙ = 1/(10*sqrt(ρ))                

            if typeof(Variaveis_de_Entrada.Geometria) == PARALELEPIPEDO
                GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"
                N = round(Int64,ρ*(L_instantaneo)*Lₐ^2)
            elseif typeof(Variaveis_de_Entrada.Geometria) == TUBO
                GEOMETRIA_DA_NUVEM = "TUBO"
                N = round(Int64,ρ*(L_instantaneo)*(π*Lₐ^2))
            end

            for i in 1:N_Realizações

                entrada_extratora = E2_ENTRADA(
                    Variaveis_de_Entrada.Γ₀,
                    Variaveis_de_Entrada.ωₐ,
                    Variaveis_de_Entrada.Ω,
                    Variaveis_de_Entrada.k,
                    N,
                    1,
                    ρ,
                    rₘᵢₙ,
                    Variaveis_de_Entrada.Angulo_da_luz_incidente,
                    Variaveis_de_Entrada.vetor_de_onda,
                    ω₀,
                    Variaveis_de_Entrada.λ,
                    Variaveis_de_Entrada.ωₗ,
                    Variaveis_de_Entrada.Δ,
                    Variaveis_de_Entrada.Tipo_de_kernel,
                    Variaveis_de_Entrada.Tipo_de_Onda,
                    Variaveis_de_Entrada.N_Sensores,
                    Variaveis_de_Entrada.Distancia_dos_Sensores,
                    0,
                    1,
                    Variaveis_de_Entrada.Tipo_de_beta,
                    Variaveis_de_Entrada.Desordem_Diagonal,
                    Variaveis_de_Entrada.W,
                    GEOMETRIA_DA_NUVEM,
                    L_instantaneo,
                    Variaveis_de_Entrada.Lₐ,
                    "SIM,para distancia fixada",
                    1,
                    Variaveis_de_Entrada.Geometria
                )

                All_Intensitys = E2_extração_de_dados_Intensidade(entrada_extratora)

                append!(Intensidade_Resultante_PURA,All_Intensitys[1 , : ])

            end

            Intensidade_media = get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Variaveis_de_Entrada.Geometria)
        

            entrada_Sensores = get_cloud_sensors_ENTRADA(
                1,
                Variaveis_de_Entrada.N_Sensores,
                Variaveis_de_Entrada.Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_da_luz_incidente,
                0,
                1,
                GEOMETRIA_DA_NUVEM,
                L_instantaneo,
                Variaveis_de_Entrada.Lₐ,
                "SIM,para distancia fixada",
                1
            )
            
            Posição_Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria) 

            σ² = get_one_σ(Posição_Sensores,Intensidade_media,Variaveis_de_Entrada.Geometria)[2]
            
            valor_retorno = σ²
        end

        for n in 1:N_pontos
            σ²_L²[n,j] = tuplasResultados[n]
        end
    end
    
    tuplasResultados_caso_vazio = ProgressMeter.@showprogress 2 "Calculo_Cintura_L, ρ = 0===>" pmap(Range_L) do L_instantaneo

        # ωᵦ = Lₐ/10
        # find_raiz(x) = (ωᵦ^2)*(x^2) - x^4 - 4*(L_instantaneo/2)^2
        # ω₀ = find_zeros(find_raiz,ωᵦ-3,ωᵦ+3)[1]

        ωᵦ = Lₐ/5
        ω₀ = sqrt(((ωᵦ^2) + sqrt((ωᵦ^4) + 16*(L_instantaneo/2)^2))/2)

        if typeof(Variaveis_de_Entrada.Geometria) == PARALELEPIPEDO
            GEOMETRIA_DA_NUVEM = "PARALELEPIPEDO"
        elseif typeof(Variaveis_de_Entrada.Geometria) == TUBO
            GEOMETRIA_DA_NUVEM = "TUBO"
        end

        entrada_Sensores = get_cloud_sensors_ENTRADA(
            1,
            Variaveis_de_Entrada.N_Sensores,
            Variaveis_de_Entrada.Distancia_dos_Sensores,
            Variaveis_de_Entrada.Angulo_da_luz_incidente,
            0,
            1,
            GEOMETRIA_DA_NUVEM,
            L_instantaneo,
            Variaveis_de_Entrada.Lₐ,
            "SIM,para distancia fixada",
            1
        )
        # Gero pontos na borda do Disco que irão se comportar como Sensores da Intensidade da luz  
        Sensores = get_cloud_sensors(entrada_Sensores,Variaveis_de_Entrada.Geometria)

        Intensidade_PURA = zeros(N_Sensores^2)
        for i in 1:(N_Sensores^2)
            
            entrada_campo = get_eletric_field_LG_real_ENTRADA(
                Sensores[i, : ],
                Variaveis_de_Entrada.Ω,
                ω₀,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Δ
            )
            campo_laser = get_eletric_field_LG_real(entrada_campo,Variaveis_de_Entrada.Geometria)
            
            Intensidade_PURA[i] = abs(campo_laser^2) 
        end

        σ² = get_one_σ(Sensores,Intensidade_PURA,Variaveis_de_Entrada.Geometria)[2]

        valor_retorno = σ²
    end

    for n in 1:N_pontos
        σ²_L²[n,end] = tuplasResultados_caso_vazio[n]
    end
    
    return σ²_L²,Range_L
end



function get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Geometria::TwoD)
    
    Intensidade_media = zeros(N_Sensores)

    for i in 1:N_Sensores

        fator_soma = 0
        aux_3 = 0

        for k in 1:N_Realizações

            fator_soma += Intensidade_Resultante_PURA[i + aux_3]
            aux_3 += N_Sensores

        end

        Intensidade_media[i] = fator_soma/N_Realizações
    end
    
    return Intensidade_media
end

function get_intensidade_media_to_cherroret(N_Sensores,N_Realizações,Intensidade_Resultante_PURA,Geometria::ThreeD)
    
    Intensidade_media = zeros(N_Sensores^2)

    for i in 1:(N_Sensores^2)

        fator_soma = 0
        aux_3 = 0

        for k in 1:N_Realizações

            fator_soma += Intensidade_Resultante_PURA[i + aux_3]
            aux_3 += (N_Sensores^2)

        end

        Intensidade_media[i] = fator_soma/N_Realizações
    end
    
    return Intensidade_media
end
