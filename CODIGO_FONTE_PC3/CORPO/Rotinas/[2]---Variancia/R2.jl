#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_2__Variancia_Densidade_FIXA(Variaveis_de_entrada::R2_ENTRADA)
    

    N_div_Deturn = Variaveis_de_entrada.N_div_Deturn
    N_Realizações = Variaveis_de_entrada.N_Realizações
    Δ_Inicial = Variaveis_de_entrada.Δ_Inicial
    Δ_Final = Variaveis_de_entrada.Δ_Final
    N_Sensores = Variaveis_de_entrada.N_Sensores
    PERFIL_DA_INTENSIDADE = "NAO"
    N_Telas = 1

    ΔD = Δ_Final - Δ_Inicial
    Deturn = zeros(N_div_Deturn)
    Variacias_SIMULADAS = zeros(N_div_Deturn)
    Variacias_TEORICAS = zeros(N_div_Deturn)

    N_total_de_Sensores = N_Sensores*N_Realizações
    Intensidade_Resultante = zeros(2,N_total_de_Sensores)
    range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)


    for j in 1:N_div_Deturn  
            
        Δ = range_Δ[j]*Γ₀
        ωₗ = Δ + ωₐ
        aux_1 = 0

        entrada_Extratora = E2_ENTRADA(
            Variaveis_de_entrada.Γ₀,
            Variaveis_de_entrada.ωₐ,
            Variaveis_de_entrada.Ω,
            Variaveis_de_entrada.k,
            Variaveis_de_entrada.N,
            Variaveis_de_entrada.Radius,
            Variaveis_de_entrada.ρ,
            Variaveis_de_entrada.rₘᵢₙ,
            Variaveis_de_entrada.Angulo_da_luz_incidente,
            Variaveis_de_entrada.vetor_de_onda,
            Variaveis_de_entrada.ω₀,
            Variaveis_de_entrada.λ,
            ωₗ,
            Δ,
            Variaveis_de_entrada.Tipo_de_kernel,
            Variaveis_de_entrada.Tipo_de_Onda,
            Variaveis_de_entrada.N_Sensores,
            Variaveis_de_entrada.Distancia_dos_Sensores,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
            Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
            Variaveis_de_entrada.Tipo_de_beta,
            Variaveis_de_entrada.Desordem_Diagonal,
            Variaveis_de_entrada.W,
            Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
            Variaveis_de_entrada.L,
            Variaveis_de_entrada.Lₐ,
            PERFIL_DA_INTENSIDADE,
            N_Telas
        )

        for i in 1:N_Realizações

            All_Intensitys = E2_extração_de_dados_Intensidade(entrada_Extratora)

            Intensidade_Resultante[1,aux_1 + 1:aux_1+N_Sensores] = All_Intensitys[1 , : ]
            Intensidade_Resultante[2,aux_1 + 1:aux_1+N_Sensores] = All_Intensitys[2 , : ]

            aux_1 += N_Sensores

        end

        Variacias_SIMULADAS[j],Variacias_TEORICAS[j] = get_dados_to_diagram_fase(Intensidade_Resultante)
        Deturn[j] = Δ/Γ₀ 

    end

     

    return R2_SAIDA(Deturn,Variacias_SIMULADAS,Variacias_TEORICAS)
end

function get_dados_to_diagram_fase(All_Intensitys)

    Intensidade_Normalizada = All_Intensitys[1, : ]/mean(All_Intensitys[1, : ])
    variancia_teo = (mean(Intensidade_Normalizada))^2
    variancia_sim = mean(Intensidade_Normalizada.^2) - (mean(Intensidade_Normalizada))^2
    # variancia_sim = Statistics.var(Intensidade_Normalizada)

    return variancia_sim,variancia_teo
end 


