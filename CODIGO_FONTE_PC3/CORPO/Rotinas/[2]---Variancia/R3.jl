#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_3__Diagrama_de_Fase_Variancia(Variaveis_de_Entrada::R3_ENTRADA)
  
    N_div_Densidade = Variaveis_de_Entrada.N_div_Densidade
    N_div_Deturn = Variaveis_de_Entrada.N_div_Deturn
    Densidade_Inicial = Variaveis_de_Entrada.Densidade_Inicial
    Densidade_Final = Variaveis_de_Entrada.Densidade_Final
    Variavel_Constante = Variaveis_de_Entrada.Variavel_Constante
    Escala_de_Densidade = Variaveis_de_Entrada.Escala_de_Densidade

    Q_Dados = N_div_Deturn*N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,3) 
    aux_1 = 1

    if Escala_de_Densidade == "LOG"

        Range_ρ = get_points_in_log_scale(Densidade_Inicial,Densidade_Final,N_div_Densidade) 
    
    elseif Escala_de_Densidade == "LINEAR"    

        Range_ρ = range(Densidade_Inicial,Densidade_Final,length=N_div_Densidade) 
    
    end

    
    if Variavel_Constante == "N"

        tuplasResultados = ProgressMeter.@showprogress 2 "Variancia Global ===>" pmap(Range_ρ) do ρ_normalizado

            Radius = 1 
            L = 1

            if Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "DISCO"
                
                ρ = ρ_normalizado*(Variaveis_de_Entrada.k^2)
                Radius = sqrt(Variaveis_de_Entrada.N/(π*ρ))/Variaveis_de_Entrada.k
                rₘᵢₙ = 1/(10*sqrt(ρ))   
                ω₀ = Radius/2                                                                                                                                                                                                                                    
                Distancia_dos_Sensores = 10*Radius

            elseif Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM == "SLAB"

                ρ = ρ_normalizado*(Variaveis_de_Entrada.k^2)
                L = (N/(ρ*Variaveis_de_Entrada.Lₐ))                                                                                                                                                                                                                                                                                   
                rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                                                                     
                ω₀ = Variaveis_de_Entrada.Lₐ/4                                                                                                                                                                                                                                                                                        # Cintura do Laser Gaussiano
                Distancia_dos_Sensores = 5*(sqrt(Variaveis_de_Entrada.Lₐ^2 + L^2)) 
                                                                                                
            end

            entrada_rotina = R2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.N,
                Radius,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.N_Sensores,
                Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.N_div_Deturn,
                Variaveis_de_Entrada.N_Realizações,
                Variaveis_de_Entrada.Δ_Inicial,
                Variaveis_de_Entrada.Δ_Final,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                L,
                Variaveis_de_Entrada.Lₐ
            )
        
            Dados = ROTINA_2__Variancia_Densidade_FIXA(entrada_rotina)
            
            Deturn_temporario = Dados.Deturn
            Variancias__temporario = Dados.Variacias_SIMULADAS


            Deturn_temporario,Variancias__temporario
            ρ_normalizados = fill(ρ_normalizado,(N_div_Deturn,1))

            valor_retorno = (Deturn_temporario=Deturn_temporario, ρ_normalizados=ρ_normalizados, Variancias__temporario=Variancias__temporario)
        end
        
        for i in 1:N_div_Densidade
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),1] = tuplasResultados[i].Deturn_temporario
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),2] = tuplasResultados[i].ρ_normalizados
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),3] = tuplasResultados[i].Variancias__temporario 
            aux_1 +=  N_div_Deturn
        end
        
    elseif Variavel_Constante == "kR"

        tuplasResultados = ProgressMeter.@showprogress 2 "Variancia Global ===>" pmap(Range_ρ) do ρ_normalizado

            ρ = ρ_normalizado*(k^2)
            N = round(Int64,ρ_normalizado*π*(Variaveis_de_Entrada.kR^2))
            Radius = Variaveis_de_Entrada.kR/Variaveis_de_Entrada.k
            rₘᵢₙ = 1/(10*sqrt(ρ))   
            ω₀ = Radius/2                                                                                                                 
            Distancia_dos_Sensores = 10*Radius


            entrada_rotina = R2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                Radius,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.N_Sensores,
                Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.N_div_Deturn,
                Variaveis_de_Entrada.N_Realizações,
                Variaveis_de_Entrada.Δ_Inicial,
                Variaveis_de_Entrada.Δ_Final,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                "DISCO",
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ
            )
        
            Dados = ROTINA_2__Variancia_Densidade_FIXA(entrada_rotina)

            Deturn_temporario = Dados.Deturn
            Variancias__temporario = Dados.Variacias_SIMULADAS


            Deturn_temporario,Variancias__temporario
            ρ_normalizados = fill(ρ_normalizado,(N_div_Deturn,1))
        
            valor_retorno = (Deturn_temporario=Deturn_temporario, ρ_normalizados=ρ_normalizados, Variancias__temporario=Variancias__temporario)
        end

        for i in 1:N_div_Densidade
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),1] = tuplasResultados[i].Deturn_temporario
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),2] = tuplasResultados[i].ρ_normalizados
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),3] = tuplasResultados[i].Variancias__temporario 
            aux_1 +=  N_div_Deturn
        end

    elseif Variavel_Constante == "L"

        tuplasResultados = ProgressMeter.@showprogress 2 "Variancia Global ===>" pmap(Range_ρ) do ρ_normalizado

            Radius = 1
            ρ = ρ_normalizado*(Variaveis_de_Entrada.k^2)
            N = round(Int64,ρ_normalizado*(Variaveis_de_Entrada.L*Variaveis_de_Entrada.Lₐ*Variaveis_de_Entrada.k))                                                                                                                 
            ω₀ = Variaveis_de_Entrada.Lₐ/4     
            Distancia_dos_Sensores = 5*(sqrt(Variaveis_de_Entrada.Lₐ^2 + Variaveis_de_Entrada.L^2))
            rₘᵢₙ = 1/(10*sqrt(ρ))                                                                                

            entrada_rotina = R2_ENTRADA(
                Variaveis_de_Entrada.Γ₀,
                Variaveis_de_Entrada.ωₐ,
                Variaveis_de_Entrada.Ω,
                Variaveis_de_Entrada.k,
                N,
                Radius,
                ρ,
                rₘᵢₙ,
                Variaveis_de_Entrada.angulo_da_luz_incidente,
                Variaveis_de_Entrada.vetor_de_onda,
                ω₀,
                Variaveis_de_Entrada.λ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Tipo_de_Onda,
                Variaveis_de_Entrada.N_Sensores,
                Distancia_dos_Sensores,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_Entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_Entrada.Tipo_de_beta,
                Variaveis_de_Entrada.N_div_Deturn,
                Variaveis_de_Entrada.N_Realizações,
                Variaveis_de_Entrada.Δ_Inicial,
                Variaveis_de_Entrada.Δ_Final,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                "SLAB",
                Variaveis_de_Entrada.L,
                Variaveis_de_Entrada.Lₐ
            )
        
            Dados = ROTINA_2__Variancia_Densidade_FIXA(entrada_rotina)
            
            Deturn_temporario = Dados.Deturn
            Variancias__temporario = Dados.Variacias_SIMULADAS

            Deturn_temporario,Variancias__temporario
            ρ_normalizados = fill(ρ_normalizado,(N_div_Deturn,1))
        
            valor_retorno = (Deturn_temporario=Deturn_temporario, ρ_normalizados=ρ_normalizados, Variancias__temporario=Variancias__temporario)
        end

        for i in 1:N_div_Densidade
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),1] = tuplasResultados[i].Deturn_temporario
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),2] = tuplasResultados[i].ρ_normalizados
            Dados_Resultantes[aux_1:(N_div_Deturn-1 + aux_1),3] = tuplasResultados[i].Variancias__temporario 
            aux_1 +=  N_div_Deturn
        end

    end

    return Dados_Resultantes

end

function get_points_in_log_scale(Valor_Inicial::Number,Valor_Final::Number,N_div::Integer)
    
    a = log10(Valor_Inicial)
    b = log10(Valor_Final)

    c = range(a,b, length = N_div)
    pontos = zeros(N_div)

    for i in 1:N_div
        pontos[i] = 10^c[i]

    end

    return pontos
end



    



































