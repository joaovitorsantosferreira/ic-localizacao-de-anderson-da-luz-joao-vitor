###############################################################################################################################################################################################################
#------------------------------------------------------------------------------------ Analise da Flutuação da Intensidade ------------------------------------------------------------------------------------#
###############################################################################################################################################################################################################


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------- Dados para o Histograma da Intensidade ----------------------------------------------------------------------------------#  
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_10__Estatisticas_da_Transmission(Variaveis_de_entrada::R10_ENTRADA)
    
    N_Sensores = Variaveis_de_entrada.N_Sensores    
    b_INICIAL = Variaveis_de_entrada.b_INICIAL 
    b_FINAL = Variaveis_de_entrada.b_FINAL
    N_pontos = Variaveis_de_entrada.N_pontos
    N_Realizações = Variaveis_de_entrada.N_Realizações
    PERFIL_DA_INTENSIDADE = "NAO"
    GEOMETRIA_DA_NUVEM = "SLAB"
    Radius = 1

    Q_total = N_Sensores*N_Realizações
    T = zeros(N_pontos,2)
    N_Telas = 1
    aux_1 = 1

    if Variaveis_de_entrada.Escala_de_b == "LOG"

        Range_b = get_points_in_log_scale(b_INICIAL,b_FINAL,N_pontos) 
    
    elseif Variaveis_de_entrada.Escala_de_b == "LINEAR"    

        Range_b = range(b_INICIAL,b_FINAL,length=N_pontos) 
    
    end

    for j in 1:N_pontos

        bδ = Range_b[j]
        Δ = (Variaveis_de_entrada.Γ₀/2)*sqrt((Variaveis_de_entrada.b₀/bδ) - 1 ) 
        ωₗ = Δ + Variaveis_de_entrada.ωₐ                                                                                                                  
        Intensidade_Resultante_PURA = zeros(2,Q_total)
        auxiliar = 0

        ProgressMeter.@showprogress 1 "Extraido Transmissão da Luz {$aux_1}===>" for i in 1:N_Realizações

            entrada_Extratora = E9_ENTRADA(
                Variaveis_de_entrada.Γ₀,
                Variaveis_de_entrada.ωₐ,
                Variaveis_de_entrada.Ω,
                Variaveis_de_entrada.k,
                Variaveis_de_entrada.N,
                Radius,
                Variaveis_de_entrada.ρ,
                Variaveis_de_entrada.rₘᵢₙ,
                Variaveis_de_entrada.Angulo_da_luz_incidente,
                Variaveis_de_entrada.vetor_de_onda,
                Variaveis_de_entrada.ω₀,
                Variaveis_de_entrada.λ,
                ωₗ,
                Δ,
                Variaveis_de_entrada.Tipo_de_kernel,
                Variaveis_de_entrada.Tipo_de_Onda,
                Variaveis_de_entrada.N_Sensores,
                Variaveis_de_entrada.Distancia_dos_Sensores,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_1,
                Variaveis_de_entrada.Angulo_de_variação_da_tela_circular_2,
                Variaveis_de_entrada.Tipo_de_beta,
                Variaveis_de_entrada.Desordem_Diagonal,
                Variaveis_de_entrada.W,
                GEOMETRIA_DA_NUVEM,
                Variaveis_de_entrada.L,
                Variaveis_de_entrada.Lₐ,
                PERFIL_DA_INTENSIDADE,
                N_Telas
            )

            All_Intensitys = E9_extração_de_dados_Transmission(entrada_Extratora)                                                                                                    

            Intensidade_Resultante_PURA[1,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[1 , : ]
            Intensidade_Resultante_PURA[2,(auxiliar + 1):(auxiliar+N_Sensores)] = All_Intensitys[2 , : ]
            auxiliar +=  N_Sensores
        
        end

        T_DIFUSO,T_COERENTE = get_Tramission(Intensidade_Resultante_PURA,N_Sensores,N_Realizações,Variaveis_de_entrada.angulo_coerente)

        T[j,1] = T_DIFUSO
        T[j,2] = T_COERENTE 
        aux_1 += 1
    end 

    return Range_b,T

end

function get_Tramission(Intensidade_Resultante_PURA,N_Sensores,N_Realizações,angulo_coerente)
    
    Intensidade_media = zeros(2,N_Sensores)

    Intensidade_media[2, : ] = Intensidade_Resultante_PURA[2,1:N_Sensores]
    
    for i in 1:N_Sensores
        
        fator_soma = 0
        aux_1 = 0

        for c in 1:N_Realizações

            fator_soma += Intensidade_Resultante_PURA[1,i + aux_1]
            aux_1 += N_Sensores

        end

        Intensidade_media[1,i] = fator_soma/N_Realizações

    end

    θ = Intensidade_media[2, : ]
    index_sensor_coerente_1 = findall(  (θ[ : ] .≥ (360-angulo_coerente)))
    index_sensor_coerente_2 = findall((θ[ : ] .≤ angulo_coerente))
    I_coerente = sum(Intensidade_media[1,index_sensor_coerente_1]) + sum(Intensidade_media[1,index_sensor_coerente_2])
    

    index_sensor_difuso_1 = findall(  (θ[ : ] .≥ 270 ).*(θ[ : ] .≤ 360))
    index_sensor_difuso_2 = findall(  (θ[ : ] .≥ 0 ).*(θ[ : ] .≤ 90))
    I_espalhada = sum(Intensidade_media[1,index_sensor_difuso_1]) + sum(Intensidade_media[1,index_sensor_difuso_2])


    index_sensor_incidente = findall(  (θ[ : ] .≥ 90).*(θ[ : ] .≤ 270))
    I_incidente = sum(Intensidade_media[1,index_sensor_incidente])


    T_DIFUSO = I_espalhada/I_incidente
    T_COERENTE = I_coerente/I_incidente

    return T_DIFUSO,T_COERENTE

end


function get_points_in_log_scale(Valor_Inicial,Valor_Final,N_div)
    
    a = log10(Valor_Inicial)
    b = log10(Valor_Final)

    sep = abs(b-a) / (N_div-1)

    aux_1 = 1
    pontos = zeros(N_div)

    for i in 0:N_div-1
        pontos[aux_1] = 10.0^(a + sep*i)
        aux_1 += 1
    end

    return pontos
end
