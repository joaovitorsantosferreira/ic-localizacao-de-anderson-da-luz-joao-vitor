function ROTINA_14__Energia_Localizada(Variaveis_de_Entrada::R14_ENTRADA)


    if Variaveis_de_Entrada.Escala_do_range == "LOG"

        Range_ρ = get_points_in_log_scale(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,Variaveis_de_Entrada.N_pontos) 
    
    elseif Variaveis_de_Entrada.Escala_do_range == "LINEAR"    

        Range_ρ = range(Variaveis_de_Entrada.RANGE_INICIAL,Variaveis_de_Entrada.RANGE_FINAL,length=Variaveis_de_Entrada.N_pontos) 
    
    end

    tuplasResultados = ProgressMeter.@showprogress 2 "Energia Modos Localizados ==>" pmap(Range_ρ) do ρ_Intantaneo

        Geometria = get_geometria(Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM) 
        Radius,L,rₘᵢₙ,ω₀ = get_parametros_da_nuvem(Variaveis_de_Entrada.N,ρ_Intantaneo,Variaveis_de_Entrada.Lₐ,Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM)

        Δₙ_total = Any[]
            
        for i in 1:Variaveis_de_Entrada.N_Realizações

            entrada_extratora = E10_ENTRADA(
                Variaveis_de_Entrada.N,
                ρ_Intantaneo,
                Variaveis_de_Entrada.k,
                Variaveis_de_Entrada.Γ₀,
                0,
                Radius,
                rₘᵢₙ,
                Variaveis_de_Entrada.Tipo_de_kernel,
                Variaveis_de_Entrada.Desordem_Diagonal,
                Variaveis_de_Entrada.W,
                Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
                L,
                Variaveis_de_Entrada.Lₐ,
                Geometria,
            )

            dados = E10_extração_de_dados_Modos(entrada_extratora)

            IPRs = dados.IPRs
            Δₙ = dados.ωₙ 
            R² = dados.Eₙ
            ξₙ = dados.ξₙ

            index_modes_inside_range = findall((IPRs .> 0.3).*(R² .> 0.6).*((log10.(abs.(ξₙ)/Radius)) .< 0 ) )
            Δₙ_inside = Δₙ[index_modes_inside_range]

            append!(Δₙ_total,Δₙ_inside)
        end

        valor_retorno = mean(Δₙ_total)
    end
    
    Δₙ_Localizado = zeros(Variaveis_de_Entrada.N_pontos)

    for i in 1:Variaveis_de_Entrada.N_pontos
        Δₙ_Localizado[i] = tuplasResultados[i]
    end
    
    return Range_ρ,Δₙ_Localizado
end

