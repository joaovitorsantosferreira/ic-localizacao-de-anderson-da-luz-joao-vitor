#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------ Estatisticas da Intensidade do Laser Gaussiano -------------------------------------------------------------------------------# 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

function ROTINA_5__Comprimento_de_localização_Δ_VARIAVEL(Variaveis_de_Entrada::R5_ENTRADA)
    
    N_div_Deturn = Variaveis_de_Entrada.N_div_Deturn
    Δ_Inicial = Variaveis_de_Entrada.Δ_Inicial
    Δ_Final = Variaveis_de_Entrada.Δ_Final
    N_div_Densidade = Variaveis_de_Entrada.N_div_Densidade


    Range_Δ = range(Δ_Inicial,Δ_Final,length = N_div_Deturn)
    Q_Dados = N_div_Deturn*N_div_Densidade
    Dados_Resultantes = zeros(Q_Dados,3) 
    aux_1 = 1

    ProgressMeter.@showprogress 2 "Progresso Global===>" for i in 1:N_div_Deturn
        
        Δ = Range_Δ[i]

        entrada_rotina = R4_ENTRADA(
            Variaveis_de_Entrada.Γ₀,
            Variaveis_de_Entrada.k,
            Δ,
            Variaveis_de_Entrada.N,
            Variaveis_de_Entrada.kR,
            Variaveis_de_Entrada.L,
            Variaveis_de_Entrada.Lₐ,
            Variaveis_de_Entrada.Tipo_de_kernel,
            Variaveis_de_Entrada.N_div_Densidade,
            Variaveis_de_Entrada.Densidade_Inicial,
            Variaveis_de_Entrada.Densidade_Final,
            Variaveis_de_Entrada.Variavel_Constante,
            Variaveis_de_Entrada.Escala_de_Densidade,
            Variaveis_de_Entrada.Desordem_Diagonal,
            Variaveis_de_Entrada.W,
            Variaveis_de_Entrada.GEOMETRIA_DA_NUVEM,
        )
        
        ρ_normalizados_temporario,ξs_temporario = ROTINA_4__Comprimento_de_localização_Δ_FIXO(entrada_rotina)
        Deturn_temporario = fill(Δ,(N_div_Densidade,1))
    
    
        Dados_Resultantes[aux_1:(N_div_Densidade-1 + aux_1),1] = Deturn_temporario
        Dados_Resultantes[aux_1:(N_div_Densidade-1 + aux_1),2] = ρ_normalizados_temporario
        Dados_Resultantes[aux_1:(N_div_Densidade-1 + aux_1),3] = ξs_temporario
        aux_1 +=  N_div_Densidade

    end

    return Dados_Resultantes

end
    



































