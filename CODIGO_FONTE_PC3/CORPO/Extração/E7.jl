###############################################################################################################################################################################################################
##################################################################################### Extração de dados de uma simulaçao ######################################################################################
###############################################################################################################################################################################################################


function E7_extração_de_dados_Cintura_e_Intensidade(Variaveis_de_entrada::E7_ENTRADA)


    ###############################################################################################################################################################################################################
    #------------------------------------------------------------------------------------ Ativação dos programas em conjunto -------------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    entrada_beta = get_all_beta_to_system_ENTRADA(
        Variaveis_de_entrada.cloud.G,
        Variaveis_de_entrada.Δ,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.cloud.r,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_beta,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.Desordem_Diagonal,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L
    )
    # Guardo a dinâmica dos atomos em uma matriz    
    βₙ = get_all_beta_to_system(entrada_beta)                           

    entrada_Intensidade = get_Intensity_to_sensors_ENTRADA(
        Variaveis_de_entrada.Posição_Sensores,
        βₙ,
        Variaveis_de_entrada.k,
        Variaveis_de_entrada.Γ₀,
        Variaveis_de_entrada.Ω,
        Variaveis_de_entrada.cloud.r,
        Variaveis_de_entrada.vetor_de_onda,
        Variaveis_de_entrada.ω₀,
        Variaveis_de_entrada.Tipo_de_Onda,
        Variaveis_de_entrada.Tipo_de_kernel,
        Variaveis_de_entrada.PERFIL_DA_INTENSIDADE,
        Variaveis_de_entrada.N_Telas,
        Variaveis_de_entrada.GEOMETRIA_DA_NUVEM,
        Variaveis_de_entrada.Radius,
        Variaveis_de_entrada.L
    )
    # Guardo a intensidade sentida em cada sensor 
    All_Intensitys = get_Intensity_to_sensors(entrada_Intensidade)

    # entrada_Cintura = get_waist_in_screens_ENTRADA(
    # Variaveis_de_entrada.Posição_Sensores,
    # All_Intensitys,
    # Variaveis_de_entrada.N_Telas
    # )
    # # Guardo os valores de Cintura para diferentes distancia ao longo da nuvem 
    # σ² = get_waist_in_screens(entrada_Cintura)

    ###############################################################################################################################################################################################################
    #--------------------------------------------------------------------------------- Dados Gerados para as condições de entrada --------------------------------------------------------------------------------#
    ###############################################################################################################################################################################################################


    # return All_Intensitys,σ²
    return All_Intensitys
end
